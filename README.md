# General

This application is built in .netcore, targeting the core framework.
Local development defaults to an inmemory database. On the live version a SQL database provider is used.

Both the frontend and the backend are used in this platform. So you can cache and invalidate cache from the website.

## Local development

You should be able to start the website just with f5.
Make sure you have the latest version of .net core to run.

When you add a new module / setting / entity / etc
Make sure you add:

- Migration (EF Migrations is used)
- Add default / sample data to the install service (this is used for local development, but also to generate a default install for a new customer)
- .md file in docs map with some information about your module.

### Submodules ###

The fusion libraries shall also be used in the customer specific sites. 
To do this follow the steps below:

* Start Git CMD
* Go to the root folder of your customer repo
* git submodule add git@bitbucket.org:novimedia/fusion-media-core.git (Make sure you use the SSH version, secure and easy to setup https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)
* git commit
* Now you should see a fusion-media-core folder in your repo with the source of the generic fusion admin
* Add all the fusion libraries as references (csproj)
* Add the modules that you need for this specific customer site.
* Add the 'FusionCoreModulesDummy' (look for the original in the fusion-media-core library)
* Copy the default appsettings.json and appsettings.development.json from the fusion-media-core repo
* Change the Startup.cs so it looks the same as the one in the fusion-media-core repo

If you make any changes in the subfolder, those need to be commited seperatly in the submodule folder.