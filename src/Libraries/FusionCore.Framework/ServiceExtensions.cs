using AutoMapper;
using FusionCore.Core;
using FusionCore.Core.Caching;
using FusionCore.Core.Helpers;
using FusionCore.Core.Interfaces;
using FusionCore.Core.Settings;
using FusionCore.Data;
using FusionCore.Data.Interfaces;
using FusionCore.Data.Repositories.Abstracts;
using FusionCore.Models.ModelBinder;
using FusionCore.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;

namespace FusionCore.Framework
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddCustomHttpContextAccessor(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IUrlHelper>(x =>
            {
                var actionContext = x.GetRequiredService<IActionContextAccessor>().ActionContext;
                var factory = x.GetRequiredService<IUrlHelperFactory>();
                return factory.GetUrlHelper(actionContext);
            });
            return services;
        }

        public static IServiceCollection AddDatabase(this IServiceCollection services, MainSettings config,
            string connectionString, bool dontMigrate = false)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            if (config.UseInMemoryDb)
            {
                services.AddDbContext<FusionCoreContext>(options =>
                    options.UseInMemoryDatabase("Fusion")
                );
            }
            else
            {
                if (!dontMigrate)
                {
                    services.AddDbContext<FusionCoreContext>(options =>
                        options.UseSqlServer(connectionString, c => c.MigrationsAssembly("FusionCore.Migrations")));
                }
                else
                {
                    services.AddDbContext<FusionCoreContext>(options =>
                        options.UseSqlServer(connectionString));

                }
            }

            return services;
        }

        public static IServiceCollection AddCustomMvc(this IServiceCollection services, IHostingEnvironment env)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

            var ignoredTypes = new[] { typeof(CoreModule), typeof(ServicesModule) };
            var types = AssemblyHelper.GetAssemblyImplementationsOfType<IFusionCoreModule>().Where(x => !ignoredTypes.Contains(x));
            services.AddMvc(config =>
            {
                config.ModelBinderProviders.Insert(0, new InvariantDecimalModelBinderProvider());
            })
                .AddSessionStateTempDataProvider()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                })
                .AddRazorOptions(o =>
                {
                    var basePath = env.IsDevelopment() ? new DirectoryInfo(env.ContentRootPath) : new DirectoryInfo("C:\\");
                    var dir = basePath?.Parent?.GetDirectories("modules");

                    var baseModulesPath = (dir!=null&&dir.Any() ? dir.First() : null);
                    foreach (var type in types)
                    {
                        if (env.IsDevelopment() && baseModulesPath != null)
                        {
                            o.FileProviders.Add(new PhysicalFileProvider(baseModulesPath + "//" + type.Assembly.GetName().Name));
                        }
                        else
                        {
                            o.FileProviders.Add(new EmbeddedFileProvider(type.Assembly,
                                $"{type.Assembly.GetName().Name}"));
                        }
                    }
                })
                .SetCompatibilityVersion(CompatibilityVersion.Latest);

            //services.Configure<RazorViewEngineOptions>(o =>
            //{
            //	foreach (var type in types)
            //	{
            //		o.FileProviders.Add(new EmbeddedFileProvider(type.Assembly, $"{type.Assembly.GetName().Name}"));
            //	}
            //});
            return services;
        }

        public static IServiceCollection AddIoc(this IServiceCollection services, IHostingEnvironment env)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddSingleton<ICacheManager, MemoryCacheManager>();

            var entryAssembly = Assembly.GetEntryAssembly();
            foreach (var implementationType in AssemblyHelper.GetAssemblyImplementationsOfType<IBaseService>(entryAssembly))
            {
                foreach (var interfaceType in implementationType.GetInterfaces())
                {
                    services.AddScoped(interfaceType, implementationType);
                }
            }

            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(PdfCreator));

            return services;
        }

        public static IServiceCollection AddAuthenthication(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                    {
                        options.Cookie.HttpOnly = true;
                        options.ExpireTimeSpan = TimeSpan.FromDays(7);
                        options.LoginPath = "/Login";
                        options.LogoutPath = "/Login/Logout";
                        options.AccessDeniedPath = "/Login/AccessDeniedPath";
                        options.SlidingExpiration = true;
                    }
                );
            return services;
        }

        public static IServiceCollection AddCustomAutomapper(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            var types = AssemblyHelper.GetAssemblyImplementationsOfType<Profile>();
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfiles(types);
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            return services;

        }

        internal class CustomAssemblyLoadContext : AssemblyLoadContext
        {
            public IntPtr LoadUnmanagedLibrary(string absolutePath)
            {
                return LoadUnmanagedDll(absolutePath);
            }
            protected override IntPtr LoadUnmanagedDll(String unmanagedDllName)
            {
                return LoadUnmanagedDllFromPath(unmanagedDllName);
            }

            protected override Assembly Load(AssemblyName assemblyName)
            {
                throw new NotImplementedException();
            }
        }

        public static IServiceCollection AddCultureInfo(this IServiceCollection services)
        {
            CultureInfo[] supportedCultures = new[]
   {
        new CultureInfo("nl-NL"),
        new CultureInfo("en-US")
    };

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("nl-NL");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders = new List<IRequestCultureProvider>
        {
            new QueryStringRequestCultureProvider(),
            new CookieRequestCultureProvider()
        };
            });

            return services;
        }
    }
}
