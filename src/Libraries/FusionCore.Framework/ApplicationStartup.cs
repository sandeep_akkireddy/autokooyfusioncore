using FusionCore.Core.Settings;
using FusionCore.Data;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace FusionCore.Framework
{
    public class ApplicationStartup
    {
        public static void DefaultFusionConfigure(
            IApplicationBuilder app,
            IServiceProvider serviceProvider,
            ILoggerFactory loggerFactory,
            IHostingEnvironment env,
            IConfiguration configuration)
        {
            loggerFactory.AddConsole(configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddFile("Logs/mylog-{Date}.txt");

            MainSettings config = configuration.GetSection("MainSettings").Get<MainSettings>();

            if (config.ShowDevelopmentError || env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

            }
            else
            {
                app.UseExceptionHandler("/Common/Error");
                app.UseStatusCodePagesWithReExecute("/Common/Error");
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            //app.UseStaticFiles();
            //UseFileServer = UseDefaultFiles + UseStaticFiles
            app.UseFileServer();

            app.UseSession(new SessionOptions() { IdleTimeout = TimeSpan.FromHours(2) });
            app.UseAuthentication();

            var localizationOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(localizationOptions.Value);

            if (!config.UseInMemoryDb)
            {
                FusionCoreContext dbContext = serviceProvider.GetService<FusionCoreContext>();
                dbContext.Database.Migrate();

                if (!config.IsInstalled)
                {
                    Task.WaitAll(serviceProvider.GetService<IInstallService>().Install(serviceProvider));
                }
            }
            else
            {
                Task.WaitAll(serviceProvider.GetService<IInstallService>().Install(serviceProvider, true));
            }

        }

        public static void DefaultFusionConfigureServices(IServiceCollection services, IConfiguration configuration, IHostingEnvironment env, bool dontMigrate = false)
        {
            MainSettings config = configuration.GetSection("MainSettings").Get<MainSettings>();
            EmailSettings emailconfig = configuration.GetSection("EmailSettings").Get<EmailSettings>();
            string connectionStrings = configuration.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
            services.AddDatabase(config, connectionStrings, dontMigrate);
            services.AddAuthenthication();
            services.AddCustomAutomapper();
            services.AddOptions();
            services.Configure<MainSettings>(configuration.GetSection(nameof(MainSettings)));
            services.Configure<EmailSettings>(configuration.GetSection(nameof(EmailSettings)));
            services.AddSession(x => { x.IdleTimeout = TimeSpan.FromHours(2); });
            services.AddIoc(env);
            services.AddCustomHttpContextAccessor();
            services.AddCustomMvc(env);
            services.AddCultureInfo();

        }
    }
}
