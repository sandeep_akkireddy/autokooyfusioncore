using AutoMapper;
using FusionCore.Core;
using FusionCore.Core.Enums;
using FusionCore.Core.Exceptions;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.Data;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Models.ApiModels;
using FusionCore.Models.Backend.FusionUser;
using FusionCore.Models.Backend.Login;
using FusionCore.Models.Common;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Services.User
{
    public interface IUserService : IBaseService
    {
        Data.Entities.User Validate(string identifier, string password, string loginTypeCode = Constants.DefaultLoginType);
        void SignIn(Data.Entities.User user, bool isPersistent = false);
        void SignOut();
        int GetCurrentUserId();
        Data.Entities.User GetCurrentUser();
        Task RegisterUserAsync(RegisterModel model, string loginTypeCode = Constants.DefaultLoginType);
        Task SendPasswordResetEmail(ForgotModel model, string loginTypeCode = Constants.DefaultLoginType);
        bool ValidateResetGuid(Guid guid, int uid);
        Task InsertUser(Data.Entities.User user);
        Task InsertCredential(Credential credential);
        IPagedList<Data.Entities.User> GetAllUsers<TKey>(
            Expression<Func<Data.Entities.User, bool>> predicate = null,
            Expression<Func<Data.Entities.User, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Data.Entities.User GetUser(int id);
        bool Persist(FusionUserPersistModel model, Dictionary<int, bool> accessList);
        Task DeleteUser(int userId);
        Dictionary<string, string> GetSettings(Data.Entities.User user);
        void PersistSettings(Data.Entities.User user, Dictionary<string, string> settings);
        Task SaveChangesAsync();

        void FrontendSignIn(ProfileModel user, bool isPersistent = false);
        List<Data.Entities.UserAccess> GetUserAccess(int id);

        List<Data.Entities.User> GetUsersList<TKey>(
       Expression<Func<Data.Entities.User, bool>> predicate = null,
       Expression<Func<Data.Entities.User, TKey>> orderBy = null,
       bool asc = true);
    }

    public class UserService : BaseService, IUserService
    {
        private readonly IGenericRepository<Credential> _credentialRepository;
        private readonly IGenericRepository<Data.Entities.User> _userRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEmailSender _emailSender;
        private readonly MainSettings _mainSettings;
        private readonly IMapper _mapper;
        private readonly FusionCoreContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly IGenericRepository<Data.Entities.UserAccess> _userAccessRepository;
        public UserService(
            IGenericRepository<Credential> credentialRepository,
            IGenericRepository<Data.Entities.User> userRepository,
            IHttpContextAccessor httpContextAccessor,
            IEmailSender emailSender,
            IOptions<MainSettings> optionsAccessor,
            IMapper mapper,
            FusionCoreContext dbContext,
            IConfiguration configuration,
            IGenericRepository<Data.Entities.UserAccess> userAccessRepository)
        {
            _credentialRepository = credentialRepository;
            _userRepository = userRepository;
            _httpContextAccessor = httpContextAccessor;
            _emailSender = emailSender;
            _mapper = mapper;
            _dbContext = dbContext;
            _mainSettings = optionsAccessor.Value;
            _configuration = configuration;
            _userAccessRepository = userAccessRepository;
        }

        public Data.Entities.User Validate(string identifier, string password, string loginTypeCode = Constants.DefaultLoginType)
        {
            Credential credential = _credentialRepository.Get(
              c => c.Type == loginTypeCode &&
                   string.Equals(c.Identifier, identifier, StringComparison.OrdinalIgnoreCase)
            );

            if (credential == null)
                return null;

            return PasswordHelper.ValidatePasswordHash(credential.Key, credential.Salt, password)
                ? _userRepository.GetById(credential.UserId)
                : null;
        }

        public async void SignIn(Data.Entities.User user, bool isPersistent = false)
        {
            ClaimsIdentity identity = new ClaimsIdentity(GetUserClaims(user), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await _httpContextAccessor.HttpContext.SignInAsync(
              CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties { IsPersistent = isPersistent }
            );
            user.isloggedin = true;
            _userRepository.Update(user);
        }

        public async void SignOut()
        {
            var user = _userRepository.GetById(GetCurrentUserId());
            await _httpContextAccessor.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            if (user != null)
            {
                user.isloggedin = false;
                _userRepository.Update(user);
            }
        }

        public int GetCurrentUserId()
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;

            if (!httpContext.User.Identity.IsAuthenticated)
                return -1;

            Claim claim = httpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (claim == null)
                return -1;

            if (!int.TryParse(claim.Value, out int currentUserId))
                return -1;

            return currentUserId;
        }

        public Data.Entities.User GetCurrentUser()
        {
            int currentUserId = GetCurrentUserId();
            if (currentUserId == -1) return null;

            Data.Entities.User currentUser = _userRepository.GetById(currentUserId);
            if (currentUser == null || !currentUser.Active) return null;

            //if (!string.IsNullOrEmpty(currentUser.Photo) && !currentUser.Photo.Contains("assets/images/avatar-1-xl.jpg"))
            //{
            //    var fusionurl = _configuration.GetSection("MainSettings").Get<MainSettings>().FusionUrl;
            //    var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFileRelativePath;
            //    currentUser.Photo = string.Concat(fusionurl, path, currentUser.Photo);
            //}
            //else
            //{
            //    currentUser.Photo = "assets/images/avatar-1-xl.jpg";
            //}
            return currentUser;
        }

        public async Task RegisterUserAsync(RegisterModel model, string loginTypeCode = Constants.DefaultLoginType)
        {
            Data.Entities.User check = _userRepository.Get(x => x.Email.Equals(model.Email, StringComparison.InvariantCultureIgnoreCase));
            if (check != null)
            {
                throw new FusionCoreException("A user with this email already exists.");
            }

            Data.Entities.User user = _mapper.Map<RegisterModel, Data.Entities.User>(model);
            await _userRepository.InsertAsync(user);

            (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
            Credential credential = new Credential
            {
                UserId = user.Id,
                User = user,
                Type = loginTypeCode,
                Salt = password.salt,
                Key = password.key,
                Identifier = model.Email
            };
            await _credentialRepository.InsertAsync(credential);

        }

        public async Task SendPasswordResetEmail(ForgotModel model, string loginTypeCode = Constants.DefaultLoginType)
        {
            Credential credential = _credentialRepository.Get(
                c => c.Type == loginTypeCode &&
                     string.Equals(c.Identifier, model.Email, StringComparison.OrdinalIgnoreCase)
            );

            if (credential != null)
            {
                Data.Entities.User user = _userRepository.GetById(credential.UserId);
                Guid key = Guid.NewGuid();
                credential.ResetKey = key.ToString();
                _credentialRepository.Update(credential);

                string url = $"{_httpContextAccessor.HttpContext.Request.GetBaseUrl()}/login/reset?key={key}&uid={credential.Id}";
                StringBuilder body = new StringBuilder();
                body.Append("<span style=\"font-size:14px; font-family:Arial, 'Helvetica Neue', Gotham; color: #676163;\">Beste " + user?.GetFullName + ",</span>");
                body.Append("<table cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:14px; font-family:Arial, 'Helvetica Neue', Gotham; color: #676163;\">");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("	<td>Je ontvangt deze email omdat je een nieuw wachtwoord wilt instellen voor je account, als je geen nieuw wachtwoordt hebt aangevraagd kun je deze email negeren:</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("	<td>");
                body.Append("	Via onderstaande link kun je je wachtwoord opnieuw aanmaken binnen je account omgeving:<br />");
                body.Append("	<strong><a href=\"" + url + "\">" + url + "</a></strong>");
                body.Append("	</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("	<td><em>Let op! bovenstaande link is maar 1 uur geldig!</em></td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<br />");
                body.Append("<table cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:14px;  font-family:'sans-serif', Helvetica, 'Helvetica Neue', Arial, Gotham; color:#434343;\">");
                body.Append("<tr>");
                body.Append("<td>");
                body.Append("Met vriendelijke groet |  Regards<br />");
                body.Append("Novi Media</td>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td valign=\"top\">");
                body.Append("<a href=\"https://www.novimedia.net\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/novi-media-logo-black.png\" width=\"200\" height=\"66\" alt=\"\"/></a><br />");
                body.Append("<br>");
                body.Append("Duizendknooplaan 19<br>");
                body.Append("NL-3452 AS Vleuten<br>");
                body.Append("<br>");
                body.Append("t: +31 (0)302682756<br>");
                body.Append("</td>");
                body.Append("<td valign=\"bottom\">");
                body.Append("<span style=\"color: #56af00\">info@novimedia.net<br>");
                body.Append("www.novimedia.net</span>");
                body.Append("</td>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td valign=\"top\">");
                body.Append("<table cellpadding=\"0\" cellspacing=\"4\">");
                body.Append("<tr>");
                body.Append("<td><a href=\"https://www.facebook.com/novimedia\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/icon-mail-facebook.png\" width=\"30\" height=\"\" alt=\"\"/></a></td>");
                body.Append("<td><a href=\"https://www.linkedin.com/company/novi-media\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/icon-mail-linkedin.png\" width=\"30\" height=\"\" alt=\"\"/></a></td>");
                body.Append("<td><a href=\"https://twitter.com/NoviMediaNL\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/icon-mail-twitter.png\" width=\"30\" height=\"\" alt=\"\"/></a></td>");
                body.Append("</tr>");
                body.Append("</table>");
                body.Append("</td>");
                body.Append("<td>");
                body.Append("<a href=\"https://www.novimedia.net/diensten/software-ontwikkeling/\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/fusion-media-logo-black.png\" width=\"190\" height=\"80\" alt=\"\"/></a>");
                body.Append("</td>");
                body.Append("</tr>");
                body.Append("</tr>");
                body.Append("</table>");
                body.Append("<br />");
                await _emailSender.SendEmailAsync(model.Email, "Reset password", body.ToString());
            }
        }

        public bool ValidateResetGuid(Guid guid, int uid)
        {
            Credential credential = _credentialRepository.Get(x =>
                x.UserId == uid &&
                x.ResetKey == guid.ToString() &&
                x.UpdatedOnUtc >= DateTime.UtcNow.AddHours(-_mainSettings.ResetUrlTimeoutInHours));

            return credential != null;
        }

        public async Task InsertUser(Data.Entities.User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            await _userRepository.InsertAsync(user);
        }

        public async Task InsertCredential(Credential credential)
        {
            if (credential == null) throw new ArgumentNullException(nameof(credential));

            await _credentialRepository.InsertAsync(credential);
        }

        public IPagedList<Data.Entities.User> GetAllUsers<TKey>(
            Expression<Func<Data.Entities.User, bool>> predicate = null,
            Expression<Func<Data.Entities.User, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            IQueryable<Data.Entities.User> queryable = _userRepository.GetAll(predicate);
            //.Where(x => x.Id > 1);
            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }
            return new PagedList<Data.Entities.User>(queryable, pageIndex, pageSize);
        }

        public Data.Entities.User GetUser(int id)
        {
            var _user = _userRepository.Get(x => x.Id == id, user => user.Credentials);
            if (!string.IsNullOrEmpty(_user.Photo) && !_user.Photo.Contains("assets/images/avatar-1-xl.jpg"))
            {
                var fusionurl = _configuration.GetSection("MainSettings").Get<MainSettings>().FusionUrl;
                var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFileRelativePath;
                _user.Photo = string.Concat(fusionurl, path, _user.Photo);
            }
            else
            {
                _user.Photo = "/assets/images/avatar-1-xl.jpg";
            }
            return _user;
        }

        public bool Persist(FusionUserPersistModel model, Dictionary<int, bool> accessList)
        {
            if (model.Id.HasValue)
            {
                var loginuserRole = _mapper.Map<FusionUserPersistModel>(GetCurrentUser()).Role;
                var loginuserid = GetCurrentUserId();
                //Edit
                Data.Entities.User dbEntity = _userRepository.Get(x => x.Id == model.Id.Value, user => user.Credentials);
                if (dbEntity != null)
                {
                    FusionRoles oldrole = dbEntity.Role, newrole = model.Role;

                    string deletephoto = string.Empty;
                    if (!string.IsNullOrEmpty(model.Photo) && !model.Photo.Contains("assets/images/avatar-1-xl.jpg"))
                    {
                        var patharray = model.Photo.Split('/');
                        var filename = patharray[patharray.Length - 1];
                        if (dbEntity.Photo != filename)
                        {
                            var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFiles;
                            deletephoto = path + dbEntity.Photo;
                        }
                        else
                        {
                            model.Photo = dbEntity.Photo;
                        }
                    }
                    else
                    {
                        model.Photo = dbEntity.Photo;
                    }

                    _mapper.Map(model, dbEntity);
                    _userRepository.Update(dbEntity);

                    Credential credential = dbEntity.Credentials?.FirstOrDefault(x =>
                        x.Identifier.Equals(model.Username, StringComparison.InvariantCultureIgnoreCase));

                    if (credential != null && !string.IsNullOrEmpty(model.Password))
                    {
                        (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
                        credential.Salt = password.salt;
                        credential.Key = password.key;
                        _credentialRepository.Update(credential);
                    }
                    else if (credential == null)
                    {
                        (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
                        Credential newCredential = new Credential
                        {
                            UserId = dbEntity.Id,
                            User = dbEntity,
                            Type = Constants.DefaultLoginType,
                            Salt = password.salt,
                            Key = password.key,
                            Identifier = model.Username
                        };
                        _credentialRepository.Insert(newCredential);
                    }

                    if (!string.IsNullOrEmpty(deletephoto))
                    {
                        if (System.IO.File.Exists(deletephoto))
                        {
                            System.IO.File.Delete(deletephoto);
                        }
                    }

                    if ((loginuserRole == FusionRoles.Administrator || loginuserRole == FusionRoles.SuperAdmin))
                    {
                        InsertUserAccess(dbEntity.Id, accessList);
                    }

                    if (dbEntity.Id == loginuserid && oldrole != newrole)
                    {
                        SignOut();
                        SignIn(dbEntity);
                    }

                    return true;
                }
            }
            else
            {
                //insert
                Data.Entities.User dbEntity = new Data.Entities.User();
                _mapper.Map(model, dbEntity);
                _userRepository.Insert(dbEntity);
                (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);

                Credential newCredential = new Credential
                {
                    UserId = dbEntity.Id,
                    User = dbEntity,
                    Type = Constants.DefaultLoginType,
                    Salt = password.salt,
                    Key = password.key,
                    Identifier = model.Username
                };
                _credentialRepository.Insert(newCredential);

                InsertUserAccess(dbEntity.Id, accessList);

                return true;
            }

            return false;
        }

        void InsertUserAccess(int _userid, Dictionary<int, bool> accessList)
        {
            var list = _userAccessRepository.GetAll(x => x.userId == _userid).ToList();
            _userAccessRepository.DeleteAll(list);

            List<UserAccess> userAccesses = new List<UserAccess>();

            foreach (var item in accessList)
            {
                userAccesses.Add(new UserAccess()
                {
                    userId = _userid,

                    FusionModule = item.Key,
                    Active = item.Value
                });
            }
            _userAccessRepository.InsertAll(userAccesses);

        }

        public async Task DeleteUser(int userId)
        {
            Data.Entities.User user = await _userRepository.GetByIdAsync(userId);
            if (user != null)
            {
                var useraccess = _userAccessRepository.GetAll(x => x.userId == user.Id).ToList();
                if (useraccess != null && useraccess.Any())
                {
                    _userAccessRepository.DeleteAll(useraccess);
                }
                _userRepository.Delete(user);
            }
        }

        public Dictionary<string, string> GetSettings(Data.Entities.User user)
        {
            if (user != null)
            {
                if (!string.IsNullOrEmpty(user.Settings))
                {
                    return JsonConvert.DeserializeObject<Dictionary<string, string>>(user.Settings);
                }
            }

            return new Dictionary<string, string>();
        }

        public void PersistSettings(Data.Entities.User user, Dictionary<string, string> settings)
        {
            if (user != null)
            {
                if (settings != null && settings.Any())
                {
                    user.Settings = JsonConvert.SerializeObject(settings);
                }
                else
                {
                    user.Settings = null;
                }
                _userRepository.Update(user);
            }
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        private IEnumerable<Claim> GetUserClaims(Data.Entities.User user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.GetFullName),
                new Claim(ClaimTypes.Role,((FusionRoles)user.Role).ToString())
            };

            return claims;
        }
        private IEnumerable<Claim> GetFrontendUserClaims(ProfileModel user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.ProfileID.ToString()),
                new Claim(ClaimTypes.Name, user.Token),

            };

            return claims;
        }
        public async void FrontendSignIn(ProfileModel user, bool isPersistent = false)
        {
            ClaimsIdentity identity = new ClaimsIdentity(GetFrontendUserClaims(user), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await _httpContextAccessor.HttpContext.SignInAsync(
              CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties { IsPersistent = isPersistent }
            );
        }

        public List<UserAccess> GetUserAccess(int id)
        {
            return _userAccessRepository.GetAll(x => x.userId == id).ToList();
        }

        public List<Data.Entities.User> GetUsersList<TKey>(Expression<Func<Data.Entities.User, bool>> predicate = null, Expression<Func<Data.Entities.User, TKey>> orderBy = null, bool asc = true)
        {

            IQueryable<Data.Entities.User> queryable = _userRepository.GetAll(predicate);

            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }
            return new List<Data.Entities.User>(queryable);
        }
    }
}
