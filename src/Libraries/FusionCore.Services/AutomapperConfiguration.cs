using AutoMapper;
using FusionCore.Core;
using FusionCore.Core.Helpers;
using FusionCore.Data.Settings;
using FusionCore.Models.Backend.FusionUser;
using FusionCore.Models.Backend.Login;
using FusionCore.Models.Backend.Settings;
using System.Linq;
using FusionCore.Core.Extensions;
using System;

namespace FusionCore.Services
{
	public class AutomapperConfiguration : Profile
	{
		public AutomapperConfiguration()
		{
			CreateMap<RegisterModel, Data.Entities.User>()
				.ForMember(x => x.Credentials, m => m.Ignore())
				;

			CreateMap<CompanySettingsModel, CompanySettings>();
			CreateMap<CompanySettings, CompanySettingsModel>();

            CreateMap<WebsiteSettingsModel, WebsiteSettings>();
            CreateMap<WebsiteSettings, WebsiteSettingsModel>();


            CreateMap<Data.Entities.User, FusionUserRow>()
				.ForMember(d => d.CreatedOn, o => o.MapFrom(s => s.CreatedOnUtc.ConvertToUserTime()))
				.ForMember(d => d.Name, o => o.MapFrom(s => s.GetFullName))
				.ForMember(d => d.Status, o => o.MapFrom(s => s.Role.ToString()))
				;

            //CreateMap<Data.Entities.User, FusionUserPersistModel>()
            //    .ForMember(d => d.Username, o => o.MapFrom(s=>s.Firstname));

            Func<Data.Entities.User, string> convert = s => s.Credentials.FirstOrDefault(x => x.Type == Constants.DefaultLoginType)?.Identifier ?? string.Empty;
            CreateMap<Data.Entities.User, FusionUserPersistModel>()
                .ForMember(d => d.Username, o => o.MapFrom(s => convert(s)));


            CreateMap<FusionUserPersistModel, Data.Entities.User>()
				.ForMember(d => d.Credentials, o => o.Ignore());
		}
	}

	public class CredentialResolver : IValueResolver<Data.Entities.User, FusionUserPersistModel, string>

	{
		public string Resolve(Data.Entities.User source, FusionUserPersistModel destination, string destMember, ResolutionContext context)
		{
			if (source.Credentials != null)
			{
				return source.Credentials.FirstOrDefault(x => x.Type == Constants.DefaultLoginType)?.Identifier ?? string.Empty;
			}

			return string.Empty;
		}
	}
}

