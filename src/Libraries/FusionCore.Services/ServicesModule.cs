﻿using FusionCore.Core.Interfaces;

namespace FusionCore.Services
{
    public class ServicesModule : IFusionCoreModule
    {
        public string GetSystemName()
        {
            return "FusionCore.Services.Core";
        }
    }
}