using AutoMapper;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Frontend.Cars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FusionCore.Services.Car
{
    public interface ICarsService : IBaseService
    {
        WeeklyCar GetWeeklyCar();
        bool AddWeeklyCar(WeeklyCarModel car);
        bool RemoveWeeklyCar();

    }
    public class CarsService : BaseService, ICarsService
    {
        private readonly IGenericRepository<WeeklyCar> _weeklycarRepository;
        private readonly IMapper _mapper;

        public CarsService(IGenericRepository<WeeklyCar> weeklycarRepository,
         IMapper mapper)
        {
            _weeklycarRepository = weeklycarRepository;
            _mapper = mapper;
        }


        public bool AddWeeklyCar(WeeklyCarModel car)
        {
            RemoveWeeklyCar();

            WeeklyCar dbEntity = new WeeklyCar();
            dbEntity = _mapper.Map<WeeklyCar>(car);

            _weeklycarRepository.Insert(dbEntity);
            return true;
        }


        public WeeklyCar GetWeeklyCar()
        {
            return _weeklycarRepository.GetAll().FirstOrDefault();
        }

        public bool RemoveWeeklyCar()
        {
            _weeklycarRepository.DeleteAll(_weeklycarRepository.GetAll().ToList());
            return true;
        }
    }
}
