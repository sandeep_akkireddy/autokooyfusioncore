﻿using FusionCore.Core;
using FusionCore.Core.Enums;
using FusionCore.Core.Helpers;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Threading.Tasks;

namespace FusionCore.Services.Common
{
    public interface IInstallService : IBaseService
    {
        Task Install(IServiceProvider serviceProvider, bool sampleData = false);
        void RestartApplication();
    }

    public class InstallService : BaseService, IInstallService
    {
        private readonly IUserService _userService;
        private readonly IMenuService _menuService;
        private readonly IApplicationLifetime _applicationLifetime;

        public InstallService(
            IUserService userService,
            IApplicationLifetime applicationLifetime,
            IMenuService menuService)
        {
            _userService = userService;
            _applicationLifetime = applicationLifetime;
            _menuService = menuService;
        }

        public async Task Install(IServiceProvider serviceProvider, bool sampleData = false)
        {
            await InstallMainAdminUser();
            await InstallDefaultFusionMenu();
            await LoadModuleInstalls(serviceProvider, sampleData);
            if (sampleData)
            {
                await InstallRandomFusionUsers(25);
            }
        }

        private async Task LoadModuleInstalls(IServiceProvider serviceProvider, bool sampleData)
        {
            System.Collections.Generic.IEnumerable<System.Reflection.TypeInfo> types = AssemblyHelper.GetAssemblyImplementationsOfType<IInstallData>();

            foreach (System.Reflection.TypeInfo type in types)
            {
                IInstallData installer = (IInstallData)Activator.CreateInstance(type);
                if (installer != null) await installer.Install(serviceProvider, sampleData);
            }
        }

        private async Task InstallRandomFusionUsers(int amountOfUsers)
        {
            for (int i = 0; i <= amountOfUsers; i++)
            {
                int role = new Random().Next(0, 4);
                Data.Entities.User user = new Data.Entities.User
                {
                    Email = i.ToString(),
                    Firstname = "User" + i,
                    Active = true,
                    Address = "",
                    City = "",
                    Gender = GenderType.Unknown,
                    Role = (FusionRoles)role,
                    PostalCode = "",
                    PrivacyAccepted = true
                };
                await _userService.InsertUser(user);
            }
        }

        private async Task InstallDefaultFusionMenu()
        {
            await _menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "clip-home",
                Name = "Dashboard",
                Controller = "Dashboard",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 0,
                ParentId = null
            });

            MenuItem systemInfoMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Systeem informatie",
                IsDivider = true,
                DisplayOrder = 100,
                ParentId = null
            };
            await _menuService.InsertMenuItem(systemInfoMenuItem);

            MenuItem settingsMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "fa fa-cog",
                Name = "Instellingen",
                DisplayOrder = 200,
                ParentId = null
            };
            await _menuService.InsertMenuItem(settingsMenuItem);

            await _menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Bedrijfsinformatie",
                Controller = "Settings",
                Action = "Company",
                Parameters = null,
                DisplayOrder = 0,
                ParentId = settingsMenuItem.Id
            });

            await _menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Website gegevens",
                Controller = "Settings",
                Action = "Website",
                Parameters = null,
                DisplayOrder = 100,
                ParentId = settingsMenuItem.Id
            });

            await _menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Fusion gebruikers",
                Controller = "FusionUser",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 200,
                ParentId = settingsMenuItem.Id
            });
        }

        public void RestartApplication()
        {
            _applicationLifetime.StopApplication();
        }

        private async Task InstallMainAdminUser()
        {
            Data.Entities.User user = new Data.Entities.User
            {
                Email = "info@novimediasolutions.nl",
                Firstname = "Super",
                Lastname = "admin",
                Active = true,
                Address = "",
                City = "",
                Gender = GenderType.Unknown,
                Role = FusionRoles.SuperAdmin,
                PostalCode = "",
                PrivacyAccepted = true
            };
            await _userService.InsertUser(user);

            (byte[] key, byte[] salt) result = PasswordHelper.GetPasswordHashAndSalt("Wizzard-!05");
            Credential credential = new Credential
            {
                Identifier = "fusion_admin",
                Type = Constants.DefaultLoginType,
                Key = result.key,
                Salt = result.salt,
                UserId = user.Id,
                User = user
            };
            await _userService.InsertCredential(credential);


        }
    }
}
