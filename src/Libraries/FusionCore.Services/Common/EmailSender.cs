﻿using FusionCore.Core.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FusionCore.Services.Common
{
    public interface IEmailSender : IBaseService
    {
        Task SendEmailAsync(string email, string subject, string message);

        Task SendEmailAsync(string fromName, string fromEmail, string toName, string toEmail, string subject, string message, List<string> cc = null, List<string> bcc = null, Dictionary<string, byte[]> attachments = null);
    }

    public class EmailSender : BaseService, IEmailSender
    {
        private readonly ILogger _logger;
        public EmailSender(IOptions<MainSettings> optionsAccessor, ILogger<FileService> logger)
        {
            _logger = logger;
            MainSettings = optionsAccessor.Value;
        }

        public MainSettings MainSettings { get; } //set only via Key Manager

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            await Execute(MainSettings.SendGridKey, MainSettings.FromName, MainSettings.FromEmail, email, email, subject, message);
        }

        public async Task SendEmailAsync(string fromName, string fromEmail, string toName, string toEmail, string subject, string message, List<string> cc = null, List<string> bcc = null, Dictionary<string, byte[]> attachments = null)
        {
            await Execute(MainSettings.SendGridKey, fromName, fromEmail, toName, toEmail, subject, message, cc, bcc, attachments);
        }

        private async Task Execute(string apiKey, string fromName, string fromEmail, string toName, string toEmail, string subject, string message, List<string> cc = null, List<string> bcc = null, Dictionary<string, byte[]> attachments = null)
        {
            SendGridClient client = new SendGridClient(apiKey);
            SendGridMessage msg = new SendGridMessage
            {
                From = new EmailAddress(fromEmail, fromName),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            string[] addresses = toEmail.Split(';');
            foreach (string address in addresses)
            {
                msg.AddTo(new EmailAddress(address, toName));
            }

            if (cc != null && cc.Any())
            {
                foreach (string email in cc)
                {
                    msg.AddCc(email);
                }
            }
            if (bcc != null && bcc.Any())
            {
                foreach (string email in bcc)
                {
                    msg.AddBcc(email);
                }
            }
            if (attachments != null && attachments.Any())
            {
                foreach (KeyValuePair<string, byte[]> attachment in attachments)
                {
                    msg.AddAttachment(attachment.Key, Convert.ToBase64String(attachment.Value));
                }
            }
            Response response = await client.SendEmailAsync(msg);
            if (response.StatusCode != HttpStatusCode.Accepted)
            {
                _logger.LogError($"Error sending email, status code {response.StatusCode}, {await response.Body.ReadAsStringAsync()}");
            }
        }
    }
}
