using FusionCore.Core.Exceptions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.ImageCrop;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using File = FusionCore.Data.Entities.File;

namespace FusionCore.Services.Common
{
    public interface IFileService : IBaseService
    {
        Task<int?> UploadFile(IFormFile file, string elementid = null);
        File GetFile(int id);
        void DeleteFile(File file);

        List<File> GetFiles(int[] id);
    }

    public class FileService : BaseService, IFileService
    {
        private readonly IGenericRepository<File> _fileRepository;
        private readonly IContentTypeProvider _contentTypeProvider;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        public FileService(
            IGenericRepository<File> fileRepository,
            ILogger<FileService> logger, IConfiguration configuration)
        {
            _fileRepository = fileRepository;
            _contentTypeProvider = new FileExtensionContentTypeProvider();
            _logger = logger;
            _configuration = configuration;
        }


        public async Task<int?> UploadFile(IFormFile file, string elementid = null)
        {
            if (file == null) throw new ArgumentException($"{nameof(file)} not is null");

            if (file.Length <= 0) throw new FusionCoreException("File is empty");

            string fileName = RemoveInvalidCharacters(file);

            string path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot", "upload",
                fileName);

            _contentTypeProvider.TryGetContentType(path, out string contentType);

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    await file.CopyToAsync(ms);
                    byte[] fileBinary = ms.ToArray();
                    File dbFile = new File
                    {
                        AltAttribute = fileName,
                        IsNew = true,
                        ContentType = contentType,
                        FileName = fileName,
                        TitleAttribute = fileName
                    };
                    if (elementid != null)
                    {
                        FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();


                        elementid = elementid.ToLower().Trim();
                        if (elementid.Contains("team"))
                        {
                            dbFile.FileLocal = FileHelper.FilePath(fileSettings.TeamsFiles, fileName, fileBinary, false);
                            string filepath = fileSettings.TeamsFiles + dbFile.FileLocal;
                            FileHelper.savebitmap(ms, filepath, 1000, 0);
                        }
                        else if (elementid.Contains("news"))
                        {
                            dbFile.FileLocal = FileHelper.FilePath(fileSettings.NewsFiles, fileName, fileBinary, false);
                            string filepath = fileSettings.NewsFiles + dbFile.FileLocal;
                            FileHelper.savebitmap(ms, filepath, 1024, 576, true);
                        }
                        else if (elementid.Contains("splashimage") || elementid.Contains("thumbnailimage"))
                        {
                            if (elementid.Contains("thumbnailimage"))
                            {
                                dbFile.FileLocal = FileHelper.FilePath(fileSettings.FundFiles, fileName, fileBinary, false);
                                string filepath = fileSettings.FundFiles + dbFile.FileLocal;
                                //FileHelper.savebitmap(ms, filepath, 1024, 576, true);
                                ImageCropHelper.CropFundImage(fileBinary, filepath);
                            }
                            else
                            {
                                dbFile.FileLocal = FileHelper.FilePath(fileSettings.FundFiles, fileName, fileBinary);
                            }
                        }
                        else if (elementid.Contains("bannerfileid"))
                        {
                            dbFile.FileLocal = FileHelper.FilePath(fileSettings.BannerFiles, fileName, fileBinary, false);
                            string filepath = fileSettings.BannerFiles + dbFile.FileLocal;
                            FileHelper.savebitmap(ms, filepath, float.MaxValue, float.MaxValue, true);
                        }
                        else if (elementid.Contains("support"))
                        {
                            dbFile.FileLocal = FileHelper.FilePath(fileSettings.SupportFiles, fileName, fileBinary);
                        }
                        else
                        {
                            dbFile.FileBinary = fileBinary;
                        }
                    }
                    else
                    {
                        dbFile.FileBinary = fileBinary;
                    }

                    await _fileRepository.InsertAsync(dbFile);
                    return dbFile.Id;

                }
            }
            catch (Exception er)
            {
                _logger.LogError(er, "Error uploading image");
                return null;
            }
        }

        public File GetFile(int id)
        {
            return _fileRepository.GetById(id);
        }

        public void DeleteFile(File file)
        {
            _fileRepository.Delete(file);
        }

        public static string RemoveInvalidCharacters(IFormFile file)
        {
            string returnString = "";
            char[] invalidCharacters = Path.GetInvalidFileNameChars();

            foreach (char c in file.FileName)
            {
                if (!invalidCharacters.Contains(c))
                {
                    returnString += c;
                }
            }

            if (string.IsNullOrEmpty(returnString)) throw new FusionCoreException("Filename empty");

            return returnString;
        }

        public List<File> GetFiles(int[] id)
        {
            return _fileRepository.GetAll(x => id.Contains(x.Id)).ToList();
        }
    }
}
