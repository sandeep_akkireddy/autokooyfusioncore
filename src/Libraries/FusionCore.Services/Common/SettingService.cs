﻿using FusionCore.Core.Caching;
using FusionCore.Core.Helpers;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Services.Common
{
    public interface ISettingService : IBaseService
    {
        Task InsertSetting(Setting setting, bool clearCache = true);
        void UpdateSetting(Setting setting, bool clearCache = true);
        void DeleteSetting(Setting setting);
        Task<Setting> GetSettingById(int settingId);
        Task<Setting> GetSetting(string key);
        T GetSettingByKey<T>(string key, T defaultValue = default(T));
        Task SetSetting<T>(string key, T value, bool clearCache = true, bool useCache = true);
        IList<Setting> GetAllSettings();
        void ClearCache();
        T LoadSetting<T>() where T : ISettings, new();
        Task SaveSetting<T>(T settings) where T : ISettings, new();
        void DeleteSetting<T>() where T : ISettings, new();
    }

    public class SettingService : BaseService, ISettingService
    {
        private const string SETTINGS_ALL_KEY = "FusionCore.setting.all";
        private const string SETTINGS_PATTERN_KEY = "FusionCore.setting.";

        private readonly IGenericRepository<Setting> _settingRepository;
        private readonly ICacheManager _cacheManager;

        public SettingService(IGenericRepository<Setting> settingRepository, ICacheManager cacheManager)
        {
            _settingRepository = settingRepository;
            _cacheManager = cacheManager;
        }

        [Serializable]
        public class SettingForCaching
        {
            public int Id { get; set; }
            public string Key { get; set; }
            public string Value { get; set; }
        }

        protected IDictionary<string, SettingForCaching> GetAllSettingsCached()
        {
            string key = string.Format(SETTINGS_ALL_KEY);
            return _cacheManager.Get(key, () =>
            {
                IQueryable<Setting> query = _settingRepository.GetAll(null, false);
                List<Setting> settings = query.ToList();
                Dictionary<string, SettingForCaching> dictionary = new Dictionary<string, SettingForCaching>();
                foreach (Setting s in settings)
                {
                    string resourceName = s.Key.ToLowerInvariant();
                    SettingForCaching settingForCaching = new SettingForCaching
                    {
                        Id = s.Id,
                        Key = s.Key,
                        Value = s.Value
                    };
                    if (!dictionary.ContainsKey(resourceName))
                    {
                        dictionary.Add(resourceName, settingForCaching);
                    }
                }
                return dictionary;
            });
        }

        public async Task InsertSetting(Setting setting, bool clearCache = true)
        {
            if (setting == null) throw new ArgumentNullException(nameof(setting));

            await _settingRepository.InsertAsync(setting);

            if (clearCache) _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);
        }

        public void UpdateSetting(Setting setting, bool clearCache = true)
        {
            if (setting == null) throw new ArgumentNullException(nameof(setting));

            _settingRepository.Update(setting);

            if (clearCache) _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);
        }

        public void DeleteSetting(Setting setting)
        {
            if (setting == null) throw new ArgumentNullException(nameof(setting));

            _settingRepository.Delete(setting);
            _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);
        }

        public async Task<Setting> GetSettingById(int settingId)
        {
            return await _settingRepository.GetByIdAsync(settingId);
        }

        public async Task<Setting> GetSetting(string key)
        {
            if (string.IsNullOrEmpty(key))
                return null;

            IDictionary<string, SettingForCaching> settings = GetAllSettingsCached();
            key = key.Trim().ToLowerInvariant();
            if (settings.ContainsKey(key))
            {
                SettingForCaching setting = settings[key];

                if (setting != null)
                    return await GetSettingById(setting.Id);
            }

            return null;
        }

        public T GetSettingByKey<T>(string key, T defaultValue = default(T))
        {
            if (string.IsNullOrEmpty(key))
                return defaultValue;

            IDictionary<string, SettingForCaching> settings = GetAllSettingsCached();
            key = key.Trim().ToLowerInvariant();
            if (settings.ContainsKey(key))
            {
                SettingForCaching setting = settings[key];

                if (setting != null)
                    return CommonHelper.To<T>(setting.Value);
            }

            return defaultValue;
        }

        public async Task SetSetting<T>(string key, T value, bool clearCache = true, bool useCache = true)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));
            key = key.Trim().ToLowerInvariant();
            string valueStr = TypeDescriptor.GetConverter(typeof(T)).ConvertToInvariantString(value);

            IDictionary<string, SettingForCaching> allSettings = GetAllSettingsCached();
            SettingForCaching settingForCaching = allSettings.ContainsKey(key) ?
                allSettings[key] : null;
            if (settingForCaching != null)
            {
                //update
                Setting setting = await GetSettingById(settingForCaching.Id);
                setting.Value = valueStr;
                UpdateSetting(setting, clearCache);
            }
            else
            {
                //insert
                Setting setting = new Setting
                {
                    Key = key,
                    Value = valueStr
                };
                await InsertSetting(setting, clearCache);
            }
        }

        public IList<Setting> GetAllSettings()
        {
            IQueryable<Setting> query = _settingRepository.GetAll();
            List<Setting> settings = query.ToList();
            return settings;
        }

        public void ClearCache()
        {
            _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);
        }

        public T LoadSetting<T>() where T : ISettings, new()
        {
            Type type = typeof(T);
            object settings = Activator.CreateInstance(type);

            foreach (System.Reflection.PropertyInfo prop in type.GetProperties())
            {
                // get properties we can read and write to
                if (!prop.CanRead || !prop.CanWrite)
                    continue;

                string key = type.Name + "." + prop.Name;
                //load by store
                string setting = GetSettingByKey<string>(key);
                if (setting == null)
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).IsValid(setting))
                    continue;

                object value = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromInvariantString(setting);

                //set property
                prop.SetValue(settings, value, null);
            }

            return (T)settings;
        }

        public async Task SaveSetting<T>(T settings) where T : ISettings, new()
        {
            foreach (System.Reflection.PropertyInfo prop in typeof(T).GetProperties())
            {
                // get properties we can read and write to
                if (!prop.CanRead || !prop.CanWrite)
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    continue;

                string key = typeof(T).Name + "." + prop.Name;
                //Duck typing is not supported in C#. That's why we're using dynamic type
                dynamic value = prop.GetValue(settings, null);
                if (value != null)
                    await SetSetting(key, value, false, false);
                else
                    await SetSetting(key, "", false, false);
            }
            ClearCache();
        }

        public void DeleteSetting<T>() where T : ISettings, new()
        {
            List<Setting> settingsToDelete = new List<Setting>();
            IList<Setting> allSettings = GetAllSettings();
            foreach (System.Reflection.PropertyInfo prop in typeof(T).GetProperties())
            {
                string key = typeof(T).Name + "." + prop.Name;
                settingsToDelete.AddRange(allSettings.Where(x => x.Key.Equals(key, StringComparison.InvariantCultureIgnoreCase)));
            }

            foreach (Setting setting in settingsToDelete)
            {
                DeleteSetting(setting);
            }

            ClearCache();
        }
    }
}
