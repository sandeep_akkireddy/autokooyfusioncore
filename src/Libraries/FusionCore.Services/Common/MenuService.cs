using FusionCore.Core.Caching;
using FusionCore.Core.Enums;
using FusionCore.Core.Exceptions;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Services.Common
{
    public interface IMenuService : IBaseService
    {
        Task InsertMenuItem(MenuItem menuItem, bool clearCache = true);
        void UpdateMenuItem(MenuItem menuItem, bool clearCache = true);
        void DeleteMenuItem(MenuItem menuItem);
        List<MenuService.MenuItemForCaching> GetMenu(MenuType type, IUrlHelper url);
    }

    public class MenuService : BaseService, IMenuService
    {
        private const string MENUITEM_ALL_KEY = "FusionCore.menuitems.all";
        private const string MENUITEM_PATTERN_KEY = "FusionCore.menuitems.";

        private readonly IGenericRepository<MenuItem> _menuItemRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MenuService(
            IGenericRepository<MenuItem> menuItemRepository,
            ICacheManager cacheManager,
            IHttpContextAccessor httpContextAccessor)
        {
            _menuItemRepository = menuItemRepository;
            _cacheManager = cacheManager;
            _httpContextAccessor = httpContextAccessor;
        }

        [Serializable]
        public class MenuItemForCaching
        {
            public MenuItemForCaching()
            {
                Children = new List<MenuItemForCaching>();
            }
            public int Id { get; set; }
            public string Icon { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
            public int DisplayOrder { get; set; }
            public List<MenuItemForCaching> Children { get; set; }
            public bool IsDivider { get; set; }
            public bool Active { get; set; }
            public bool HasNewButton { get; set; }
            public bool IsActive
            {
                get { return Active || Children.Any(x => x.Active) || Children.Any(x => x.Children.Any(y => y.Active)); }
            }
            public bool ShowMenu { get; set; }
        }

        protected Dictionary<MenuType, List<MenuItemForCaching>> GetAllMenuItemsCached(IUrlHelper helper)
        {
            string key = string.Format(MENUITEM_ALL_KEY);
            return _cacheManager.Get(key, () =>
            {
                List<MenuItem> menuItems = _menuItemRepository.GetAll(x=>x.ShowMenu, false).ToList();
                Dictionary<MenuType, List<MenuItemForCaching>> dictionary = new Dictionary<MenuType, List<MenuItemForCaching>>();
                foreach (IGrouping<MenuType, MenuItem> type in menuItems.GroupBy(x => x.Type))
                {
                    if (!dictionary.ContainsKey(type.Key))
                    {
                        dictionary.Add(type.Key, new List<MenuItemForCaching>());
                    }
                    dictionary[type.Key].AddRange(CollectMenuItemsRecursive(type.Key, menuItems, null, helper));
                }
                return dictionary;
            });
        }

        
        private List<MenuItemForCaching> CollectMenuItemsRecursive(MenuType menuType, List<MenuItem> menuItems, int? parentId,
            IUrlHelper urlHelper)
        {
            List<MenuItemForCaching> list = new List<MenuItemForCaching>();
            foreach (MenuItem item in menuItems.Where(x => x.Type == menuType && x.ParentId == parentId))
            {
                MenuItemForCaching menuItemForCaching = new MenuItemForCaching
                {
                    Id = item.Id,
                    Name = item.Name,
                    Url = !string.IsNullOrEmpty(item.Controller) && !string.IsNullOrEmpty(item.Action)
                        ? urlHelper.Action(new UrlActionContext { Controller = item.Controller, Action = item.Action, Values = new { item.Area } }) + item.Parameters
                        : null,
                    DisplayOrder = item.DisplayOrder,
                    Icon = item.Icon,
                    IsDivider = item.IsDivider,
                    HasNewButton = item.HasNewButton,
                    ShowMenu = item.ShowMenu
                };
                if (menuItems.Any(x => x.ParentId == item.Id))
                {
                    menuItemForCaching.Children.AddRange(CollectMenuItemsRecursive(menuType, menuItems, item.Id, urlHelper));
                }

                list.Add(menuItemForCaching);
            }

            return list;
        }

        public async Task InsertMenuItem(MenuItem menuItem, bool clearCache = true)
        {
            if (menuItem == null) throw new ArgumentNullException(nameof(menuItem));

            await _menuItemRepository.InsertAsync(menuItem);

            if (clearCache)
                _cacheManager.RemoveByPattern(MENUITEM_PATTERN_KEY);
        }

        public void UpdateMenuItem(MenuItem menuItem, bool clearCache = true)
        {
            if (menuItem == null) throw new ArgumentNullException(nameof(menuItem));

            _menuItemRepository.Update(menuItem);

            if (clearCache) _cacheManager.RemoveByPattern(MENUITEM_PATTERN_KEY);
        }

        public void DeleteMenuItem(MenuItem menuItem)
        {
            if (menuItem == null) throw new ArgumentNullException(nameof(menuItem));

            _menuItemRepository.Delete(menuItem);
            _cacheManager.RemoveByPattern(MENUITEM_PATTERN_KEY);
        }

        public List<MenuItemForCaching> GetMenu(MenuType type, IUrlHelper urlHelper)
        {
            Dictionary<MenuType, List<MenuItemForCaching>> all = GetAllMenuItemsCached(urlHelper);
            if (!all.ContainsKey(type)) throw new FusionCoreException($"Menu failed to load for type {type}");

            List<MenuItemForCaching> menuItems = all[type].OrderBy(x => x.DisplayOrder).ToList();
            PathString currentUrl = _httpContextAccessor.HttpContext.Request.Path;
            SetActive(menuItems, currentUrl);
            return menuItems;
        }

        private void SetActive(List<MenuItemForCaching> menuItems, string currentUrl)
        {
            foreach (MenuItemForCaching menuItem in menuItems)
            {
                if (!string.IsNullOrEmpty(menuItem.Url))
                {
                    menuItem.Active = menuItem.Url.Equals(currentUrl, StringComparison.InvariantCultureIgnoreCase);
                }

                if (menuItem.Children.Any())
                {
                    SetActive(menuItem.Children, currentUrl);
                }
            }
        }
    }
}
