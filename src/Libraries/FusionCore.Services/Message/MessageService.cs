using AutoMapper;
using FusionCore.Core.Settings;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Backend.FusionUser;
using FusionCore.Models.Backend.Messages;
using FusionCore.Models.Common;
using FusionCore.Services.User;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FusionCore.Services.Message
{
    public interface IMessageService : IBaseService
    {
        MessageGroups GetMessageGroup(string groupname);
        List<Messages> GetGroupMessages(int groupid, int pageIndex);
        GroupChatBox GetDashboardChat(int pageIndex);
        void PostMessageinGroup(GroupChatBox model);
        void PostMessageToUser(GroupChatBox model);

        GroupChatBox GetUserMessages(int userid);

        List<FusionUserPersistModel> GetActiveMessageUsers();
        bool UpdateUserMessagesToRead(int userid);
        List<MessagePersistModel> ShowUnreadMessages(int userid);
         bool DeleteMessage(int msgid,string type);
    }
    public class MessageService : BaseService, IMessageService
    {
        private readonly IGenericRepository<Messages> _messageRepository;
        private readonly IGenericRepository<MessageGroups> _messageGroupsRepository;
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;
        private readonly int skipcount = 100;
        private readonly IMapper _mapper;
        public MessageService(IGenericRepository<Messages> messageRepository,
            IGenericRepository<MessageGroups> messageGroupsRepository,
            IConfiguration configuration,
            IUserService userService,
            IMapper mapper)
        {
            _messageRepository = messageRepository;
            _messageGroupsRepository = messageGroupsRepository;
            _configuration = configuration;
            _userService = userService;
            _mapper = mapper;
        }

        public List<Messages> GetGroupMessages(int groupid, int pageIndex)
        {
            return _messageRepository.GetAll(x => x.groupid.HasValue && x.groupid.Value == groupid, true, x => x.sender, x => x.receiver).OrderByDescending(x => x.CreatedOnUtc).Skip(pageIndex * skipcount).Take(skipcount).OrderBy(x => x.CreatedOnUtc).ToList();
        }

        public MessageGroups GetMessageGroup(string groupname)
        {
            return _messageGroupsRepository.Get(x => x.Name == groupname);
        }

        public GroupChatBox GetDashboardChat(int pageIndex)
        {
            var group = GetMessageGroup("dashboard");

            GroupChatBox chat = new GroupChatBox();
            chat.senderid = _userService.GetCurrentUserId();
            chat.messageThreads = new List<MessagePersistModel>();
            if (group != null)
            {
                chat.groupid = group.Id;

                var messages = GetGroupMessages(group.Id, pageIndex);
                messages.ForEach(x =>
                {
                    MessagePersistModel msg = new MessagePersistModel();
                    msg.groupid = x.groupid.HasValue ? x.groupid.Value : 0;
                    msg.Id = x.Id;
                    msg.isread = x.isread;
                    msg.message = x.message;
                    msg.orgmsgid = x.orgmsgid;

                    msg.receiverId = x.receiverId;
                    msg.receivername = x.receiver != null ? x.receiver.GetFullName : "";
                    msg.receiverphoto = x.receiver != null ? x.receiver.Photo : "";
                    if (!string.IsNullOrEmpty(msg.receiverphoto) && !msg.receiverphoto.Contains("assets/images/avatar-1-xl.jpg"))
                    {
                        var fusionurl = _configuration.GetSection("MainSettings").Get<MainSettings>().FusionUrl;
                        var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFileRelativePath;
                        msg.receiverphoto = string.Concat(fusionurl, path, msg.receiverphoto);
                    }
                    else
                    {
                        msg.receiverphoto = "/assets/images/avatar-1-xl.jpg";
                    }

                    msg.senderId = x.senderId;
                    msg.sendername = x.sender != null ? x.sender.GetFullName : "";
                    msg.senderphoto = x.sender != null ? x.sender.Photo : "";
                    if (!string.IsNullOrEmpty(msg.senderphoto) && !msg.senderphoto.Contains("assets/images/avatar-1-xl.jpg"))
                    {
                        var fusionurl = _configuration.GetSection("MainSettings").Get<MainSettings>().FusionUrl;
                        var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFileRelativePath;
                        msg.senderphoto = string.Concat(fusionurl, path, msg.senderphoto);
                    }
                    else
                    {
                        msg.senderphoto = "/assets/images/avatar-1-xl.jpg";
                    }

                    msg.sentdate = x.CreatedOnUtc;

                    chat.messageThreads.Add(msg);
                });
            }

            return chat;
        }

        public void PostMessageinGroup(GroupChatBox model)
        {
            Messages msg = new Messages();
            msg.CreatedOnUtc = DateTime.Now;
            msg.UpdatedOnUtc = DateTime.Now;
            msg.groupid = model.groupid;
            msg.message = model.message;
            msg.senderId = model.senderid;
            msg.isread = true;

            _messageRepository.Insert(msg);

        }

        public GroupChatBox GetUserMessages(int userid)
        {
            var loggedinuserid = _userService.GetCurrentUserId();
            GroupChatBox chat = new GroupChatBox();
            chat.senderid = loggedinuserid;
            chat.receiverid = userid;
            chat.messageThreads = new List<MessagePersistModel>();

            var messages = _messageRepository.GetAll(x =>
              (((x.senderId == userid && x.receiverId == loggedinuserid) || (x.senderId == loggedinuserid && x.receiverId == userid))
              && !x.groupid.HasValue)
              , true, x => x.sender, x => x.receiver).OrderByDescending(x => x.CreatedOnUtc).ToList();

            var fusionurl = _configuration.GetSection("MainSettings").Get<MainSettings>().FusionUrl;
            var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFileRelativePath;

            messages.ForEach(x =>
            {
                MessagePersistModel msg = new MessagePersistModel();
                msg.groupid = x.groupid.HasValue ? x.groupid.Value : 0;
                msg.Id = x.Id;
                msg.isread = x.isread;
                msg.message = x.message;
                msg.orgmsgid = x.orgmsgid;

                msg.receiverId = x.receiverId;
                msg.receivername = x.receiver != null ? x.receiver.GetFullName : "";
                msg.receiverphoto = x.receiver != null ? x.receiver.Photo : "";
                if (!string.IsNullOrEmpty(msg.receiverphoto) && !msg.receiverphoto.Contains("assets/images/avatar-1-xl.jpg"))
                {
                    msg.receiverphoto = string.Concat(fusionurl, path, msg.receiverphoto);
                }
                else
                {
                    msg.receiverphoto = "/assets/images/avatar-1-xl.jpg";
                }

                msg.senderId = x.senderId;
                msg.sendername = x.sender != null ? x.sender.GetFullName : "";
                msg.senderphoto = x.sender != null ? x.sender.Photo : "";
                if (!string.IsNullOrEmpty(msg.senderphoto) && !msg.senderphoto.Contains("assets/images/avatar-1-xl.jpg"))
                {
                    msg.senderphoto = string.Concat(fusionurl, path, msg.senderphoto);
                }
                else
                {
                    msg.senderphoto = "/assets/images/avatar-1-xl.jpg";
                }

                msg.sentdate = x.CreatedOnUtc;

                chat.messageThreads.Add(msg);
            });
            return chat;
        }

        public void PostMessageToUser(GroupChatBox model)
        {
            Messages msg = new Messages();
            msg.CreatedOnUtc = DateTime.Now;
            msg.UpdatedOnUtc = DateTime.Now;
            msg.receiverId = model.receiverid;
            msg.message = model.message;
            msg.senderId = model.senderid;
            msg.isread = false;

            _messageRepository.Insert(msg);
        }

        public List<FusionUserPersistModel> GetActiveMessageUsers()
        {
            var userid = _userService.GetCurrentUserId();
            var users = _mapper.Map<List<FusionUserPersistModel>>(_userService.GetUsersList(x => x.Active && x.Id != userid, x => x.Firstname, true));
            var msgs = _messageRepository.GetAll(x => !x.groupid.HasValue && x.receiverId == userid && !x.isread);
            var lastusermsg = _messageRepository.GetAll(x => !x.groupid.HasValue && (x.receiverId == userid || x.senderId == userid));

            var fusionurl = _configuration.GetSection("MainSettings").Get<MainSettings>().FusionUrl;
            var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFileRelativePath;

            users.ForEach(x =>
            {
                x.Username = x.Firstname + " " + x.Lastname;
                if (!string.IsNullOrEmpty(x.Photo) && !x.Photo.Contains("assets/images/avatar-1-xl.jpg"))
                {
                    x.Photo = string.Concat(fusionurl, path, x.Photo);
                }
                else
                {
                    x.Photo = "/assets/images/avatar-1-xl.jpg";
                }
                x.unreadmsgs = msgs.Count(y => y.senderId == x.Id);
                x.lastmsg = lastusermsg.Where(z => z.senderId == x.Id || z.receiverId == x.Id).OrderByDescending(z => z.CreatedOnUtc).FirstOrDefault()?.CreatedOnUtc;

            });

            return users;
        }

        public bool UpdateUserMessagesToRead(int userid)
        {
            var currentuserid = _userService.GetCurrentUserId();
            var receivemsgs = _messageRepository.GetAll(x => !x.groupid.HasValue && x.receiverId == currentuserid && x.senderId == userid && !x.isread).ToList();
            receivemsgs.ForEach(x => { x.isread = true; });
            _messageRepository.UpdateAll(receivemsgs);
            return true;
        }

        public List<MessagePersistModel> ShowUnreadMessages(int userid)
        {
            var messageThreads = new List<MessagePersistModel>();
            var unreadmessages = _messageRepository.GetAll(x => !x.groupid.HasValue && !x.isread && x.receiverId == userid,
                true, x => x.sender, x => x.receiver).OrderByDescending(x => x.CreatedOnUtc).ToList();
            var fusionurl = _configuration.GetSection("MainSettings").Get<MainSettings>().FusionUrl;
            var path = _configuration.GetSection("FileSettings").Get<FileSettings>().UserFileRelativePath;

            unreadmessages.ForEach(x =>
            {
                MessagePersistModel msg = new MessagePersistModel();
                msg.groupid = x.groupid.HasValue ? x.groupid.Value : 0;
                msg.Id = x.Id;
                msg.isread = x.isread;
                msg.message = x.message;
                msg.orgmsgid = x.orgmsgid;

                msg.receiverId = x.receiverId;
                msg.receivername = x.receiver != null ? x.receiver.GetFullName : "";
                msg.receiverphoto = x.receiver != null ? x.receiver.Photo : "";
                if (!string.IsNullOrEmpty(msg.receiverphoto) && !msg.receiverphoto.Contains("assets/images/avatar-1-xl.jpg"))
                {
                    msg.receiverphoto = string.Concat(fusionurl, path, msg.receiverphoto);
                }
                else
                {
                    msg.receiverphoto = "/assets/images/avatar-1-xl.jpg";
                }

                msg.senderId = x.senderId;
                msg.sendername = x.sender != null ? x.sender.GetFullName : "";
                msg.senderphoto = x.sender != null ? x.sender.Photo : "";
                if (!string.IsNullOrEmpty(msg.senderphoto) && !msg.senderphoto.Contains("assets/images/avatar-1-xl.jpg"))
                {
                    msg.senderphoto = string.Concat(fusionurl, path, msg.senderphoto);
                }
                else
                {
                    msg.senderphoto = "/assets/images/avatar-1-xl.jpg";
                }

                msg.sentdate = x.CreatedOnUtc;

                messageThreads.Add(msg);
            });
            return messageThreads;
        }

     

        public bool DeleteMessage(int msgid, string type)
        {
            var msg=_messageRepository.GetById(msgid);
            if (msg != null)
            {
                _messageRepository.Delete(msg);
                return true;
            }
            return false;
        }
    }
}
