using FusionCore.Data.Interfaces;
using FusionCore.Models.Backend.ToDo;
using System;
using System.Collections.Generic;
using System.Text;
using FusionCore.Data.Entities;
using AutoMapper;
using FusionCore.Core.Enums;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Services.ToDO
{
    public interface IToDoService : IBaseService
    {
        bool AddToDO(ToDoPersistModel model);
        List<ToDoPersistModel> GetToDO(int userid);

        bool MarkDone(int id);
        bool DeleteToDo(int id);
    }
    public class ToDoService : IToDoService
    {
        private readonly IGenericRepository<ToDo> _todoRepository;
        private readonly IMapper _mapper;
        public ToDoService(IGenericRepository<ToDo> todoRepository, IMapper mapper)
        {
            _todoRepository = todoRepository;
            _mapper = mapper;
        }

        public bool AddToDO(ToDoPersistModel model)
        {
            ToDo dbEntity = _todoRepository.Get(x => x.Id == model.Id);
            var date = DateTime.Now;//if need only date, with time as 0's then use DateTime.Now.Date and in automapper write conversion from .Now to .Now.Date
            switch (model.Period)
            {
                case ToDoPeriod.Today:
                    model.expiryDate = date;
                    break;
                case ToDoPeriod.Tomorrow:
                    model.expiryDate = date.AddDays(1);
                    break;
                case ToDoPeriod.ThisWeek:
                    var sundayDate = date.Subtract(new TimeSpan((int)date.DayOfWeek, 0, 0, 0));
                    var saturdayDate = sundayDate.AddDays(6);
                    model.expiryDate = saturdayDate;
                    break;
                case ToDoPeriod.ThisMonth:
                    model.expiryDate = date.AddMonths(1);
                    break;
            }


            if (dbEntity != null)
            {
                dbEntity.Name = model.ToDoTitle;
                dbEntity.Period = model.Period;
                dbEntity.expiryDate = model.expiryDate;
                
                _todoRepository.Update(dbEntity);
            }
            else
            {
                dbEntity = new ToDo();
                _mapper.Map(model, dbEntity);
                _todoRepository.Insert(dbEntity);
            }
            return true;
        }

        public bool DeleteToDo(int id)
        {
            var item = _todoRepository.GetById(id);
            if (item != null)
            {
                _todoRepository.Delete(item);
            }
            return true;
        }

        public List<ToDoPersistModel> GetToDO(int userid)
        {
            var date = DateTime.Now.Date;
            var donelist = _todoRepository.GetAll(x => x.userId == userid && x.done
            && x.expiryDate < date).ToList();
            _todoRepository.DeleteAll(donelist);

            var pastlist = _todoRepository.GetAll(x => x.userId == userid && !x.done && x.expiryDate < date
            ).ToList();
            pastlist.ForEach(x =>
            {
                x.expiryDate = DateTime.Now;
            });
            _todoRepository.UpdateAll(pastlist);

            var todolist = _todoRepository.GetAll(x => x.userId == userid && x.expiryDate >= date).ToList();
            List<ToDoPersistModel> model = new List<ToDoPersistModel>();
            _mapper.Map(todolist, model);
            return model;
        }

        public bool MarkDone(int id)
        {
            var item = _todoRepository.Get(x => x.Id == id);
            if (item != null)
            {
                item.done = !item.done;
                _todoRepository.Update(item);
            }
            return true;
        }
    }
}
