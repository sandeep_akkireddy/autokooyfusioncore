using FusionCore.Core.Interfaces;

namespace FusionCore.Data.Settings
{
    public class WebsiteSettings : ISettings
    {
        public string GoogleAnalyticsCode { get; set; }
        public string TwitterCode { get; set; }
        public string FacebookCode { get; set; }
        public string GooglePlusCode { get; set; }
        public string LinkedInCode { get; set; }
        public string SkypeCode { get; set; }
        public string WhatsappCode { get; set; }
        public string InstagramCode { get; set; }
        public string YoutubeCode { get; set; }
    }
}
