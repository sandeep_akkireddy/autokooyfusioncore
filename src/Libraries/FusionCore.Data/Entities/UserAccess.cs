using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Entities
{
    public class UserAccess : BaseEntity
    {
        public int? userId { get; set; }
        public virtual User user { get; set; }
        public int FusionModule { get; set; }
        public bool Active { get; set; }
    }
}
