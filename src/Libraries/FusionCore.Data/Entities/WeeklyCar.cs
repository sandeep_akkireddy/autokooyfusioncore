using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Entities
{
    public class WeeklyCar : BaseEntity
    {
        public string FileName { get; set; }
        public string VehicleNumber { get; set; }
        public DateTime WeekExpiry { get; set; }
    }
}
