﻿using System.ComponentModel.DataAnnotations;

namespace FusionCore.Data.Entities
{
    public class Credential : BaseEntity
    {
        public int UserId { get; set; }
        [Required]
        public string Type { get; set; }
        [MaxLength(64)]
        public string Identifier { get; set; }
        [MaxLength(40)]
        public string ResetKey { get; set; }
        public byte[] Key { get; set; }
        public byte[] Salt { get; set; }

        public virtual User User { get; set; }
    }
}
