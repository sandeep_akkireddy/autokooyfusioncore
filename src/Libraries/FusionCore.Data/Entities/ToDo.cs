using FusionCore.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Entities
{
    public class ToDo : BaseEntity
    {
        public string Name { get; set; }
        public ToDoPeriod Period { get; set; }
        public DateTime expiryDate { get; set; }
        public bool done { get; set; }
        public int userId { get; set; }
        public virtual User user { get; set; }
    }
}
