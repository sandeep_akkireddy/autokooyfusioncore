﻿using System.ComponentModel.DataAnnotations;

namespace FusionCore.Data.Entities
{
    public class Setting : BaseEntity
    {
        [Required]
        [MaxLength(255)]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
