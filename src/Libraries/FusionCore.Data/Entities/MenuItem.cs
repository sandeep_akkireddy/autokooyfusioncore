using FusionCore.Core.Enums;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Data.Entities
{
    public class MenuItem : BaseEntity
    {
        [MaxLength(100)]
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int DisplayOrder { get; set; }
        public bool HasNewButton { get; set; }
        public MenuType Type { get; set; }
        [MaxLength(50)]
        public string Icon { get; set; }
        public bool IsDivider { get; set; }
        [MaxLength(256)]
        public string Controller { get; set; }
        [MaxLength(256)]
        public string Action { get; set; }
        [MaxLength(256)]
        public string Parameters { get; set; }
        [MaxLength(256)]
        public string Area { get; set; }
        public bool ShowMenu { get; set; }
    }
}
