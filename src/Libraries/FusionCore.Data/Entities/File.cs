using System.ComponentModel.DataAnnotations;

namespace FusionCore.Data.Entities
{
    public class File : BaseEntity
    {
        [Required]
        [MaxLength(256)]
        public string FileName { get; set; }

        [Required]
        [MaxLength(50)]
        public string ContentType { get; set; }
        [MaxLength(256)]
        public string AltAttribute { get; set; }
        [MaxLength(256)]
        public string TitleAttribute { get; set; }
        public bool IsNew { get; set; }

        public byte[] FileBinary { get; set; }

        public string FileLocal { get; set; }
    }
}
