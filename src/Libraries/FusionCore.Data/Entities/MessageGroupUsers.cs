using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Entities
{
    public class MessageGroupUsers : BaseEntity
    {
        public int? userId { get; set; }
        public virtual User user { get; set; }
        public int? groupid { get; set; }
        public virtual MessageGroups group { get; set; }   
    }
}
