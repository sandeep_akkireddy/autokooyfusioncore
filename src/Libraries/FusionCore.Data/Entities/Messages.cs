using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FusionCore.Data.Entities
{
    public class Messages : BaseEntity
    {
        public int? senderId { get; set; }
        public virtual User sender { get; set; }
        public int? receiverId { get; set; }
        public virtual User receiver { get; set; }
        public int? groupid { get; set; }
        public virtual MessageGroups group { get; set; }
        public string message { get; set; }
        public bool isread { get; set; }
        public int? orgmsgid { get; set; }
        public virtual Messages Messagethread { get; set; }
    }
}
