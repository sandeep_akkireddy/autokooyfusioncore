using FusionCore.Core.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Data.Entities
{
    public class User : BaseEntity
    {
        [MaxLength(256)]
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public GenderType Gender { get; set; }
        public bool PrivacyAccepted { get; set; }
        public bool Active { get; set; }
        public FusionRoles Role { get; set; }

        public string Settings { get; set; }

        public virtual ICollection<Credential> Credentials { get; set; }

        public string GetFullName => $"{Firstname}{(!string.IsNullOrEmpty(Middlename) ? " " + Middlename : "")} {Lastname}";

        public string Photo { get; set; }
        [MaxLength(50)]
        public string Telephone { get; set; }
        public bool isloggedin { get; set; }
        public string Occupation { get; set; }

    }
}
