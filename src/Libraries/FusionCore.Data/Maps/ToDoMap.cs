using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Maps
{
    public class ToDoMap : IEntityTypeConfiguration<ToDo>
    {
        public void Configure(EntityTypeBuilder<ToDo> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasOne(x => x.user).WithMany().HasForeignKey(x => x.userId);
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.UpdatedOnUtc);
            builder.Property(x => x.done);
            builder.Property(x => x.expiryDate);
            builder.Property(x => x.Name);
            builder.Property(x => x.Period);
        }
    }
}
