﻿using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Data.Maps
{
    public class CredentialMap : IEntityTypeConfiguration<Credential>
    {
        public void Configure(EntityTypeBuilder<Credential> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.Identifier).IsRequired().HasMaxLength(64);
            builder.Property(e => e.Key).HasMaxLength(1024);
            builder.Property(e => e.ResetKey).HasMaxLength(32);
        }
    }
}
