using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Maps
{
    public class WeeklyCarMap : IEntityTypeConfiguration<WeeklyCar>
    {
        public void Configure(EntityTypeBuilder<WeeklyCar> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.UpdatedOnUtc);
            builder.Property(x => x.WeekExpiry);
            builder.Property(x => x.VehicleNumber);
            builder.Property(x => x.FileName);
        }
    }
}
