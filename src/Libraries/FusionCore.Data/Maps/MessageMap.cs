using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Maps
{
    public class MessageMap : IEntityTypeConfiguration<Messages>
    {
        public void Configure(EntityTypeBuilder<Messages> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasOne(x => x.sender).WithMany().HasForeignKey(x => x.senderId);
            builder.HasOne(x => x.receiver).WithMany().HasForeignKey(x => x.receiverId);
            builder.Property(x => x.message);
            builder.Property(x => x.groupid);
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.UpdatedOnUtc);
            builder.Property(x => x.isread);
            builder.HasOne(x => x.Messagethread).WithMany().HasForeignKey(x => x.orgmsgid);
        }
    }
}
