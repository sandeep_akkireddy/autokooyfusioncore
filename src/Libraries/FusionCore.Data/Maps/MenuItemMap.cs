﻿using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Data.Maps
{
    public class MenuItemMap : IEntityTypeConfiguration<MenuItem>
    {
        public void Configure(EntityTypeBuilder<MenuItem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
            builder.Property(x => x.Controller).HasMaxLength(256);
            builder.Property(x => x.Action).HasMaxLength(256);
            builder.Property(x => x.Parameters).HasMaxLength(256);
            builder.Property(x => x.Icon).HasMaxLength(50);
        }
    }
}
