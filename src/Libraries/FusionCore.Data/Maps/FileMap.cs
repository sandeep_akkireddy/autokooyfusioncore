﻿using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Data.Maps
{
    public class SettiFileMapngMap : IEntityTypeConfiguration<File>
    {
        public void Configure(EntityTypeBuilder<File> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.FileName).IsRequired().HasMaxLength(256);
            builder.Property(x => x.ContentType).IsRequired().HasMaxLength(50);
            builder.Property(x => x.AltAttribute).HasMaxLength(256);
            builder.Property(x => x.TitleAttribute).HasMaxLength(256);
        }
    }
}
