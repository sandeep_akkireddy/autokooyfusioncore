using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Data.Maps
{
    public class MessageGroupUsersMap : IEntityTypeConfiguration<MessageGroupUsers>
    {
        public void Configure(EntityTypeBuilder<MessageGroupUsers> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasOne(x => x.user).WithMany().HasForeignKey(x => x.userId);
            builder.HasOne(x => x.group).WithMany().HasForeignKey(x => x.groupid);
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.UpdatedOnUtc);

        }
    }
}
