using FusionCore.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Data.Maps
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Email).HasMaxLength(256);
            builder.Property(x => x.Firstname).HasMaxLength(256);
            builder.Property(x => x.Lastname).HasMaxLength(256);
            builder.Property(x => x.Middlename).HasMaxLength(50);
            builder.Property(x => x.Address).HasMaxLength(256);
            builder.Property(x => x.PostalCode).HasMaxLength(50);
            builder.Property(x => x.City).HasMaxLength(256);
            builder.Property(x => x.isloggedin);
            builder.Property(x => x.Occupation);
        }
    }
}
