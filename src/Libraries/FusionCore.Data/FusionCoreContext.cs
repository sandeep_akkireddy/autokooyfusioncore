using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.SqlServer.Infrastructure.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FusionCore.Data
{
    public class FusionCoreContext : DbContext
    {
        //string _connectionstring = null;
        public FusionCoreContext(DbContextOptions<FusionCoreContext> options) : base(options)
        {
            //SqlServerOptionsExtension extension = options.FindExtension<SqlServerOptionsExtension>();
            //if (extension != null)
            //{
            //    _connectionstring = extension.ConnectionString;
            //}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            System.Collections.Generic.List<TypeInfo> coreTypes = Assembly.Load("FusionCore.Data").DefinedTypes
                .Where(t => !t.IsAbstract
                            && !t.IsGenericTypeDefinition
                            && t.GetTypeInfo().ImplementedInterfaces.Any(i =>
                                i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))).ToList();

            foreach (TypeInfo type in coreTypes)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.ApplyConfiguration(configurationInstance);
            }

            Assembly entryAssembly = Assembly.GetEntryAssembly();
            //var entryAssembly = Assembly.Load("FusionCore");
            System.Collections.Generic.IEnumerable<TypeInfo> types = entryAssembly
                .GetReferencedAssemblies()
                .Select(Assembly.Load)
                .SelectMany(x => x.DefinedTypes)
                .Where(t => !t.IsAbstract
                            && !t.IsGenericTypeDefinition
                            && t.GetTypeInfo().ImplementedInterfaces.Any(i =>
                                i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>)));

            foreach (TypeInfo type in types)
            {
                if (coreTypes.Contains(type)) continue;

                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.ApplyConfiguration(configurationInstance);
            }

            System.Collections.Generic.IEnumerable<Type> entryTypes = entryAssembly.GetTypes().Where(t => !t.IsAbstract
                            && !t.IsGenericTypeDefinition
                            && t.GetTypeInfo().ImplementedInterfaces.Any(i =>
                                i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>)));

            foreach (Type type in entryTypes)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.ApplyConfiguration(configurationInstance);
            }

            base.OnModelCreating(modelBuilder);
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (_connectionstring != null)
        //        optionsBuilder.UseSqlServer(_connectionstring);
        //}

    }


}
