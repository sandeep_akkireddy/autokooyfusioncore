using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace FusionCore.Data.Interfaces
{
	public interface IGenericRepository<T> : IDisposable where T : BaseEntity
	{
		T GetById(object id);
		Task<T> GetByIdAsync(object id, CancellationToken cancellationToken = default(CancellationToken));
		T Get(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);
		Task<T> GetAsync(Expression<Func<T, bool>> predicate, CancellationToken cancellationToken = default(CancellationToken),
			params Expression<Func<T, object>>[] includes);
		IQueryable<T> GetAll(Expression<Func<T, bool>> predicate = null, bool trackEntities = true, params Expression<Func<T, object>>[] includes);
		void Insert(T entity);
		Task InsertAsync(T entity, CancellationToken cancellationToken = default(CancellationToken));
		void Update(T entity);
		Task UpdateAsync(T entity);
		void Delete(T entity);
		Task DeleteAsync(T entity);
        IQueryable<T> Table { get; }

		void InsertAll(ICollection<T> entity);
		void UpdateAll(ICollection<T> entity);
		void DeleteAll(ICollection<T> entity);

        

    }
}
