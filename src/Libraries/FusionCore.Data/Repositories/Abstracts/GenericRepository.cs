using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FusionCore.Data.Repositories.Abstracts
{
    [SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global")]
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly FusionCoreContext _dbContext;

        public GenericRepository(FusionCoreContext dbContext)
        {
            _dbContext = dbContext;
            Entities = Context.Set<T>();
        }

        private DbContext Context => _dbContext;

        private DbSet<T> Entities { get; }

        public T GetById(object id)
        {
            return Entities.Find(id);
        }

        public async Task<T> GetByIdAsync(object id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await Entities.FindAsync(new[] { id }, cancellationToken);
        }

        public T Get(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            var result = GetAll();
            if (includes.Any())
            {
                foreach (var include in includes)
                {
                    result = result.Include(include);
                }
            }
            return result.FirstOrDefault(predicate);
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>> predicate, CancellationToken cancellationToken = default(CancellationToken),
            params Expression<Func<T, object>>[] includes)
        {
            var result = GetAll();
            if (includes.Any())
            {
                foreach (var include in includes)
                {
                    result = result.Include(include);
                }
            }
            return await result.FirstOrDefaultAsync(predicate, cancellationToken);
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate = null, bool trackEntities = true, params Expression<Func<T, object>>[] includes)
        {
            var queryable = trackEntities ? Entities : Entities.AsNoTracking();
            if (includes.Any())
            {
                foreach (var include in includes)
                {
                    queryable = queryable.Include(include);
                }
            }
            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }
            return queryable;
        }

        public void Insert(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.UpdatedOnUtc = DateTime.UtcNow;
            entity.CreatedOnUtc = DateTime.UtcNow;

            Entities.Add(entity);
            _dbContext.SaveChanges();
        }

        public async Task InsertAsync(T entity, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.UpdatedOnUtc = DateTime.UtcNow;
            entity.CreatedOnUtc = DateTime.UtcNow;

            await Entities.AddAsync(entity, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public void Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.UpdatedOnUtc = DateTime.UtcNow;

            Context.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public async Task UpdateAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.UpdatedOnUtc = DateTime.UtcNow;

            Context.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            if (entity is IHasSoftDelete delete)
            {
                delete.DeletedOnUtc = DateTime.UtcNow;
                Update(entity);
                return;
            }

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Entities.Remove(entity);
            _dbContext.SaveChanges();
        }

        public async Task DeleteAsync(T entity)
        {
            if (entity is IHasSoftDelete delete)
            {
                delete.DeletedOnUtc = DateTime.UtcNow;
                await UpdateAsync(entity);
                return;
            }

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Entities.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public IQueryable<T> Table => Entities;

        public void Dispose()
        {
            _dbContext?.Dispose();
        }

        public void InsertAll(ICollection<T> entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            foreach (var e in entity)
            {
                e.UpdatedOnUtc = DateTime.UtcNow;
                e.CreatedOnUtc = DateTime.UtcNow;

                Entities.Add(e);
            }

            _dbContext.SaveChanges();
        }

        public void UpdateAll(ICollection<T> entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            foreach (var e in entity)
            {
                e.UpdatedOnUtc = DateTime.UtcNow;
            }
            //Context.Entry(entity).State = EntityState.Modified;

            _dbContext.SaveChanges();
        }

        public void DeleteAll(ICollection<T> entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Entities.RemoveRange(entity);
            _dbContext.SaveChanges();
        }
    }
}

