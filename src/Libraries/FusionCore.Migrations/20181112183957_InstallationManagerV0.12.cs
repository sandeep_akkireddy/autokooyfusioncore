﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV012 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Timesheet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    WeekNumber = table.Column<int>(nullable: false),
                    KilometerStart = table.Column<int>(nullable: false),
                    KilometerEnd = table.Column<int>(nullable: false),
                    SendOnUtc = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timesheet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TimeSheetHomeWorkRow",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    TimesheetId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    HomeWorkTime = table.Column<TimeSpan>(nullable: false),
                    WorkHomeTime = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeSheetHomeWorkRow", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TimeSheetHomeWorkRow_Timesheet_TimesheetId",
                        column: x => x.TimesheetId,
                        principalTable: "Timesheet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TimeSheetTimeRow",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    TimesheetId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    PlanningId = table.Column<int>(nullable: true),
                    CalendarItemId = table.Column<int>(nullable: true),
                    WorkTime = table.Column<TimeSpan>(nullable: false),
                    TravelTime = table.Column<TimeSpan>(nullable: false),
                    SubmittedOnUtc = table.Column<DateTime>(nullable: true),
                    DeletedOnUtc = table.Column<DateTime>(nullable: true),
                    OfferNumber = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeSheetTimeRow", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TimeSheetTimeRow_CalendarItem_CalendarItemId",
                        column: x => x.CalendarItemId,
                        principalTable: "CalendarItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TimeSheetTimeRow_Planning_PlanningId",
                        column: x => x.PlanningId,
                        principalTable: "Planning",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TimeSheetTimeRow_Timesheet_TimesheetId",
                        column: x => x.TimesheetId,
                        principalTable: "Timesheet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheetHomeWorkRow_TimesheetId",
                table: "TimeSheetHomeWorkRow",
                column: "TimesheetId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheetTimeRow_CalendarItemId",
                table: "TimeSheetTimeRow",
                column: "CalendarItemId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheetTimeRow_PlanningId",
                table: "TimeSheetTimeRow",
                column: "PlanningId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheetTimeRow_TimesheetId",
                table: "TimeSheetTimeRow",
                column: "TimesheetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TimeSheetHomeWorkRow");

            migrationBuilder.DropTable(
                name: "TimeSheetTimeRow");

            migrationBuilder.DropTable(
                name: "Timesheet");
        }
    }
}
