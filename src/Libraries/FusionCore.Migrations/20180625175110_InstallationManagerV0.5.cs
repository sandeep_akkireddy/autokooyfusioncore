﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV05 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeeGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employee2EmployeeGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    EmployeeGroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee2EmployeeGroup", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employee2EmployeeGroup_EmployeeGroup_EmployeeGroupId",
                        column: x => x.EmployeeGroupId,
                        principalTable: "EmployeeGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee2EmployeeGroup_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employee2EmployeeGroup_EmployeeGroupId",
                table: "Employee2EmployeeGroup",
                column: "EmployeeGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee2EmployeeGroup_EmployeeId",
                table: "Employee2EmployeeGroup",
                column: "EmployeeId");

            migrationBuilder.Sql(@"
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('Service', GETUTCDATE(), GETUTCDATE())
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('Water', GETUTCDATE(), GETUTCDATE())
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('Air', GETUTCDATE(), GETUTCDATE())
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('Improvement', GETUTCDATE(), GETUTCDATE())
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('Vervoer', GETUTCDATE(), GETUTCDATE())
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('AQ Cumulus', GETUTCDATE(), GETUTCDATE())
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('MGT', GETUTCDATE(), GETUTCDATE())
				INSERT INTO EmployeeGroup (Name,CreatedOnUtc, UpdatedOnUtc)
					VALUES ('AQ International', GETUTCDATE(), GETUTCDATE())
			");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employee2EmployeeGroup");

            migrationBuilder.DropTable(
                name: "EmployeeGroup");
        }
    }
}
