﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientUserRight_Client_ClientId",
                table: "ClientUserRight");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientUserRight_ClientUser_ClientUserId1",
                table: "ClientUserRight");

            migrationBuilder.DropIndex(
                name: "IX_ClientUserRight_ClientId",
                table: "ClientUserRight");

            migrationBuilder.DropIndex(
                name: "IX_ClientUserRight_ClientUserId1",
                table: "ClientUserRight");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "ClientUserRight");

            migrationBuilder.DropColumn(
                name: "ClientUserId1",
                table: "ClientUserRight");

            migrationBuilder.AddColumn<bool>(
                name: "AllBuildings",
                table: "ClientUserRight",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AllLocations",
                table: "ClientUserRight",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_ClientUserRight_BuildingId",
                table: "ClientUserRight",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientUserRight_BuildingLocationId",
                table: "ClientUserRight",
                column: "BuildingLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientUserRight_Building_BuildingId",
                table: "ClientUserRight",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientUserRight_BuildingLocation_BuildingLocationId",
                table: "ClientUserRight",
                column: "BuildingLocationId",
                principalTable: "BuildingLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientUserRight_Building_BuildingId",
                table: "ClientUserRight");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientUserRight_BuildingLocation_BuildingLocationId",
                table: "ClientUserRight");

            migrationBuilder.DropIndex(
                name: "IX_ClientUserRight_BuildingId",
                table: "ClientUserRight");

            migrationBuilder.DropIndex(
                name: "IX_ClientUserRight_BuildingLocationId",
                table: "ClientUserRight");

            migrationBuilder.DropColumn(
                name: "AllBuildings",
                table: "ClientUserRight");

            migrationBuilder.DropColumn(
                name: "AllLocations",
                table: "ClientUserRight");

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "ClientUserRight",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClientUserId1",
                table: "ClientUserRight",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClientUserRight_ClientId",
                table: "ClientUserRight",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientUserRight_ClientUserId1",
                table: "ClientUserRight",
                column: "ClientUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientUserRight_Client_ClientId",
                table: "ClientUserRight",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientUserRight_ClientUser_ClientUserId1",
                table: "ClientUserRight",
                column: "ClientUserId1",
                principalTable: "ClientUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
