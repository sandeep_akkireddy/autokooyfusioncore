﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationMAnagerV14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClientUserRight",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    ClientUserId = table.Column<int>(nullable: false),
                    BuildingId = table.Column<int>(nullable: true),
                    BuildingLocationId = table.Column<int>(nullable: true),
                    ClientUserId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientUserRight", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientUserRight_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClientUserRight_ClientUser_ClientUserId",
                        column: x => x.ClientUserId,
                        principalTable: "ClientUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ClientUserRight_ClientUser_ClientUserId1",
                        column: x => x.ClientUserId1,
                        principalTable: "ClientUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientUserRight_ClientId",
                table: "ClientUserRight",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientUserRight_ClientUserId",
                table: "ClientUserRight",
                column: "ClientUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientUserRight_ClientUserId1",
                table: "ClientUserRight",
                column: "ClientUserId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientUserRight");
        }
    }
}
