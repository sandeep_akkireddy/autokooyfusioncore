﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV013 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TimeSheetHomeWorkRow_Timesheet_TimesheetId",
                table: "TimeSheetHomeWorkRow");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSheetTimeRow_CalendarItem_CalendarItemId",
                table: "TimeSheetTimeRow");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSheetTimeRow_Planning_PlanningId",
                table: "TimeSheetTimeRow");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSheetTimeRow_Timesheet_TimesheetId",
                table: "TimeSheetTimeRow");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TimeSheetTimeRow",
                table: "TimeSheetTimeRow");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TimeSheetHomeWorkRow",
                table: "TimeSheetHomeWorkRow");

            migrationBuilder.RenameTable(
                name: "TimeSheetTimeRow",
                newName: "TimesheetTimeRow");

            migrationBuilder.RenameTable(
                name: "TimeSheetHomeWorkRow",
                newName: "TimesheetHomeWorkRow");

            migrationBuilder.RenameIndex(
                name: "IX_TimeSheetTimeRow_TimesheetId",
                table: "TimesheetTimeRow",
                newName: "IX_TimesheetTimeRow_TimesheetId");

            migrationBuilder.RenameIndex(
                name: "IX_TimeSheetTimeRow_PlanningId",
                table: "TimesheetTimeRow",
                newName: "IX_TimesheetTimeRow_PlanningId");

            migrationBuilder.RenameIndex(
                name: "IX_TimeSheetTimeRow_CalendarItemId",
                table: "TimesheetTimeRow",
                newName: "IX_TimesheetTimeRow_CalendarItemId");

            migrationBuilder.RenameIndex(
                name: "IX_TimeSheetHomeWorkRow_TimesheetId",
                table: "TimesheetHomeWorkRow",
                newName: "IX_TimesheetHomeWorkRow_TimesheetId");

            migrationBuilder.AddColumn<int>(
                name: "SortOrder",
                table: "AssetDynamicField",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TimesheetTimeRow",
                table: "TimesheetTimeRow",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TimesheetHomeWorkRow",
                table: "TimesheetHomeWorkRow",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TimesheetHomeWorkRow_Timesheet_TimesheetId",
                table: "TimesheetHomeWorkRow",
                column: "TimesheetId",
                principalTable: "Timesheet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimesheetTimeRow_CalendarItem_CalendarItemId",
                table: "TimesheetTimeRow",
                column: "CalendarItemId",
                principalTable: "CalendarItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimesheetTimeRow_Planning_PlanningId",
                table: "TimesheetTimeRow",
                column: "PlanningId",
                principalTable: "Planning",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimesheetTimeRow_Timesheet_TimesheetId",
                table: "TimesheetTimeRow",
                column: "TimesheetId",
                principalTable: "Timesheet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TimesheetHomeWorkRow_Timesheet_TimesheetId",
                table: "TimesheetHomeWorkRow");

            migrationBuilder.DropForeignKey(
                name: "FK_TimesheetTimeRow_CalendarItem_CalendarItemId",
                table: "TimesheetTimeRow");

            migrationBuilder.DropForeignKey(
                name: "FK_TimesheetTimeRow_Planning_PlanningId",
                table: "TimesheetTimeRow");

            migrationBuilder.DropForeignKey(
                name: "FK_TimesheetTimeRow_Timesheet_TimesheetId",
                table: "TimesheetTimeRow");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TimesheetTimeRow",
                table: "TimesheetTimeRow");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TimesheetHomeWorkRow",
                table: "TimesheetHomeWorkRow");

            migrationBuilder.DropColumn(
                name: "SortOrder",
                table: "AssetDynamicField");

            migrationBuilder.RenameTable(
                name: "TimesheetTimeRow",
                newName: "TimeSheetTimeRow");

            migrationBuilder.RenameTable(
                name: "TimesheetHomeWorkRow",
                newName: "TimeSheetHomeWorkRow");

            migrationBuilder.RenameIndex(
                name: "IX_TimesheetTimeRow_TimesheetId",
                table: "TimeSheetTimeRow",
                newName: "IX_TimeSheetTimeRow_TimesheetId");

            migrationBuilder.RenameIndex(
                name: "IX_TimesheetTimeRow_PlanningId",
                table: "TimeSheetTimeRow",
                newName: "IX_TimeSheetTimeRow_PlanningId");

            migrationBuilder.RenameIndex(
                name: "IX_TimesheetTimeRow_CalendarItemId",
                table: "TimeSheetTimeRow",
                newName: "IX_TimeSheetTimeRow_CalendarItemId");

            migrationBuilder.RenameIndex(
                name: "IX_TimesheetHomeWorkRow_TimesheetId",
                table: "TimeSheetHomeWorkRow",
                newName: "IX_TimeSheetHomeWorkRow_TimesheetId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TimeSheetTimeRow",
                table: "TimeSheetTimeRow",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TimeSheetHomeWorkRow",
                table: "TimeSheetHomeWorkRow",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSheetHomeWorkRow_Timesheet_TimesheetId",
                table: "TimeSheetHomeWorkRow",
                column: "TimesheetId",
                principalTable: "Timesheet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSheetTimeRow_CalendarItem_CalendarItemId",
                table: "TimeSheetTimeRow",
                column: "CalendarItemId",
                principalTable: "CalendarItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSheetTimeRow_Planning_PlanningId",
                table: "TimeSheetTimeRow",
                column: "PlanningId",
                principalTable: "Planning",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSheetTimeRow_Timesheet_TimesheetId",
                table: "TimeSheetTimeRow",
                column: "TimesheetId",
                principalTable: "Timesheet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
