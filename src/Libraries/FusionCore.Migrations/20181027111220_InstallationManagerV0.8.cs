﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV08 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PlanningId",
                table: "AssetFile",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssetFile_PlanningId",
                table: "AssetFile",
                column: "PlanningId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetFile_Planning_PlanningId",
                table: "AssetFile",
                column: "PlanningId",
                principalTable: "Planning",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetFile_Planning_PlanningId",
                table: "AssetFile");

            migrationBuilder.DropIndex(
                name: "IX_AssetFile_PlanningId",
                table: "AssetFile");

            migrationBuilder.DropColumn(
                name: "PlanningId",
                table: "AssetFile");
        }
    }
}
