﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV07 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Planning_AssetActivity_AssetActivityId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Asset_AssetId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Building_BuildingId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_BuildingLocation_BuildingLocationId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Client_ClientId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Division_DivisionId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Employee_MainEmployeeId",
                table: "Planning");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "AssetActivityHistory",
                newName: "Name");

            migrationBuilder.AddColumn<DateTime>(
                name: "CheckListEnd",
                table: "Planning",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CheckListPauze",
                table: "Planning",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CheckListStart",
                table: "Planning",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExtraEmail",
                table: "Planning",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notification",
                table: "Planning",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "NotificationReadOn",
                table: "Planning",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Kind",
                table: "AssetActivityHistory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOnUtc",
                table: "Asset",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivityHistory_PlanningId",
                table: "AssetActivityHistory",
                column: "PlanningId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetActivityHistory_Planning_PlanningId",
                table: "AssetActivityHistory",
                column: "PlanningId",
                principalTable: "Planning",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_AssetActivity_AssetActivityId",
                table: "Planning",
                column: "AssetActivityId",
                principalTable: "AssetActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Asset_AssetId",
                table: "Planning",
                column: "AssetId",
                principalTable: "Asset",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Building_BuildingId",
                table: "Planning",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_BuildingLocation_BuildingLocationId",
                table: "Planning",
                column: "BuildingLocationId",
                principalTable: "BuildingLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Client_ClientId",
                table: "Planning",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Division_DivisionId",
                table: "Planning",
                column: "DivisionId",
                principalTable: "Division",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Employee_MainEmployeeId",
                table: "Planning",
                column: "MainEmployeeId",
                principalTable: "Employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetActivityHistory_Planning_PlanningId",
                table: "AssetActivityHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_AssetActivity_AssetActivityId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Asset_AssetId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Building_BuildingId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_BuildingLocation_BuildingLocationId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Client_ClientId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Division_DivisionId",
                table: "Planning");

            migrationBuilder.DropForeignKey(
                name: "FK_Planning_Employee_MainEmployeeId",
                table: "Planning");

            migrationBuilder.DropIndex(
                name: "IX_AssetActivityHistory_PlanningId",
                table: "AssetActivityHistory");

            migrationBuilder.DropColumn(
                name: "CheckListEnd",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "CheckListPauze",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "CheckListStart",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "ExtraEmail",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "Notification",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "NotificationReadOn",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "Kind",
                table: "AssetActivityHistory");

            migrationBuilder.DropColumn(
                name: "DeletedOnUtc",
                table: "Asset");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "AssetActivityHistory",
                newName: "Title");

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_AssetActivity_AssetActivityId",
                table: "Planning",
                column: "AssetActivityId",
                principalTable: "AssetActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Asset_AssetId",
                table: "Planning",
                column: "AssetId",
                principalTable: "Asset",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Building_BuildingId",
                table: "Planning",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_BuildingLocation_BuildingLocationId",
                table: "Planning",
                column: "BuildingLocationId",
                principalTable: "BuildingLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Client_ClientId",
                table: "Planning",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Division_DivisionId",
                table: "Planning",
                column: "DivisionId",
                principalTable: "Division",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_Employee_MainEmployeeId",
                table: "Planning",
                column: "MainEmployeeId",
                principalTable: "Employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
