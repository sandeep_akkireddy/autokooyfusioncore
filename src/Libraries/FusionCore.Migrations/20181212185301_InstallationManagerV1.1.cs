﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "AssetFileBinary",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetFileId = table.Column<int>(nullable: false),
                    File = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetFileBinary", x => x.Id);
                });

            //      migrationBuilder.Sql(@"
            //	INSERT INTO AssetFileBinary (CreatedOnUtc, UpdatedOnUtc, AssetFileId, [File])
            //	SELECT CreatedOnUtc, UpdatedOnUtc, Id, [File] FROM AssetFile
            //");

            //      migrationBuilder.DropColumn(
            //       name: "File",
            //       table: "AssetFile");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssetFileBinary");

            migrationBuilder.AddColumn<byte[]>(
                name: "File",
                table: "AssetFile",
                nullable: true);
        }
    }
}
