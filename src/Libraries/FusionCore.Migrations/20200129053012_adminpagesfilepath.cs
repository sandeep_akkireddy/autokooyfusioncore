﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class adminpagesfilepath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "PageFiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "PageFiles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "PageFiles");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "PageFiles");
        }
    }
}
