﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class subscriptionfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Assets",
                table: "LegalSubscriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherAssets",
                table: "LegalSubscriptions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WWFT",
                table: "LegalSubscriptions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Assets",
                table: "GeneralSubscriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherAssets",
                table: "GeneralSubscriptions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WWFT",
                table: "GeneralSubscriptions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Assets",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "OtherAssets",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "WWFT",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "Assets",
                table: "GeneralSubscriptions");

            migrationBuilder.DropColumn(
                name: "OtherAssets",
                table: "GeneralSubscriptions");

            migrationBuilder.DropColumn(
                name: "WWFT",
                table: "GeneralSubscriptions");
        }
    }
}
