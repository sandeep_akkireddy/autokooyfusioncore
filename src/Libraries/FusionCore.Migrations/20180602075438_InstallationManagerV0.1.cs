﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Firstname = table.Column<string>(maxLength: 256, nullable: true),
                    Middlename = table.Column<string>(maxLength: 50, nullable: true),
                    Lastname = table.Column<string>(maxLength: 256, nullable: true),
                    Address = table.Column<string>(maxLength: 256, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 50, nullable: true),
                    City = table.Column<string>(maxLength: 256, nullable: true),
                    Country = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Phonenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Mobilenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Website = table.Column<string>(maxLength: 256, nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    CompanyLogoPictureId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Division",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Firstname = table.Column<string>(maxLength: 256, nullable: true),
                    Middlename = table.Column<string>(maxLength: 50, nullable: true),
                    Lastname = table.Column<string>(maxLength: 256, nullable: true),
                    Address = table.Column<string>(maxLength: 256, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 50, nullable: true),
                    City = table.Column<string>(maxLength: 256, nullable: true),
                    Country = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Phonenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Mobilenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Website = table.Column<string>(maxLength: 256, nullable: true),
                    LogoHeaderPictureId = table.Column<int>(nullable: true),
                    LogoFooterPictureId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Division", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Firstname = table.Column<string>(maxLength: 256, nullable: false),
                    Middlename = table.Column<string>(maxLength: 50, nullable: true),
                    Lastname = table.Column<string>(maxLength: 256, nullable: false),
                    Address = table.Column<string>(maxLength: 256, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 50, nullable: true),
                    City = table.Column<string>(maxLength: 256, nullable: true),
                    Country = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Phonenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Mobilenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Key = table.Column<byte[]>(nullable: true),
                    Salt = table.Column<byte[]>(nullable: true),
                    EmployeeFunctionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivityField",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    ActivityId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    DynamicFieldTypeId = table.Column<int>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityField_Activity_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Building",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    Firstname = table.Column<string>(maxLength: 256, nullable: true),
                    Middlename = table.Column<string>(maxLength: 50, nullable: true),
                    Lastname = table.Column<string>(maxLength: 256, nullable: true),
                    Address = table.Column<string>(maxLength: 256, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 50, nullable: true),
                    City = table.Column<string>(maxLength: 256, nullable: true),
                    Country = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Phonenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Mobilenumber = table.Column<string>(maxLength: 50, nullable: true),
                    Website = table.Column<string>(maxLength: 256, nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longtitude = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(maxLength: 2147483647, nullable: true),
                    LogoPictureId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Building", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Building_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClientUser",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    Firstname = table.Column<string>(maxLength: 256, nullable: true),
                    Middlename = table.Column<string>(maxLength: 50, nullable: true),
                    Lastname = table.Column<string>(maxLength: 256, nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Username = table.Column<string>(maxLength: 256, nullable: false),
                    Key = table.Column<byte[]>(nullable: true),
                    Salt = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientUser_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClientDivision",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    DivisionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientDivision", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientDivision_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClientDivision_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingLocation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    BuildingId = table.Column<int>(nullable: false),
                    Floor = table.Column<string>(maxLength: 256, nullable: true),
                    RoomNumber = table.Column<string>(maxLength: 256, nullable: true),
                    Remarks = table.Column<string>(maxLength: 2147483647, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuildingLocation_Building_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Asset",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Type = table.Column<string>(maxLength: 256, nullable: false),
                    BuildingId = table.Column<int>(nullable: true),
                    BuildingLocationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Asset", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Asset_Building_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Asset_BuildingLocation_BuildingLocationId",
                        column: x => x.BuildingLocationId,
                        principalTable: "BuildingLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetActivity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    ActivityId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    ActivityId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetActivity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetActivity_Activity_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetActivity_Activity_ActivityId1",
                        column: x => x.ActivityId1,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AssetActivity_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssetDynamicField",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Value = table.Column<string>(maxLength: 256, nullable: false),
                    AssetId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetDynamicField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetDynamicField_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetDynamicField_Asset_AssetId1",
                        column: x => x.AssetId1,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetField",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    ActivityId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    LatestValue = table.Column<string>(maxLength: 200, nullable: true),
                    DynamicFieldTypeId = table.Column<int>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    ActivitySortOrder = table.Column<int>(nullable: false),
                    AssetId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetField_Activity_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetField_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetField_Asset_AssetId1",
                        column: x => x.AssetId1,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetFile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    FileTypeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 100, nullable: true),
                    File = table.Column<byte[]>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    FileStatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetFile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetFile_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssetActivityHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    PlanningId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Signature = table.Column<byte[]>(nullable: true),
                    ActivityId = table.Column<int>(nullable: true),
                    AssetFileId = table.Column<int>(nullable: true),
                    Title = table.Column<string>(maxLength: 200, nullable: true),
                    Value = table.Column<string>(nullable: true),
                    DynamicFieldTypeId = table.Column<int>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    ActivitySortOrder = table.Column<int>(nullable: false),
                    NormHours = table.Column<int>(nullable: false),
                    RealHours = table.Column<int>(nullable: false),
                    RealMinutes = table.Column<int>(nullable: false),
                    TravelHours = table.Column<int>(nullable: false),
                    TravelMinutes = table.Column<int>(nullable: false),
                    Actions = table.Column<string>(nullable: true),
                    OkayGivenBy = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetActivityHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetActivityHistory_Activity_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AssetActivityHistory_AssetFile_AssetFileId",
                        column: x => x.AssetFileId,
                        principalTable: "AssetFile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AssetActivityHistory_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityField_ActivityId",
                table: "ActivityField",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Asset_BuildingId",
                table: "Asset",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Asset_BuildingLocationId",
                table: "Asset",
                column: "BuildingLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivity_ActivityId",
                table: "AssetActivity",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivity_ActivityId1",
                table: "AssetActivity",
                column: "ActivityId1");

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivity_AssetId",
                table: "AssetActivity",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivityHistory_ActivityId",
                table: "AssetActivityHistory",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivityHistory_AssetFileId",
                table: "AssetActivityHistory",
                column: "AssetFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivityHistory_AssetId",
                table: "AssetActivityHistory",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetDynamicField_AssetId",
                table: "AssetDynamicField",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetDynamicField_AssetId1",
                table: "AssetDynamicField",
                column: "AssetId1");

            migrationBuilder.CreateIndex(
                name: "IX_AssetField_ActivityId",
                table: "AssetField",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetField_AssetId",
                table: "AssetField",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetField_AssetId1",
                table: "AssetField",
                column: "AssetId1");

            migrationBuilder.CreateIndex(
                name: "IX_AssetFile_AssetId",
                table: "AssetFile",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_Building_ClientId",
                table: "Building",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingLocation_BuildingId",
                table: "BuildingLocation",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientDivision_ClientId",
                table: "ClientDivision",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientDivision_DivisionId",
                table: "ClientDivision",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientUser_ClientId",
                table: "ClientUser",
                column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityField");

            migrationBuilder.DropTable(
                name: "AssetActivity");

            migrationBuilder.DropTable(
                name: "AssetActivityHistory");

            migrationBuilder.DropTable(
                name: "AssetDynamicField");

            migrationBuilder.DropTable(
                name: "AssetField");

            migrationBuilder.DropTable(
                name: "ClientDivision");

            migrationBuilder.DropTable(
                name: "ClientUser");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "AssetFile");

            migrationBuilder.DropTable(
                name: "Activity");

            migrationBuilder.DropTable(
                name: "Division");

            migrationBuilder.DropTable(
                name: "Asset");

            migrationBuilder.DropTable(
                name: "BuildingLocation");

            migrationBuilder.DropTable(
                name: "Building");

            migrationBuilder.DropTable(
                name: "Client");
        }
    }
}
