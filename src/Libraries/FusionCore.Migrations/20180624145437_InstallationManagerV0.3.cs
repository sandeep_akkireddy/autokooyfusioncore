﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AssetTypeId",
                table: "Activity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ActivityDynamicField",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    ActivityId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Value = table.Column<string>(maxLength: 256, nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    ActivityId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityDynamicField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityDynamicField_Activity_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDynamicField_Activity_ActivityId1",
                        column: x => x.ActivityId1,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    SortOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activity_AssetTypeId",
                table: "Activity",
                column: "AssetTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDynamicField_ActivityId",
                table: "ActivityDynamicField",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDynamicField_ActivityId1",
                table: "ActivityDynamicField",
                column: "ActivityId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Activity_AssetType_AssetTypeId",
                table: "Activity",
                column: "AssetTypeId",
                principalTable: "AssetType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activity_AssetType_AssetTypeId",
                table: "Activity");

            migrationBuilder.DropTable(
                name: "ActivityDynamicField");

            migrationBuilder.DropTable(
                name: "AssetType");

            migrationBuilder.DropIndex(
                name: "IX_Activity_AssetTypeId",
                table: "Activity");

            migrationBuilder.DropColumn(
                name: "AssetTypeId",
                table: "Activity");
        }
    }
}
