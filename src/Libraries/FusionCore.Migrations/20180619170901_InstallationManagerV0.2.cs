﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetActivity_Activity_ActivityId",
                table: "AssetActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetActivity_Activity_ActivityId1",
                table: "AssetActivity");

            migrationBuilder.DropIndex(
                name: "IX_AssetActivity_ActivityId",
                table: "AssetActivity");

            migrationBuilder.DropIndex(
                name: "IX_AssetActivity_ActivityId1",
                table: "AssetActivity");

            migrationBuilder.DropColumn(
                name: "ActivityId1",
                table: "AssetActivity");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Activity");

            migrationBuilder.DropColumn(
                name: "AssetId",
                table: "Activity");

            migrationBuilder.RenameColumn(
                name: "ActivityId",
                table: "AssetActivity",
                newName: "SortOrder");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "User",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.AlterColumn<string>(
                name: "Lastname",
                table: "Division",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.AlterColumn<string>(
                name: "Firstname",
                table: "Division",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.AlterColumn<string>(
                name: "RoomNumber",
                table: "BuildingLocation",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "BuildingLocation",
                maxLength: 2147483647,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 2147483647);

            migrationBuilder.AlterColumn<string>(
                name: "Floor",
                table: "BuildingLocation",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "Building",
                maxLength: 2147483647,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 2147483647);

            migrationBuilder.AlterColumn<string>(
                name: "Lastname",
                table: "Building",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.AlterColumn<string>(
                name: "Firstname",
                table: "Building",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AssetActivity",
                maxLength: 256,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "AssetActivityField",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetActivityId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    LatestValue = table.Column<string>(maxLength: 256, nullable: true),
                    DynamicFieldTypeId = table.Column<int>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetActivityField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetActivityField_AssetActivity_AssetActivityId",
                        column: x => x.AssetActivityId,
                        principalTable: "AssetActivity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivityField_AssetActivityId",
                table: "AssetActivityField",
                column: "AssetActivityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssetActivityField");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AssetActivity");

            migrationBuilder.RenameColumn(
                name: "SortOrder",
                table: "AssetActivity",
                newName: "ActivityId");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "User",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Lastname",
                table: "Division",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Firstname",
                table: "Division",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RoomNumber",
                table: "BuildingLocation",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "BuildingLocation",
                maxLength: 2147483647,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 2147483647,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Floor",
                table: "BuildingLocation",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "Building",
                maxLength: 2147483647,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 2147483647,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Lastname",
                table: "Building",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Firstname",
                table: "Building",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ActivityId1",
                table: "AssetActivity",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Activity",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "AssetId",
                table: "Activity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivity_ActivityId",
                table: "AssetActivity",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetActivity_ActivityId1",
                table: "AssetActivity",
                column: "ActivityId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetActivity_Activity_ActivityId",
                table: "AssetActivity",
                column: "ActivityId",
                principalTable: "Activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetActivity_Activity_ActivityId1",
                table: "AssetActivity",
                column: "ActivityId1",
                principalTable: "Activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
