﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV010 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetActivityHistory_Activity_ActivityId",
                table: "AssetActivityHistory");

            migrationBuilder.RenameColumn(
                name: "ActivityId",
                table: "AssetActivityHistory",
                newName: "AssetActivityId");

            migrationBuilder.RenameIndex(
                name: "IX_AssetActivityHistory_ActivityId",
                table: "AssetActivityHistory",
                newName: "IX_AssetActivityHistory_AssetActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetActivityHistory_AssetActivity_AssetActivityId",
                table: "AssetActivityHistory",
                column: "AssetActivityId",
                principalTable: "AssetActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetActivityHistory_AssetActivity_AssetActivityId",
                table: "AssetActivityHistory");

            migrationBuilder.RenameColumn(
                name: "AssetActivityId",
                table: "AssetActivityHistory",
                newName: "ActivityId");

            migrationBuilder.RenameIndex(
                name: "IX_AssetActivityHistory_AssetActivityId",
                table: "AssetActivityHistory",
                newName: "IX_AssetActivityHistory_ActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetActivityHistory_Activity_ActivityId",
                table: "AssetActivityHistory",
                column: "ActivityId",
                principalTable: "Activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
