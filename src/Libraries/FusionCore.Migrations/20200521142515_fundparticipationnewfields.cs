﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class fundparticipationnewfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "chapter",
                table: "Funds",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "designation",
                table: "Funds",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sectionnumber",
                table: "Funds",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "chapter",
                table: "Funds");

            migrationBuilder.DropColumn(
                name: "designation",
                table: "Funds");

            migrationBuilder.DropColumn(
                name: "sectionnumber",
                table: "Funds");
        }
    }
}
