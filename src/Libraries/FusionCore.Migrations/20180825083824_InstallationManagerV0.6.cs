﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV06 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Asset");

            migrationBuilder.AddColumn<string>(
                name: "ResetKey",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailTemplateCustomerBody",
                table: "Division",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailTemplateCustomerEmail",
                table: "Division",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailTemplatePlannerBody",
                table: "Division",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailTemplatePlannerEmail",
                table: "Division",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AssetTypeId",
                table: "Asset",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CalendarItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Subject = table.Column<string>(maxLength: 256, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    WholeDay = table.Column<bool>(nullable: false),
                    CalendarStatusId = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CalendarItem_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Planning",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    AssetId = table.Column<int>(nullable: true),
                    AssetActivityId = table.Column<int>(nullable: true),
                    DivisionId = table.Column<int>(nullable: true),
                    ClientId = table.Column<int>(nullable: true),
                    BuildingId = table.Column<int>(nullable: true),
                    BuildingLocationId = table.Column<int>(nullable: true),
                    MainEmployeeId = table.Column<int>(nullable: true),
                    StatusId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Repeatable = table.Column<bool>(nullable: false),
                    RepeatableType = table.Column<int>(nullable: true),
                    RepeatableCount = table.Column<int>(nullable: true),
                    RepeatablePlanningParentId = table.Column<int>(nullable: true),
                    ErrorMessage = table.Column<string>(maxLength: 256, nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    Materials = table.Column<string>(nullable: true),
                    MaterialsRemark = table.Column<string>(nullable: true),
                    Signature = table.Column<byte[]>(nullable: true),
                    OfferNumber = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Planning", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Planning_AssetActivity_AssetActivityId",
                        column: x => x.AssetActivityId,
                        principalTable: "AssetActivity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Planning_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Planning_Building_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Planning_BuildingLocation_BuildingLocationId",
                        column: x => x.BuildingLocationId,
                        principalTable: "BuildingLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Planning_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Planning_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Planning_Employee_MainEmployeeId",
                        column: x => x.MainEmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "PlanningEmployee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    PlanningId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningEmployee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlanningEmployee_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningEmployee_Planning_PlanningId",
                        column: x => x.PlanningId,
                        principalTable: "Planning",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Asset_AssetTypeId",
                table: "Asset",
                column: "AssetTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CalendarItem_EmployeeId",
                table: "CalendarItem",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Planning_AssetActivityId",
                table: "Planning",
                column: "AssetActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Planning_AssetId",
                table: "Planning",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_Planning_BuildingId",
                table: "Planning",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Planning_BuildingLocationId",
                table: "Planning",
                column: "BuildingLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Planning_ClientId",
                table: "Planning",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Planning_DivisionId",
                table: "Planning",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Planning_MainEmployeeId",
                table: "Planning",
                column: "MainEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningEmployee_EmployeeId",
                table: "PlanningEmployee",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningEmployee_PlanningId",
                table: "PlanningEmployee",
                column: "PlanningId");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Asset_AssetType_AssetTypeId",
                table: "Asset");

            migrationBuilder.DropTable(
                name: "CalendarItem");

            migrationBuilder.DropTable(
                name: "PlanningEmployee");

            migrationBuilder.DropTable(
                name: "Planning");

            migrationBuilder.DropIndex(
                name: "IX_Asset_AssetTypeId",
                table: "Asset");

            migrationBuilder.DropColumn(
                name: "ResetKey",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "EmailTemplateCustomerBody",
                table: "Division");

            migrationBuilder.DropColumn(
                name: "EmailTemplateCustomerEmail",
                table: "Division");

            migrationBuilder.DropColumn(
                name: "EmailTemplatePlannerBody",
                table: "Division");

            migrationBuilder.DropColumn(
                name: "EmailTemplatePlannerEmail",
                table: "Division");

            migrationBuilder.DropColumn(
                name: "AssetTypeId",
                table: "Asset");

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Asset",
                maxLength: 256,
                nullable: false,
                defaultValue: "");
        }
    }
}
