﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class supportmodule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Support",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(maxLength: 256, nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    SupportDate = table.Column<DateTime>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    SupportLink = table.Column<string>(nullable: true),
                    SupportLogoId = table.Column<int>(nullable: true),
                    Likes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Support", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SupportIpAddressLikes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    IPAddress = table.Column<string>(nullable: true),
                    SupportId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportIpAddressLikes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupportIpAddressLikes_Support_SupportId",
                        column: x => x.SupportId,
                        principalTable: "Support",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SupportIpAddressLikes_SupportId",
                table: "SupportIpAddressLikes",
                column: "SupportId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SupportIpAddressLikes");

            migrationBuilder.DropTable(
                name: "Support");
        }
    }
}
