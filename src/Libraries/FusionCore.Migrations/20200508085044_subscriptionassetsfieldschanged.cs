﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class subscriptionassetsfieldschanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Assets",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "Assets",
                table: "GeneralSubscriptions");

            migrationBuilder.AddColumn<bool>(
                name: "BusinessActivities",
                table: "LegalSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Heritage",
                table: "LegalSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Others",
                table: "LegalSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SalaryBonusWork",
                table: "LegalSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SalesCompany",
                table: "LegalSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "BusinessActivities",
                table: "GeneralSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Heritage",
                table: "GeneralSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Others",
                table: "GeneralSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SalaryBonusWork",
                table: "GeneralSubscriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SalesCompany",
                table: "GeneralSubscriptions",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BusinessActivities",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "Heritage",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "Others",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "SalaryBonusWork",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "SalesCompany",
                table: "LegalSubscriptions");

            migrationBuilder.DropColumn(
                name: "BusinessActivities",
                table: "GeneralSubscriptions");

            migrationBuilder.DropColumn(
                name: "Heritage",
                table: "GeneralSubscriptions");

            migrationBuilder.DropColumn(
                name: "Others",
                table: "GeneralSubscriptions");

            migrationBuilder.DropColumn(
                name: "SalaryBonusWork",
                table: "GeneralSubscriptions");

            migrationBuilder.DropColumn(
                name: "SalesCompany",
                table: "GeneralSubscriptions");

            migrationBuilder.AddColumn<int>(
                name: "Assets",
                table: "LegalSubscriptions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Assets",
                table: "GeneralSubscriptions",
                nullable: true);
        }
    }
}
