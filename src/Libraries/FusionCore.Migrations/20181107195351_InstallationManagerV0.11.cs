﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV011 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Planning",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "AssetActivityHistory",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Planning_UserId",
                table: "Planning",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Planning_User_UserId",
                table: "Planning",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Planning_User_UserId",
                table: "Planning");

            migrationBuilder.DropIndex(
                name: "IX_Planning_UserId",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Planning");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "AssetActivityHistory");
        }
    }
}
