﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class pageidtobanner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PageID",
                table: "Banner",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Banner_PageID",
                table: "Banner",
                column: "PageID");

            migrationBuilder.AddForeignKey(
                name: "FK_Banner_Page_PageID",
                table: "Banner",
                column: "PageID",
                principalTable: "Page",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Banner_Page_PageID",
                table: "Banner");

            migrationBuilder.DropIndex(
                name: "IX_Banner_PageID",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "PageID",
                table: "Banner");
        }
    }
}
