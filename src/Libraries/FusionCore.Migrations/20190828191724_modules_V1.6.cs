using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FusionCore.Migrations
{
    public partial class modules_V16 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Content",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    ContentId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Content", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Funds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(maxLength: 256, nullable: false),
                    fundStatus = table.Column<int>(nullable: false),
                    FundStartDate = table.Column<DateTime>(nullable: true),
                    FundEndDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Rendementen = table.Column<string>(nullable: true),
                    GeneralDescription = table.Column<string>(nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 100, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 150, nullable: true),
                    MetaKeywords = table.Column<string>(maxLength: 100, nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Zipcode = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Phonenumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    SplashActive = table.Column<bool>(nullable: false),
                    SplashImageId = table.Column<int>(nullable: true),
                    SplashImageTitle = table.Column<string>(nullable: true),
                    SplashImageCaption = table.Column<string>(nullable: true),
                    ThumbnailImageId = table.Column<int>(nullable: true),
                    ThumbnailImageTitle = table.Column<string>(nullable: true),
                    ThumbnailImageCaption = table.Column<string>(nullable: true),
                    External_Id = table.Column<int>(nullable: true),
                    activeregistration = table.Column<bool>(nullable: false),
                    participationprice = table.Column<string>(nullable: true),
                    participation_price = table.Column<decimal>(nullable: false),
                    Iban = table.Column<string>(nullable: true),
                    Dateofcapital = table.Column<DateTime>(nullable: true),
                    emissioncost = table.Column<string>(nullable: true),
                    emission_cost = table.Column<decimal>(nullable: false),
                    MaximumSubscriptions = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Funds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(maxLength: 256, nullable: false),
                    NewsDate = table.Column<DateTime>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    YouTubeLink = table.Column<string>(nullable: true),
                    MetaTitle = table.Column<string>(nullable: true),
                    MetaDescription = table.Column<string>(nullable: true),
                    MetaKeywords = table.Column<string>(nullable: true),
                    NewsLogoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Function = table.Column<string>(maxLength: 256, nullable: true),
                    PageContent = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    TeamLogoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Page",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(maxLength: 256, nullable: false),
                    Header = table.Column<string>(maxLength: 256, nullable: true),
                    Url = table.Column<string>(nullable: true),
                    LocationId = table.Column<int>(nullable: false),
                    Location = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    MetaTitle = table.Column<string>(nullable: true),
                    MetaDescription = table.Column<string>(nullable: true),
                    MetaKeywords = table.Column<string>(nullable: true),
                    TemplateId = table.Column<int>(nullable: false),
                    Template = table.Column<int>(nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    ContentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Page", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Page_Content_ContentId",
                        column: x => x.ContentId,
                        principalTable: "Content",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FundCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    FundId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FundCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FundCategory_Funds_FundId",
                        column: x => x.FundId,
                        principalTable: "Funds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FundCategory_FundCategory_ParentId",
                        column: x => x.ParentId,
                        principalTable: "FundCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GeneralSubscriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    Title = table.Column<string>(maxLength: 256, nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    Sex = table.Column<string>(nullable: true),
                    BirthPlace = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    HomeAddress = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    Residence = table.Column<string>(nullable: true),
                    CorrespondenceAddressType = table.Column<bool>(nullable: false),
                    CorrespondenceAddress = table.Column<string>(nullable: true),
                    CorrespondencePostalCode = table.Column<string>(nullable: true),
                    CorrespondenceResidence = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    MobilePhone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Iban = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    IssuedAt = table.Column<string>(nullable: true),
                    IssuedOn = table.Column<DateTime>(nullable: false),
                    ValidityDate = table.Column<DateTime>(nullable: false),
                    MartialStatus = table.Column<string>(nullable: true),
                    MarriedWithContract = table.Column<string>(nullable: true),
                    MarriedWithoutContract = table.Column<string>(nullable: true),
                    UnMarriedWithContract = table.Column<string>(nullable: true),
                    UnMarriedWithoutContract = table.Column<string>(nullable: true),
                    SpouseLastName = table.Column<string>(nullable: true),
                    SpouseFirstName = table.Column<string>(nullable: true),
                    SpouseSex = table.Column<string>(nullable: true),
                    SpouseBirthPlace = table.Column<string>(nullable: true),
                    SpouseDateOfBirth = table.Column<DateTime>(nullable: false),
                    NumberofParticipations = table.Column<string>(nullable: true),
                    AgreementStatus = table.Column<bool>(nullable: false),
                    Identificationtype = table.Column<int>(nullable: true),
                    IdentificationPaymenttype = table.Column<int>(nullable: true),
                    IDinstatus = table.Column<int>(nullable: true),
                    Idealpaymentstatus = table.Column<int>(nullable: true),
                    FundsId = table.Column<int>(nullable: false),
                    IsIdentified = table.Column<bool>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralSubscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeneralSubscriptions_Funds_FundsId",
                        column: x => x.FundsId,
                        principalTable: "Funds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LegalSubscriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    RegisteredName = table.Column<string>(nullable: true),
                    RegisteredAddress = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    RegistrationNumber = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    CorrespondenceAddress = table.Column<string>(nullable: true),
                    CorrespondencePostalCode = table.Column<string>(nullable: true),
                    Place = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    MobilePhone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Iban = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Title = table.Column<string>(maxLength: 256, nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    Sex = table.Column<string>(nullable: true),
                    BirthPlace = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    IdentityFileName = table.Column<string>(nullable: true),
                    IssuedAt = table.Column<string>(nullable: true),
                    IssuedOn = table.Column<DateTime>(nullable: false),
                    ValidityDate = table.Column<DateTime>(nullable: false),
                    NumberofParticipations = table.Column<string>(nullable: true),
                    AgreementStatus = table.Column<bool>(nullable: false),
                    Identificationtype = table.Column<int>(nullable: true),
                    IdentificationPaymenttype = table.Column<int>(nullable: true),
                    IDinstatus = table.Column<int>(nullable: true),
                    Idealpaymentstatus = table.Column<int>(nullable: true),
                    FundsId = table.Column<int>(nullable: false),
                    IsIdentified = table.Column<bool>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalSubscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LegalSubscriptions_Funds_FundsId",
                        column: x => x.FundsId,
                        principalTable: "Funds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    File = table.Column<byte[]>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    PageId = table.Column<int>(nullable: false),
                    FileName = table.Column<string>(nullable: true),
                    PageId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PageFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PageFiles_Page_PageId",
                        column: x => x.PageId,
                        principalTable: "Page",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PageFiles_Page_PageId1",
                        column: x => x.PageId1,
                        principalTable: "Page",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FundFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false),
                    File = table.Column<byte[]>(nullable: true),
                    FundId = table.Column<int>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Kind = table.Column<int>(nullable: true),
                    FundCategoryId = table.Column<int>(nullable: true),
                    websitedownloadfiles = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FundFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FundFiles_FundCategory_FundCategoryId",
                        column: x => x.FundCategoryId,
                        principalTable: "FundCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FundFiles_Funds_FundId",
                        column: x => x.FundId,
                        principalTable: "Funds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FundCategory_FundId",
                table: "FundCategory",
                column: "FundId");

            migrationBuilder.CreateIndex(
                name: "IX_FundCategory_ParentId",
                table: "FundCategory",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_FundFiles_FundCategoryId",
                table: "FundFiles",
                column: "FundCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_FundFiles_FundId",
                table: "FundFiles",
                column: "FundId");

            migrationBuilder.CreateIndex(
                name: "IX_GeneralSubscriptions_FundsId",
                table: "GeneralSubscriptions",
                column: "FundsId");

            migrationBuilder.CreateIndex(
                name: "IX_LegalSubscriptions_FundsId",
                table: "LegalSubscriptions",
                column: "FundsId");

            migrationBuilder.CreateIndex(
                name: "IX_Page_ContentId",
                table: "Page",
                column: "ContentId");

            migrationBuilder.CreateIndex(
                name: "IX_PageFiles_PageId",
                table: "PageFiles",
                column: "PageId");

            migrationBuilder.CreateIndex(
                name: "IX_PageFiles_PageId1",
                table: "PageFiles",
                column: "PageId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FundFiles");

            migrationBuilder.DropTable(
                name: "GeneralSubscriptions");

            migrationBuilder.DropTable(
                name: "LegalSubscriptions");

            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "PageFiles");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "FundCategory");

            migrationBuilder.DropTable(
                name: "Page");

            migrationBuilder.DropTable(
                name: "Funds");

            migrationBuilder.DropTable(
                name: "Content");
        }
    }
}
