﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace FusionCore.Migrations
{
    public partial class InstallationManagerV09 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOnUtc",
                table: "Employee",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedOnUtc",
                table: "Employee");
        }
    }
}
