﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;

namespace FusionCore.Core.Extensions
{
    [SuppressMessage("ReSharper", "AccessToModifiedClosure")]
    public static class Strings
    {
        private const string STRIP_HTML_REGEX = "<.+?>";
        private const string STRIP_HTML_REGEX_CONDITIONAL_FORMAT = "<(?!({0})\\b)[^>]*>";

        public static int WordCount(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }
            return str.Split(new[] { ' ', '.', '?' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static string FirstCharToUpper(this string input)
        {
            return string.IsNullOrEmpty(input) ? string.Empty : string.Concat(input.Substring(0, 1).ToUpper(), input.Substring(1));
        }

        public static string HighlightKeywords(this string input, List<string> keywords, string className)
        {
            if (string.IsNullOrEmpty(input) || keywords == null || !keywords.Any())
                return input;

            foreach (string keyword in keywords)
            {
                input = Regex.Replace(input, keyword, string.Format("<span class=\"{1}\">{0}</span>", "$0", className), RegexOptions.IgnoreCase);
            }
            return input;
        }

        public static string StripHtml(this string input, bool ignoreParagraphs = true, bool ignoreItalic = true, bool ignoreUnderline = true, bool ignoreBold = true, bool ignoreLinebreak = true, List<string> otherTagsToIgnore = null)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            if (ignoreParagraphs || ignoreItalic || ignoreUnderline || ignoreBold || ignoreLinebreak || (otherTagsToIgnore != null && otherTagsToIgnore.Any()))
            {
                string conditions = string.Empty;

                if (ignoreParagraphs)
                    conditions += "/?p|";
                if (ignoreItalic)
                    conditions += "/?i|/?em|";
                if (ignoreUnderline)
                    conditions += "/?u|";
                if (ignoreBold)
                    conditions += "/?b|/?strong|";
                if (ignoreLinebreak)
                    conditions += "br|";
                if (otherTagsToIgnore != null && otherTagsToIgnore.Any())
                {
                    otherTagsToIgnore.ForEach(x =>
                    {
                        conditions += string.Concat("/?", x, "|");
                    });
                }

                conditions = conditions.Substring(0, conditions.Length - 1); // Remove last '|'

                string regex = string.Format(STRIP_HTML_REGEX_CONDITIONAL_FORMAT, conditions);
                Regex rgx = new Regex(regex, RegexOptions.Singleline);

                return rgx.Replace(input, string.Empty);
            }

            return new Regex(STRIP_HTML_REGEX, RegexOptions.Singleline).Replace(input, string.Empty);
        }
    }
}
