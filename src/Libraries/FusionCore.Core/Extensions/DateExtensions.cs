using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace FusionCore.Core.Extensions
{
    public static class DateExtensions
    {
        internal const string DefaultDateFormat = "dd MMMM yyyy";

        public static int Age(this DateTime dateOfBirth)
        {
            DateTime today = DateTime.Today;

            if (today.Month < dateOfBirth.Month || today.Month == dateOfBirth.Month && today.Day < dateOfBirth.Day)
            {
                return today.Year - dateOfBirth.Year - 1;
            }
            return today.Year - dateOfBirth.Year;
        }

        public static string GetDayNumber(this DateTime date)
        {
            switch (date.Day)
            {
                case 1:
                case 21:
                case 31:
                    return date.Day + "st";
                case 2:
                case 22:
                    return date.Day + "nd";
                case 3:
                case 23:
                    return date.Day + "rd";
                default:
                    return date.Day + "th";
            }
        }

        public static bool IsWeekday(this DateTime date)
        {
            DayOfWeek day = date.DayOfWeek;

            switch (day)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    return false;

                default:
                    return true;
            }
        }

        public static bool IsWeekend(this DateTime date)
        {
            return !IsWeekday(date);
        }

        public static bool IsLeapYear(this DateTime date)
        {
            return DateTime.DaysInMonth(date.Year, 2).Equals(29);
        }

        public static double ElapsedSeconds(this DateTime date)
        {

            return DateTime.Now.Subtract(date).TotalSeconds;
        }

        public static DateTime GetFirstDayOfMonth(this DateTime date)
        {
            DateTime firstDay = new DateTime(date.Year, date.Month, 1);
            return firstDay;
        }

        public static DateTime GetLastDayOfMonth(this DateTime date)
        {
            DateTime lastDay = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1);
            return lastDay;
        }

        public static string PrettyDate(this DateTime date)
        {
            TimeSpan s = DateTime.Now.Subtract(date);
            int dayDiff = (int)s.TotalDays;
            int secDiff = (int)s.TotalSeconds;

            if (dayDiff < 0 || dayDiff >= 31)
            {
                return date.ToString(CultureInfo.InvariantCulture);
            }

            if (dayDiff == 0)
            {
                if (secDiff < 60)
                {
                    return "just now";
                }

                if (secDiff < 120)
                {
                    return "1 minute ago";
                }

                if (secDiff < 3600)
                {
                    return $"{Math.Floor((double)secDiff / 60)} minutes ago";
                }

                if (secDiff < 7200)
                {
                    return "1 hour ago";
                }

                if (secDiff < 86400)
                {
                    return $"{Math.Floor((double)secDiff / 3600)} hours ago";
                }
            }

            if (dayDiff == 1)
            {
                return "yesterday";
            }

            if (dayDiff < 7)
            {
                return $"{dayDiff} days ago";
            }

            if (dayDiff < 14)
            {
                return "1 week ago";
            }

            if (dayDiff < 31)
            {
                return $"{Math.Ceiling((double)dayDiff / 7)} weeks ago";
            }

            return date.ToString(CultureInfo.InvariantCulture);
        }


        public static string FormatDateTime(this DateTime date, string format)
        {
            format = string.IsNullOrEmpty(format)
                ? DefaultDateFormat
                : Regex.Replace(format, @"(?<!\\)((\\\\)*)(S)", "$1" + GetDayNumberSuffix(date));

            return date.ToString(format);
        }

        private static string GetDayNumberSuffix(DateTime date)
        {
            switch (date.Day)
            {
                case 1:
                case 21:
                case 31:
                    return @"\s\t";
                case 2:
                case 22:
                    return @"\n\d";
                case 3:
                case 23:
                    return @"\r\d";
                default:
                    return @"\t\h";
            }
        }

        public static string GetMonthName(this DateTime date)
        {
            return date.ToString("MMMM");
        }
        public static string GetShortMonthName(this DateTime date)
        {
            return date.ToString("MMM");
        }
        public static string GetDayName(this DateTime date)
        {
            return date.DayOfWeek.ToString();
        }

        public static DateTime ConvertToUserTime(this DateTime dt)
        {
            dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);

            TimeZoneInfo currentUserTimeZoneInfo = TimeZoneInfo.Local;
            return TimeZoneInfo.ConvertTime(dt, currentUserTimeZoneInfo);
        }

        public static DateTime GetRandomDayInCurrentMonth(this DateTime dt)
        {
            Random gen = new Random();
            DateTime start = new DateTime(dt.Year, dt.Month, 1, 0, 0, 0, DateTimeKind.Local);
            dt = dt.AddMonths(1);
            DateTime end = new DateTime(dt.Year, dt.Month, 1, 0, 0, 0, DateTimeKind.Local).AddDays(-1);
            int range = (end - start).Days;
            return start.AddDays(gen.Next(range));
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static int GetWeekNumber(this DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }

        public static DateTime GetFirstDayOfWeek(this DateTime dt, int weekNum)
        {
            DateTime jan1 = new DateTime(dt.Year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            Calendar cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            DateTime result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        public static int GetIso8601WeekOfYear(this DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static int GetYearOfWeek(this DateTime date)
        {
            int weeknumber = GetIso8601WeekOfYear(date);
            if (weeknumber == 1 && date.Month == 12) return date.Year + 1;
            if (date.Month == 1 && weeknumber > 50) return date.Year - 1;
            return date.Year;
        }

        public static DateTime UnixTimeStampToDateTime(this long unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static int ToUnixTimestamp(this DateTime value)
        {
            return (int)Math.Truncate((value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
        }

        public static DateTime ParseDateTime(this string[] format, string value)
        {
            return DateTime.ParseExact(value, format,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);
        }
    }
}
