using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Core.Extensions
{
    public static class SessionExtensions
    {
        public static T GetComplexData<T>(this ISession session, string key)
        {
            var data = session.GetString(key);
            if (data == null)
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(data);
            //return XmlExtensions.Deserialize<T>(data);
        }

        public static void SetComplexData(this ISession session, string key, object value)
        {
            string data = JsonConvert.SerializeObject(value, new JsonSerializerSettings { Error = (se, ev) => { ev.ErrorContext.Handled = true; }, Formatting = Formatting.Indented });
            //string data = XmlExtensions.Serialize(value);
            session.SetString(key, data);
        }

        public static double? GetDouble(this ISession session, string key)
        {
            var data = session.Get(key);
            if (data == null)
            {
                return null;
            }
            return BitConverter.ToDouble(data, 0);
        }

        public static void SetDouble(this ISession session, string key, double value)
        {
            session.Set(key, BitConverter.GetBytes(value));
        }
    }
}
