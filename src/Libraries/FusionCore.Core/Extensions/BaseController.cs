﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FusionCore.Core.Extensions
{
    [Authorize]
    public class FusionBaseController : BaseController
    {

    }

    [AutoValidateAntiforgeryToken]
    public class BaseController : Controller
    {
        public void AddErrorMessage(string message, bool persistForTheNextRequest = true, string type = "Main")
        {
            AddMessage(message, type, persistForTheNextRequest, Constants.TempDataErrorKey);
        }

        public void AddSuccessMessage(string message, bool persistForTheNextRequest = true, string type = "Main")
        {
            AddMessage(message, type, persistForTheNextRequest, Constants.TempDataSuccesKey);
        }

        private void AddMessage(string message, string type, bool persistForTheNextRequest, string keyName)
        {
            string dataKey = keyName + type;
            if (persistForTheNextRequest)
            {
                if (TempData[dataKey] == null)
                    TempData[dataKey] = new List<string>();
                ((List<string>)TempData[dataKey]).Add(message);
            }
            else
            {
                if (ViewData[dataKey] == null)
                    ViewData[dataKey] = new List<string>();
                ((List<string>)ViewData[dataKey]).Add(message);
            }
        }
    }
}