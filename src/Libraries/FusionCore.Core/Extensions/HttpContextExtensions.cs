﻿using Microsoft.AspNetCore.Http;

namespace FusionCore.Core.Extensions
{
    public static class HttpContextExtensions
    {
        public static string GetBaseUrl(this HttpRequest request)
        {
            return $"{request.Scheme}://{request.Host}{request.PathBase}";
        }
    }
}
