﻿namespace FusionCore.Core.Interfaces
{
    public interface IFusionCoreModule
    {
        string GetSystemName();
    }
}
