﻿using System;

namespace FusionCore.Core.Interfaces
{
    public interface IHasSoftDelete
    {
        DateTime? DeletedOnUtc { get; set; }
    }
}
