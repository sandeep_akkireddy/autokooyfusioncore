﻿using System;
using System.Threading.Tasks;

namespace FusionCore.Core.Interfaces
{
    public interface IInstallData
    {
        Task Install(IServiceProvider serviceProvider, bool sampleData);
    }
}
