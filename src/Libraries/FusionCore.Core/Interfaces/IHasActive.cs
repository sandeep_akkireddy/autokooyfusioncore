﻿namespace FusionCore.Core.Interfaces
{
    public interface IHasActive
    {
        bool Active { get; set; }
    }
}
