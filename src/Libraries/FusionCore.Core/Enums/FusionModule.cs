using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FusionCore.Core.Enums
{
    public enum FusionModule
    {
        [Description("none")]
        none =0,
        [Description("Dashboard")]
        Dashboard=1,
        [Description("Systeem informatie")]
        Systeem_informatie =2,
        [Description("Instellingen")]
        Instellingen =3,
        [Description("Bedrijfsinformatie")]
        Bedrijfsinformatie =4,
        [Description("Website gegevens")]
        Website_gegevens =5,
        [Description("Fusion gebruikers")]
        Fusion_gebruikers =6,
        [Description("Installatie manager")]
        Installatie_manager =7,
        [Description("Divisie")]
        Divisie =8,
        [Description("Opdrachtgevers")]
        Opdrachtgevers =9,
        [Description("Gebouwen")]
        Gebouwen =10,
        [Description("Assets")]
        Assets =11,
        [Description("Werknemers")]
        Werknemers =12,
        [Description("Planning")]
        Planning =13,
        [Description("Overzicht")]
        Overzicht =14,
        [Description("Controle")]
        Controle =15,
        [Description("Rapportages")]
        Rapportages =16,
        [Description("Activiteiten beheer")]
        Activiteiten_beheer =17,
        [Description("Groepen beheer")]
        Groepen_beheer =18,
        [Description("Pagina's")]
        Paginas =19,
        [Description("Beheerder")]
        Beheerder =20,
        [Description("Team")]
        Team =21,
        [Description("Nieuws")]
        Nieuws =22,
        [Description("Fondsen")]
        Fondsen =23,
        [Description("Banner")]
        Banner =24,
        [Description("Support")]
        Support = 25
    }
}
