﻿namespace FusionCore.Core.Enums
{
    public enum FileStatus
    {
        Good = 1,
        Alert = 2,
        Fail = 3
    }
}
