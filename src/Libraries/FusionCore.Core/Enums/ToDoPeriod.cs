using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Core.Enums
{
    public enum ToDoPeriod
    {
        None = 0,
        Today = 1,
        Tomorrow = 2,
        ThisWeek = 3,
        ThisMonth = 4
    }
}
