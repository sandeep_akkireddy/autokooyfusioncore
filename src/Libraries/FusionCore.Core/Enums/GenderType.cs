﻿namespace FusionCore.Core.Enums
{
    public enum GenderType
    {
        Male,
        Female,
        Unknown
    }
}
