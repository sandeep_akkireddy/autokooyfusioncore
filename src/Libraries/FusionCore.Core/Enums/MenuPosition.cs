using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Core.Enums
{
    public enum MenuPosition
    {
        Hoofdmenu = 1,
        Footermenu = 2
    }
}
