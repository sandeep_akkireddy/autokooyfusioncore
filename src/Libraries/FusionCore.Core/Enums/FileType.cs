﻿namespace FusionCore.Core.Enums
{
    public enum FileType
    {
        Pdf = 1,
        Image = 2,
        Report = 3
    }
}
