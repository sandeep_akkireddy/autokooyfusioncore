﻿namespace FusionCore.Core.Enums
{
    public enum FusionRoles
    {
        SuperAdmin = 0,
        Administrator = 1,
        Moderator = 2,
        ReadOnly = 3
    }
}
