using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Core.Settings
{
    public class ApiUrlSettings
    {
        public string Staging { get; set; }
        public string Production { get; set; }
        public string MappedTo { get; set; }

    }
}
