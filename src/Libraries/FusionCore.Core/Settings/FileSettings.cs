using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Core.Settings
{
    public class FileSettings
    {
        public string FundFiles { get; set; }
        public string FundFileRelativePath { get; set; }
        public string PageFiles { get; set; }
        public string PageFileRelativePath { get; set; }
        public string NewsFiles { get; set; }
        public string NewsFileRelativePath { get; set; }
        public string TeamsFiles { get; set; }
        public string TeamsFileRelativePath { get; set; }
        public string UserFiles { get; set; }
        public string UserFileRelativePath { get; set; }
        public string BannerFiles { get; set; }
        public string BannerFileRelativePath { get; set; }
        public string SupportFiles { get; set; }
        public string SupportFileRelativePath { get; set; }
        public string XmlFiles { get; set; }
        public string XmlFileImagesRelativePath { get; set; }
    }
}
