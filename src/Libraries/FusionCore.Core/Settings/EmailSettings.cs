using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Core.Settings
{
    public class EmailSettings
    {
        public string SMTPServer { get; set; }
        public string SMTPUser { get; set; }
        public string SMTPPassword { get; set; }
        public string SMTPPort { get; set; }
        public bool SMTPIsSSL { get; set; }
        public string AutokooyEmail { get; set; }
    }
}
