namespace FusionCore.Core.Settings
{
    public class MainSettings
    {
        public bool IsInstalled { get; set; }
        public bool ShowDevelopmentError { get; set; }
        public bool UseInMemoryDb { get; set; }
        public int ResetUrlTimeoutInHours { get; set; }
        public int PageSize { get; set; }
        public string SendGridUser { get; set; }
        public string SendGridKey { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public string WkhtmltopdfLocation { get; set; }
        public string WebsiteUrl { get; set; }
        public string FusionUrl { get; set; }
        public string AnalyticsCode { get; set; }
        public string FusionModules { get; set; }
        public string FusionSuperAdminExtraModules { get; set; }
        public string SubscriptionEmails { get; set; }
        public bool EnableClientSubscriptionMails { get; set; }
    }
}
