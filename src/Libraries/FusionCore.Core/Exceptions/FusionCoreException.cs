﻿using System;

namespace FusionCore.Core.Exceptions
{
    public class FusionCoreException : Exception
    {
        public FusionCoreException() { }
        public FusionCoreException(string message) : base(message) { }
        public FusionCoreException(string message, Exception er) : base(message, er) { }
    }
}
