﻿using FusionCore.Core.Interfaces;

namespace FusionCore.Core
{
    public class CoreModule : IFusionCoreModule
    {
        public string GetSystemName()
        {
            return "FusionCore.Core";
        }
    }
}