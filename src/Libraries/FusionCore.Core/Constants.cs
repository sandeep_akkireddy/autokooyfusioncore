﻿namespace FusionCore.Core
{
    public class Constants
    {
        public const string TempDataErrorKey = "FusionCore.Error-";
        public const string TempDataSuccesKey = "FusionCore.Succes-";
        public const string DefaultLoginType = "Email";
    }
}
