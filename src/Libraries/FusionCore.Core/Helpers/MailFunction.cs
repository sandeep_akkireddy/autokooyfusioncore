using FusionCore.Core.Settings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;

namespace FusionCore.Core.Helpers
{
    public class MailFunction
    {
        public string EmailFrom { get; set; }
        public string EmailFromName { get; set; }
        public string EmailTo { get; set; }
        public string EmailToName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsHtml { get; set; }
        public bool IsSSL { get; set; }
        public Dictionary<string, string> CCList { get; set; }
        public Dictionary<string, string> BCCList { get; set; }
        public List<string> Attachments { get; set; }
        public Dictionary<string, MemoryStream> AttachmentsStreams { get; set; }

        public bool DontUseCC { get; set; }
        public MailFunction(EmailSettings emailSettings)
        {
            Port = Convert.ToInt32(emailSettings.SMTPPort);
            IsSSL = Convert.ToBoolean(emailSettings.SMTPIsSSL);
            BCCList = new Dictionary<string, string>();
            CCList = new Dictionary<string, string>();
            Attachments = new List<string>();
            AttachmentsStreams = new Dictionary<string, MemoryStream>();
            Server = emailSettings.SMTPServer;
            Username = emailSettings.SMTPUser;
            Password = emailSettings.SMTPPassword;
        }
        public void SendEmail()
        {
            MailMessage message = new MailMessage
            {
                Subject = Subject
            };

            if (EmailTo.IndexOf(";") > -1)
            {
                bool isFirst = true;
                foreach (string email in EmailTo.Split(Convert.ToChar(";")))
                {
                    string address = email.Replace(";", "");
                    if (isFirst || DontUseCC)
                    {
                        message.To.Add(new MailAddress(address));
                        isFirst = false;
                    }
                    else
                    {
                        message.CC.Add(new MailAddress(address));
                    }
                }
            }
            else
            {
                message.To.Add(new MailAddress(EmailTo, EmailToName));
            }

            message.Body = Body;
            message.IsBodyHtml = IsHtml;
            message.From = new MailAddress(EmailFrom, EmailFromName);
            foreach (KeyValuePair<string, string> item in CCList)
            {
                if (!string.IsNullOrEmpty(item.Value))
                {
                    message.CC.Add(new MailAddress(item.Key, item.Value));
                }
                else
                {
                    message.CC.Add(item.Key);
                }
            }

            foreach (KeyValuePair<string, string> item in BCCList)
            {
                message.Bcc.Add(new MailAddress(item.Key, item.Value));
            }

            foreach (string item in Attachments)
            {
                message.Attachments.Add(new Attachment(item));
            }
            foreach (KeyValuePair<string, MemoryStream> item in AttachmentsStreams)
            {
                message.Attachments.Add(new Attachment(item.Value, item.Key));
            }
            SmtpClient smtp = new SmtpClient(Server, Port) { EnableSsl = IsSSL };

            if (!string.IsNullOrEmpty(Username))
            {
                smtp.Credentials = new System.Net.NetworkCredential(Username, Password);
            }
            smtp.Send(message);

        }
    }
}
