using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FusionCore.Core.Helpers
{
    public static class AssemblyHelper
    {
        public static IEnumerable<TypeInfo> GetAssemblyImplementationsOfType<T>()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();

            List<TypeInfo> definedTypes = entryAssembly
                .GetReferencedAssemblies()
                .Select(Assembly.Load)
                .SelectMany(x => x.DefinedTypes)
                .Where(x => !x.IsInterface && !x.IsAbstract && x.IsClass)
                .Where(x => x.FullName.Contains("FusionCore")).ToList();

            IEnumerable<TypeInfo> types = definedTypes
                .Where(x => typeof(T).IsAssignableFrom(x));

            return types;
        }

        public static IEnumerable<System.Type> GetAssemblyImplementationsOfType<T>(Assembly assembly)
        {
            IEnumerable<TypeInfo> types = assembly
                .GetReferencedAssemblies()
                .Select(Assembly.Load)
                .SelectMany(x => x.DefinedTypes)
                .Where(type => !type.GetTypeInfo().IsAbstract)
                .Where(type => typeof(T).IsAssignableFrom(type));

            return types;
        }
    }
}
