using FusionCore.Core.Enums;
using System;
using System.Globalization;

namespace FusionCore.Core.Helpers
{
    public static class StringHelpers
    {
        public static string GetMonthName(int month)
        {
            DateTimeFormatInfo dtFormat = new DateTimeFormatInfo();
            return dtFormat.GetMonthName(month);
        }

        public static string GetShortInfo(GenderType gender, string firstName, string middleName, string lastName,
            DateTime birthDate)
        {
            return "(" + (gender.ToString()) + ") " + GetFullname(firstName, middleName, lastName, true) + " - '" + birthDate.Year.ToString().Substring(2, 2);
        }

        public static string GetFullname(string firstName, string middleName, string lastName, bool onlyFirstLetter = false)
        {
            return (onlyFirstLetter ? firstName.Substring(0, 1).ToUpper() + "." : firstName) + " " + (!string.IsNullOrEmpty(middleName) ? middleName + " " : "") + lastName;
        }

        public static string MaxLength(this string source, int length = 100)
        {
            if (source.Length > length)
                return source.Substring(0, length);
            return source;
        }
        public static bool IsNumeric(string source)
        {
            int result;
            return Int32.TryParse(source, out result);
        }
        public static bool IsDecimal(string source)
        {
            decimal result;
            return Decimal.TryParse(source, out result);
        }
    }
}
