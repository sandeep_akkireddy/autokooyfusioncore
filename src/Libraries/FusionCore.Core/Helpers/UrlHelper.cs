using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FusionCore.Core.Helpers
{
    public class UrlHelper
    {
        public static string CleanUrl(string url)
        {
            url = url.Replace("/", "");
            url = url.Replace("\\", "");
            return url;
        }
        public static string GetFrienlyUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return url;
            // make it all lower case
            url = url.ToLower();
            // remove entities
            url = Regex.Replace(url, @"&\w+;", "");
            // remove anything that is not letters, numbers, dash, or space
            url = Regex.Replace(url, @"[^a-z0-9\-\s]", "");
            // replace spaces
            url = url.Replace(' ', '-');
            // collapse dashes
            url = Regex.Replace(url, @"-{2,}", "-");
            // trim excessive dashes at the beginning
            url = url.TrimStart(new[] { '-' });

            return url;
        }

        public static string IsActive(string currentUrl, string menuUrl)
        {
            if (menuUrl == "/" && currentUrl == menuUrl)
            {
                return "current";
            }
            else if (menuUrl != "/" && currentUrl.StartsWith(menuUrl))
            {
                return "current";
            }
            return "";

        }
    }
}
