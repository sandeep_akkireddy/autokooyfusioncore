﻿using FusionCore.Core.Exceptions;
using FusionCore.Core.Settings;
using Microsoft.Extensions.Options;
using System;

namespace FusionCore.Core.Helpers
{
    public class PdfCreator
    {
        private readonly MainSettings _mainSettings;

        public PdfCreator(IOptions<MainSettings> optionsAccessor)
        {
            _mainSettings = optionsAccessor.Value;
        }

        public string Html { get; set; }

        public byte[] CreatePdfFromHtml()
        {
            try
            {
                NReco.PdfGenerator.HtmlToPdfConverter htmlToPdfConv = new NReco.PdfGenerator.HtmlToPdfConverter();
                htmlToPdfConv.License.SetLicenseKey(
                    "PDF_Generator_Bin_Examples_Pack_250061254067",
                    "FQp7TNrAZ6SdlUWecy/ODsuQmS4z52qf+00+U486nFJzCGvWN7Fm++yf2gnhRU2jA5qK2Vv+n4GdTwEC5gQzeQ0HoAwZ9gTmHInuWZQG9EhpPlt27waebHql722QAfKlTc47FzZMkMU8TV3EqX59xue9wUj0chVgVoVD1rIxfJ0="
                );
                // path where wkhtmltopdf binaries are installed/deployed 
                htmlToPdfConv.PdfToolPath = _mainSettings.WkhtmltopdfLocation;
                byte[] result = htmlToPdfConv.GeneratePdf(Html);
                return result;
            }
            catch (Exception er)
            {
                throw new FusionCoreException("Error creating pdf", er);
            }
        }
    }
}
