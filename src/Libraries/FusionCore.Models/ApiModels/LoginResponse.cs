using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.ApiModels
{
    public class ResponseStatus
    {
        public string errorCode { get; set; }
        public string message { get; set; }
        public string stackTrace { get; set; }
        public string errors { get; set; }
        public string meta { get; set; }
    }

    public class LoginResponse
    {
        public string userId { get; set; }
        public string sessionId { get; set; }
        public string userName { get; set; }
        public string displayName { get; set; }
        public object referrerUrl { get; set; }
        public string bearerToken { get; set; }
        public object refreshToken { get; set; }
        public ResponseStatus responseStatus { get; set; }
        public string meta { get; set; }
    }

    public class ResetPasswordResponse
    {
        public string code { get; set; }
        public DateTime? codeExpireDate { get; set; }
        public string emailAddress { get; set; }
        public ResponseStatus responseStatus { get; set; }
    }

    public class ChangePasswordResponse
    {
        public bool Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
