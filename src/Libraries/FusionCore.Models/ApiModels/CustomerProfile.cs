using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.ApiModels
{
    public class CustomerProfile
    {
        public long Id { get; set; }
        public string CustomerType { get; set; }
        public string DisplayName { get; set; }
    }
}
