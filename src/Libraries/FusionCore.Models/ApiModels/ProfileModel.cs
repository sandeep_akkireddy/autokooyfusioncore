using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.ApiModels
{
    public class ProfileModel
    {
        public ClientModel ClientModel { get; set; }
        public CompanyModel CompanyModel { get; set; }
        public string ProfileType { get; set; } // C = company
        public string Token { get; set; }
        public string Username { get; set; }
        public long ProfileID;

        public string Email => ProfileType != null ? ProfileType.Equals("C") ? CompanyModel.Email : ClientModel.Email : string.Empty;
        public List<FormDataPropertyModel> Properties => ProfileType != null ? ProfileType.Equals("C") ? CompanyModel.Properties : ClientModel.Properties : null;
        public string FullName => ProfileType != null ? ProfileType.Equals("C") ? CompanyModel.FullName : ClientModel.FullName : string.Empty;
    }
}
