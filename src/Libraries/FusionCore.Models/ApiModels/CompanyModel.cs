using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Models.ApiModels
{
    public class CompanyModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string AccountNumber { get; set; }
        public string AccountNumberBIC { get; set; }
        // public long ChamberNumber { get; set; }
        public string ChamberNumber { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string Representation { get; set; }
        public string RepresentationLastName { get; set; }
        public string RepresentationSalutation { get; set; }
        public string RepresentationTitlePrefix { get; set; }
        public string RepresentationTitleSuffix { get; set; }
        public string RepresentationInitials { get; set; }
        public string TypeOfId { get; set; }
        public string IDNumber { get; set; }
        public string IDIssuedAt { get; set; }
        [DataType(DataType.Text), DisplayFormat(DataFormatString = @"{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime IDIssuedOn { get; set; }
        [DataType(DataType.Text), DisplayFormat(DataFormatString = @"{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime IDExpireDate { get; set; }
        public string FullName { get { return $"{RepresentationSalutation} {RepresentationLastName}"; } }

        public List<FormDataPropertyModel> Properties
        {
            get
            {
                return new List<FormDataPropertyModel>
                {
                    new FormDataPropertyModel { Key = "Statutaire naam", Value = Name },
                    new FormDataPropertyModel { Key = "Statutair adres", Value = Address },
                    new FormDataPropertyModel { Key = "Vestigingsplaats", Value = City },
                    new FormDataPropertyModel { Key = "Postcode", Value = ZipCode },
                    new FormDataPropertyModel { Key = "Inschrijfnummer KvK", Value = ChamberNumber.ToString() },
                    new FormDataPropertyModel { Key = "Email", Value = Email },
                    new FormDataPropertyModel { Key = "IBAN", Value = AccountNumber },
                    new FormDataPropertyModel { Key = "BIC Code", Value = AccountNumberBIC },
                    new FormDataPropertyModel { Key = "Aanhef", Value = RepresentationSalutation },
                    new FormDataPropertyModel { Key = "Achternaam", Value = RepresentationLastName },
                    new FormDataPropertyModel { Key = "Initialen", Value = RepresentationInitials },
                    new FormDataPropertyModel { Key = "Voorna(a)m(en) voluit", Value = Representation },
                    new FormDataPropertyModel { Key = "Titel(s) voor", Value = RepresentationTitlePrefix },
                    new FormDataPropertyModel { Key = "Titel(s) achter", Value = RepresentationTitleSuffix },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs", Value = TypeOfId },
                    new FormDataPropertyModel { Key = "Nummer legitimatiebewijs", Value = IDNumber },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs afgegeven te", Value = IDIssuedAt },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs afgegeven op", Value = IDIssuedOn.ToString("dd-MM-yyyy") },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs geldig tot", Value = IDExpireDate.ToString("dd-MM-yyyy") }
                };
            }
        }
    }
}
