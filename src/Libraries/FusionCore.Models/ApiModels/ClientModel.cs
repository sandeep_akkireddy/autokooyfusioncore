using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Models.ApiModels
{
    public class ClientModel
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        // clientmodel
        public DateTime CreatedOn { get; set; }
        //holding the token provided from REST API
        public string Token { get; set; }
        public string Salutation { get; set; }
        public string TitlePrefix { get; set; }
        public string Initials { get; set; }
        public string FullName { get { return $"{SalutationName} {Lastname}"; } }
        public string SalutationName
        {
            get
            {
                return (Salutation.IndexOf("heer", StringComparison.InvariantCultureIgnoreCase) != -1) ? "Mijnheer" : "Mevrouw";
            }
        }
        public string TitleSuffix { get; set; }
        public string Address { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberAddition { get; set; }
        public string HouseNumberAddition2 { get; set; }
        public string HouseNumberFull { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string TypeOfID { get; set; }
        public string IdNumber { get; set; }
        public string IdIssuedAt { get; set; }
        [DataType(DataType.Text), DisplayFormat(DataFormatString = @"{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime IdIssuedOn { get; set; }
        [DataType(DataType.Text), DisplayFormat(DataFormatString = @"{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime IdExpireDate { get; set; }
        public string MaritalStatus { get; set; }
        public bool PrenuptialAgreements { get; set; }
        public bool RegisteredPartnershipAgreement { get; set; }

        public List<FormDataPropertyModel> Properties
        {
            get
            {
                return new List<FormDataPropertyModel>
                {
                    new FormDataPropertyModel { Key = "Aanhef", Value = Salutation },
                    new FormDataPropertyModel { Key = "Titel(s) voor", Value = TitlePrefix },
                    new FormDataPropertyModel { Key = "Voorletters", Value = Initials },
                    new FormDataPropertyModel { Key = "Voornamen voluit", Value = Firstname },
                    new FormDataPropertyModel { Key = "Achternaam", Value = Lastname },
                    new FormDataPropertyModel { Key = "Titel(s) achter", Value = TitleSuffix },
                     new FormDataPropertyModel { Key = "Telefoonnummer", Value = PhoneNumber },
                    new FormDataPropertyModel { Key = "Mobiel", Value = MobilePhoneNumber },
                    new FormDataPropertyModel { Key = "Email", Value = Email },
                    new FormDataPropertyModel { Key = "Straatnaam", Value = Address },
                    new FormDataPropertyModel { Key = "Huisnummer", Value = HouseNumber },
                    new FormDataPropertyModel { Key = "Huisnummer toevoeging", Value = HouseNumberAddition },
                    new FormDataPropertyModel { Key = "Postcode", Value = ZipCode },
                    new FormDataPropertyModel { Key = "Woonplaats", Value = City },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs", Value = TypeOfID },
                    new FormDataPropertyModel { Key = "Nummer legitimatiebewijs", Value = IdNumber },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs afgegeven te", Value = IdIssuedAt },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs afgegeven op", Value = IdIssuedOn.ToString("dd-MM-yyyy") },
                    new FormDataPropertyModel { Key = "Legitimatiebewijs geldig tot", Value = IdExpireDate.ToString("dd-MM-yyyy") },
                    new FormDataPropertyModel { Key = "Burgelijke staat", Value = MaritalStatus },
                    new FormDataPropertyModel { Key = "Huwelijkse voorwaarden", Value = PrenuptialAgreements ? "ja" : "nee" },
                    new FormDataPropertyModel { Key = "Geregistreerd partnerschap", Value = RegisteredPartnershipAgreement ? "ja" : "nee" }
                };
            }
        }

        public string PhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
    }

    public class FormDataPropertyModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
