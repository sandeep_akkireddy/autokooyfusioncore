using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.ApiModels
{
    public class ResetPasswordModel
    {
        public string Code { get; set; }
        public DateTime? CodeExpireDate { get; set; }
        public string EmailAddress { get; set; }
    }
}
