using FusionCore.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.ApiModels
{
    public class FundModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public MenuPosition Position { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
        public string Rendementen { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public DateTime CreatedOn { get; set; }
        public string GeneralDescription { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double? Investment { get; set; }
        public string InvestmentString => Investment.HasValue ? Investment.Value.ToString("C") : 0.ToString("C");
        public DateTime? StartDate { get; set; }
        public DateTime? StopDate { get; set; }
        public DateTime? JoinDate { get; set; }
        public string StopDateString => StopDate.HasValue ? StopDate.Value.ToShortDateString() : "n.b.";
        public string JoinDateString => JoinDate.HasValue ? JoinDate.Value.ToShortDateString() : "n.b.";

        public bool SplashActive { get; set; }
        public string SplashImage { get; set; }
        public string SplashTitle { get; set; }
        public string SplashText { get; set; }

        public string ThumbnailImage { get; set; }
        public string ThumbnailText { get; set; }
        public List<FundDividend> Dividends { get; set; }

        public double CumulativeDividends { get; set; }
        public double CumulativeDividendsTax { get; set; }
        public double CumulativeDividendsPayment { get; set; }
        /// <summary>
        /// This is the string value !!! Use CumulativeDividends property for double
        /// </summary>
        public string CumulativeDividendsString => CumulativeDividends.ToString("C");
        /// <summary>
        /// This is the string value !!! Use CumulativeDividendsTax property for double
        /// </summary>
        public string CumulativeDividendsTaxString => CumulativeDividendsTax.ToString("C");
        /// <summary>
        /// This is the string value !!! Use CumulativeDividendsPayment property for double
        /// </summary>
        public string CumulativeDividendsPaymentString => CumulativeDividendsPayment.ToString("C");

        public double CumulativeAdditionalPayments { get; set; }
        public double CumulativeAdditionalPaymentsTax { get; set; }
        public double CumulativeAdditionalPaymentsPayment { get; set; }
        /// <summary>
        /// This is the string value !!! Use CumulativeAdditionalPayments property for double
        /// </summary>
        public string CumulativeAdditionalPaymentsString => CumulativeAdditionalPayments.ToString("C");
        /// <summary>
        /// This is the string value !!! Use CumulativeAdditionalPaymentsTax property for double
        /// </summary>
        public string CumulativeAdditionalPaymentsTaxString => CumulativeAdditionalPaymentsTax.ToString("C");
        /// <summary>
        /// This is the string value !!! Use CumulativeAdditionalPaymentsPayment property for double
        /// </summary>
        public string CumulativeAdditionalPaymentsPaymentString => CumulativeAdditionalPaymentsPayment.ToString("C");

        public int? ExternalId { get; set; }

        public List<FundAdditionalPayment> AdditionalPayments { get; set; }

        public bool activeregistration { get; set; }
        public string participationprice { get; set; }
        public double participation_price { get; set; }
        public string Iban { get; set; }

        public DateTime Dateofcapital { get; set; }

        public ServiceProvider? ServiceProvider { get; set; }

        public string emissioncost { get; set; }
        public double emission_cost { get; set; }

        public int MaxSubscriptions { get; set; }
        public int SubscribedRegistrations { get; set; }
        public int CompletedRegistrations { get; set; }
        public double PossibleCapital { get; set; }
        public double TotalCapital { get; set; }
    }



    public class EditFundClientViewModel
    {
        public int Id { get; set; }
        public string GeneralDescription { get; set; }
        public List<int> CategoryId { get; set; }
        public List<string> FileName { get; set; }
    }


    public class FundDividend
    {
        public int FundId { get; set; }
        public int Year { get; set; }
        public List<QuarterDetails> Quarters { get; set; }
    }

    public class QuarterDetails
    {
        public int Quarter { get; set; }
        public string QuarterString => string.Format("Q{0}", Quarter);
        public double? DividendValue { get; set; }
        public string DividendValueString => DividendValue.HasValue ? DividendValue.Value.ToString("C") : 0.ToString("C");
        public double? TaxValue { get; set; }
        public string TaxValueString => TaxValue.HasValue ? TaxValue.Value.ToString("C") : 0.ToString("C");
        public double? PaymentValue { get; set; }
        public string PaymentValueString => PaymentValue.HasValue ? PaymentValue.Value.ToString("C") : 0.ToString("C");
    }

    public class FundAdditionalPayment
    {
        public string Remark { get; set; }
        public DateTime? PaymentDate { get; set; }
        public double? Value { get; set; }
        public double? PaymentValue { get; set; }
        public double? TaxValue { get; set; }

        public string PaymentDateString => PaymentDate.HasValue ? PaymentDate.Value.ToShortDateString() : "n.b.";
        public string ValueString => Value.HasValue ? Value.Value.ToString("C") : 0.ToString("C");
        public string PaymentValueString => PaymentValue.HasValue ? PaymentValue.Value.ToString("C") : 0.ToString("C");
        public string TaxValueString => TaxValue.HasValue ? TaxValue.Value.ToString("C") : 0.ToString("C");
    }
}
