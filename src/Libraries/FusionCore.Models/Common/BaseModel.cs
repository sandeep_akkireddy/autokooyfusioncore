﻿namespace FusionCore.Models.Common
{
    public class BaseModel
    {
        public int? Id { get; set; }
    }
}
