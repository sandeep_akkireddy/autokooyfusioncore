﻿namespace FusionCore.Models.Common
{
    public class BaseDataTable
    {
        public BaseDataTable()
        {
            Length = 15;
        }
        public int Start { get; set; }
        public int Length { get; set; }
        public string Draw { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
    }
}
