﻿namespace FusionCore.Models.Common
{
    public class ErrorViewModel
    {
        public bool ShowRequestId { get; set; }
        public string RequestId { get; set; }
    }
}
