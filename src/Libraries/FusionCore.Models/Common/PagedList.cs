using System;
using System.Collections.Generic;
using System.Linq;

namespace FusionCore.Models.Common
{
    public interface IPagedList<T> : IList<T>
    {
        int PageIndex { get; }
        int PageSize { get; }
        int TotalCount { get; }
        int TotalPages { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
     

    }

    [Serializable]
    public class PagedList<T> : List<T>, IPagedList<T>
    {
        public PagedList(IQueryable<T> source, int pageIndex, int pageSize)
        {
            pageSize = (pageSize != 0 ? pageSize : 1);

            int total = source.Count();
            TotalCount = total;
            TotalPages = total / pageSize;

            if (total % pageSize > 0)
                TotalPages++;

            PageSize = pageSize;
            PageIndex = pageIndex;
            if (total > 0)
            {
                AddRange(pageIndex > 0
                    ? source.Skip(pageIndex * pageSize).Take(pageSize).ToList()
                    : source.Take(pageSize).ToList());
            }
        }

        public PagedList(IList<T> source, int pageIndex, int pageSize)
        {
            pageSize = (pageSize != 0 ? pageSize : 1);

            TotalCount = source.Count;
            TotalPages = TotalCount / pageSize;

            if (TotalCount % pageSize > 0)
                TotalPages++;

            PageSize = pageSize;
            PageIndex = pageIndex;
            AddRange(source.Skip(pageIndex * pageSize).Take(pageSize).ToList());
            
        }

        // only used this when your source data is already paged!
        public PagedList(IEnumerable<T> source, int pageIndex, int pageSize, int totalCount)
        {
            pageSize = (pageSize != 0 ? pageSize : 1);

            TotalCount = totalCount;
            TotalPages = TotalCount / pageSize;

            if (TotalCount % pageSize > 0)
                TotalPages++;

            PageSize = pageSize;
            PageIndex = pageIndex;
            AddRange(source);
        }

        public int PageIndex { get; }
        public int PageSize { get; }
        public int TotalCount { get; }
        public int TotalPages { get; }

        public bool HasPreviousPage => PageIndex > 0;

        public bool HasNextPage => PageIndex + 1 < TotalPages;

      
    }
}
