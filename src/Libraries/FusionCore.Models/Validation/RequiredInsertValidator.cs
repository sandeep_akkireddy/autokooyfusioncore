﻿using FusionCore.Models.Common;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Models.Validation
{
    public class RequiredInsertValidator : ValidationAttribute, IClientModelValidator
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null) return ValidationResult.Success;

            BaseModel entity = (BaseModel)validationContext.ObjectInstance;
            return !entity.Id.HasValue ? new ValidationResult("test") : ValidationResult.Success;
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            ViewContext viewContext = (ViewContext)context.ActionContext;
            BaseModel model = (BaseModel)viewContext.ViewData.Model;

            if (!model.Id.HasValue)
            {
                MergeAttribute(context.Attributes, "data-val", "true");
                MergeAttribute(context.Attributes, "data-val-required", "");
            }
        }

        private static void MergeAttribute(IDictionary<string, string> attributes, string key, string value)
        {
            if (attributes.ContainsKey(key)) return;
            attributes.Add(key, value);
        }
    }
}
