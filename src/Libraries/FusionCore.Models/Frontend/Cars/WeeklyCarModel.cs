using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.Frontend.Cars
{
    public class WeeklyCarModel : BaseModel
    {
        public string FileName { get; set; }
        public string VehicleNumber { get; set; }
        public DateTime WeekExpiry { get; set; }
    }
}
