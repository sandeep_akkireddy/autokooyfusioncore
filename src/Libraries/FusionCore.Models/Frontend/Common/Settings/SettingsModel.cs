using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.Frontend.Common.Settings
{
    public class SettingsModel
    {
        public int Id { get; set; }
        public string FacebookUrl { get; set; }
        public string LinkedInUrl { get; set; }
        public string TwitterName { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string WhatsappUrl { get; set; }
        public string AnalyticsCode { get; set; }
        public SettingsCompany Company { get; set; }
        public MailingSettings MailingSettings { get; set; }
    }

    public class MailingSettings
    {
        public string HeaderText { get; set; }
        public string HeaderTextColor { get; set; }
        public string HeaderColor { get; set; }
        public string ContentTextColor { get; set; }
        public string ContentColor { get; set; }
        public string ReadMoreTextColor { get; set; }
        public string ReadMoreColor { get; set; }
        public string FooterText { get; set; }
    }

    public class SettingsCompany
    {
        public string CompanyName { get; set; }
        public string ContactFirstname { get; set; }
        public string ContactMiddlename { get; set; }
        public string ContactLastname { get; set; }
        public string Phonenumber { get; set; }
        public string Mobilenumber { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Website { get; set; }
        public string Fullname
        {
            get
            {
                return ContactFirstname + " " + (!string.IsNullOrEmpty(ContactMiddlename) ? ContactMiddlename + " " : "") + ContactLastname;
            }
        }
    }
}
