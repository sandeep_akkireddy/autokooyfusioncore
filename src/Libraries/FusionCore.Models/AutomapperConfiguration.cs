using AutoMapper;
using FusionCore.Core;
using FusionCore.Core.Extensions;
using FusionCore.Data.Entities;
using FusionCore.Data.Settings;
using FusionCore.Models.Backend.FusionUser;
using FusionCore.Models.Backend.Settings;
using System;
using System.Linq;
using FusionCore.Models.Backend.ToDo;

using FusionCore.Models.Frontend.Cars;

namespace FusionCore.Models
{
    public class AutomapperConfiguration : Profile
    {
        public AutomapperConfiguration()
        {
            CreateMap<CompanySettingsModel, CompanySettings>();
            CreateMap<CompanySettings, CompanySettingsModel>();

            CreateMap<User, FusionUserRow>()
                .ForMember(d => d.CreatedOn, o => o.MapFrom(s => s.CreatedOnUtc.ConvertToUserTime()))
                .ForMember(d => d.Name, o => o.MapFrom(s => s.GetFullName))
                .ForMember(d => d.Status, o => o.MapFrom(s => s.Role.ToString()))
                ;

            Func<User, string> convert = s => s.Credentials.FirstOrDefault(x => x.Type == Constants.DefaultLoginType)?.Identifier ?? string.Empty;

            CreateMap<User, FusionUserPersistModel>()
                .ForMember(d => d.Username, o => o.MapFrom(s => convert(s)));
            //.ForMember(d => d.Username, o => o.MapFrom<CredentialResolver>());

            CreateMap<FusionUserPersistModel, User>();
            //.ForMember(d => d.Credentials, o => o.Ignore());

            CreateMap<UserAccess, FusionUserAccessModel>();
            CreateMap<FusionUserAccessModel, UserAccess>();

            CreateMap<ToDo, ToDoPersistModel>().ForMember(d=>d.ToDoTitle,o=>o.MapFrom(s=>s.Name));
            CreateMap<ToDoPersistModel, ToDo>().ForMember(d => d.Name, o => o.MapFrom(s => s.ToDoTitle));

            CreateMap<WeeklyCar, WeeklyCarModel>();
            CreateMap<WeeklyCarModel, WeeklyCar>();
        }
    }

    public class CredentialResolver : IValueResolver<User, FusionUserPersistModel, string>

    {
        public string Resolve(User source, FusionUserPersistModel destination, string destMember, ResolutionContext context)
        {
            if (source.Credentials != null)
            {
                return source.Credentials.FirstOrDefault(x => x.Type == Constants.DefaultLoginType)?.Identifier ?? string.Empty;
            }

            return string.Empty;
        }
    }
}
