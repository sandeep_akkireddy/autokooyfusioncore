﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Models.Backend.Login
{
    public class ResetModel
    {
        public Guid Guid { get; set; }
        public int UserId { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "De wachtwoorden zijn niet gelijk.")]
        public string PasswordControle { get; set; }
    }
}