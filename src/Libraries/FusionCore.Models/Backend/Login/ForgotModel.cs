﻿using System.ComponentModel.DataAnnotations;

namespace FusionCore.Models.Backend.Login
{
    public class ForgotModel
    {
        [Required]
        public string Email { get; set; }
    }
}