﻿namespace FusionCore.Models.Backend.Login
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            LoginModel = new LoginModel();
            RegisterModel = new RegisterModel();
            ForgotModel = new ForgotModel();
        }
        public LoginModel LoginModel { get; set; }
        public RegisterModel RegisterModel { get; set; }
        public ForgotModel ForgotModel { get; set; }
    }
}
