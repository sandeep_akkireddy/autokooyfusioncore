﻿using FusionCore.Core.Enums;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Models.Backend.Login
{
    public class RegisterModel
    {

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "De wachtwoorden zijn niet gelijk.")]
        public string PasswordControle { get; set; }

        [Required]
        public string Name { get; set; }

        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public GenderType Gender { get; set; }
        public bool PrivacyAccepted { get; set; }
    }
}