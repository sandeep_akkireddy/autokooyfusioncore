using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.Backend.Messages
{
    public class GroupChatBox : BaseModel
    {
        public int groupid { get; set; }
        public string message { get; set; }
        public int senderid { get; set; }
        public int receiverid { get; set; }
        public List<MessagePersistModel> messageThreads { get; set; }
    }
}
