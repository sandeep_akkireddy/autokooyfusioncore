using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.Backend.Messages
{
    public class MessagePersistModel : BaseModel
    {
        public int? senderId { get; set; }
        public string sendername { get; set; }
        public string senderphoto { get; set; }
        public int? receiverId { get; set; }
        public string receivername { get; set; }
        public string receiverphoto { get; set; }
        public int groupid { get; set; }
        public string message { get; set; }
        public bool isread { get; set; }
        public int? orgmsgid { get; set; }
        public DateTime sentdate { get; set; }
    }
}
