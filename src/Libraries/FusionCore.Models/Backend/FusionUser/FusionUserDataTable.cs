﻿using FusionCore.Core.Enums;
using FusionCore.Models.Common;
using System;
using System.Collections.Generic;

namespace FusionCore.Models.Backend.FusionUser
{
    public class FusionUserDataTable : BaseDataTable
    {
        public List<FusionUserRow> Data { get; set; }
    }

    public class FusionUserRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Status { get; set; }
        public FusionRoles Role { get; set; }
    }
}
