using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Models.Backend.FusionUser
{
    public class FusionUserAccessModel: BaseModel
    {
        public int? userId { get; set; }
        
        public int FusionModule { get; set; }
        public bool Active { get; set; }
    }
}
