using FusionCore.Core.Enums;
using FusionCore.Data.Entities;
using FusionCore.Models.Common;
using FusionCore.Models.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Models.Backend.FusionUser
{
    public class FusionUserPersistModel : BaseModel
    {
        [Required]
        [MaxLength(150)]
        public string Firstname { get; set; }

        [MaxLength(50)]
        public string Middlename { get; set; }

        [Required]
        [MaxLength(250)]
        public string Lastname { get; set; }

	    public FusionRoles Role { get; set; }

		[Required]
		[MaxLength(250)]
		public string Username { get; set; }


		[RequiredInsertValidator]
		public string Password { get; set; }


		[RequiredInsertValidator]
		[Compare("Password", ErrorMessage = "De wachtwoorden zijn niet gelijk.")]
		public string PasswordControle { get; set; }

		public bool Active { get; set; }
        [Required]
        [MaxLength(256)]
        public string Email { get; set; }

        public string Photo { get; set; }
        [MaxLength(50)]
        public string Telephone { get; set; }

        public ICollection<FusionUserAccessModel> UserAccess { get; set; }

        public bool isloggedin { get; set; }
        public string Occupation { get; set; }
        public int unreadmsgs { get; set; }

        public string GetFullName => $"{Firstname}{(!string.IsNullOrEmpty(Middlename) ? " " + Middlename : "")} {Lastname}";

        public DateTime? lastmsg { get; set; }
    }
}
