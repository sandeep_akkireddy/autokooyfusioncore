using FusionCore.Core.Enums;
using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Models.Backend.ToDo
{
    public class ToDoPersistModel : BaseModel
    {
        public string ToDoTitle { get; set; }
        public ToDoPeriod Period { get; set; }
        public DateTime expiryDate { get; set; }
        public bool done { get; set; }
        public int userId { get; set; }
    }
}
