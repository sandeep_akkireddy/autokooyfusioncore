﻿namespace FusionCore.Models.Backend.Settings
{
    public class CompanySettingsModel
    {
        public string CompanyName { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactMiddleName { get; set; }
        public string ContactLastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressPostalCode { get; set; }
        public string AddressCity { get; set; }
        public string AddressCountry { get; set; }
        public string ContactPhonenumber { get; set; }
        public string ContactMobilenumber { get; set; }
        public string ContactEmail { get; set; }
        public string OtherWebsite { get; set; }
    }
}
