using FusionCore.Data.Entities;
using FusionCore.Models.Backend.FusionUser;
using FusionCore.Models.Backend.Messages;
using FusionCore.Models.Backend.ToDo;
using System.Collections.Generic;

namespace FusionCore.Models.Backend.Common
{
    public class NavigationTopModel
    {
        public FusionUserPersistModel CurrentUser { get; set; }
        public int AmountOfTasks { get; set; }
        public List<ToDoPersistModel> todolist { get; set; }
        public int NewMessages { get; set; }
        public List<MessagePersistModel> messageThreads { get; set; }
    }
}
