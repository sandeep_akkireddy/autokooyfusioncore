using FusionCore.Core.Settings;
using FusionCore.Framework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.Extensions.Logging;
using FusionCore.Services;
using FusionCore.MiddleWare;

namespace FusionCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            HostingEnvironment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            FusionCoreModulesDummy.Start();
            ApplicationStartup.DefaultFusionConfigureServices(services, Configuration, HostingEnvironment);
            services.AddScoped<IDashboardService, DashboardService>();
            services.AddSignalR();
        }

        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            IServiceProvider serviceProvider,
            ILoggerFactory loggerFactory)
        {

            
            ApplicationStartup.DefaultFusionConfigure(app, serviceProvider, loggerFactory, env, Configuration);

            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chatHub");
            });

            app.UseMiddleware<EventIdUrlRewritingMiddleware>();
            var config = Configuration.GetSection("MainSettings").Get<MainSettings>();
            //TODO: Clean this up, make a propper install
            if (false && !config.IsInstalled)
            {
                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Install}/{action=Index}/{id?}");

                });
            }
            else
            {
                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "areas",
                        template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                    );
                    routes.MapRoute("Error", "error",
                        new { controller = "Common", action = "Error" });
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Login}/{action=Index}/{id?}");
                });
            }
        }
    }
}
