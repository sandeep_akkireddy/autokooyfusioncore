using FusionCore.Core.Enums;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.Services.Common;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using FusionCore.Core.Extensions;
using Microsoft.AspNetCore.Http;

namespace FusionCore.ViewComponents
{
    public class MainMenu : ViewComponent
    {
        private readonly IMenuService _menuService;
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;

        private readonly IHttpContextAccessor _httpcontextAccessor;

        public MainMenu(IMenuService menuService, IConfiguration configuration, IUserService userService,IHttpContextAccessor httpContextAccessor)
        {
            _menuService = menuService;
            _configuration = configuration;
            _userService = userService;
            _httpcontextAccessor = httpContextAccessor;
        }

        public IViewComponentResult Invoke()
        {
            List<MenuService.MenuItemForCaching> menu = _menuService.GetMenu(MenuType.Backend, Url);

            var useraccess = _userService.GetUserAccess(_userService.GetCurrentUserId()).Where(x => x.Active).ToList();
            var _configfusionmodules = _configuration.GetSection("MainSettings").Get<MainSettings>();
            var fusionmodules = CommonHelper.GetActiveModulesNames(_configfusionmodules.FusionModules);

            List<string> modules = fusionmodules.Where(x => useraccess.Any(z => z.FusionModule == (int)x.module)).Select(x => x.displayname).ToList();
            var menuitems = menu.Where(x => modules.Any(z => z.ToLower().Trim().Equals(x.Name.ToLower().Trim()))).ToList();

            //if (HttpContext.User.IsInRole(FusionRoles.Moderator.ToString()) || HttpContext.User.IsInRole(FusionRoles.Moderator.ToString()))
            //{
            //    modules = fusionmodules.Select(x => x.displayname).ToList();
            //    menuitems = menuitems.Where(x => modules.Any(z => z.ToLower().Trim().Equals(x.Name.ToLower().Trim()))).ToList();
            //}
            //else
            if (HttpContext.User.IsInRole(FusionRoles.Administrator.ToString()) || HttpContext.User.IsInRole(FusionRoles.SuperAdmin.ToString()))
            {
                var Admin_fusionmodules = CommonHelper.GetActiveModulesNames(_configfusionmodules.FusionSuperAdminExtraModules).Select(x => x.displayname).ToList();
                var admin = menu.Where(x => Admin_fusionmodules.Any(z => z.ToLower().Trim().Equals(x.Name.ToLower().Trim()))).ToList();
                menuitems.AddRange(admin);
            }

            if (!menuitems.Any(x => x.Name.ToLower().Trim() == FusionModule.Dashboard.GetDescription().ToLower().Trim())
                &&
                menu.Any(x => x.Name.ToLower().Trim() == FusionModule.Dashboard.GetDescription().ToLower().Trim()))
            {
                menuitems.Insert(0, menu.FirstOrDefault(x => x.Name.ToLower().Trim() == FusionModule.Dashboard.GetDescription().ToLower().Trim()));
            }
            _httpcontextAccessor.HttpContext.Session.SetComplexData("menu", menuitems);
            return View(menuitems);
        }
    }
}
