using FusionCore.Models.Backend.Common;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Mvc;
using FusionCore.Core.Extensions;
using Microsoft.AspNetCore.Http;
using FusionCore.Models.Backend.ToDo;
using System.Collections.Generic;
using FusionCore.Services.ToDO;
using System.Linq;
using AutoMapper;
using FusionCore.Models.Backend.FusionUser;
using AutoMapper.Configuration;
using FusionCore.Core.Settings;
using FusionCore.Services.Message;

namespace FusionCore.ViewComponents
{
    public class NavigationTop : ViewComponent
    {
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpcontextAccessor;
        private readonly IToDoService _toDoService;
        private readonly IMapper _mapper;
        private readonly IMessageService _messageService;

        public NavigationTop(IUserService userService, IHttpContextAccessor httpContextAccessor, IToDoService toDoService,
            IMapper mapper, IMessageService messageService)
        {
            _userService = userService;
            _httpcontextAccessor = httpContextAccessor;
            _toDoService = toDoService;
            _mapper = mapper;
            _messageService = messageService;
        }

        public IViewComponentResult Invoke()
        {
            var res = HttpContext.Session.GetComplexData<List<ToDoPersistModel>>("todolist");
            var userid = _userService.GetCurrentUserId();
            if (res == null)
            {
                res = _toDoService.GetToDO(userid);
                HttpContext.Session.SetComplexData("todolist", res);
            }
            var todolist = res.Where(x => !x.done).ToList();
            FusionUserPersistModel currentUser = _mapper.Map<FusionUserPersistModel>(_userService.GetCurrentUser());

            var messages = _messageService.ShowUnreadMessages(userid);
            NavigationTopModel navigationTopModel = new NavigationTopModel
            {
                CurrentUser = currentUser,
                AmountOfTasks = todolist.Count(),
                todolist = todolist,
                NewMessages = messages.Count,
                messageThreads = messages
            };
            return View(navigationTopModel);
        }
    }
}
