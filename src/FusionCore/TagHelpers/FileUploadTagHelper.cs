﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace FusionCore.TagHelpers
{
    [HtmlTargetElement("fileupload", Attributes = "asp-id,asp-value")]
    public class FileUploadTagHelper : TagHelper
    {
        private readonly IUrlHelper _urlHelper;

        public FileUploadTagHelper(IUrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
        }

        public override int Order { get; } = int.MaxValue;

        [HtmlAttributeName("asp-id")]
        public ModelExpression Id { get; set; }

        [HtmlAttributeName("asp-value")]
        public ModelExpression Value { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);
            output.Content.AppendHtml($"<input type=\"hidden\" id=\"{Id.Name}\" name=\"{Id.Name}\" value=\"{Value.Model}\" />");
            string deleteUrl = !string.IsNullOrEmpty(Value.Model?.ToString()) ? _urlHelper.Action(new UrlActionContext { Action = "Delete", Controller = "File", Values = new { Area = "", id = Value.Model } }) : string.Empty;
            string image = !string.IsNullOrEmpty(Value.Model?.ToString()) ? _urlHelper.Action(new UrlActionContext { Action = "Get", Controller = "File", Values = new { Area = "", id = Value.Model, type=Id.Name } }) : string.Empty;
            output.Content.AppendHtml($"<input id=\"file{Id.Name}\" type=\"file\" class=\"file js-fileinput\" data-id-input=\"{Id.Name}\" data-deleteUrl=\"{deleteUrl}\" data-initialImage=\"{image}\" />");
        }
    }
}
