using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Models.Backend.Common;
using FusionCore.Models.Backend.Messages;
using FusionCore.Models.Backend.ToDo;
using FusionCore.Services;
using FusionCore.Services.Message;
using FusionCore.Services.ToDO;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Controllers
{
    public class DashboardController : FusionBaseController
    {
        private readonly IDashboardService _dashboardService;
        private readonly IMapper _mapper;
        private readonly IMessageService _messageService;
        private readonly IToDoService _toDoService;
        private readonly IUserService _userService;
        public DashboardController(IDashboardService dashboardService, IMapper mapper, IMessageService messageService, IToDoService toDoService, IUserService userService)
        {
            _dashboardService = dashboardService;
            _mapper = mapper;
            _messageService = messageService;
            _toDoService = toDoService;
            _userService = userService;
        }

        public IActionResult Index()
        {

            return View();
        }
        public JsonResult DataTable(RecentlyUpdatedDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();
            int length = 5;
            if (!string.IsNullOrEmpty(Request.Query["length"]))
            {
                length = Convert.ToInt32(Request.Query["length"]);
            }
            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Func<RecentlyUpdated, IComparable> orderBy;

            switch (sortColumn)
            {
                case "title":
                    orderBy = x => x.Title;
                    break;
                default:
                    orderBy = x => x.Updated;
                    //sortColumnDir = "desc";
                    break;
            }

            Expression<Func<RecentlyUpdated, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x => (
                        x.Title.ToLower().Contains(searchValue.ToLower()));

            var list = _dashboardService.GetRecentlyUpdated(pageIndex, length, pageSize, predicate);

            model.Data = _mapper.Map<List<RecentlyUpdated>>(list);
            model.RecordsFiltered = list.TotalCount;
            model.RecordsTotal = list.TotalCount;

            if (sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase))
            {
                model.Data = model.Data.OrderBy(orderBy).ToList();
            }
            else
            {
                model.Data = model.Data.OrderByDescending(orderBy).ToList();
            }

            return new JsonResult(model);
        }


        public IActionResult GetDashboardMessages(int pageindex)
        {
            var res = _messageService.GetDashboardChat(pageindex);
            return PartialView("GroupChatBox", res);
        }

        [HttpPost]
        public IActionResult SendMessage(GroupChatBox model)
        {
            _messageService.PostMessageinGroup(model);
            var res = _messageService.GetDashboardChat(0);
            return PartialView("GroupChatBox", res);
        }

        [HttpPost]
        public async Task<IActionResult> AddToDo(ToDoPersistModel model)
        {
            var userid = _userService.GetCurrentUserId();
            model.userId = userid;
            _toDoService.AddToDO(model);
            var res = _toDoService.GetToDO(model.userId);
            HttpContext.Session.SetComplexData("todolist", res);
            //return PartialView("ToDoList", res);
            var navtodo = res.Where(x => !x.done).ToList();
            NavigationTopModel navigationTopModel = new NavigationTopModel
            {
                AmountOfTasks = navtodo.Count(),
                todolist = navtodo
            };
            string dashboardtodo = await this.RenderViewToStringAsync("ToDoList", res);
            string navigationtoptodo = await this.RenderViewToStringAsync("_NavigationToDoList", navigationTopModel);

            return Json(new { dashboardtodo, navigationtoptodo });
        }

        public IActionResult GetToDoList()
        {
            //var userid = _userService.GetCurrentUserId();
            //var res = _toDoService.GetToDO(userid);
            var res = HttpContext.Session.GetComplexData<List<ToDoPersistModel>>("todolist");
            return PartialView("ToDoList", res);
        }

        public async Task<IActionResult> MarkasDone(int id)
        {
            var mark = _toDoService.MarkDone(id);
            var res = HttpContext.Session.GetComplexData<List<ToDoPersistModel>>("todolist");
            if (mark)
            {
                if (res != null && res.Any())
                {
                    var item = res.FirstOrDefault(x => x.Id == id);
                    if (item != null)
                    {
                        item.done = !item.done;

                        HttpContext.Session.SetComplexData("todolist", res);
                    }
                }
            }
            var navtodo = res.Where(x => !x.done).ToList();
            NavigationTopModel navigationTopModel = new NavigationTopModel
            {
                AmountOfTasks = navtodo.Count(),
                todolist = navtodo
            };
            string dashboardtodo = await this.RenderViewToStringAsync("ToDoList", res);
            string navigationtoptodo = await this.RenderViewToStringAsync("_NavigationToDoList", navigationTopModel);

            return Json(new { dashboardtodo, navigationtoptodo });
        }

        public async Task<IActionResult> DeleteToDo(int id)
        {
            var del = _toDoService.DeleteToDo(id);
            //var userid = _userService.GetCurrentUserId();
            //var res = _toDoService.GetToDO(userid);
            var res = HttpContext.Session.GetComplexData<List<ToDoPersistModel>>("todolist");
            if (del)
            {
                if (res != null && res.Any())
                {
                    var item = res.FirstOrDefault(x => x.Id == id);
                    if (item != null)
                    {
                        res.Remove(item);
                        HttpContext.Session.SetComplexData("todolist", res);
                    }
                }
            }

            //return PartialView("ToDoList", res);
            var navtodo = res.Where(x => !x.done).ToList();
            NavigationTopModel navigationTopModel = new NavigationTopModel
            {
                AmountOfTasks = navtodo.Count(),
                todolist = navtodo
            };
            string dashboardtodo = await this.RenderViewToStringAsync("ToDoList", res);
            string navigationtoptodo = await this.RenderViewToStringAsync("_NavigationToDoList", navigationTopModel);

            return Json(new { dashboardtodo, navigationtoptodo });
        }

        public IActionResult GetActiveMessageUsers()
        {
            var users = _messageService.GetActiveMessageUsers();
            return PartialView("_ProfileBarUsersList", users);
        }

        public async Task<IActionResult> GetUserMessages(int userid)
        {
            var res = _messageService.GetUserMessages(userid);
            _messageService.UpdateUserMessagesToRead(userid);
            int loginuserid = _userService.GetCurrentUserId();
            var messages = _messageService.ShowUnreadMessages(loginuserid);
            NavigationTopModel navigationTopModel = new NavigationTopModel
            {
                NewMessages = messages.Count,
                messageThreads = messages
            };
            string navmsgs = await this.RenderViewToStringAsync("_NavigationMessageList", navigationTopModel);
            string usermsgs = await this.RenderViewToStringAsync("_ProfileBarUserMessages", res);

            return Json(new { navmsgs, usermsgs });

        }
        public IActionResult UpdateUserMessagesToRead(int userid)
        {
            _messageService.UpdateUserMessagesToRead(userid);
            return Json(true);
        }

        [HttpPost]
        public IActionResult SendUserMessage(GroupChatBox model)
        {
            _messageService.PostMessageToUser(model);
            var res = _messageService.GetUserMessages(model.receiverid);
            return PartialView("_ProfileBarUserMessages", res);
        }

        public IActionResult GetUserHeaderMessages()
        {
            int userid = _userService.GetCurrentUserId();
            var messages = _messageService.ShowUnreadMessages(userid);
            NavigationTopModel navigationTopModel = new NavigationTopModel
            {
                NewMessages = messages.Count,
                messageThreads = messages
            };
            return PartialView("_NavigationMessageList", navigationTopModel);
        }

        public IActionResult DeleteMessage(int msgid,string type)
        {
            _messageService.DeleteMessage(msgid,type);
            return Json(true);
        }
    }
}