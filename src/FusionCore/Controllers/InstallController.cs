using FusionCore.Core.Extensions;
using FusionCore.Models.Common;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FusionCore.Controllers
{
    public class InstallController : BaseController
    {
        private readonly IInstallService _installService;
        private readonly IServiceProvider _serviceProvider;

        public InstallController(IInstallService installService, IServiceProvider serviceProvider)
        {
            _installService = installService;
            _serviceProvider = serviceProvider;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(InstallViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _installService.Install(_serviceProvider, model.InstallSampleData);
                _installService.RestartApplication();
            }

            return View(model);
        }
    }
}