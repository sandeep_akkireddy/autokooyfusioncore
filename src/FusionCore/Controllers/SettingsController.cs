using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Data.Settings;
using FusionCore.Models.Backend.Settings;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FusionCore.Controllers
{
    public class SettingsController : FusionBaseController
    {
        private readonly ISettingService _settingService;
        private readonly IMapper _mapper;

        public SettingsController(
            ISettingService settingService,
            IMapper mapper)
        {
            _settingService = settingService;
            _mapper = mapper;
        }

        public IActionResult Company()
        {
            CompanySettingsModel model = _mapper.Map<CompanySettingsModel>(_settingService.LoadSetting<CompanySettings>());
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Company(CompanySettingsModel model)
        {
            if (ModelState.IsValid)
            {
                CompanySettings dbSetting = _settingService.LoadSetting<CompanySettings>();
                CompanySettings setting = _mapper.Map(model, dbSetting);
                await _settingService.SaveSetting(setting);
                AddSuccessMessage("De wijzigingen zijn opgeslagen");
                return RedirectToAction("Company");
            }

            return View(model);
        }

        public IActionResult Website()
        {
            WebsiteSettings dbSetting = _settingService.LoadSetting<WebsiteSettings>();
            WebsiteSettingsModel model = _mapper.Map<WebsiteSettingsModel>(dbSetting);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Website(WebsiteSettingsModel model)
        {
            if (ModelState.IsValid)
            {
                WebsiteSettings dbSetting = _settingService.LoadSetting<WebsiteSettings>();
                WebsiteSettings setting = _mapper.Map(model, dbSetting);
                await _settingService.SaveSetting(setting);
                AddSuccessMessage("De wijzigingen zijn opgeslagen");
                return RedirectToAction("Website");
            }

            return View(model);
        }
    }
}