using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.Data.Entities;
using FusionCore.Models.Backend.FusionUser;
using FusionCore.Services.Common;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Controllers
{
    public class FusionUserController : FusionBaseController
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public FusionUserController(IUserService userService, IMapper mapper, IConfiguration configuration)
        {
            _userService = userService;
            _mapper = mapper;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult DataTable(FusionUserDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"].FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<User, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "email":
                    orderBy = x => x.Email;
                    break;
                case "createdOn":
                    orderBy = x => x.CreatedOnUtc;
                    break;
                case "status":
                    orderBy = x => x.Role;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<User, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
            {
                predicate = x =>
                    x.Firstname.ToLower().Contains(searchValue.ToLower())
                    || x.Lastname.ToLower().Contains(searchValue.ToLower())
                    || x.Email.ToLower().Contains(searchValue.ToLower())
                    ;
            }

            Models.Common.IPagedList<User> users = _userService.GetAllUsers(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<FusionUserRow>>(users);
            model.RecordsFiltered = users.TotalCount;
            model.RecordsTotal = users.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {
            FusionUserPersistModel model = new FusionUserPersistModel();
            model.UserAccess = new List<FusionUserAccessModel>();
            if (id.HasValue)
            {
                model = _mapper.Map<FusionUserPersistModel>(_userService.GetUser(id.Value));
                
                model.UserAccess = _mapper.Map<List<FusionUserAccessModel>>(_userService.GetUserAccess(id.Value));
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(FusionUserPersistModel model)
        {
            if (ModelState.IsValid)
            {
                var accessList = new Dictionary<int, bool>();
                foreach (string key in Request.Form.Keys)
                {
                    if (key.StartsWith("chkUA"))
                    {
                        var id = Convert.ToInt32(key.Split(Convert.ToChar("_"))[1]);
                        //var value = Request.Form["chkUA_" + id].ToString().ToLower().Trim() == "on";
                        accessList.Add(id, true);
                    }
                }

                var modules = CommonHelper.GetActiveModulesNames(_configuration.GetSection("MainSettings").Get<MainSettings>().FusionModules).Select(x => (int)x.module).ToList();
                modules.Where(x => !accessList.Any(z => z.Key == x)).ToList().ForEach(x =>
                {
                    accessList.Add(x, false);
                });

                if (_userService.Persist(model, accessList))
                    return Json(new { pagereload = true });
            }

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _userService.DeleteUser(id);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FileUpload(int Id)
        {
            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
            var file = HttpContext.Request.Form.Files;
            if (file != null && file.Any())
            {
                var formfile = file[0];
                var fileName = FileService.RemoveInvalidCharacters(formfile);
                using (var ms = new MemoryStream())
                {
                    await formfile.CopyToAsync(ms);
                    var fileBinary = ms.ToArray();

                    string fname = FileHelper.FilePath(fileSettings.UserFiles, fileName, fileBinary, false);
                    string filepath = fileSettings.UserFiles + fname;
                    FileHelper.savebitmap(ms, filepath, 500, 500, true);

                    return Ok(new { id = Id, filename = fname, modelname = "Photo" });
                }
            }
            return BadRequest();
        }
    }
}