﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Reflection;

namespace FusionCore.Controllers
{
    public class CommonController : Controller
    {
        [AllowAnonymous]
        public IActionResult Error()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetJs(string filename, string assemblyName)
        {
            Assembly assembly = Assembly.Load(assemblyName);
            Stream stream = assembly.GetManifestResourceStream($"{assemblyName}.{filename}");
            StreamReader reader = new StreamReader(stream);
            string text = reader.ReadToEnd();
            return Content(text, "application/javascript");
        }

        [HttpPost]
        public IActionResult Search(string query)
        {
            return RedirectToAction("Index", "Dashboard");
        }
    }
}