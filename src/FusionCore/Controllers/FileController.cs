using FusionCore.Core.Settings;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;

namespace FusionCore.Controllers
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [Authorize]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        private readonly IConfiguration _configuration;
        public FileController(IFileService fileService, IConfiguration configuration)
        {
            _fileService = fileService;
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile file_data, string eleid)
        {
            if (file_data != null)
            {
                int? dbFile = await _fileService.UploadFile(file_data, eleid);
                if (dbFile.HasValue)
                {
                    return Ok(new { id = dbFile.Value });
                }
            }

            return BadRequest();
        }

        public IActionResult Delete(int id)
        {
            Data.Entities.File file = _fileService.GetFile(id);
            if (file != null)
            {
                _fileService.DeleteFile(file);
            }

            return Ok(new { result = "deleted" });
        }

        public IActionResult Get(int id, string type)
        {
            Data.Entities.File file = _fileService.GetFile(id);
            if (file != null)
            {
                if (file.FileLocal != null)
                {
                    FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
                    MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

                    string imagepath = "";
                    if (type != null)
                    {
                        type = type.ToLower().Trim();
                        if (type.Contains("team"))
                        {
                            imagepath = (fileSettings.TeamsFiles + file.FileLocal);
                        }
                        else if (type.Contains("news"))
                        {
                            imagepath = (fileSettings.NewsFiles + file.FileLocal);
                        }
                        else if (type.Contains("splashimage") || type.Contains("thumbnailimage"))
                        {
                            imagepath = (fileSettings.FundFiles + file.FileLocal);
                        }
                        else if (type.Contains("bannerfileid"))
                        {
                            imagepath = (fileSettings.BannerFiles + file.FileLocal);
                        }
                        else if (type.Contains("support"))
                        {
                            imagepath = (fileSettings.SupportFiles + file.FileLocal);
                        }

                    }
                    byte[] binaryfile = System.IO.File.ReadAllBytes(imagepath);
                    return new FileStreamResult(new MemoryStream(binaryfile), file.ContentType);
                }
                else if (file.FileBinary != null)
                {
                    return new FileStreamResult(new MemoryStream(file.FileBinary), file.ContentType);
                }
            }

            return Content("");
        }
    }
}