using FusionCore.Core.Exceptions;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Models.Backend.Login;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Controllers
{
    public class LoginController : FusionBaseController
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        public IActionResult Index(string returnUrl = null)
        {
            if (_userService.GetCurrentUser() != null)
            {
                RedirectToAction("Index", "Dashboard");
            }

            IndexViewModel model = new IndexViewModel
            {
                LoginModel = new LoginModel
                {
                    ReturnUrl = returnUrl
                }
            };
            return View(model);
        }

        [AllowAnonymous]
        public IActionResult Logout()
        {
            _userService.SignOut();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Index(LoginModel model)
        {
            _userService.SignOut();
            if (ModelState.IsValid)
            {
                Data.Entities.User user = _userService.Validate(model.UserName, model.Password);

                if (user != null)
                {
                    _userService.SignIn(user, model.RememberMe);

                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }

                    return RedirectToAction("Index", "Dashboard");
                }
                ModelState.AddModelError("LoginModel", "De login gegevens zijn niet correct.");
            }

            IndexViewModel returnModel = new IndexViewModel
            {
                LoginModel = model
            };
            return View(returnModel);
        }

        [AllowAnonymous]
        public IActionResult Reset(string key, int uid)
        {
            if (!Guid.TryParse(key, out Guid parsedGuid))
            {
                return RedirectToAction("Index");
            }

            if (_userService.ValidateResetGuid(parsedGuid, uid))
            {
                ResetModel model = new ResetModel
                {
                    Guid = parsedGuid,
                    UserId = uid
                };
                return View(model);
            }
            AddErrorMessage("The information supplied is incorrect, or the reset link is no longer valid.");
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Reset(ResetModel model)
        {
            if (ModelState.IsValid)
            {
                Data.Entities.User user = _userService.GetUser(model.UserId);
                Data.Entities.Credential credential = user?.Credentials.FirstOrDefault(x => x.ResetKey == model.Guid.ToString());
                if (credential != null)
                {
                    (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
                    credential.Key = password.key;
                    credential.Salt = password.salt;
                    await _userService.SaveChangesAsync();
                    AddSuccessMessage("Het wachtwoord is aangepast. Log nu in met het nieuwe wachtwoord");
                }
            }
            else
            {
                AddErrorMessage("Het resetten van het wachtwoord is mislukt. Probeer opnieuw");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            _userService.SignOut();
            if (ModelState.IsValid)
            {
                try
                {
                    await _userService.RegisterUserAsync(model);
                    Data.Entities.User user = _userService.Validate(model.Email, model.Password);
                    if (user != null)
                    {
                        AddSuccessMessage("The account has been registered. The admin needs to enable your account before you can continue.");
                    }
                    else
                    {
                        AddErrorMessage("Registering the user failed.");
                    }
                }
                catch (FusionCoreException ex)
                {
                    AddErrorMessage(ex.Message);
                }
                return RedirectToAction("Index");
            }
            AddErrorMessage("The information you entered is incorrect.");
            return RedirectToAction("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Forgot(ForgotModel model)
        {
            _userService.SignOut();
            if (ModelState.IsValid)
            {
                try
                {
                    await _userService.SendPasswordResetEmail(model);
                    AddSuccessMessage("If the email address exists, an email will be sent to reset your password.");
                }
                catch (FusionCoreException ex)
                {
                    AddErrorMessage(ex.Message);
                }
                return RedirectToAction("Index");
            }
            AddErrorMessage("The information you entered is incorrect.");
            return RedirectToAction("Index");
        }
    }
}