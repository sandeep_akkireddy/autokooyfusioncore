using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FusionCore.Core.Enums;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Services.Common;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using FusionCore.Core.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;

namespace FusionCore.MiddleWare
{
    public class EventIdUrlRewritingMiddleware
    {
        private readonly RequestDelegate _next;


        //Your constructor will have the dependencies needed for database access
        public EventIdUrlRewritingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var path = context.Request.Path.ToUriComponent();

            if (!PathIsEventId(path, context)
                && path != "/"
                && !path.ToLower().Contains("/dashboard")
                && !path.ToLower().Contains("/datatable")
                && !path.ToLower().Contains("/uploads/")
                && !path.ToLower().Contains("/file/")
                && !path.ToLower().Contains("/fusionuser/")
                && !path.ToLower().Contains("/login"))
            {
                //If is an eventId, change the request path to be "Event/Index/{path}" so it is handled by the event controller, index action
                context.Request.Path = "/";
            }

            //Let the next middleware (MVC routing) handle the request
            //In case the path was updated, the MVC routing will see the updated path
            await _next.Invoke(context);

        }

        private bool PathIsEventId(string path, HttpContext httpContext)
        {
            //The real midleware will try to find an event in the database that matches the current path

            if (!string.IsNullOrEmpty(path))
            {
                var menu = httpContext.Session.GetComplexData<List<MenuService.MenuItemForCaching>>("menu");
                if (menu != null)
                {
                    return (menu.Any(x => x.Url != null && path.ToLower().Trim().Contains(x.Url.ToLower().Trim())) || menu.Any(m => m.Children != null && m.Children.Any(x => x.Url != null && path.ToLower().Trim().Contains(x.Url.ToLower().Trim()))));
                }
                return true;
            }
            return true;
        }
    }
}
