var Modals = function () {
  var createModal = function (title, url, cssClass, callbackFunction, afterloadFunction) {
    var $modal = $("#js-defaultModal");
    var uuid = Date.now();
    //Duplicate html
    var modalId = "js-defaultModal-" + uuid;
    var headerId = "js-defaultModalHeader-" + uuid;
    var bodyId = "js-defaultModalBody-" + uuid;
    var modalHtml = $modal.wrap('<p/>').parent().html();
    $modal.unwrap();
    modalHtml = modalHtml.replace("js-defaultModalHeader", headerId);
    modalHtml = modalHtml.replace("js-defaultModalBody", bodyId);
    modalHtml = modalHtml.replace("js-defaultModal", modalId);

    var $copy = $(modalHtml);
    $copy.insertBefore($modal);


    $("#" + headerId).html(title);
    var $modalBody = $("#" + bodyId);
    $copy.find(".modal-dialog").addClass(cssClass);
    $.get(url, function (data) {
      $modalBody.html(data);
      Main.runModuleTools();
      if ($modalBody.find("[data-toggle='toggle']").length > 0) {
        $modalBody.find("[data-toggle='toggle']").bootstrapToggle();
      }
      var $bodyForm = $modalBody.find("form");
      $bodyForm.validate({
        ignore: ".ignore, :hidden",
        submitHandler: function (form) {

          for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
            // console.log(CKEDITOR.instances[instance].getData());
          }

          var $form = $(form);
          var data = $form.serialize();

          $.ajax({
            type: "POST",
            url: $form.attr("action"),
            data: data,
            beforeSend: function (xhr) {
              xhr.setRequestHeader("RequestVerificationToken",
                $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (returnData) {

              if (typeof (returnData) === 'object') {

                if (returnData.result) {
                  $copy.modal('hide');
                  callbackFunction();
                } else if (returnData.pagereload) {
                  window.location.reload();
                } else {
                  //nothing
                  var errors = returnData.errors;
                  $modalBody.find('.panel-body .alert.alert-danger').remove();
                  $modalBody.find('.panel-body').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + errors + '</div>');
                }
              }
              else if (returnData) {
                $copy.modal('hide');
                callbackFunction();
              }
            }
          });
          return false;
        }
      });
      $bodyForm.removeData('validator');
      $bodyForm.removeData('unobtrusiveValidation');
      $.validator.unobtrusive.parse($bodyForm);
      $copy.modal('show');
      Main.runLoadContent();
      if ($(".js-load-content").length === 0) {
        Main.coupleDynamics(modalId);
      }
      Main.runCustomCheck();
      FormElements.runTimePicker();
      FormElements.runColorPickerComplex();
      FormElements.runDateRangePicker();
      FormElements.runDatePicker();
      $copy.on('hidden.bs.modal',
        function () {
          $copy.find('.dataTable').each(function (i, elm) {
            $(elm).dataTable({
              "destroy": true
            });
          });
          if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
          }
          $copy.remove();
        });
      if (afterloadFunction) {
        afterloadFunction();
      }
    });
  };

  return {
    CreateModal: createModal
  };
}();