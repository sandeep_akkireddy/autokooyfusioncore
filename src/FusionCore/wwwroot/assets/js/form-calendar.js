var Calendar = function () {
    //function to initiate Full CAlendar
    var runCalendar = function () {
        var $modal = $('#event-management');
        $('#event-categories div.event-category').each(function () {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 50 //  original position after the drag
            });
        });
        /* initialize the calendar
				 -----------------------------------------------------------------*/
        var form = '';
	    var $calendar = $('#calendar');
	    var calendar = $calendar.fullCalendar({
            themeButtonIcons: {
                prev: '<i class="fa fa-angle-left"></i>',
                next: '<i class="fa fa-angle-right"></i>'
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
			},
			locale: "nl",
            events: $calendar.data('items'),
			weekNumbers: true,
			defaultView: 'month',
            editable: true,
            selectable: true,
			select: function (start, end, allDay) {
				Modals.CreateModal("Agenda item", $calendar.data("url") + "&start=" + start.format('MM/DD/YYYY') + "&end=" + end.format('MM/DD/YYYY'), "", function () {
					calendar.fullCalendar('refetchEvents');
				});
            },
			eventClick: function (calEvent, jsEvent, view) {
				Modals.CreateModal("Agenda item", $calendar.data("url") + "&id=" + calEvent.id, "", function () {
					calendar.fullCalendar('refetchEvents');
				});
            },
            eventDrop: function (event, delta, revertFunc) {
	            SaveEvent(event);
            },
            eventResize: function (event, delta, revertFunc) {
	            SaveEvent(event);
            }
		});
    };

	
	SaveEvent = function(event) {
	
		var start = event.start;
		var end = event.end;
		start = new Date(start);
		var startDate = start.getFullYear() + '-' + (start.getMonth() + 1) + '-' + start.getDate() + ' ' + start.getHours() + ':' + start.getMinutes();
		var endDate = null;
		if (end !== null) {
			end = new Date(end);
			endDate = end.getFullYear() + '-' + (end.getMonth() + 1) + '-' + end.getDate() + ' ' + end.getHours() + ':' + end.getMinutes();
		}
		var $calendar = $('#calendar');
		$.ajax({
			url: $calendar.data('updateurl'),
			data: {
				id: event.id,
				start: startDate,
				end: endDate,
				resourceId: event.resourceId !== null ? event.resourceId : event.resourceIds
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader("RequestVerificationToken",
					$('input:hidden[name="__RequestVerificationToken"]').val());
			},
			type: "POST",
			async: false
		});
		$calendar.fullCalendar('refetchEvents');
	}
    return {
        init: function () {
            runCalendar();
        }
    };
}();