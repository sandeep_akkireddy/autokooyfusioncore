const connection = new signalR.HubConnectionBuilder()
  .withUrl("/chatHub")
  .build();

//This method receive the message and Append to our list
connection.on("ReceiveMessage", (user, message) => {
  const loginuserid = $("#navtopuserimage").attr("data-navtopuserid");

  const response = JSON.parse(message);

  if (loginuserid != response.senderid) {

    if (response.isdelete == "true") {

      DeleteMessage(response.group, response.msgid);
    }
    else {
      const msg = response.message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
      if (response.group == "dashboard") {

        DashboardMessageTrigger(response.sendername, response.senderimage, msg, response.msgid);

      }
      else if (response.receiverid != "" && !isNaN(response.receiverid)) {

        UserMessageTrigger(response.sendername, response.senderimage, msg, response.senderid, response.msgid);

      }
    }
  }
});

connection.start().catch(err => console.error(err.toString()));

//Send the message
$("#buttonchat").on("click", function (event) {

  const user = "";//const only assign value at declaration time. cant change value later

  var sentuserid = $(this).attr("data-senderid");
  var sentusername = $(this).attr("data-sendername");
  var sentuserimage = $(this).attr("data-senderimage");
  var group = $(this).attr("data-group");
  var msg = $(this).attr("data-msg");
  var msgreceiverid = $(this).attr("data-receiverid");
  var msg_id = $(this).attr("data-msgid");
  var is_delete = $(this).attr("data-isdelete");



  const message = JSON.stringify({ "senderimage": sentuserimage, "message": msg, "group": group, senderid: sentuserid, "sendername": sentusername, receiverid: msgreceiverid, msgid: msg_id, "isdelete": is_delete });



  connection.invoke("SendMessage", user, message).catch(err => console.error(err.toString()));
  event.preventDefault();

});

function DashboardMessageTrigger(sendername, senderphoto, message, msgid) {

  var isscrolldown = false;
  if ($("#msgs .panel-scroll").length) {
    var element = $('#msgs .panel-scroll');
    var maxScrollBottom = Math.ceil(element[0].scrollHeight - element.outerHeight());
    var scrolltop = Math.ceil(element.scrollTop());
    isscrolldown = (maxScrollBottom == scrolltop);

  }

  const msghtml = "<li class='other' data-msgid='" + msgid + "'>" +
    "<div class='avatar'><img alt='" + sendername + "' src='" + senderphoto + "' style='height: 50px;width: 50px;'/></div>" +
    "<div class='messages'>" +
    "<p>" + message + "</p>" +
    "</div>" +
    "</li>";
  $("#dashboardgroupchart").append(msghtml);

  if (isscrolldown) {
    var element = $('#msgs .panel-scroll');
    var maxScrollBottom = element[0].scrollHeight - element.outerHeight();
    var scrolltop = element.scrollTop();//becasue of this line below line is working
    element.scrollTop(maxScrollBottom);
  }

}

function UserMessageTrigger(sendername, senderphoto, message, senderid, msgid) {
  if ($("#page-sidebar .sb-toggle").hasClass("open")) {

    var panel_right_style = $("#page-sidebar #users").css("right");

    if (panel_right_style == "0px") {
      UnReadMessageCountInUsersList(sendername, senderphoto, message, senderid);
    }
    else if (panel_right_style == ($("#page-sidebar").outerWidth() + "px") && $("#page-sidebar").outerWidth() > 0) {

      var currentchatinguser = $("#usermsgbackbutton").attr("data-userid");

      if (currentchatinguser != "" && !isNaN(currentchatinguser) && currentchatinguser != senderid) {

        UnReadMessageCountInUsersList(sendername, senderphoto, message, senderid);
      } else {
        //current chatting user window
        var msgtemplate = "<li class='other' data-msgid='" + msgid + "'>" +
          "<div class='avatar'> <img alt=" + sendername + " src=" + senderphoto + " style='height: 50px;width: 50px;'/></div>" +
          "<div class='messages'>" +
          "<p>" + message + "</p>" +
          "</div>" +
          "</li>";
        $("#userchatdiscussion").prepend(msgtemplate);
        //mark message as read in db
        AjaxCall("/dashboard/UpdateUserMessagesToRead?userid=" + senderid).done(function (response) { });
      }


    }
  }
  else {
    LoadHeaderNavMessages();
  }
}

function LoadHeaderNavMessages() {
  AjaxCall("/dashboard/GetUserHeaderMessages").done(function (response) {
    $("#navlinewmsglist").html(response);
    NavbarMessageEvents();
  });
}

function UnReadMessageCountInUsersList(sendername, senderphoto, message, senderid) {
  var $offlineuser = $("#offlineuserslist").find("[data-userid='" + senderid + "']");
  if ($offlineuser.length) {


    $($($offlineuser[0]).find(".status-online")[0]).removeClass("hide");

    if (!$("#onlineuserslist").length) {

      var addonline = "<h5 class='sidebar-title'>On-line</h5>" +
        "<ul class='media-list' id='onlineuserslist'>" +
        "</ul>";

      $("#profilebaruserslist").prepend(addonline);
    }

    $("#onlineuserslist").prepend("<li class='media'>" + $($($offlineuser[0]).parent()).html() + "</li>");

    $("#offlineuserslist").find($offlineuser[0]).parent().remove();

  }

  var $onlineuser = $("#onlineuserslist").find("[data-userid='" + senderid + "']");

  if ($onlineuser.length) {
    var $msgcountlbl = $($onlineuser[0]).find(".label-success");
    var count = 0;
    if ($msgcountlbl.length) {
      var lblcount = $($msgcountlbl[0]).text();
      if (lblcount != "" && !isNaN(lblcount)) {
        count = parseInt(lblcount);
      }
      count = count + 1;
      $($msgcountlbl[0]).text(count);
    }
    else {
      var label = "<div class='user-label'>" +
        "<span class='label label-success'>1</span>" +
        "</div>";
      $($onlineuser[0]).prepend(label);
    }

  }

  LoadHeaderNavMessages();
}

function DeleteMessage(group, msgid) {
  var element;
  if (group == "dashboard") {
    element = $('#msgs .panel-scroll');

  }
  else {
    element = $("#page-sidebar .sidebar-wrapper");
  }
  if ($(".other[data-msgid='" + msgid + "']").length) {

    var scrolltop = element.scrollTop();

    var elehight = $(".other[data-msgid='" + msgid + "']").height();

    $(".other[data-msgid='" + msgid + "']").remove();
    element.scrollTop(scrolltop - elehight);
  }
}