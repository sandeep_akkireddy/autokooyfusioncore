/* List menu URL */
$(document).ready(function() {
	$("a.toggler").parent("li").children(".wrapper").hide();

	$('a.navigation-head').each(function () {
	    openMenu($(this));
	});
    
	$('a.toggler').each(function () {
	    openMenu($(this));
	});
	
	if ($("#sidebar-right").length || $("#sidebar-left").length) {
		$(".sticky").stickyScroll({
			container: "#sidebar-right, #sidebar-left"
		}); 
	}
	$(".saveButton").each(function (i, elm) {
	    $(elm).click(function () {
	        $(elm).closest("form").submit();
	    });
	});
	$(".link-button.green").each(function (i, elm) {
	    
	        $(elm).click(function() {
	            $(elm).closest("form").submit();
	        });
	});
	$(".link-button.orange").each(function (i, elm) {
	    $(elm).click(function () {
	        $(elm).closest("form").submit();
	    });
	});
    $(".button.red").find("[type=button]").click(function() {
        var url = document.URL;
        var arr = url.replace("http://","").split("/");
        window.location.href = "/"+arr[1]+"/";
    });

	$("#saveFile").click(function () {
	    $("#saveFile").closest("form").submit();
	});
    
	$("#saveFile2").click(function () {
	    $("#saveFile2").closest("form").submit();
	});
	$(".resendRapportage").click(function (event) {
	    event.preventDefault();
	    var id = $(this).data().id;
	    var totalData = { "id": id };
	    var json = JSON.stringify(totalData);
	    $.ajax({
	        url: "/fusionmedia/controle/ResendPlanning",
	        type: 'POST',
	        data: json,
	        contentType: 'application/json; charset=utf-8',
	        success: function (result) {
	            if (result === true) {
	                window.location.href = window.location.href;
	            }
	        }
	    });
	});
    $("#navigation").find("select").each(function(i, elm) {
        $(elm).change(function() {
            $(elm).closest('form').submit();
        });
    });

    $(".deleteimage").each(function (i, elm) {
        
        $(elm).click(function(event) {
            event.preventDefault();
            $(elm).parent().find('input[type="hidden"][name="Image"]').val('');
            $(elm).parent().parent().hide();
        } );
    });
});

function openMenu(elem) {
    var toggler = $(elem);
    var li = toggler.parent('li');
    var wrapper = li.children('.wrapper');

    if (!li.hasClass('open')) {
        wrapper.hide();
    }

    var current = $("#currentOpen").val();
    if (current === toggler.data("id") && current !== undefined) {
        toggler.parent("li").children(".wrapper").show();
        toggler.parent("li").addClass('open');
    }

    toggler.click(function () {
        if (li.hasClass('open')) {
            li.removeClass('open');
            wrapper.slideUp('fast');
        } else {
            li.addClass('open');
            wrapper.slideDown('fast');
        }
    });
}

function swapElements(obj1, obj2) {

  // create marker element and insert it where obj1 is
  var temp = document.createElement("div");
  obj1.parentNode.insertBefore(temp, obj1);

  // move obj1 to right before obj2
  obj2.parentNode.insertBefore(obj1, obj2);

  // move obj2 to right before where obj1 used to be
  temp.parentNode.insertBefore(obj2, temp);

  // remove temporary marker node
  temp.parentNode.removeChild(temp);
}

