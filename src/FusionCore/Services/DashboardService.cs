using FusionCore.Core.Enums;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.NewsManager.Data.Entities;
using FusionCore.Modules.SupportManager.Data.Entities;
using FusionCore.Modules.TeamManager.Data.Entities;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FusionCore.Services
{
    public interface IDashboardService : IBaseService
    {
        IPagedList<RecentlyUpdated> GetRecentlyUpdated(int pageIndex, int n = 5, int pageSize = int.MaxValue, Expression<Func<RecentlyUpdated, bool>> predicate = null);
    }
    public class RecentlyUpdatedDataTable : BaseDataTable
    {
        public List<RecentlyUpdated> Data { get; set; }
    }
    public class RecentlyUpdated
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime Updated { get; set; }
        public string Pagetype { get; set; }
        public bool Active { get; set; }
    }

    public class DashboardService : BaseService, IDashboardService
    {
        private readonly IGenericRepository<Funds> _fundRepository;
        private readonly IGenericRepository<Banner> _bannerRepository;
        private readonly IGenericRepository<Page> _pageRepository;
        private readonly IGenericRepository<News> _newsRepository;
        private readonly IGenericRepository<Teams> _teamRepository;
        private readonly IGenericRepository<Support> _supportRepository;

        private readonly IUserService _userService;
        public DashboardService(IGenericRepository<Funds> fundRepository,
             IGenericRepository<Banner> bannerRepository,
             IGenericRepository<Page> pageRepository,
             IGenericRepository<News> newsRepository,
             IGenericRepository<Teams> teamRepository,
             IGenericRepository<Support> supportRepository,
             IUserService userService)
        {
            _fundRepository = fundRepository;
            _bannerRepository = bannerRepository;
            _pageRepository = pageRepository;
            _newsRepository = newsRepository;
            _teamRepository = teamRepository;
            _supportRepository = supportRepository;
            _userService = userService;
        }

        public IPagedList<RecentlyUpdated> GetRecentlyUpdated(int pageIndex, int n = 5, int pageSize = int.MaxValue, Expression<Func<RecentlyUpdated, bool>> predicate = null)
        {
            List<RecentlyUpdated> recentlyUpdated = new List<RecentlyUpdated>();

            var funds = _fundRepository.GetAll().OrderByDescending(x => x.UpdatedOnUtc).Take(n).Select(x => new RecentlyUpdated { ID = x.Id, Pagetype = "fund", Title = x.Title, Updated = x.UpdatedOnUtc, Active = x.Active });
            var banner = _bannerRepository.GetAll().OrderByDescending(x => x.UpdatedOnUtc).Take(n).Select(x => new RecentlyUpdated { ID = x.Id, Pagetype = "banner", Title = x.Title, Updated = x.UpdatedOnUtc, Active = x.Active });
            var page = _pageRepository.GetAll(x => !x.IsAdmin).OrderByDescending(x => x.UpdatedOnUtc).Take(n).Select(x => new RecentlyUpdated { ID = x.Id, Pagetype = "page", Title = x.Title, Updated = x.UpdatedOnUtc, Active = x.Active });
            var adminpage = _pageRepository.GetAll(x => x.IsAdmin).OrderByDescending(x => x.UpdatedOnUtc).Take(n).Select(x => new RecentlyUpdated { ID = x.Id, Pagetype = "adminpage", Title = x.Title, Updated = x.UpdatedOnUtc, Active = x.Active });
            var news = _newsRepository.GetAll().OrderByDescending(x => x.UpdatedOnUtc).Take(n).Select(x => new RecentlyUpdated { ID = x.Id, Pagetype = "news", Title = x.Title, Updated = x.UpdatedOnUtc, Active = x.Active });
            var teams = _teamRepository.GetAll().OrderByDescending(x => x.UpdatedOnUtc).Take(n).Select(x => new RecentlyUpdated { ID = x.Id, Pagetype = "team", Title = x.Name, Updated = x.UpdatedOnUtc, Active = x.IsActive });
            var support= _supportRepository.GetAll().OrderByDescending(x => x.UpdatedOnUtc).Take(n).Select(x => new RecentlyUpdated { ID = x.Id, Pagetype = "support", Title = x.Title, Updated = x.UpdatedOnUtc, Active = x.Active });

            if (predicate != null)
            {
                funds = funds.Where(predicate);
                banner = banner.Where(predicate);
                page = page.Where(predicate);
                adminpage = adminpage.Where(predicate);
                news = news.Where(predicate);
                teams = teams.Where(predicate);
                support = support.Where(predicate);
            }
            List<UserAccess> useraccess = _userService.GetUserAccess(_userService.GetCurrentUserId());
            if (useraccess.Any(x => x.Active && x.FusionModule == (int)FusionModule.Fondsen))
            {
                recentlyUpdated.AddRange(funds.ToList());
            }
            if (useraccess.Any(x => x.Active && x.FusionModule == (int)FusionModule.Banner))
            {
                recentlyUpdated.AddRange(banner.ToList());
            }
            if (useraccess.Any(x => x.Active && x.FusionModule == (int)FusionModule.Paginas))
            {
                recentlyUpdated.AddRange(page.ToList());
            }
            if (useraccess.Any(x => x.Active && x.FusionModule == (int)FusionModule.Beheerder))
            {
                recentlyUpdated.AddRange(adminpage.ToList());
            }
            if (useraccess.Any(x => x.Active && x.FusionModule == (int)FusionModule.Nieuws))
            {
                recentlyUpdated.AddRange(news.ToList());
            }
            if (useraccess.Any(x => x.Active && x.FusionModule == (int)FusionModule.Team))
            {
                recentlyUpdated.AddRange(teams.ToList());
            }
            if (useraccess.Any(x => x.Active && x.FusionModule == (int)FusionModule.Support))
            {
                recentlyUpdated.AddRange(support.ToList());
            }

            return new PagedList<RecentlyUpdated>(recentlyUpdated.OrderByDescending(x => x.Updated).Take(n).ToList(), pageIndex, pageSize);
        }

    }
}
