using System.Diagnostics.CodeAnalysis;

namespace FusionCore
{
	[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
	public static class FusionCoreModulesDummy
	{
		public static void Start()
		{
			//Dummy needed to spin up referenced modules
			//https://stackoverflow.com/questions/26733/getting-all-types-that-implement-an-interface
			new FusionCore.Core.CoreModule();
			new FusionCore.Services.ServicesModule();
			new FusionCore.Modules.InstallationManager.InstallationManagerModule();
			//new FusionCore.Modules.PageManager.PageManagerModule();
			new FusionCore.Modules.TeamManager.TeamManagerModule();
            new FusionCore.Modules.NewsManager.NewsManagerModule();
            new FusionCore.Modules.FundsManager.FundsManagerModule();
            //new FusionCore.Modules.BannerManager.BannerManagerModule();
            new FusionCore.Modules.SupportManager.SupportManagerModule();
        }
	}
}
