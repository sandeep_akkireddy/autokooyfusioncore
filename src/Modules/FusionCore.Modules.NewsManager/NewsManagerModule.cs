using FusionCore.Core.Interfaces;
using System;

namespace FusionCore.Modules.NewsManager
{
    public class NewsManagerModule : IFusionCoreModule
    {
            public string GetSystemName()
            {
                return "FusionCore.Module.NewsManager";
            }
        
    }
}
