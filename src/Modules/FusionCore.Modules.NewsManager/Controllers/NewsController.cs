using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.NewsManager.Data.Entities;
using FusionCore.Modules.NewsManager.Interfaces;
using FusionCore.Modules.NewsManager.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.NewsManager.Controllers
{
    [Area("NewsManager")]
    public class NewsController : FusionBaseController
    {
        private readonly INewsService _newsService;
        private readonly IMapper _mapper;

        public NewsController(INewsService newsService, IMapper mapper)
        {
            _newsService = newsService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult DataTable(NewsDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Expression<Func<News, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "title":
                    orderBy = x => x.Title;
                    break;
                default:
                    orderBy = x => x.NewsDate;
                    //sortColumnDir = "desc";
                    break;
            }

            Expression<Func<News, bool>> predicate=null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>(
                        x.Title.ToLower().Contains(searchValue.ToLower())
                        || x.NewsDate.Value.ToLongDateString().Contains(searchValue.ToLower()));

            var pages = _newsService.GetAllNews(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<NewsDataRow>>(pages);
            model.RecordsFiltered = pages.TotalCount;
            model.RecordsTotal = pages.TotalCount;
            return new JsonResult(model);
        }

        [HttpPost]
        public IActionResult Active(int id, bool value)
        {
            return Json(_newsService.ActiveNews(id, value));
        }
        public IActionResult Get(int? id)
        {
            var model = id.HasValue ? _mapper.Map<NewsPersistModel>(_newsService.GetNews(id.Value)) : new NewsPersistModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(NewsPersistModel model)
        {
            if (ModelState.IsValid) return Json(_newsService.PersistNews(model));

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _newsService.DeleteNews(id);

            return RedirectToAction("Index");
        }

    }
}
