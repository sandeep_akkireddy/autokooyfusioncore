using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.NewsManager.Data.Entities;
using FusionCore.Modules.NewsManager.Interfaces;
using FusionCore.Modules.NewsManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.NewsManager.Services
{
    public class NewsService : BaseService, INewsService
    {
        private readonly IGenericRepository<News> _newsRepository;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<NewsIPAddressLikes> _newsIPAddressRepository;

        public NewsService(
            IGenericRepository<News> newsRepository,
            IMapper mapper,
            IGenericRepository<NewsIPAddressLikes> newsIPAddressRepository)
        {
            _newsRepository = newsRepository;
            _mapper = mapper;
            _newsIPAddressRepository = newsIPAddressRepository;
        }
        public News GetNews(int id)
        {
            return _newsRepository.Get(x => x.Id == id);
        }
        public bool ActiveNews(int id, bool value)
        {
            var dbEntity = GetNews(id);
            if (dbEntity != null)
            {
                dbEntity.Active = value;
                _newsRepository.Update(dbEntity);

                return true;
            }
            return false;
        }

        public async Task DeleteNews(int id)
        {
            var news = await _newsRepository.GetByIdAsync(id);
            if (news != null)
            {
                var newslikes = _newsIPAddressRepository.GetAll(x => x.NewsId == news.Id).ToList();
                if (newslikes != null && newslikes
                    .Any())
                {
                    _newsIPAddressRepository.DeleteAll(newslikes);
                }
                _newsRepository.Delete(news);
            }
        }

        public IPagedList<News> GetAllNews<TKey>(Expression<Func<News, bool>> predicate = null, Expression<Func<News, TKey>> orderBy = null, bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var queryable = _newsRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<News>(queryable, pageIndex, pageSize);
        }

        public bool PersistNews(NewsPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                var dbEntity = GetNews(model.Id.Value);
                if (dbEntity != null)
                {
                    dbEntity.Active = model.Active;
                    dbEntity.Content = model.Content;
                    dbEntity.YouTubeLink = model.YouTubeLink;
                    dbEntity.Title = model.Title;
                    dbEntity.NewsDate = model.NewsDate;
                    dbEntity.UpdatedOnUtc = DateTime.Now;
                    dbEntity.NewsLogoId = model.NewsLogoId;
                    dbEntity.FileLink = model.FileLink;

                    _newsRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                var dbEntity = new News();
                _mapper.Map(model, dbEntity);
                _newsRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }


        public bool UpdateNewsLikes(int id, string ipaddress)
        {
            var dbEntity = GetNews(id);
            if (dbEntity != null)
            {
                var newslikes = _newsIPAddressRepository.GetAll();
                if (!newslikes.Any(x => x.IPAddress == ipaddress && x.NewsId == id))
                {
                    NewsIPAddressLikes newsIPAddressLikes = new NewsIPAddressLikes();
                    newsIPAddressLikes.IPAddress = ipaddress;
                    newsIPAddressLikes.NewsId = id;
                    _newsIPAddressRepository.Insert(newsIPAddressLikes);

                    dbEntity.Likes += 1;
                    _newsRepository.Update(dbEntity);

                    return true;
                }
                else { return false; }
            }
            return false;
        }
    }
}
