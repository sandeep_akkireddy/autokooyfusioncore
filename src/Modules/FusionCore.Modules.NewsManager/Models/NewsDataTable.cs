using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.NewsManager.Models
{
    public class NewsDataTable : BaseDataTable
    {
        public List<NewsDataRow> Data { get; set; }
    }
    public class NewsDataRow
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime? NewsDate { get; set; }
        public bool Active { get; set; }

    }
}
