using FusionCore.Data.Entities;
using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.NewsManager.Models
{
    public class NewsPersistModel : BaseModel
    {
        [Required]
        [MaxLength(256)]
        public string Title { get; set; }
        [Required]
        public DateTime? NewsDate { get; set; }

        public string Content { get; set; }

        public bool Active { get; set; }
        [RegularExpression(@"((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)")]

        public string YouTubeLink { get; set; }

        public string MetaTitle { get; set; }

        public string MetaDescription { get; set; }

        public string MetaKeywords { get; set; }

        public int? NewsLogoId { get; set; }

        public int Likes { get; set; }
        [DataType(DataType.Url)]
        public string FileLink { get; set; }
    }
}
