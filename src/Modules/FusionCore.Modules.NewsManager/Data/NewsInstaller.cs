using FusionCore.Core.Enums;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.NewsManager.Data.Entities;
using FusionCore.Modules.NewsManager.Interfaces;
using FusionCore.Modules.NewsManager.Models;
using FusionCore.Services.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.NewsManager.Data
{
    public class NewsInstaller : IInstallData
    {
        private INewsService _newsService;

        private IGenericRepository<News> _newsRepository;

        public async Task Install(IServiceProvider serviceProvider, bool sampleData)
        {
            LoadServices(serviceProvider);
            await InstallNewsManagerMenu(serviceProvider);
            if (sampleData)
            {
                InstallNewsDummyData();
            }
            DisposeServices();
        }
        private void DisposeServices()
        {
            _newsService = null;

        }
        private void LoadServices(IServiceProvider serviceProvider)
        {
            _newsService = (INewsService)serviceProvider.GetService(typeof(INewsService));

        }
        private void InstallNewsDummyData()
        {
            for (var i = 0; i < 16; i++)
            {
                _newsService.PersistNews(new NewsPersistModel
                {
                    Title = "News - Novi Media " + i,
                    Active = true,
                    MetaTitle = "test",
                    MetaDescription = "test",
                    MetaKeywords = "test",
                    NewsDate = DateTime.Now
                });
            }
        }


        private async Task InstallNewsManagerMenu(IServiceProvider serviceProvider)
        {
            var menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

            var mainMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "clip-stack",
                Name = "Nieuws",
                Area = "NewsManager",
                Controller = "News",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 0,
                ParentId = null,
                HasNewButton = true
            };
            await menuService.InsertMenuItem(mainMenuItem);

        }


    }
}
