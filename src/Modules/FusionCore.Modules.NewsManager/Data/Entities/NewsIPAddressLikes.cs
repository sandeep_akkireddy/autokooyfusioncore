using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.NewsManager.Data.Entities
{
    public class NewsIPAddressLikes : BaseEntity
    {
        public string IPAddress { get; set; }
        public int? NewsId { get; set; }

        public virtual News News { get; set; }

    }
}
