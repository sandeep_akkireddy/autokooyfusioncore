using FusionCore.Modules.NewsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.NewsManager.Data.Maps
{
    public class NewsIPAddressLikesMap : IEntityTypeConfiguration<NewsIPAddressLikes>
    {
        public void Configure(EntityTypeBuilder<NewsIPAddressLikes> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.IPAddress);
            builder.HasOne(x => x.News).WithMany().HasForeignKey(x => x.NewsId);

        }
    }
}
