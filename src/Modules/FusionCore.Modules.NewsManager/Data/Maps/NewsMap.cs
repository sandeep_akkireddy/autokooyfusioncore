using FusionCore.Modules.NewsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.NewsManager.Data.Maps
{
    public class NewsMap : IEntityTypeConfiguration<News>
    {
        public void Configure(EntityTypeBuilder<News> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Title).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Content);
            builder.Property(x => x.Active);
            builder.Property(x => x.MetaTitle);
            builder.Property(x => x.MetaDescription);
            builder.Property(x => x.MetaKeywords);
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.YouTubeLink);
            builder.Property(x => x.NewsDate);
            builder.Property(x => x.NewsLogoId);
            builder.Property(x => x.Likes);
            builder.Property(x => x.FileLink);
        }
    }
}
