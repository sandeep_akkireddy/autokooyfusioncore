using AutoMapper;
using FusionCore.Modules.NewsManager.Data.Entities;
using FusionCore.Modules.NewsManager.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.NewsManager.Data
{
    public class AutomapperConfiguration : Profile
    {
        public AutomapperConfiguration()
        {
            CreateMap<News, NewsDataRow>();

            CreateMap<News, NewsPersistModel>();
            CreateMap<NewsPersistModel, News>();
        }
    }
}
