using FusionCore.Models.Common;
using FusionCore.Modules.NewsManager.Data.Entities;
using FusionCore.Modules.NewsManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.NewsManager.Interfaces
{
    public interface INewsService : IBaseService
    {
        IPagedList<News> GetAllNews<TKey>(
            Expression<Func<News, bool>> predicate = null,
            Expression<Func<News, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        News GetNews(int id);
        bool PersistNews(NewsPersistModel model);
        Task DeleteNews(int id);

        bool ActiveNews(int id, bool value);

        bool UpdateNewsLikes(int id,string ipaddress);

    }
}
