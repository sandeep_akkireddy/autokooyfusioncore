using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.SupportManager.Data.Entities;
using FusionCore.Modules.SupportManager.Interfaces;
using FusionCore.Modules.SupportManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.SupportManager.Services
{
    public class SupportService : BaseService, ISupportService
    {
        private readonly IGenericRepository<Support> _supportRepository;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<SupportIpAddressLikes> _supportIPAddressRepository;
        

        public SupportService(
            IGenericRepository<Support> supportRepository,
            IMapper mapper,
            IGenericRepository<SupportIpAddressLikes> supportIPAddressRepository)
        {
            _supportRepository = supportRepository;
            _mapper = mapper;
            _supportIPAddressRepository = supportIPAddressRepository;
        }
        public bool ActiveSupport(int id, bool value)
        {
            var dbEntity = GetSupport(id);
            if (dbEntity != null)
            {
                dbEntity.Active = value;
                _supportRepository.Update(dbEntity);

                return true;
            }
            return false;
        }

        public async Task DeleteSupport(int id)
        {
            var support = await _supportRepository.GetByIdAsync(id);
            if (support != null)
            {
                var supportlikes = _supportIPAddressRepository.GetAll(x => x.SupportId == support.Id).ToList();
                if (supportlikes != null && supportlikes.Any())
                {
                    _supportIPAddressRepository.DeleteAll(supportlikes);
                }
                _supportRepository.Delete(support);
            }
        }

        public List<SupportPersistModel> GetActiveSupport()
        {
            List<SupportPersistModel> supportPersistModels = new List<SupportPersistModel>();
            var support = _supportRepository.GetAll(x => x.Active).OrderByDescending(x=>x.SupportDate).ToList();
            _mapper.Map(support, supportPersistModels);

            return supportPersistModels;
        }

        public IPagedList<Support> GetAllSupport<TKey>(Expression<Func<Support, bool>> predicate = null, Expression<Func<Support, TKey>> orderBy = null, bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var queryable = _supportRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Support>(queryable, pageIndex, pageSize);

        }

        public Support GetSupport(int id)
        {
            return _supportRepository.Get(x => x.Id == id);
        }

        public bool PersistSupport(SupportPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                var dbEntity = GetSupport(model.Id.Value);
                if (dbEntity != null)
                {
                    dbEntity.Active = model.Active;
                    dbEntity.Content = model.Content;
                    dbEntity.SupportLink = model.SupportLink;
                    dbEntity.Title = model.Title;
                    dbEntity.Caption = model.Caption;
                    if (!string.IsNullOrEmpty(model.SupportDateString) && !string.IsNullOrWhiteSpace(model.SupportDateString))
                    {
                        dbEntity.SupportDate = DateExtensions.ParseDateTime(new string[] { "dd-MM-yyyy" }, model.SupportDateString);
                    }
                    dbEntity.UpdatedOnUtc = DateTime.Now;
                    dbEntity.SupportLogoId = model.SupportLogoId;

                    _supportRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                var dbEntity = new Support();
                _mapper.Map(model, dbEntity);
                _supportRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }

        public int UpdateSupportLikes(int id, string ipaddress)
        {
            int count = 0;
            var dbEntity = GetSupport(id);
            if (dbEntity != null)
            {
                var supportlikes = _supportIPAddressRepository.GetAll();
                if (!supportlikes.Any(x => x.IPAddress == ipaddress && x.SupportId == id))
                {
                    SupportIpAddressLikes supportIPAddressLikes = new SupportIpAddressLikes();
                    supportIPAddressLikes.IPAddress = ipaddress;
                    supportIPAddressLikes.SupportId = id;
                    _supportIPAddressRepository.Insert(supportIPAddressLikes);

                    dbEntity.Likes += 1;

                    count = dbEntity.Likes;

                    _supportRepository.Update(dbEntity);

                    return count;
                }
                else { return count; }
            }
            return count;
        }
    }
}
