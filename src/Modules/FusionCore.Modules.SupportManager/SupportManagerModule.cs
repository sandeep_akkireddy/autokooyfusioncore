using FusionCore.Core.Interfaces;
using System;

namespace FusionCore.Modules.SupportManager
{
    public class SupportManagerModule : IFusionCoreModule
    {
        public string GetSystemName()
        {
            return "FusionCore.Module.SupportManager";
        }
    }
}
