using FusionCore.Core.Enums;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.SupportManager.Data.Entities;
using FusionCore.Modules.SupportManager.Interfaces;
using FusionCore.Modules.SupportManager.Models;
using FusionCore.Services.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.SupportManager.Data
{
    public class SupportInstaller : IInstallData
    {
        private ISupportService _supportService;

        private IGenericRepository<Support> _supportRepository;

        public async Task Install(IServiceProvider serviceProvider, bool sampleData)
        {
            LoadServices(serviceProvider);
            await InstallNewsManagerMenu(serviceProvider);
            if (sampleData)
            {
                InstallNewsDummyData();
            }
            DisposeServices();
        }
        private void DisposeServices()
        {
            _supportService = null;

        }
        private void LoadServices(IServiceProvider serviceProvider)
        {
            _supportService = (ISupportService)serviceProvider.GetService(typeof(ISupportService));

        }
        private void InstallNewsDummyData()
        {
            for (var i = 0; i < 16; i++)
            {
                _supportService.PersistSupport(new SupportPersistModel
                {
                    Title = "Support - Novi Media " + i,
                    Active = true,
                    Caption = "Support " + i,
                    SupportLink = "",
                    SupportDate = DateTime.Now
                });
            }
        }


        private async Task InstallNewsManagerMenu(IServiceProvider serviceProvider)
        {
            var menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

            var mainMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "clip-stack",
                Name = "Support",
                Area = "SupportManager",
                Controller = "Support",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 0,
                ParentId = null,
                HasNewButton = true
            };
            await menuService.InsertMenuItem(mainMenuItem);

        }

    }
}
