using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.SupportManager.Data.Entities
{
    public class SupportIpAddressLikes : BaseEntity
    {
        public string IPAddress { get; set; }
        public int? SupportId { get; set; }

        public virtual Support Support { get; set; }
    }
}
