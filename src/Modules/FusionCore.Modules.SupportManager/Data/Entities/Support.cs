using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.SupportManager.Data.Entities
{
    public class Support : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        public string Caption { get; set; }
        
        public DateTime? SupportDate { get; set; }

        public string Content { get; set; }

        public bool Active { get; set; }
        public string SupportLink { get; set; }
        public int? SupportLogoId { get; set; }

        public int Likes { get; set; }
    }
}
