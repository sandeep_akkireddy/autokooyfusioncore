using FusionCore.Modules.SupportManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.SupportManager.Data.Maps
{
    public class SupportMap : IEntityTypeConfiguration<Support>
    {
        public void Configure(EntityTypeBuilder<Support> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Title).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Content);
            builder.Property(x => x.Active);
            builder.Property(x => x.Caption);
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.UpdatedOnUtc);
            builder.Property(x => x.SupportLink);
            builder.Property(x => x.SupportDate);
            builder.Property(x => x.SupportLogoId);
            builder.Property(x => x.Likes);
            
        }
    }
}
