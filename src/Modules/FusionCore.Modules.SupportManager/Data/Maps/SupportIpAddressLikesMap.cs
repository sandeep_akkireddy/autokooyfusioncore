using FusionCore.Modules.SupportManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.SupportManager.Data.Maps
{
    public class SupportIpAddressLikesMap : IEntityTypeConfiguration<SupportIpAddressLikes>
    {
        public void Configure(EntityTypeBuilder<SupportIpAddressLikes> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.IPAddress);
            builder.HasOne(x => x.Support).WithMany().HasForeignKey(x => x.SupportId);

        }
    }
}
