using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.SupportManager.Data.Entities;
using FusionCore.Modules.SupportManager.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace FusionCore.Modules.SupportManager.Data
{
    public class AutomapperConfiguration : Profile
    {
        public AutomapperConfiguration()
        {
            CreateMap<Support, SupportDataRow>();

            CreateMap<Support, SupportPersistModel>().ForMember(x => x.SupportDateString, o => o.MapFrom(s => s.SupportDate.HasValue ? s.SupportDate.Value.ToString("dd-MM-yyyy") : ""));
            CreateMap<SupportPersistModel, Support>().ForMember(x => x.SupportDate, o => o.MapFrom(s =>
            DateExtensions.ParseDateTime(new string[] { "dd-MM-yyyy" }, s.SupportDateString)));
        }
    }
}
