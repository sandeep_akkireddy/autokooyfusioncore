using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.SupportManager.Models
{
    public class SupportPersistModel : BaseModel
    {
        [Required]
        [MaxLength(256)]
        public string Title { get; set; }
        [MaxLength(256)]
        public string Caption { get; set; }
        
        public DateTime? SupportDate { get; set; }

        public string Content { get; set; }

        public bool Active { get; set; }
        [DataType(DataType.Url)]
        public string SupportLink { get; set; }
        public int? SupportLogoId { get; set; }

        public int Likes { get; set; }
        [Required]
        public string SupportDateString { get; set; }
    }
}
