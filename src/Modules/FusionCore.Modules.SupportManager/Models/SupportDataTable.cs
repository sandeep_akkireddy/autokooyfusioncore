using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.SupportManager.Models
{
   
    public class SupportDataTable : BaseDataTable
    {
        public List<SupportDataRow> Data { get; set; }
    }
    public class SupportDataRow
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime? SupportDate { get; set; }
        public bool Active { get; set; }

    }
}
