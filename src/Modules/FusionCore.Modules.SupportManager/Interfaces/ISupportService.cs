using FusionCore.Models.Common;
using FusionCore.Modules.SupportManager.Data.Entities;
using FusionCore.Modules.SupportManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.SupportManager.Interfaces
{
    public interface ISupportService : IBaseService
    {
        IPagedList<Support> GetAllSupport<TKey>(
            Expression<Func<Support, bool>> predicate = null,
            Expression<Func<Support, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Support GetSupport(int id);
        bool PersistSupport(SupportPersistModel model);
        Task DeleteSupport(int id);

        bool ActiveSupport(int id, bool value);

        int UpdateSupportLikes(int id, string ipaddress);

        List<SupportPersistModel> GetActiveSupport();

    }
}
