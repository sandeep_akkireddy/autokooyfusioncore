using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.SupportManager.Data.Entities;
using FusionCore.Modules.SupportManager.Interfaces;
using FusionCore.Modules.SupportManager.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.SupportManager.Controller
{
    [Area("SupportManager")]
    public class SupportController : FusionBaseController
    {
        private readonly ISupportService _supportService;
        private readonly IMapper _mapper;

        public SupportController(ISupportService supportService, IMapper mapper)
        {
            _supportService = supportService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult DataTable(SupportDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Expression<Func<Support, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "title":
                    orderBy = x => x.Title;
                    break;
                default:
                    orderBy = x => x.SupportDate;
                    //sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Support, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x => (
                        x.Title.ToLower().Contains(searchValue.ToLower())
                        || x.SupportDate.Value.ToLongDateString().Contains(searchValue.ToLower()));

            var pages = _supportService.GetAllSupport(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<SupportDataRow>>(pages);
            model.RecordsFiltered = pages.TotalCount;
            model.RecordsTotal = pages.TotalCount;
            return new JsonResult(model);
        }

        [HttpPost]
        public IActionResult Active(int id, bool value)
        {
            return Json(_supportService.ActiveSupport(id, value));
        }
        public IActionResult Get(int? id)
        {
            var model = id.HasValue ? _mapper.Map<SupportPersistModel>(_supportService.GetSupport(id.Value)) : new SupportPersistModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(SupportPersistModel model)
        {
            if (ModelState.IsValid) return Json(_supportService.PersistSupport(model));

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _supportService.DeleteSupport(id);

            return RedirectToAction("Index");
        }

    }
}
