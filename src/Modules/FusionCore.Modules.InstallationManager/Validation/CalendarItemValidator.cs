﻿using FusionCore.Modules.InstallationManager.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Validation
{
    public class CalendarItemValidator : ValidationAttribute, IClientModelValidator
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            CalendarItemPersistModel item = (CalendarItemPersistModel)validationContext.ObjectInstance;
            if (!item.WholeDay && (string.IsNullOrEmpty(item.StartTimeString) || string.IsNullOrEmpty(item.EndTimeString)))
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            MergeAttribute(context.Attributes, "data-val", "true");
            MergeAttribute(context.Attributes, "data-val-calendaritem", GetErrorMessage());
        }

        private static bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
        {
            if (attributes.ContainsKey(key)) return false;
            attributes.Add(key, value); return true;
        }
        private string GetErrorMessage()
        {
            return "This field is required";
        }
    }
}
