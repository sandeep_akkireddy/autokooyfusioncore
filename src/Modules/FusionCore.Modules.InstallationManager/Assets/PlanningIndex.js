﻿jQuery(document).ready(function () {
	$("body").on("click",
		".js-planning-popup-modal",
		function (e) {
			e.preventDefault();
			var $this = $(this);

			Modals.CreateModal($this.data("title"), $this.data("url"), $this.data('class'), function () {
				$('#calendar').fullCalendar('refetchEvents');
			});
			return false;
		});

	$("body").on("click",
			".js-employee-remove",
			function(event) {
				event.preventDefault();
				$(this).parent().remove();
				RenumberEmployees();
			})
		.on("click",
			".js-employee-add",
			function(event) {
				event.preventDefault();
				var $parent = $(this).parent();
				var clone = $parent.clone();
				clone.insertAfter($parent);
				var select = clone.find("select");
				var name = select.attr('name');
				var currentId = name.match("([0-9])+")[0];
				var number = parseInt(currentId) + 1;
				select.attr('name', name.replace(currentId, number));
				var button = $parent.find('.js-employee-add');
				button.removeClass("js-employee-add").addClass('js-employee-remove');
				button.removeClass("btn-teal").addClass('btn-bricky');
				button.data("originalTitle", 'Remove');
				button.find("i").removeClass('fa-plus').addClass('fa-times');
			})
		.on('click',
			'.js-remove-mainemployee',
			function(e) {
				e.preventDefault();
				$(this).parent().find('select').val('');
			})
		.on('change',
			'.js-loadclients',
			function(e) {
				e.preventDefault();
				var $this = $(this);
				var $clientList = $("#ClientId");
				var currentValue = $clientList.val();
				$.ajax({
					type: "GET",
					url: $this.data('url') + '/' + $this.val(),
					beforeSend: function(xhr) {
						xhr.setRequestHeader("RequestVerificationToken",
							$('input:hidden[name="__RequestVerificationToken"]').val());
					},
					success: function(returnData) {
						if (returnData !== undefined) {
							var len = returnData.length;
							$clientList.find("option:gt(0)").remove();
							var valueFound = false;
							console.log(currentValue);
							for (var i = 0; i < len; i++) {
								var id = returnData[i]['id'];
								var name = returnData[i]['name'];
								if (String(id) === String(currentValue)) {
									valueFound = true;
								}
								$clientList.append("<option value='" + id + "'>" + name + "</option>");
							}
							if (!valueFound) {
								$clientList.val('');
								$clientList.trigger('change');
							} else {
								$clientList.val(currentValue);
							}
						}
					}
				});
			})
		.on('change',
			'.js-loadbuildings',
			function(e) {
				e.preventDefault();
				var $this = $(this);
				$.ajax({
					type: "GET",
					url: $this.data('url') + '/' + $this.val(),
					beforeSend: function(xhr) {
						xhr.setRequestHeader("RequestVerificationToken",
							$('input:hidden[name="__RequestVerificationToken"]').val());
					},
					success: function(returnData) {
						if (returnData !== undefined) {
							var len = returnData.length;
							var clientList = $("#BuildingLocationId");
							clientList.find("option:gt(0)").remove();
							for (var i = 0; i < len; i++) {
								var id = returnData[i]['id'];
								var name = returnData[i]['name'];

								clientList.append("<option value='" + id + "'>" + name + "</option>");

							}
						}
					}
				});
			})
		.on('change',
			'.js-loadassets',
			function(e) {
				e.preventDefault();
				var $this = $(this);
				$.ajax({
					type: "GET",
					url: $this.data('url') + '/' + $this.val(),
					beforeSend: function(xhr) {
						xhr.setRequestHeader("RequestVerificationToken",
							$('input:hidden[name="__RequestVerificationToken"]').val());
					},
					success: function(returnData) {
						if (returnData !== undefined) {
							var len = returnData.length;
							var clientList = $("#AssetId");
							clientList.find("option:gt(0)").remove();
							for (var i = 0; i < len; i++) {
								var id = returnData[i]['id'];
								var name = returnData[i]['name'];

								clientList.append("<option value='" + id + "'>" + name + "</option>");

							}
						}
					}
				});
			})
		.on('change',
			'.js-loadactivities',
			function(e) {
				e.preventDefault();
				var $this = $(this);
				$.ajax({
					type: "GET",
					url: $this.data('url') + '/' + $this.val(),
					beforeSend: function(xhr) {
						xhr.setRequestHeader("RequestVerificationToken",
							$('input:hidden[name="__RequestVerificationToken"]').val());
					},
					success: function(returnData) {
						if (returnData !== undefined) {
							var len = returnData.length;
							var clientList = $("#AssetActivityId");
							clientList.find("option:gt(0)").remove();
							for (var i = 0; i < len; i++) {
								var id = returnData[i]['id'];
								var name = returnData[i]['name'];

								clientList.append("<option value='" + id + "'>" + name + "</option>");

							}
						}
					}
				});
			})
		.on('click',
			'.js-delete-planning',
			function(e) {
				e.preventDefault();
				var $this = $(this);
				bootbox.confirm("Weet u het zeker?",
					function (result) {
						if (result) {
							$.ajax({
								type: "GET",
								url: $this.data('url'),
								beforeSend: function(xhr) {
									xhr.setRequestHeader("RequestVerificationToken",
										$('input:hidden[name="__RequestVerificationToken"]').val());
								},
								success: function(returnData) {
									if (returnData) {
										$this.closest(".modal").modal('toggle');
										$('#calendar').fullCalendar('refetchEvents');
									}
								}
							});
						}
					});
				return false;
			});
		;

	var $calendar = $('#calendar');
	$calendar.fullCalendar({
		schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
		now: $calendar.data('date'),
		lang: 'nl',
		editable: true,
		weekNumbers: true,
		height: '100%',
		timezone: 'local',
		resourceAreaWidth: '15%',
		header: {
			left: 'today prev,next',	
			center: 'title',
			right: 'timelineTenDay,agendaWeek,month'
		},
		defaultView: 'timelineTenDay',
		slotDuration: '02:00:00',
		slotLabelInterval: '02:00',
		views: {
			timelineTenDay: {
				type: 'timeline',
				duration: { days: 7 }
			}
		},
		resourceLabelText: 'Werknemers',
		resources: function(callback) {
			var employee = $("#employee").val();
			var group = $("#group").val();

			$.ajax({
				url:  $calendar.data('urlplanners'),
				type: "POST",
				dataType: 'json',
				data: {
					employeeId: employee,
					groupId: group
				},
				success: function(doc) {
					callback(doc);
				}
			});
		},
		eventLimit: true, // allow "more" link when too many events
		events: function(start, end, timezone, callback) {
			var employee = $("#employee").val();
			var group = $("#group").val();
			var buildingLocation = $("#buildingLocationId").val();
			$.ajax({
				url: $calendar.data('urlplannings'),
				type: "POST",
				dataType: 'json',
				data: {
					// our hypothetical feed requires UNIX timestamps
					start: start.format(),
					end: end.format(),
					buildingLocationId : buildingLocation,
					employeeId: employee,
					groupId: group
				},
				success: function(doc) {
					callback(doc);
				}
			});
		},
		eventRender: function (event, element) {
			var contentComplete = event.title;

			element.find('.fc-title').html(contentComplete);

			if (event.extraInfo !== "" && event.extraInfo !== null) {

				var extraInfo = event.extraInfo;
				contentComplete = contentComplete + "<br/>" + extraInfo;
			}
			element.qtip({
				content: contentComplete,
				show: {
					delay: 1000
				}

			});
		},
		eventClick: function (event, jsEvent, view) {
			var id = event.id;
			if (id.startsWith('cal-')) {
				//calendar
				id = id.replace('cal-', '');
				Modals.CreateModal('Agenda item',
					$calendar.data("url-calendar") + '/' + id,
					'',
					function() {
						$('#calendar').fullCalendar('refetchEvents');
					});
			} else {
				//Planning
				id = id.replace('-sub', '');
				Modals.CreateModal('Planning',
					$calendar.data("url") + '/' + id,
					'big',
					function() {
						$('#calendar').fullCalendar('refetchEvents');
					});
			}
			/*if (event.className.includes("js-employeeactivity"))*/
		},
		eventDrop: function (event, delta, revertFunc) {
			SaveEvent(event);
		},
		eventResize: function (event, delta, revertFunc) {
			SaveEvent(event);
		}
	});

	Main.coupleDynamics();
	$('body').on('change',
		'#js-weeknumber-dropdown',
		function() {
			SetWeek($(this).val());
		})
		.on('click',
			'#js-setweeknumber',
			function() {
				SetWeek($(this).data('number'));
			});

	$('body').on('click',
		'#js-filters',
		function() {
			$('#js-filters-collaps').trigger('click');
			var employee = $("#employee").val();
			var group = $("#group").val();
			var buildingLocation = $("#buildingLocationId").val();
			var url = window.location.pathname;
			if (isNotEmpty(employee) || isNotEmpty(group) || buildingLocation.length > 0) {
				url = url + '?';
				if (isNotEmpty(employee)) {
					url = url + 'employeeId=' + employee + '&';
				}
				if (isNotEmpty(group)) {
					url = url + 'groupId=' + group + '&';
				}
				if (buildingLocation.length > 0) {
					url = url + 'buildingLocationId=' + buildingLocation + '&';
				}
				url = url.substr(0, url.length - 1);
			}

			window.history.pushState("", "", url );
			$('#calendar').fullCalendar('refetchResources');

			setTimeout(
				$('#calendar').fullCalendar('refetchEvents'),
				500);
		});

	$('body').on('click',
		'#js-print',
		function() {
			var employee = $("#employee").val();
			var group = $("#group").val();
			var buildingLocation = $("#buildingLocationId").val();
			var url = $(this).attr('href');
			if (isNotEmpty(employee) || isNotEmpty(group) || buildingLocation.length > 0) {
				url = url + '&';
				if (isNotEmpty(employee)) {
					url = url + 'employeeId=' + employee + '&';
				}
				if (isNotEmpty(group)) {
					url = url + 'groupId=' + group + '&';
				}
				if (buildingLocation.length > 0) {
					url = url + 'buildingLocationId=' + buildingLocation + '&';
				}
				url = url.substr(0, url.length - 1);
			}

			window.open(url);
		});
});

function SetWeek(weeknumber) {
	$("#js-weeknumber").html(weeknumber);
	var $calendar = $('#calendar');
	var start = moment().locale("en").week(weeknumber).day("Monday");
	$calendar.fullCalendar('gotoDate', start);
}

function isNotEmpty(arr) {
	for(var key in arr) {
		if (arr[key].length > 0) {
			return true;
		}
	}
	return false;
}

function InitialTrigger() {
	$(".js-loadclients").trigger('change');
	$(".js-loadbuildings").trigger('change');
	$(".js-loadassets").trigger('change');
	$(".js-loadactivities").trigger('change');
}

function RenumberEmployees() {
	var dropdowns = $("select[name^='PlanningEmployees']");
	dropdowns.each(function (index) {
		$(this).attr('name', 'PlanningEmployees[' + index + '].EmployeeId');
	});
}

function SaveEvent(event) {
	
	var start = event.start;
	var end = event.end;
	start = new Date(start);
	var startDate = start.getFullYear() + '-' + (start.getMonth() + 1) + '-' + start.getDate() + ' ' + start.getHours() + ':' + start.getMinutes();
	var endDate = null;
	if (end !== null) {
		end = new Date(end);
		endDate = end.getFullYear() + '-' + (end.getMonth() + 1) + '-' + end.getDate() + ' ' + end.getHours() + ':' + end.getMinutes();
	}
	var $calendar = $('#calendar');
	$.ajax({
		url: $calendar.data('updateurl'),
		data: {
			id: event.id,
			start: startDate,
			end: endDate,
			resourceId: event.resourceId !== null ? event.resourceId : event.resourceIds
		},
		beforeSend: function (xhr) {
			xhr.setRequestHeader("RequestVerificationToken",
				$('input:hidden[name="__RequestVerificationToken"]').val());
		},
		type: "POST",
		async: false
	});
	$calendar.fullCalendar('refetchEvents');
}