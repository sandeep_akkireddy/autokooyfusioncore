﻿jQuery(document).ready(function () {
	$("#js-Tab-Fill").on('click',
		function () {
			$("#js-dropdownAssetTypes").hide();
			$("#js-dropdownActivityAssets").show();
			$("#js-dropdownIcon").show();
			LoadDropDownActivityAssets();
		});
	$("#js-Tab-Activities").on('click',
		function () {
			$("#js-dropdownAssetTypes").show();
			$("#js-dropdownActivityAssets").hide();
			$("#js-dropdownIcon").show();
			LoadDropDownAssets();
		});
	$("#js-Tab-AssetType").on('click',
		function () {
			$("#js-dropdownAssetTypes").hide();
			$("#js-dropdownActivityAssets").hide();
			$("#js-dropdownIcon").hide();
			LoadAssetTypes();
		});

	runAddAssetType();
	runAddActivity();
	runAddActivityDynamicField();
	runAddActivityField();
	runDeleteAssetType();
	runDeleteActivity();
	runDropDownAssetType();
	runDropDownActivityAsset();
	runDeleteActivityDynamicField();
	runDeleteActivityField();

	$("#js-Tab-Fill").trigger('click');
});

function runDeleteActivityField() {
	$("body").on('click',
		'.js-activityFieldsDelete',
		function (e) {
			e.preventDefault();
			$("#panel_tab_beheer .alert.alert-danger").remove();
			$.ajax({
				type: "POST",
				url: $(this).data('url') + '/' + $(this).data("id"),
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadActivityFields();
					} else {
						Main.AddAlert("Verwijderen is niet mogelijk", "#panel_tab_beheer .alert");
					}
				}
			});
		});
}
function runDeleteActivityDynamicField() {
	$("body").on('click',
		'.js-activityDynamicFieldsDelete',
		function (e) {
			e.preventDefault();
			$("#panel_tab_beheer .alert.alert-danger").remove();
			$.ajax({
				type: "POST",
				url: $(this).data('url') + '/' + $(this).data("id"),
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadDynamicFields();
					} else {
						Main.AddAlert("Verwijderen is niet mogelijk", "#panel_tab_beheer .alert");
					}
				}
			});
		});
}
function LoadActivityFields() {
	$activityActivityFields = $("#activityActivityFields");
	$activityActivityFields.html("");
	$.ajax({
		type: "GET",
		url: $activityActivityFields.data('url') + '/' + $("#js-dropdownActivityAssets").val(),
		beforeSend: function (xhr) {
			xhr.setRequestHeader("RequestVerificationToken",
				$('input:hidden[name="__RequestVerificationToken"]').val());
		},
		success: function (returnData) {
			var html = "";
			$(returnData).each(function () {

				html += ('<li class="dd-item dd3-item" data-id="' +
					this.id +
					'">' +
					'<div class="dd-handle dd3-handle"></div>' +
					'<div class="dd3-content">' +
					'<a class="btn btn-bricky form_drag_del js-activityFieldsDelete" href="#" data-id="' +
					this.id +
					'" data-url="' + $activityActivityFields.data("deleteurl") + '"><i class="fa fa-times fa fa-white"></i> </a>' +
					'' +
					'<a href="#" class="editable editable-click" data-url="' + $activityActivityFields.data("updateurl") + '" data-pk="' + this.id + '" data-type="text" data-placement="right" data-placeholder="Required" data-original-title="Activiteit veld aanpassen">' + this.name + '</a>' +
					'<span class="pull-right-button">' + this.dynamicType + '</span>' +
					'</div>' +
					'</li>');
			});
			$activityActivityFields.append(html);
			Main.coupleDynamics();
		}
	});
}
function LoadDynamicFields() {
	$activityDynamicFields = $("#activityDynamicFields");
	$activityDynamicFields.html("");
	$.ajax({
		type: "GET",
		url: $activityDynamicFields.data('url') + '/' + $("#js-dropdownActivityAssets").val(),
		beforeSend: function (xhr) {
			xhr.setRequestHeader("RequestVerificationToken",
				$('input:hidden[name="__RequestVerificationToken"]').val());
		},
		success: function (returnData) {
			var html = "";
			$(returnData).each(function () {

				html += ('<li class="dd-item dd3-item" data-id="' +
					this.id +
					'">' +
					'<div class="dd-handle dd3-handle"></div>' +
					'<div class="dd3-content">' +
					'<a class="btn btn-bricky form_drag_del js-activityDynamicFieldsDelete" href="#" data-id="' +
					this.id +
					'" data-url="' + $activityDynamicFields.data("deleteurl") + '"><i class="fa fa-times fa fa-white"></i> </a>' +
					'' +
					'<a href="#" class="editable editable-click" data-url="' + $activityDynamicFields.data("updateurl") + '" data-pk="' + this.id + '" data-type="text" data-placement="right" data-placeholder="Required" data-original-title="Dynamisch veld aanpassen">' + this.name + '</a>' +
					'<span class="pull-right-button">' + this.dynamicType + '</span>' +
					'</div>' +
					'</li>');
			});
			$activityDynamicFields.append(html);
			Main.coupleDynamics();
		}
	});
}

function runAddActivityField() {
	$("#js-addActivityField").on("click",
		function () {
			var $form = $("#NewActivityFieldForm");
			var data = $form.serialize();
			$.ajax({
				type: "POST",
				url: $form.attr("action") + '/' + $("#js-dropdownActivityAssets").val(),
				data: data,
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadActivityFields();
						$form.find("input[type='text']").val("");
					}
				}
			});
		});
}

function runAddActivityDynamicField() {
	$("#js-addActivityDynamicField").on("click",
		function () {
			var $form = $("#NewDynamicFieldForm");
			var data = $form.serialize();
			$.ajax({
				type: "POST",
				url: $form.attr("action") + '/' + $("#js-dropdownActivityAssets").val(),
				data: data,
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadDynamicFields();
						$form.find("input[type='text']").val("");
					}
				}
			});
		});
}

function LoadDropDownActivityAssets() {
	$dropdownActivityAssets = $("#js-dropdownActivityAssets");
	$dropdownActivityAssets.find('option')
		.remove()
		.end();

	$.ajax({
		type: "GET",
		url: $dropdownActivityAssets.data('url'),
		beforeSend: function (xhr) {
			xhr.setRequestHeader("RequestVerificationToken",
				$('input:hidden[name="__RequestVerificationToken"]').val());
		},
		success: function (returnData) {
			$.each(returnData, function () {
				$dropdownActivityAssets.append($("<option />").val(this.id).text(this.name));
			});
			$dropdownActivityAssets.trigger('change');
		}
	});
}

function runAddActivity() {
	$("#js-addActivity").on("click",
		function () {
			var $form = $("#NewActivityForm");
			var data = $form.serialize();
			$.ajax({
				type: "POST",
				url: $form.attr("action"),
				data: data,
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadActivities();
						$form.find("input[type='text']").val("");
					}
				}
			});
		});
}

function LoadDropDownAssets() {
	$dropdownAssetTypes = $("#js-dropdownAssetTypes");
	$assetType = $("#AssetType");
	$dropdownAssetTypes.find('option')
		.remove()
		.end();
	$assetType.find('option')
		.remove()
		.end();

	$.ajax({
		type: "GET",
		url: $("#assetTypeList").data('url'),
		beforeSend: function (xhr) {
			xhr.setRequestHeader("RequestVerificationToken",
				$('input:hidden[name="__RequestVerificationToken"]').val());
		},
		success: function (returnData) {
			$.each(returnData, function () {
				$dropdownAssetTypes.append($("<option />").val(this.id).text(this.name));
				$assetType.append($("<option />").val(this.id).text(this.name));
			});
			$dropdownAssetTypes.trigger('change');
		}
	});
}

function runDropDownActivityAsset() {
	$dropdownActivityAssets = $("#js-dropdownActivityAssets");
	$dropdownActivityAssets.on('change',
		function () {
			var parent = $("#activityDynamicFields").parent();
			var parent2 = $("#activityActivityFields").parent();
			parent.data("url", parent.data("originalurl") + '/' + $(this).val());
			parent2.data("url", parent2.data("originalurl") + '/' + $(this).val());
			LoadDynamicFields();
			LoadActivityFields();
		});

}

function runDropDownAssetType() {
	$dropdownAssetTypes = $("#js-dropdownAssetTypes");
	$dropdownAssetTypes.on('change',
		function () {
			var parent = $("#activityList").parent();
			var newUrl = parent.data("originalurl") + '/' + $(this).val();
			parent.data("url", newUrl);
			LoadActivities();
		});
}

function LoadActivities() {
	$activityList = $("#activityList");
	$activityList.html("");
	$.ajax({
		type: "GET",
		url: $activityList.data("url") + '/' + $("#js-dropdownAssetTypes").val(),
		beforeSend: function (xhr) {
			xhr.setRequestHeader("RequestVerificationToken",
				$('input:hidden[name="__RequestVerificationToken"]').val());
		},
		success: function (returnData) {
			var html = "";
			$(returnData).each(function () {
				html += ('<li class="dd-item dd3-item" data-id="' +
					this.id +
					'">' +
					'<div class="dd-handle dd3-handle"></div>' +
					'<div class="dd3-content">' +
					'<a class="btn btn-bricky form_drag_del js-activityDelete" href="#" data-id="' +
					this.id +
					'" data-url="' + $activityList.data("deleteurl") + '"><i class="fa fa-times fa fa-white"></i> </a>' +
					'' +
					'<a href="#" class="editable editable-click" data-url="' + $activityList.data("updateurl") + '" data-pk="' + this.id + '" data-type="text" data-placement="right" data-placeholder="Required" data-original-title="Activiteit aanpassen">' + this.name + '</a>' +
					'</div>' +
					'</li>');
			});
			$activityList.append(html);
			Main.coupleDynamics();
		}
	});
}

function runDeleteActivity() {
	$("body").on('click',
		'.js-activityDelete',
		function (e) {
			e.preventDefault();
			$("#panel_tab_activiteiten .alert.alert-danger").remove();
			$.ajax({
				type: "POST",
				url: $(this).data('url') + '/' + $(this).data("id"),
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadActivities();
					} else {
						Main.AddAlert("Verwijderen is niet mogelijk", "#panel_tab_activiteiten .alert");
					}
				}
			});
		});
}

function runDeleteAssetType() {
	$("body").on('click',
		'.js-assetTypeDelete',
		function (e) {
			e.preventDefault();
			$("#panel_tab_assets .alert.alert-danger").remove();
			$.ajax({
				type: "POST",
				url: $(this).data('url') + '/' + $(this).data("id"),
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadAssetTypes();
					} else {
						Main.AddAlert("Verwijderen is niet mogelijk", "#panel_tab_assets .alert");
					}
				}
			});
		});
}

function runAddAssetType() {
	$("#js-addAssetType").on("click",
		function () {
			var $form = $("#NewAssetTypeForm");
			var data = $form.serialize();
			$.ajax({
				type: "POST",
				url: $form.attr("action"),
				data: data,
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (returnData) {
					if (returnData) {
						LoadAssetTypes();
						$form.find("input[type='text']").val("");
					}
				}
			});
		});
}

function LoadAssetTypes() {
	$assetTypeList = $("#assetTypeList");
	$assetTypeList.html("");
	$.ajax({
		type: "GET",
		url: $assetTypeList.data('url'),
		beforeSend: function (xhr) {
			xhr.setRequestHeader("RequestVerificationToken",
				$('input:hidden[name="__RequestVerificationToken"]').val());
		},
		success: function (returnData) {
			var html = "";
			$(returnData).each(function () {
				html += ('<li class="dd-item dd3-item" data-id="' +
					this.id +
					'">' +
					'<div class="dd-handle dd3-handle"></div>' +
					'<div class="dd3-content">' +
					'<a class="btn btn-bricky form_drag_del js-assetTypeDelete" href="#" data-id="' +
					this.id +
					'" data-url="' + $assetTypeList.data("deleteurl") + '"><i class="fa fa-times fa fa-white"></i> </a>' +
					'' +
					'<a href="#" class="editable editable-click" data-url="' + $assetTypeList.data("updateurl") + '" data-pk="' + this.id + '" data-type="text" data-placement="right" data-placeholder="Required" data-original-title="Asset type aanpassen">' + this.name + '</a>' +
					'</div>' +
					'</li>');
			});
			$assetTypeList.append(html);
			Main.coupleDynamics();
		}
	});

}