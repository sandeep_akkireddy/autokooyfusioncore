﻿jQuery(document).ready(function () {
	$("body").on("click",
		".js-popupuser-modal",
		function (e) {
			e.preventDefault();
			var $this = $(this);

			Modals.CreateModal($this.data("title"), $this.data("url"), $this.data('class'), function () {

				try {
					$("#clientusers").dataTable().api().ajax.reload(null, false);
				} catch (err) {
					//unused
				}
				try {
					item.closest("#clientusers").DataTable().ajax.reload();
				} catch (err) {
					//unused
				}
			});

			return false;
		})
		.on("click",
			".js-popup-modal-custom",
			function (e) {
				e.preventDefault();
				var $this = $(this);
				Modals.CreateModal($this.data("title"), $this.data("url"), $this.data('class'), function () {

					if ($this.data("reloadContent") === undefined) {
						try {
							$("table.dataTable:first").dataTable().api().ajax.reload(null, false);
						} catch (err) {
							//unused
						}
						try {
							item.closest("table.dataTable").DataTable().ajax.reload();
						} catch (err) {
							//unused
						}
					} else {
						runLoadContent();
					}
				}, function () {
					$("select[name*='.BuildingId'].js-dummy-input").attr('name', 'ClientUserRights[99999].BuildingId');
					$("select[name*='.BuildingLocationId'].js-dummy-input").attr('name', 'ClientUserRights[99999].BuildingLocationId');
					$("input[name*='.Id'].js-dummy-input").attr('name', 'ClientUserRights[99999].Id');
					console.log($("input[name*='.AllLocations'].js-dummy-input"));
					$("input[name*='.AllBuildings'].js-dummy-input").attr('name', 'ClientUserRights[99999].AllBuildings');
					$("input[name*='.AllLocations'].js-dummy-input").attr('name', 'ClientUserRights[99999].AllLocations');

					$('.js-building').each(function (i, elm) {
						var $this = $(elm);
						var $parent = $this.closest('.panel-body');
						var $location = $parent.find('.js-location');
						$location.find('option[value!=""]').remove();
						if ($this.val() === '') {
							return;
						}
						$.ajax({
							url: $this.data('url') + '/' + $this.val(),
							type: 'POST',
							contentType: 'application/json; charset=utf-8',
							beforeSend: function (xhr) {
								xhr.setRequestHeader("RequestVerificationToken",
									$('input:hidden[name="__RequestVerificationToken"]').val());
							},
							success: function (result) {
								$.each(result, function (key, value) {
									$location
										.append($("<option></option>")
											.attr("value", value.id)
											.text(value.name));
								});

								$($location).val($($location).data('oldvalue'));
							}
						});
					});
				});

				return false;
			});
	$("body").on("click",
		".js-division-remove",
		function (event) {
			event.preventDefault();
			$(this).parent().remove();
			RenumberClientDivisions();
		})
		.on("click",
			".js-division-add",
			function (event) {
				event.preventDefault();
				var $parent = $(this).parent();
				var clone = $parent.clone();
				clone.insertAfter($parent);
				var select = clone.find("select");
				var name = select.attr('name');
				var currentId = name.match("([0-9])+")[0];
				var number = parseInt(currentId) + 1;
				select.attr('name', name.replace(currentId, number));
			})
		.on('click',
			'.js-clientuserright-delete',
			function (event) {
				event.preventDefault();
				$(this).parent().parent().remove();
				RenumberClientUserRights();
			})
		.on('change ifChanged', '.js-chk-allcompany', function () {
			var $this = $(this);
			var $parent = $this.closest('.panel-body');
			var $building = $parent.find('.js-building');
			var $location = $parent.find('.js-location');
			if ($this.prop('checked')) {
				$building.prop('disabled', true);
				$building.val('');
				$location.prop('disabled', true);
				$location.val('');
			}
			else {
				$building.prop('disabled', false);
				$location.prop('disabled', false);
			}
		})
		.on('change ifChanged', '.js-chk-allbuilding', function () {
			var $this = $(this);
			var $parent = $this.closest('.panel-body');
			var $location = $parent.find('.js-location');
			if ($this.prop('checked')) {
				$location.prop('disabled', true);
				$location.val('');
			}
			else {
				$location.prop('disabled', false);
			}
		})
		.on('change', '.js-building', function () {
			var $this = $(this);
			var $parent = $this.closest('.panel-body');
			var $location = $parent.find('.js-location');
			$location.find('option[value!=""]').remove();
			if ($this.val() === '') {
				return;
			}
			$.ajax({
				url: $this.data('url') + '/' + $this.val(),
				type: 'POST',
				contentType: 'application/json; charset=utf-8',
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				success: function (result) {
					$.each(result, function (key, value) {
						$location
							.append($("<option></option>")
								.attr("value", value.id)
								.text(value.name));
					});
				}
			});
		})
		.on('click', '#js-addClientUserRight', function (e) {
			e.preventDefault();
			var $dummy = $('.js-dummy');
			var $clone = $dummy.clone();
			$clone = $clone.removeClass('js-dummy');
			$clone = $clone.attr('style', '');
			var html = $clone.html().split($dummy.data('id'));
			html = html.join('collapse' + getRandomInt(99999));
			html = html.split('js-dummy-input').join('');
			var $html = $(html);
			$html.find('input[type=checkbox]').prop('checked', false).data('toggle', 'toggle').data('size', 'normal');
			$html.iCheck('destroy');
			$html.find('input[type="checkbox"]').bootstrapToggle();
			//Main.runCustomCheck();
			$('#accordion').append($html);
			RenumberClientUserRights();
		})
		;
});

function RenumberClientUserRights() {
	var dropdowns = $("select[name*='.BuildingId']").not('.js-dummy-input');
	dropdowns.each(function (index) {
		$(this).attr('name', 'ClientUserRights[' + index + '].BuildingId');
	});

	dropdowns = $("select[name*='.BuildingLocationId']").not('.js-dummy-input');
	dropdowns.each(function (index) {
		$(this).attr('name', 'ClientUserRights[' + index + '].BuildingLocationId');
	});

	dropdowns = $("[name*='.AllLocations']").not('.js-dummy-input');
	dropdowns.each(function (index) {
		$(this).attr('name', 'ClientUserRights[' + index + '].AllLocations');
	});

	dropdowns = $("[name*='.AllBuildings']").not('.js-dummy-input');
	dropdowns.each(function (index) {
		$(this).attr('name', 'ClientUserRights[' + index + '].AllBuildings');
	});

	dropdowns = $("select[name*='.Id']").not('.js-dummy-input');
	dropdowns.each(function (index) {
		$(this).attr('name', 'ClientUserRights[' + index + '].Id');
	});
}

function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}