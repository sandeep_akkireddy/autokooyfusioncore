﻿var newRow = false;
var actualEditingRow = null;
var oTable;
jQuery(document).ready(function () {

	$('body').on('click', '.add-row', function (e) {
		e.preventDefault();
		if (newRow == false) {
			if (actualEditingRow) {
				restoreRow(oTable, actualEditingRow);
			}
			newRow = true;
			var aiNew = oTable.fnAddData({ 'name': 'naam', 'value': 'waarde', 'id': '0' });
			var nRow = oTable.fnGetNodes(aiNew[0]);
			editRow(oTable, nRow);
			actualEditingRow = nRow;
		}
	});
	var $dynamicTable = $('#dynamicFields');
	$dynamicTable.on('click', '.cancel-row', function (e) {

		e.preventDefault();
		if (newRow) {
			newRow = false;
			actualEditingRow = null;
			var nRow = $(this).parents('tr')[0];
			oTable.fnDeleteRow(nRow);

		} else {
			restoreRow(oTable, actualEditingRow);
			actualEditingRow = null;
		}
	});

	$dynamicTable.on('click', '.save-row', function (e) {
		e.preventDefault();

		var nRow = $(this).parents('tr')[0];
		$.blockUI({
			message: '<i class="fa fa-spinner fa-spin"></i> Do some ajax to sync with backend...'
		});
		var jqInputs = $('input', nRow);
		var data = {};
		data["assetId"] = $dynamicTable.data('id');
		for (var i = 0, iLen = jqInputs.length; i < iLen; i++) {
			data[jqInputs[i].name] = jqInputs[i].value;
		}
		var updateUrl = $dynamicTable.data('updateurl') + "/";
		$.ajax({
			url: updateUrl,
			dataType: 'json',
			data: data,
			success: function (json) {
				$.unblockUI();
				if (json !== undefined && json > 0) {

					var aData = oTable.fnGetData(nRow);
					aData['id'] = json;
					aData['name'] = data['name'];
					aData['value'] = data['value'];
					$('input[name="id"]', nRow).val(json);
					oTable.fnUpdate(aData, nRow, null, false);

					saveRow(oTable, nRow, aData);
				}
			}
		});
	});
	$dynamicTable.on('click', '.edit-row', function (e) {
		e.preventDefault();
		if (actualEditingRow) {
			if (newRow) {
				oTable.fnDeleteRow(actualEditingRow);
				newRow = false;
			} else {
				restoreRow(oTable, actualEditingRow);

			}
		}
		var nRow = $(this).parents('tr')[0];
		editRow(oTable, nRow);
		actualEditingRow = nRow;

	});
	LoadDynamicFields();

	$("body").on('blur keypress',
		'.js-auto-update',
		function(e) {
			if (e.which === 0 || e.which === 13) {
				debounce(postValues(this),300);
			}
		});

	$("body").on('change ifChanged',
		'.js-auto-update',
		function(e) {
			debounce(postValues(this),300);
		});

	$('body').off('click', '.js-sortOrderDown').on('click',
		'.js-sortOrderDown',
		function(e) {
			e.preventDefault();
			var data = {};
			data["id"] = $(this).data('id');
			data["order"] = -1;
			$.ajax({
				url: $dynamicTable.data('sortorderurl'),
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				dataType: 'json',
				data: data,
				success: function (json) {
					LoadDynamicFields();
				}
			});
		});
	$('body').off('click', '.js-sortOrderUp').on('click',
		'.js-sortOrderUp',
		function(e) {
			e.preventDefault();
			var data = {};
			data["id"] = $(this).data('id');
			data["order"] = 1;
			$.ajax({
				url: $dynamicTable.data('sortorderurl'),
				beforeSend: function (xhr) {
					xhr.setRequestHeader("RequestVerificationToken",
						$('input:hidden[name="__RequestVerificationToken"]').val());
				},
				dataType: 'json',
				data: data,
				success: function (json) {
					LoadDynamicFields();
				}
			});
		});

	coupleInputs("js-installation-hve", SaveHVE);
	coupleInputs("js-installation-hv", SaveHV);
	coupleInputs("js-installation-analyse", SaveAnalyse);
	coupleInputs("js-installation-value", SaveValue);
	coupleInputs("js-installation-value-usuage", SaveValueUsuage);
	coupleInputs("js-installation-air-change", SaveAirChange);
	coupleInputs("js-installation-filtersvsnaren", SaveFiltersVSnaren);
});

function LoadDynamicFields() {
	var $dynamicTable = $('#dynamicFields');
	try {
		$dynamicTable.DataTable().destroy();
	} catch{
		//empty
	}
	$.ajax({
		url: $dynamicTable.data('geturl'),
		dataType: 'json',
		success: function (json) {

			oTable = $dynamicTable.dataTable({
				'stateSave': true,
				"data": json.data,
				"filter": false,
				"lengthChange": false,
				"orderMulti": false,
				'ordering': false,
				'sorting': false,
				"columns": [
					{ "data": "name" },
					{ "data": "value" },
					{ "data": "sortorder" },
					{ "data": "edit" },
					{ "data": "delete" }
				],
				"aoColumnDefs": [
					{
						"targets": 0,
						"orderable": false
					},
					{
						"targets": 1,
						"orderable": false
					},
					{
						'className': 'center',
						"render": function (data, type, row) {
							var returnValue ='';
							returnValue += '<a class="btn btn-sm btn-light-grey js-sortOrderDown" data-id="'+row.id+'" href="#"><i class="fa fa-arrow-circle-up"></i></a>\r\n';
							returnValue += '<a class="btn btn-sm btn-light-grey js-sortOrderUp" data-id="'+row.id+'" href="#"><i class="fa fa-arrow-circle-down"></i></a>';
							return returnValue;
						},
						"targets": 2,
						"orderable": false
					},
					{
						"render": function (data, type, row) {
							var returnValue = "<a href=\"#\" class=\"edit-row js-editRow\"> Edit </a>";

							return returnValue;
						},
						"targets": 3,
						"orderable": false
					},
					{
						"render": function (data, type, row) {

							var returnValue = "<a href=\"#\" onclick=\"DeleteRow(" + row.id + ", this)\" class=\"delete-row\"> Delete </a>";

							return returnValue;
						},
						"targets": 4,
						"orderable": false
					},
				]
			});

		}
	});
}

function DeleteRow(rowId, item) {
	if (newRow && actualEditingRow) {
		oTable.fnDeleteRow(actualEditingRow);
		newRow = false;
	}
	var nRow = $(item).parents('tr')[0];
	bootbox.confirm("Weet u het zeker?",
		function (result) {
			if (result) {
				event.preventDefault();
				var deleteUrl = $("#dynamicFields").data('deleteurl') + "/" + rowId;
				$.ajax({
					type: "POST",
					beforeSend: function (xhr) {
						xhr.setRequestHeader("RequestVerificationToken",
							$('input:hidden[name="__RequestVerificationToken"]').val());
					},
					url: deleteUrl,
					success: function (returnData) {
						if (returnData) {
							oTable.fnDeleteRow(nRow);
						}
					}
				});
			}
		});
}

function restoreRow(oTable, nRow) {
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);

	for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
		if (i == 0) {
			oTable.fnUpdate(aData.name, nRow, i, false);
		}
		else if (i == 1) {
			oTable.fnUpdate(aData.value, nRow, i, false);
		}
	}
	oTable.fnUpdate('<a class="edit-row" href="">Edit</a>', nRow, 2, false);
	oTable.fnUpdate('<a class="delete-row" href="">Delete</a>', nRow, 3, false);

	oTable.fnDraw();
}

function editRow(oTable, nRow) {
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);
	jqTds[0].innerHTML = '<input type="text" class="form-control" name="name" value="' + aData.name + '">';
	jqTds[0].innerHTML += '<input type="hidden" class="form-control" name="id" value="' + aData.id + '">';
	jqTds[1].innerHTML = '<input type="text" class="form-control" name="value" value="' + aData.value + '">';

	jqTds[2].innerHTML = '<a class="save-row" href="">Save</a>';
	jqTds[3].innerHTML = '<a class="cancel-row" href="">Cancel</a>';

}

function saveRow(oTable, nRow, data) {
	oTable.fnUpdate(data.name, nRow, 0, false);
	oTable.fnUpdate(data.value, nRow, 1, false);
	oTable.fnUpdate('<a class="edit-row" href="">Edit</a>', nRow, 2, false);
	oTable.fnUpdate('<a class="delete-row" href="">Delete</a>', nRow, 3, false);
	oTable.fnDraw();
	newRow = false;
	actualEditingRow = null;
}

function postInstallationValues(postUrl, latestvalue, data) {

	if (latestvalue === data) {
		return;
	}
	$.ajax({
		url: postUrl,
		type: 'POST',
		data: 'value=' + data,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		success: function (result) {
		}
	});
}

function coupleInputs(className, functionName) {
	$("body").on('blur keypress change ifChanged',
		'.' + className,
		function(e) {
			if ($(this).attr('type') === 'text') {
				if (e.which === 0 || e.which === 13) {
					debounce(functionName(this), 300);
				}
			} else {
				debounce(functionName(this), 300);
			}
		});
}

function postValues(elm) {
	$.ajax({
		url: $(elm).data("url"),
		type: 'POST',
		data: "value=" + $(elm).val(),
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		success: function (result) {
		}
	});
}

function SaveHVE(elm) {
	var inputs = $(elm).parent().parent().find("input");
	var completeValue = $(inputs.get(0)).val() + ";" + $(inputs.get(1)).val() + ";" + $(inputs.get(2)).val();
	postInstallationValues($(elm).parent().parent().data("url"), $(elm).parent().parent().data("latestvalue"), completeValue);
}

function SaveHV(elm) {
	var inputs = $(elm).parent().parent().find("input");
	var completeValue = $(inputs.get(0)).val() + ";" + $(inputs.get(1)).val() + ";" + $(inputs.get(2)).val() + ";" + $(inputs.get(3)).val() + ";" + $(inputs.get(4)).val() + ";" + $(inputs.get(5)).val();
	postInstallationValues($(elm).parent().parent().data("url"), $(elm).parent().parent().data("latestvalue"), completeValue);
}

function SaveAnalyse(elm) {
	var inputs = $(elm).parent().parent().find("input");
	var completeValue = $(inputs.get(0)).val() + ";" + $(inputs.get(1)).val() + ";" + $(inputs.get(2)).val() + ";" + $(inputs.get(3)).val() + ";" + $(inputs.get(4)).val();
	postInstallationValues($(elm).parent().parent().data("url"), $(elm).parent().parent().data("latestvalue"), completeValue);
}

function SaveValue(elm) {
	var inputs = $(elm).parent().parent().find("input");
	var completeValue = $(inputs.get(0)).val() + ";" + $(inputs.get(1)).val();
	postInstallationValues($(elm).parent().parent().data("url"), $(elm).parent().parent().data("latestvalue"), completeValue);
}

function SaveValueUsuage(elm) {
	var inputs = $(elm).parent().parent().find("input");
	var completeValue = $(inputs.get(0)).val() + ";" + $(inputs.get(1)).val();
	postInstallationValues($(elm).parent().parent().data("url"), $(elm).parent().parent().data("latestvalue"), completeValue);
}

function SaveAirChange(elm) {
	var closest = $(elm).closest(".dd3-content");
	console.log(closest);
	var inputs = closest.find("input");
	var firstInput = $(inputs.get(0));
	var secondInput = $(inputs.get(1));
	var firstValue = firstInput.is(':checked') ? firstInput.val() : (secondInput.is(':checked') ? secondInput.val() : "");

	var completeValue = firstValue + ";" + $(inputs.get(2)).val();
	postInstallationValues(closest.data("url"), closest.data("latestvalue"), completeValue);
}

function SaveFiltersVSnaren(elm) {
	var inputs = $(elm).parent().parent().find("input");
	var firstInput = $(inputs.get(0)).val();
	var secondInput = $(inputs.get(1)).val();
	var thirdInput = $(inputs.get(2)).val();
	var fourthInput = $(inputs.get(3)).val();
	var fifth = $(inputs.get(4)).val();
	var value = firstInput + ";" + secondInput + ";" + thirdInput + ";" + fourthInput + ';' + fifth;
	postInstallationValues($(elm).parent().parent().data("url"), $(elm).parent().parent().data("latestvalue"), value);

}

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};