﻿namespace FusionCore.Modules.InstallationManager
{
    public class InstallationManagerSettings
    {
        public string TimesheetPath { get; set; }
        public string TemplatePath { get; set; }
        public string PdfPath { get; set; }
    }
}
