﻿namespace FusionCore.Modules.InstallationManager.Data.Enums
{
    public enum FieldKind
    {
        Field = 1,
        DynamicField = 2
    }
}
