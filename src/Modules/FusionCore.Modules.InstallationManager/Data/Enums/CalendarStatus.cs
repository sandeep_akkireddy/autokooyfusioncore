﻿using FusionCore.Core.Extensions;

namespace FusionCore.Modules.InstallationManager.Data.Enums
{
    public enum CalendarStatus
    {
        [StringValue("Feestdag(en)")]
        Holidays = 1,
        [StringValue("Vakantie")]
        Vacation = 2,
        [StringValue("Ziek")]
        Ill = 3,
        [StringValue("Kortverzuim (Tand)Arts")]
        Training = 4,
        [StringValue("Cursus/Scholing")]
        Course = 5,
        [StringValue("Onbetaald verlof")]
        UnpaidLeave = 6,
        [StringValue("Bijzonder Verlof")]
        SpecialLeave = 7
    }
}
