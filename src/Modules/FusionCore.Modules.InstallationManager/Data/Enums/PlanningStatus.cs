﻿using FusionCore.Core.Extensions;

namespace FusionCore.Modules.InstallationManager.Data.Enums
{
    public enum PlanningStatus
    {
        [StringValue("Pre-planning")]
        PrePlanning = 1,
        [StringValue("Planning actief")]
        PlanningActive = 2,
        [StringValue("Technisch gereed")]
        TechnicallyReady = 3,
        [StringValue("Compleet afgerond")]
        Complete = 4,

        Deleted = 99
    }
}
