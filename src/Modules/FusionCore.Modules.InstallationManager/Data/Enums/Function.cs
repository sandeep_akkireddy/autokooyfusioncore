﻿namespace FusionCore.Modules.InstallationManager.Data.Enums
{
    public enum EmployeeFunction
    {
        Employee = 1,
        Planner = 2
    }
}
