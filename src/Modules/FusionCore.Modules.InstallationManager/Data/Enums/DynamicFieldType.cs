using FusionCore.Core.Extensions;

namespace FusionCore.Modules.InstallationManager.Data.Enums
{
    public enum DynamicFieldType
    {
        [StringValue("Header")]
        Header = 1,
        [StringValue("Tekst veld")]
        Text = 2,
        [StringValue("Bullet")]
        Bullet = 3,
        [StringValue("H / V")]
        Hv = 4,
        [StringValue("H / V toelichting")]
        HvExplain = 5,
        [StringValue("Analyse SK")]
        AnalyseSk = 6,
        [StringValue("Waarde")]
        Value = 7,
        [StringValue("Waarde / Verbruik")]
        ValueUsuage = 8,
        //[StringValue("Air TV / RT / AV")]
        //AirTvRtAv = 9,
        //[StringValue("Air waarde")]
        //AirWaarde = 10,
        //[StringValue("Air type")]
        //AirType = 11,
        [StringValue("Air vervangen")]
        AirChange = 12,
        [StringValue("Foto")]
        Image = 13,
        [StringValue("Analyse KT")]
        AnalyseKt = 14,
        [StringValue("Pagebreak")]
        Pagebreak = 15,
        [StringValue("Filters / V-Snaren")]
        FiltersVSnaren = 16,
        [StringValue("File")]
        File = 17,
        [StringValue("Filters / V-Snaren WV")]
        FiltersVSnarenWV = 18,
    }
}
