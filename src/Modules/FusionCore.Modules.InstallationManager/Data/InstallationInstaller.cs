using FusionCore.Core.Enums;
using FusionCore.Core.Extensions;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Modules.InstallationManager.Services;
using FusionCore.Services.Common;
using FusionCore.Services.User;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Data
{
    public class InstallationInstaller : IInstallData
    {
        private IActivityService _activityService;
        private IAssetService _assetService;
        private IBuildingService _buildingService;
        private ICalendarService _calendarService;
        private IClientService _clientService;
        private IDivisionService _divisionService;
        private IEmployeeService _employeeService;
        private IPlanningService _planningService;
        private AssetActivity _activity;
        private IUserService _userService;

        private IGenericRepository<Asset> _assetRepository;
        private IGenericRepository<AssetActivity> _assetActivityRepository;
        private IGenericRepository<Building> _buildingRepository;
        private IGenericRepository<BuildingLocation> _buildingLocationRepository;
        private IGenericRepository<Client> _clientRepository;
        private IGenericRepository<ClientUser> _clientUserRepository;

        public async Task Install(IServiceProvider serviceProvider, bool sampleData)
        {
            LoadServices(serviceProvider);
            await InstallInstallationManagerMenu(serviceProvider);
            if (sampleData)
            {
                InstallDivisionDummyData();
                InstallClientDummyData();
                InstallBuildingDummyData();
                InstallActivityDummyData();
                await InstallAssetDummyDataAsync();
                InstallEmployeeDummyData();
                await InstallPlanningDummyData();
            }

            DisposeServices();
        }

        private void DisposeServices()
        {
            _activityService = null;
            _assetService = null;
            _buildingService = null;
            _calendarService = null;
            _clientService = null;
            _divisionService = null;
            _employeeService = null;
            _planningService = null;
            _userService = null;

            _assetRepository = null;
            _assetActivityRepository = null;
            _buildingRepository = null;
            _buildingLocationRepository = null;
            _clientRepository = null;
            _clientUserRepository = null;
        }

        private void LoadServices(IServiceProvider serviceProvider)
        {
            _activityService = (IActivityService)serviceProvider.GetService(typeof(IActivityService));
            _assetService = (IAssetService)serviceProvider.GetService(typeof(IAssetService));
            _buildingService = (IBuildingService)serviceProvider.GetService(typeof(IBuildingService));
            _calendarService = (ICalendarService)serviceProvider.GetService(typeof(ICalendarService));
            _clientService = (IClientService)serviceProvider.GetService(typeof(IClientService));
            _divisionService = (IDivisionService)serviceProvider.GetService(typeof(IDivisionService));
            _employeeService = (IEmployeeService)serviceProvider.GetService(typeof(IEmployeeService));
            _planningService = (IPlanningService)serviceProvider.GetService(typeof(IPlanningService));
            _userService = (IUserService)serviceProvider.GetService(typeof(IUserService));

            _assetRepository = (IGenericRepository<Asset>)serviceProvider.GetService(typeof(IGenericRepository<Asset>));
            _assetActivityRepository = (IGenericRepository<AssetActivity>)serviceProvider.GetService(typeof(IGenericRepository<AssetActivity>));
            _buildingRepository = (IGenericRepository<Building>)serviceProvider.GetService(typeof(IGenericRepository<Building>));
            _buildingLocationRepository = (IGenericRepository<BuildingLocation>)serviceProvider.GetService(typeof(IGenericRepository<BuildingLocation>));
            _clientRepository = (IGenericRepository<Client>)serviceProvider.GetService(typeof(IGenericRepository<Client>));
            _clientUserRepository = (IGenericRepository<ClientUser>)serviceProvider.GetService(typeof(IGenericRepository<ClientUser>));
        }

        private async Task InstallPlanningDummyData()
        {
            BuildingLocation buildingLocation = _buildingService.GetBuildingLocation(1);
            Employee employee = _employeeService.GetEmployee(1);
            User user = _userService.GetUser(1);
            PlanningPersistModel planningPersistModel = new PlanningPersistModel
            {
                AssetId = _activity.AssetId,
                AssetActivityId = _activity.Id,
                BuildingLocationId = buildingLocation.Id,
                BuildingId = buildingLocation.BuildingId,
                ClientId = buildingLocation.Building.ClientId,
                StartTimeString = "09:00",
                EndTimeString = "20:00",
                DateString = DateTime.Now.ToString("dd-MM-yyyy") + " - " +
                             DateTime.Now.AddDays(1).ToString("dd-MM-yyyy"),
                OfferNumber = "Planning 1",
                MainEmployeeId = employee.Id,
                StatusId = (int)PlanningStatus.PrePlanning
            };
            _planningService.PersistPlanning(planningPersistModel, user);

            planningPersistModel.DateString = DateTime.Now.AddDays(1).ToString("dd-MM-yyyy") + " - " +
                                              DateTime.Now.AddDays(2).ToString("dd-MM-yyyy");
            planningPersistModel.StatusId = (int)PlanningStatus.PlanningActive;
            _planningService.PersistPlanning(planningPersistModel, user);


            planningPersistModel.DateString = DateTime.Now.AddDays(2).ToString("dd-MM-yyyy") + " - " +
                                              DateTime.Now.AddDays(3).ToString("dd-MM-yyyy");
            planningPersistModel.StatusId = (int)PlanningStatus.PlanningActive;
            _planningService.PersistPlanning(planningPersistModel, user);


            planningPersistModel.StatusId = (int)PlanningStatus.TechnicallyReady;
            _planningService.PersistPlanning(planningPersistModel, user);

            planningPersistModel.StatusId = (int)PlanningStatus.Complete;
            _planningService.PersistPlanning(planningPersistModel, user);

            await _planningService.InsertHistoryAsync(new AssetActivityHistory
            {
                PlanningId = 3,
                AssetActivityId = _activity.Id,
                AssetId = _activity.AssetId
            });
            Planning planning = _planningService.GetPlanning(4);
            await PlanningMapper.PrepareHistory(planning, _planningService);

            planning = _planningService.GetPlanning(5);
            await PlanningMapper.PrepareHistory(planning, _planningService);

            planningPersistModel.MainEmployeeId = 2;
            planningPersistModel.OfferNumber += " sub";
            planningPersistModel.StatusId = (int)PlanningStatus.PlanningActive;
            planningPersistModel.PlanningEmployees.Add(new PlanningPersistModel.PlanningEmployeeModel()
            { EmployeeId = employee.Id });

            _planningService.PersistPlanning(planningPersistModel, user);
        }

        private void InstallActivityDummyData()
        {
            _activityService.AddAssetType("Type 01");
            _activityService.AddAssetType("Type 02");
            _activityService.AddAssetType("Type 03");
            _activityService.AddAssetType("Type 04");

            AssetType assetType = _activityService.GetAssetTypes(x => x.Name.Equals("Type 01")).First();

            _activityService.AddActivity("Activity 01", assetType.Id);
            _activityService.AddActivity("Activity 02", assetType.Id);
            _activityService.AddActivity("Activity 03", assetType.Id);
            _activityService.AddActivity("Activity 04", assetType.Id);

            Activity activity = _activityService.GetActivitesByAssetType(assetType.Id).First();

            _activityService.AddDynamicField("Header", activity.Id, DynamicFieldType.Header);
            _activityService.AddDynamicField("Text", activity.Id, DynamicFieldType.Text);

            _activityService.AddField("Header", activity.Id, DynamicFieldType.Header);
            _activityService.AddField("Text", activity.Id, DynamicFieldType.Text);
            _activityService.AddField("Bullet", activity.Id, DynamicFieldType.Bullet);
        }

        private void InstallEmployeeDummyData()
        {
            EmployeeGroup employeeGroup = new EmployeeGroup
            {
                Name = "Service"
            };
            _employeeService.AddEmployeeGroup(employeeGroup);
            _employeeService.AddEmployeeGroup(new EmployeeGroup
            {
                Name = "Water"
            });
            _employeeService.AddEmployeeGroup(new EmployeeGroup
            {
                Name = "Air"
            });

            for (int i = 0; i < 16; i++)
            {
                EmployeePersistModel employeePersistModel = new EmployeePersistModel
                {
                    Firstname = "Martijn - " + i,
                    Middlename = "van",
                    Lastname = "Valen",
                    Address = "Duizendknooplaan 19",
                    PostalCode = "1234AB",
                    City = "Utrecht",
                    Country = "the Netherlands",
                    Email = "info@novimediasolutions.nl",
                    Phonenumber = "123123123",
                    Mobilenumber = "06123456789",
                    EmployeeFunction = i % 2 == 0 ? EmployeeFunction.Employee : EmployeeFunction.Planner,
                    Active = true
                };
                if (i % 2 == 0)
                {
                    employeePersistModel.Employee2EmployeeGroups.Add(new Employee2EmployeeGroupModel
                    {
                        EmployeeGroupId = employeeGroup.Id
                    });
                }

                if (i == 0)
                {
                    employeePersistModel.Username = "test";
                    employeePersistModel.Password = "123";
                    employeePersistModel.PasswordControle = "123";
                }
                _employeeService.PersistEmployee(employeePersistModel);

            }

            Employee employee = _employeeService.GetEmployee(1);
            CreateRandomCalendarItems(employee);

        }

        private void CreateRandomCalendarItems(Employee employeePersistModel)
        {
            for (int i = 0; i < 10; i++)
            {
                CalendarItemPersistModel calendarItemPersistModel = new CalendarItemPersistModel
                {
                    EmployeeId = employeePersistModel.Id,
                    CalendarStatus = CalendarStatus.SpecialLeave,
                    StartTimeString = "08:00",
                    EndTimeString = "17:00",
                    Remark = "Test",
                    Subject = "Calendar item - " + i,
                    WholeDay = i % 2 == 0
                };
                DateTime startDate = DateTime.Now.GetRandomDayInCurrentMonth();
                DateTime endDate = startDate.AddDays(i % 2 == 0 ? 1 : 2).AddHours(23).AddMinutes(59);
                calendarItemPersistModel.DateString = startDate.ToString("dd-MM-yyyy") + " - " + endDate.ToString("dd-MM-yyyy");
                _calendarService.PersistCalendarItem(calendarItemPersistModel);
            }
        }

        private async Task InstallAssetDummyDataAsync()
        {
            FusionCore.Models.Common.IPagedList<Building> buildings = _buildingService.GetAllBuildings<Building>();
            if (buildings != null)
            {
                for (int i = 0; i < 12; i++)
                {
                    Building building = buildings.Skip(i % 2).FirstOrDefault();
                    if (building != null)
                    {
                        Asset asset = new Asset
                        {
                            Name = $"Asset - {i}",
                            AssetTypeId = _activityService.GetAssetTypes().FirstOrDefault().Id,
                            BuildingId = building.Id,
                            BuildingLocationId = building.BuildingLocations.FirstOrDefault()?.Id ?? 0
                        };
                        _assetRepository.Insert(asset);

                        for (int j = 0; j < (i % 2 == 0 ? 4 : 7); j++)
                        {
                            _assetService.PersistAssetDynamicField(new AssetDynamicFieldPersistModel
                            {
                                AssetId = asset.Id,
                                Name = $"Field - {j}",
                                Value = $"Value - {j}"
                            });
                        }

                        if (i == 0)
                        {
                            _activity = new AssetActivity
                            {
                                Name = "Activiteit 1",
                                AssetId = asset.Id,
                                Active = true
                            };
                            _assetActivityRepository.Insert(_activity);

                            foreach (DynamicFieldType item in Enum.GetValues(typeof(DynamicFieldType)).Cast<DynamicFieldType>())
                            {
                                _activity.AssetActivityFields.Add(new AssetActivityField
                                {
                                    AssetActivityId = _activity.Id,
                                    Name = item.GetStringValue(),
                                    DynamicFieldType = item,
                                });
                            }
                            _assetActivityRepository.Update(_activity);

                            _assetActivityRepository.Insert(new AssetActivity
                            {
                                AssetId = asset.Id,
                                Name = "Activiteit 2",
                                SortOrder = 1
                            });
                        }
                    }
                }

                _assetService.PersistAsset(new AssetPersistModel
                {
                    Name = "Asset - empty 1",
                    AssetTypeId = _activityService.GetAssetTypes().FirstOrDefault().Id,
                });

                _assetService.PersistAsset(new AssetPersistModel
                {
                    Name = "Asset - empty 2",
                    AssetTypeId = _activityService.GetAssetTypes().FirstOrDefault().Id,
                });
            }
        }

        private void InstallBuildingDummyData()
        {
            System.Collections.Generic.List<Client> clients = _clientService.GetAllClients<Client>().ToList();
            for (int i = 0; i < 12; i++)
            {
                _buildingService.PersistBuilding(new BuildingPersistModel
                {
                    Name = "Building - Novi Media " + i,
                    Firstname = "Martijn",
                    Middlename = "van",
                    Lastname = "Valen",
                    Address = "Duizendknooplaan 19",
                    PostalCode = "1234AB",
                    City = "Utrecht",
                    Country = "the Netherlands",
                    Email = "info@novimediasolutions.nl",
                    Phonenumber = "123123123",
                    Mobilenumber = "06123456789",
                    Website = "https://www.novimediasolutions.nl",
                    Latitude = "123.123",
                    Longtitude = "345.345",
                    ClientId = clients.Skip(Convert.ToInt32(i % 2)).Take(1).FirstOrDefault()?.Id ?? 0
                });
            }

            System.Collections.Generic.List<Building> buildings = _buildingRepository.GetAll().ToList();

            foreach (Building building in buildings)
            {
                for (int i = 0; i < new Random().Next(1, 5); i++)
                {
                    BuildingLocation buildingLocation = new BuildingLocation
                    {
                        BuildingId = building.Id,
                        Name = "Building location - " + i,
                        Floor = (100 + i).ToString(),
                        RoomNumber = (i + 5 * i).ToString(),
                        Remarks = "Test opmerking"
                    };
                    _buildingLocationRepository.Insert(buildingLocation);
                }

            }
        }

        private void InstallClientDummyData()
        {
            for (int i = 0; i < 12; i++)
            {
                _clientService.PersistClient(new ClientPersistModel
                {
                    Name = "Client - Novi Media " + i,
                    Firstname = "Martijn",
                    Middlename = "van",
                    Lastname = "Valen",
                    Address = "Duizendknooplaan 19",
                    PostalCode = "1234AB",
                    City = "Utrecht",
                    Country = "the Netherlands",
                    Email = "info@novimediasolutions.nl",
                    Phonenumber = "123123123",
                    Mobilenumber = "06123456789",
                    Website = "https://www.novimediasolutions.nl",
                    Active = i % 2 == 0
                });
            }

            System.Collections.Generic.List<Client> clients = _clientService.GetAllClients<Client>().ToList();
            Division[] divisions = _divisionService.GetAllDivisions<Division>().ToArray();
            bool secondUser = false;
            foreach (Client client in clients)
            {
                Division division = divisions[new Random().Next(0, divisions.Length)];
                client.ClientDivisions.Add(new ClientDivision
                {
                    ClientId = client.Id,
                    DivisionId = division.Id
                });

                _clientRepository.Update(client);

                division = divisions[new Random().Next(0, divisions.Length)];
                client.ClientDivisions.Add(new ClientDivision
                {
                    ClientId = client.Id,
                    DivisionId = division.Id
                });

                _clientRepository.Update(client);

                _clientUserRepository.Insert(new ClientUser
                {
                    ClientId = client.Id,
                    Username = "test@test.nl",
                    Firstname = "Martijn",
                    Middlename = "van",
                    Lastname = "Valen",
                    Active = true
                });
                if (secondUser)
                {
                    _clientUserRepository.Insert(new ClientUser
                    {
                        ClientId = client.Id,
                        Username = "test@test.nl",
                        Firstname = "Martijn",
                        Middlename = "van",
                        Lastname = "Valen",
                        Active = true
                    });
                    // ReSharper disable once RedundantAssignment
                    secondUser = false;
                }

                secondUser = true;
            }
        }

        private void InstallDivisionDummyData()
        {
            for (int i = 0; i < 16; i++)
            {
                _divisionService.PersistDivision(new DivisionPersistModel
                {
                    Name = "Division - Novi Media " + i,
                    Firstname = "Martijn",
                    Middlename = "van",
                    Lastname = "Valen",
                    Address = "Duizendknooplaan 19",
                    PostalCode = "1234AB",
                    City = "Utrecht",
                    Country = "the Netherlands",
                    Email = "info@novimediasolutions.nl",
                    Phonenumber = "123123123",
                    Mobilenumber = "06123456789",
                    Website = "https://www.novimediasolutions.nl"
                });
            }

        }

        private async Task InstallInstallationManagerMenu(IServiceProvider serviceProvider)
        {
            IMenuService menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

            MenuItem mainMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "fa fa-building",
                Name = "Installatie manager",
                DisplayOrder = 10,
                ParentId = null
            };
            await menuService.InsertMenuItem(mainMenuItem);

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Divisie",
                Area = "InstallationManager",
                Controller = "Division",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 0,
                ParentId = mainMenuItem.Id,
                HasNewButton = true
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Opdrachtgevers",
                Area = "InstallationManager",
                Controller = "Client",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 10,
                ParentId = mainMenuItem.Id,
                HasNewButton = true
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Gebouwen",
                Area = "InstallationManager",
                Controller = "Building",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 20,
                ParentId = mainMenuItem.Id,
                HasNewButton = true
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Assets",
                Area = "InstallationManager",
                Controller = "Asset",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 30,
                ParentId = mainMenuItem.Id,
                HasNewButton = true
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Werknemers",
                Area = "InstallationManager",
                Controller = "Employee",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 40,
                ParentId = mainMenuItem.Id,
                HasNewButton = true
            });

            MenuItem planningMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Planning",
                Area = "InstallationManager",
                Controller = "Planning",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 50,
                ParentId = mainMenuItem.Id,
                HasNewButton = false
            };
            await menuService.InsertMenuItem(planningMenuItem);

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Planning",
                Area = "InstallationManager",
                Controller = "Planning",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 10,
                ParentId = planningMenuItem.Id,
                HasNewButton = false
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Overzicht",
                Area = "InstallationManager",
                Controller = "Planning",
                Action = "Overview",
                Parameters = null,
                DisplayOrder = 20,
                ParentId = planningMenuItem.Id,
                HasNewButton = false
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Controle",
                Area = "InstallationManager",
                Controller = "Controle",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 60,
                ParentId = mainMenuItem.Id,
                HasNewButton = false
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = string.Empty,
                Name = "Rapportages",
                Area = "InstallationManager",
                Controller = "Report",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 70,
                ParentId = mainMenuItem.Id,
                HasNewButton = false
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "fa clip-cog-2",
                Name = "Activiteiten beheer",
                Area = "InstallationManager",
                Controller = "Activity",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 80,
                ParentId = mainMenuItem.Id,
                HasNewButton = false
            });

            await menuService.InsertMenuItem(new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "fa clip-cog-2",
                Name = "Groepen beheer",
                Area = "InstallationManager",
                Controller = "Employee",
                Action = "GroupManagement",
                Parameters = null,
                DisplayOrder = 90,
                ParentId = mainMenuItem.Id,
                HasNewButton = false
            });
        }
    }
}