﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class TimeSheetTimeRowMap : IEntityTypeConfiguration<TimesheetTimeRow>
    {
        public void Configure(EntityTypeBuilder<TimesheetTimeRow> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasOne(x => x.Planning).WithMany().HasForeignKey(x => x.PlanningId);
            builder.HasOne(x => x.CalendarItem).WithMany().HasForeignKey(x => x.CalendarItemId);
        }
    }
}
