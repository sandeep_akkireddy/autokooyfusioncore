﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class EmployeeMap : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Firstname).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Middlename).HasMaxLength(50);
            builder.Property(x => x.Lastname).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Address).HasMaxLength(256);
            builder.Property(x => x.PostalCode).HasMaxLength(50);
            builder.Property(x => x.City).HasMaxLength(256);
            builder.Property(x => x.Country).HasMaxLength(256);
            builder.Property(x => x.Email).HasMaxLength(256);
            builder.Property(x => x.Phonenumber).HasMaxLength(50);
            builder.Property(x => x.Mobilenumber).HasMaxLength(50);

            builder.HasMany(x => x.Employee2EmployeeGroups).WithOne(x => x.Employee).HasForeignKey(x => x.EmployeeId)
                .OnDelete(DeleteBehavior.Cascade);

            //softdelete
            builder.HasQueryFilter(x => !x.DeletedOnUtc.HasValue);
        }
    }
}
