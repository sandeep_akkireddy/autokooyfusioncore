﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class AssetActivityMap : IEntityTypeConfiguration<AssetActivity>
    {
        public void Configure(EntityTypeBuilder<AssetActivity> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);

            builder.HasMany(x => x.AssetActivityFields).WithOne().HasForeignKey(x => x.AssetActivityId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
