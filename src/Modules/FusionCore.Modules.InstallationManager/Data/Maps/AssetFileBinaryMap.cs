﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class AssetFileBinaryMap : IEntityTypeConfiguration<AssetFileBinary>
    {
        public void Configure(EntityTypeBuilder<AssetFileBinary> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.AssetFileId).IsRequired();
        }
    }
}