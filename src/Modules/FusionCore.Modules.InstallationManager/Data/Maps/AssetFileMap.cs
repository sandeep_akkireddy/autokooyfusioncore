﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class AssetFileMap : IEntityTypeConfiguration<AssetFile>
    {
        public void Configure(EntityTypeBuilder<AssetFile> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.AssetId).IsRequired();

            builder.HasOne(x => x.Planning).WithMany().HasForeignKey(x => x.PlanningId);
        }
    }
}