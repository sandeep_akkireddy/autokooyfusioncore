﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class AssetMap : IEntityTypeConfiguration<Asset>
    {
        public void Configure(EntityTypeBuilder<Asset> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);

            builder.HasOne(x => x.Building).WithMany().HasForeignKey(x => x.BuildingId);
            builder.HasOne(x => x.AssetType).WithMany().HasForeignKey(x => x.AssetTypeId);
            builder.HasOne(x => x.BuildingLocation).WithMany().HasForeignKey(x => x.BuildingLocationId);
            builder.HasMany(x => x.AssetDynamicFields).WithOne().HasForeignKey(x => x.AssetId);
            builder.HasMany(x => x.AssetFields).WithOne().HasForeignKey(x => x.AssetId);
            builder.HasMany(x => x.AssetFiles).WithOne().HasForeignKey(x => x.AssetId);

            //softdelete
            builder.HasQueryFilter(x => !x.DeletedOnUtc.HasValue);
        }
    }
}