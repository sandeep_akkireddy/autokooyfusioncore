﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class PlanningEmployeeMap : IEntityTypeConfiguration<PlanningEmployee>
    {
        public void Configure(EntityTypeBuilder<PlanningEmployee> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.EmployeeId).IsRequired();
            builder.Property(x => x.PlanningId).IsRequired();

            builder.HasOne(x => x.Planning).WithMany(x => x.PlanningEmployees).HasForeignKey(x => x.PlanningId);
        }
    }
}