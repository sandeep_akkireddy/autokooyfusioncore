﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class AssetActivityFieldMap : IEntityTypeConfiguration<AssetActivityField>
    {
        public void Configure(EntityTypeBuilder<AssetActivityField> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.LatestValue).HasMaxLength(256);
        }
    }
}
