﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class PlanningMap : IEntityTypeConfiguration<Planning>
    {
        public void Configure(EntityTypeBuilder<Planning> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.OfferNumber).IsRequired().HasMaxLength(256);
            builder.Property(x => x.ErrorMessage).HasMaxLength(256);
            builder.Property(x => x.CustomColor).HasMaxLength(50);

            builder.HasOne(x => x.Asset).WithMany().HasForeignKey(x => x.AssetId);
            builder.HasOne(x => x.AssetActivity).WithMany().HasForeignKey(x => x.AssetActivityId);
            builder.HasOne(x => x.Division).WithMany().HasForeignKey(x => x.DivisionId);
            builder.HasOne(x => x.Client).WithMany().HasForeignKey(x => x.ClientId);
            builder.HasOne(x => x.Building).WithMany().HasForeignKey(x => x.BuildingId);
            builder.HasOne(x => x.BuildingLocation).WithMany().HasForeignKey(x => x.BuildingLocationId);
            builder.HasOne(x => x.MainEmployee).WithMany().HasForeignKey(x => x.MainEmployeeId);
            builder.HasMany(x => x.AssetActivityHistories).WithOne(x => x.Planning).HasForeignKey(x => x.PlanningId);
        }
    }
}
