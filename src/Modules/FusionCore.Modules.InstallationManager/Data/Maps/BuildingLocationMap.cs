﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class BuildingLocationMap : IEntityTypeConfiguration<BuildingLocation>
    {
        public void Configure(EntityTypeBuilder<BuildingLocation> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Floor).HasMaxLength(256);
            builder.Property(x => x.RoomNumber).HasMaxLength(256);
            builder.Property(x => x.Remarks).HasMaxLength(int.MaxValue);

            builder.HasOne(x => x.Building).WithMany(x => x.BuildingLocations).HasForeignKey(x => x.BuildingId);
        }
    }
}