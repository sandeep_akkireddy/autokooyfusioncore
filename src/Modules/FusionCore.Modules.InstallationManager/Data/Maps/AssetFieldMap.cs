﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class AssetFieldMap : IEntityTypeConfiguration<AssetField>
    {
        public void Configure(EntityTypeBuilder<AssetField> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(200);
            builder.Property(x => x.LatestValue).HasMaxLength(200);

            builder.HasOne(x => x.Asset).WithMany(x => x.AssetFields).HasForeignKey(x => x.AssetId);
            builder.HasOne(x => x.Activity).WithMany().HasForeignKey(x => x.ActivityId);
        }
    }
}
