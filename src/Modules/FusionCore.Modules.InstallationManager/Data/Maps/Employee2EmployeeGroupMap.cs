﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class Employee2EmployeeGroupMap : IEntityTypeConfiguration<Employee2EmployeeGroup>
    {
        public void Configure(EntityTypeBuilder<Employee2EmployeeGroup> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasOne(x => x.Employee).WithMany(x => x.Employee2EmployeeGroups).HasForeignKey(x => x.EmployeeId);
            builder.HasOne(x => x.EmployeeGroup).WithMany().HasForeignKey(x => x.EmployeeGroupId);
        }
    }
}
