﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class CalendarItemMap : IEntityTypeConfiguration<CalendarItem>
    {
        public void Configure(EntityTypeBuilder<CalendarItem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Subject).IsRequired().HasMaxLength(256);

            builder.HasOne(x => x.Employee).WithMany(x => x.CalendarItems).HasForeignKey(x => x.EmployeeId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
