﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class AssetDynamicFieldMap : IEntityTypeConfiguration<AssetDynamicField>
    {
        public void Configure(EntityTypeBuilder<AssetDynamicField> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Value).IsRequired().HasMaxLength(256);

            builder.HasOne(x => x.Asset).WithMany().HasForeignKey(x => x.AssetId);
        }
    }
}