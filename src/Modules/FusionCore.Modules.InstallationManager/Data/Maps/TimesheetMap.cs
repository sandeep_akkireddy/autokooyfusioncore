﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class TimesheetMap : IEntityTypeConfiguration<Timesheet>
    {
        public void Configure(EntityTypeBuilder<Timesheet> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasMany(x => x.TimeSheetHomeWorkRows).WithOne().HasForeignKey(x => x.TimesheetId);
            builder.HasMany(x => x.TimeSheetTimeRows).WithOne().HasForeignKey(x => x.TimesheetId);
        }
    }
}
