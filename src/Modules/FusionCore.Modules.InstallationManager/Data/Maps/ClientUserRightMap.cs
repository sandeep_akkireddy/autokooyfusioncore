﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class ClientUserRightMap : IEntityTypeConfiguration<ClientUserRight>
    {
        public void Configure(EntityTypeBuilder<ClientUserRight> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.ClientUserId).IsRequired();

            builder.HasOne(x => x.ClientUser).WithMany(x => x.ClientUserRights).HasForeignKey(x => x.ClientUserId);
        }
    }
}