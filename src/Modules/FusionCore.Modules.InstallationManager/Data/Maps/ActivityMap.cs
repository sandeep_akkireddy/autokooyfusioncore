﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class ActivityMap : IEntityTypeConfiguration<Activity>
    {
        public void Configure(EntityTypeBuilder<Activity> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);

            builder.HasOne(x => x.AssetType).WithMany().HasForeignKey(x => x.AssetTypeId);
            builder.HasMany(x => x.ActivityFields).WithOne().HasForeignKey(x => x.ActivityId).OnDelete(DeleteBehavior.Cascade);
            builder.HasMany(x => x.ActivityDynamicFields).WithOne().HasForeignKey(x => x.ActivityId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
