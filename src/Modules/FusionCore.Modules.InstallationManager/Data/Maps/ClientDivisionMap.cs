﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class ClientDivisionMap : IEntityTypeConfiguration<ClientDivision>
    {
        public void Configure(EntityTypeBuilder<ClientDivision> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.ClientId).IsRequired();
            builder.Property(x => x.DivisionId).IsRequired();

            builder.HasOne(x => x.Client).WithMany(x => x.ClientDivisions).HasForeignKey(x => x.ClientId);
        }
    }
}