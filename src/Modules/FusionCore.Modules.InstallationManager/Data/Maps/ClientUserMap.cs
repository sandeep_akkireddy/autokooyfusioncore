﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class ClientUserMap : IEntityTypeConfiguration<ClientUser>
    {
        public void Configure(EntityTypeBuilder<ClientUser> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Username).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Firstname).HasMaxLength(256);
            builder.Property(x => x.Middlename).HasMaxLength(50);
            builder.Property(x => x.Lastname).HasMaxLength(256);

            builder.HasOne(x => x.Client).WithMany().HasForeignKey(x => x.ClientId);
            builder.HasMany(x => x.ClientUserRights).WithOne(x => x.ClientUser).HasForeignKey(x => x.ClientUserId);
        }
    }
}