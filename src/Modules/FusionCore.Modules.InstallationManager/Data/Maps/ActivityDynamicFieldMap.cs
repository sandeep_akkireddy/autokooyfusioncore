﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.InstallationManager.Data.Maps
{
    public class ActivityDynamicFieldMap : IEntityTypeConfiguration<ActivityDynamicField>
    {
        public void Configure(EntityTypeBuilder<ActivityDynamicField> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Value).HasMaxLength(256);

            builder.HasOne(x => x.Activity).WithMany(x => x.ActivityDynamicFields).HasForeignKey(x => x.ActivityId);
        }
    }
}
