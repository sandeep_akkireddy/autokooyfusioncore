using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using System;
using System.Globalization;
using System.Linq;

namespace FusionCore.Modules.InstallationManager.Data
{
    public class AutomapperConfiguration : Profile
    {
        public AutomapperConfiguration()
        {
            CreateMap<Division, DivisionDataRow>()
                .ForMember(d => d.Contact, o => o.MapFrom(s => s.GetContact))
                ;

            CreateMap<Division, DivisionPersistModel>();
            CreateMap<DivisionPersistModel, Division>();

            CreateMap<Client, ClientDataRow>()
                .ForMember(d => d.Contact, o => o.MapFrom(s => s.GetContact))
                ;

            CreateMap<ClientDivision, ClientDivisionModel>();
            CreateMap<ClientDivisionModel, ClientDivision>();

            CreateMap<Client, ClientPersistModel>()
                .ForMember(d => d.AvailableDivisions, o => o.Ignore());
            CreateMap<ClientPersistModel, Client>();

            CreateMap<ClientUser, ClientUserPersistModel>()
                .ForMember(d => d.ClientName, o => o.MapFrom(s => s.Client.Name));
            CreateMap<ClientUserPersistModel, ClientUser>()
                .ForMember(d => d.Client, o => o.Ignore())
                .ForMember(d => d.ClientUserRights, o => o.Ignore())
                ;

            CreateMap<ClientUserRight, ClientUserRightModel>();
            CreateMap<ClientUserRightModel, ClientUserRight>();

            CreateMap<ClientUser, ClientUserDataRow>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.GetFullname))
                ;

            CreateMap<Building, BuildingDataRow>()
                .ForMember(d => d.Contact, o => o.MapFrom(s => s.GetContact))
                .ForMember(d => d.Client, o => o.MapFrom(s => s.Client.Name))
                ;

            CreateMap<Building, BuildingPersistModel>()
                .ForMember(d => d.AvailableClients, o => o.Ignore());
            CreateMap<BuildingPersistModel, Building>();

            CreateMap<BuildingLocation, BuildingLocationPersistModel>()
                .ForMember(d => d.BuildingName, o => o.MapFrom(s => s.Building.Name));
            CreateMap<BuildingLocationPersistModel, BuildingLocation>()
                .ForMember(d => d.Building, o => o.Ignore());

            CreateMap<BuildingLocation, BuildingLocationDataRow>()
                ;

            CreateMap<Asset, AssetDataRow>()
                .ForMember(d => d.BuildingName, o => o.MapFrom(s => s.Building.Name))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.BuildingLocation.Name))
                ;

            CreateMap<Asset, BuildingLocationAssetDataRow>();

            CreateMap<Asset, AssetPersistModel>()
                .ForMember(d => d.AvailableLocations, o => o.Ignore())
                .ForMember(d => d.AssetTypeName, o => o.MapFrom(s => s.AssetType.Name));
            CreateMap<AssetPersistModel, Asset>();


            CreateMap<AssetDynamicField, AssetDynamicFieldDataRow>();

            CreateMap<AssetDynamicField, AssetDynamicFieldPersistModel>();
            CreateMap<AssetDynamicFieldPersistModel, AssetDynamicField>();

            CreateMap<AssetActivity, AssetActivityPersistModel>();
            CreateMap<AssetActivityPersistModel, AssetActivity>();

            CreateMap<AssetActivityField, AssetActivityFieldPersistModel>();
            CreateMap<AssetActivityFieldPersistModel, AssetActivityField>();

            CreateMap<AssetActivityField, AssetActivityFieldNamePersistModel>();
            CreateMap<AssetActivityFieldNamePersistModel, AssetActivityField>();
            CreateMap<AssetActivityFieldValuePersistModel, AssetActivityField>();

            CreateMap<Employee, EmployeeDataRow>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.GetFullname))
                .ForMember(d => d.Group, o => o.MapFrom(s => string.Join(", ", s.Employee2EmployeeGroups.Select(x => x.EmployeeGroup.Name))));

            CreateMap<Employee2EmployeeGroup, Employee2EmployeeGroupModel>();
            CreateMap<Employee2EmployeeGroupModel, Employee2EmployeeGroup>();

            CreateMap<Employee, EmployeePersistModel>()
                .ForMember(d => d.AvailableEmployeeGroups, o => o.Ignore());
            CreateMap<EmployeePersistModel, Employee>();

            CreateMap<CalendarItem, CalendarItemPersistModel>()
                .ForMember(d => d.StartTimeString, o => o.MapFrom(s => s.StartDate.Hour.ToString("D2") + ":" + s.StartDate.Minute.ToString("D2")))
                .ForMember(d => d.EndTimeString, o => o.MapFrom(s => s.EndDate.Hour.ToString("D2") + ":" + s.EndDate.Minute.ToString("D2")))
                .ForMember(d => d.DateString, o => o.MapFrom(s => s.StartDate.ToString("dd-MM-yyyy") + " - " + (s.WholeDay ? s.EndDate.AddDays(-1) : s.EndDate).ToString("dd-MM-yyyy")));




            CreateMap<CalendarItemPersistModel, CalendarItem>()
                    .ForMember(d => d.StartDate, o => o.MapFrom<DateTime>(s => CalendarStartDateResolver.convert(s)))
                .ForMember(d => d.EndDate, o => o.MapFrom<DateTime>(s => CalendarEndDateResolver.convert(s)));

            //.ForMember(d => d.StartDate, o => o.ResolveUsing<CalendarStartDateResolver>())
            //.ForMember(d => d.EndDate, o => o.ResolveUsing<CalendarEndDateResolver>());


            CreateMap<Planning, PlanningPersistModel>()
                .ForMember(d => d.CustomColorToggle, o => o.MapFrom(s => !string.IsNullOrEmpty(s.CustomColor)))
                .ForMember(d => d.StartTimeString, o => o.MapFrom(s => s.StartDate.Hour.ToString("D2") + ":" + s.StartDate.Minute.ToString("D2")))
                .ForMember(d => d.EndTimeString, o => o.MapFrom(s => s.EndDate.Hour.ToString("D2") + ":" + s.EndDate.Minute.ToString("D2")))
                .ForMember(d => d.DateString, o => o.MapFrom(s => s.StartDate.ToString("dd-MM-yyyy") + " - " + s.EndDate.ToString("dd-MM-yyyy")));
            CreateMap<PlanningPersistModel, Planning>()
                .ForMember(d => d.CustomColor, o => o.MapFrom(s => CustomColorResolver.convert(s)))
                .ForMember(d => d.StartDate, o => o.MapFrom<DateTime>(s => CalendarStartDateResolver.convert(s)))
                .ForMember(d => d.EndDate, o => o.MapFrom<DateTime>(s => CalendarEndDateResolver.convert(s)));
            //.ForMember(d => d.CustomColor, o => o.ResolveUsing<CustomColorResolver>())
            //.ForMember(d => d.StartDate, o => o.ResolveUsing<CalendarStartDateResolver>())
            //.ForMember(d => d.EndDate, o => o.ResolveUsing<CalendarEndDateResolver>());


            CreateMap<PlanningEmployee, PlanningPersistModel.PlanningEmployeeModel>();
            CreateMap<PlanningPersistModel.PlanningEmployeeModel, PlanningEmployee>();

            CreateMap<Planning, ControleDataRow>()
                .ForMember(d => d.Date, o => o.MapFrom(s => s.StartDate.ToString("dd-MM-yyyy ")))
                .ForMember(d => d.Employee, o => o.MapFrom(s => s.MainEmployee.GetFullname))
                .ForMember(d => d.Building, o => o.MapFrom(s => s.Building.Name))
                .ForMember(d => d.Asset, o => o.MapFrom(s => s.Asset.Name))
                .ForMember(d => d.Activity, o => o.MapFrom(s => s.AssetActivity.Name))
                .ForMember(d => d.Status, o => o.MapFrom(s => s.Status.GetStringValue()))
                .ForMember(d => d.Icon, o => o.MapFrom(s => ControleIconResolver.convert(s)));
            //.ForMember(d => d.Icon, o => o.ResolveUsing<ControleIconResolver>());

            CreateMap<Planning, ReportDataRow>()
                .ForMember(d => d.Date, o => o.MapFrom(s => s.StartDate.ToString("dd-MM-yyyy ")))
                .ForMember(d => d.Client, o => o.MapFrom(s => s.Client.Name))
                .ForMember(d => d.Building, o => o.MapFrom(s => s.Building.Name))
                .ForMember(d => d.AssetActivity, o => o.MapFrom(s => s.AssetActivity.Name))
                .ForMember(d => d.Asset, o => o.MapFrom(s => s.Asset.Name))
                .ForMember(d => d.Icon, o => o.MapFrom(s => ReportIconResolver.convert(s)));
            //.ForMember(d => d.Icon, o => o.ResolveUsing<ReportIconResolver>());

            CreateMap<Planning, ControlePersistModel>()
                .ForMember(d => d.Planning, o => o.MapFrom(s => s));

            CreateMap<AssetFile, AssetFileDataRow>()
                .ForMember(d => d.File, o => o.MapFrom(s => s.Title))
                .ForMember(d => d.Date, o => o.MapFrom(s => s.CreatedOnUtc.ToString("dd-MM-yyyy ")))
                .ForMember(d => d.Offernumber, o => o.MapFrom(s => s.Planning.OfferNumber))
                .ForMember(d => d.Company, o => o.MapFrom(s => s.Planning.Client.Name))
                .ForMember(d => d.BuildingLocation, o => o.MapFrom(s => s.Planning.Building.Name + " - " + s.Planning.BuildingLocation.Name));

            CreateMap<Planning, PlanningDataRow>()
                .ForMember(d => d.Date, o => o.MapFrom(s => s.StartDate.ToString("dd-MM-yyyy")))
                .ForMember(d => d.Offernumber, o => o.MapFrom(s => s.OfferNumber))
                .ForMember(d => d.Activity, o => o.MapFrom(s => s.AssetActivity.Name))
                .ForMember(d => d.Building, o => o.MapFrom(s => s.Building.Name))
                .ForMember(d => d.BuildingId, o => o.MapFrom(s => s.BuildingId))
                .ForMember(d => d.ClientId, o => o.MapFrom(s => s.ClientId))
                .ForMember(d => d.ClientName, o => o.MapFrom(s => s.Client.Name))
                .ForMember(d => d.Employee, o => o.MapFrom(s => s.MainEmployee.GetFullname))
                .ForMember(d => d.Status, o => o.MapFrom(s => s.Status.GetStringValue()))
                ;

            CreateMap<Timesheet, TimesheetPersistModel>();
            CreateMap<TimesheetPersistModel, Timesheet>();
        }
    }

    public class CustomColorResolver : IValueResolver<PlanningPersistModel, Planning, string>
    {
        public static Func<PlanningPersistModel, string> convert = source => CustomColorConvert(source);
        public static string CustomColorConvert(PlanningPersistModel source)
        {
            if (!source.CustomColorToggle)
            {
                return string.Empty;
            }

            return source.CustomColor.Replace("rgb(", "").Replace(")", "");

        }
        public string Resolve(PlanningPersistModel source, Planning destination, string destMember, ResolutionContext context)
        {
            if (!source.CustomColorToggle)
            {
                return string.Empty;
            }

            return source.CustomColor.Replace("rgb(", "").Replace(")", "");
        }
    }

    public class ControleIconResolver : IValueResolver<Planning, ControleDataRow, string>
    {
        public static Func<Planning, string> convert = source => ControleIconConvert(source);
        public static string ControleIconConvert(Planning source)
        {
            if (!string.IsNullOrEmpty(source.ErrorMessage))
            {
                return "<i class=\"fa check red size-28 fa clip-cancel-circle-2\"></i>";
            }

            return "<i class=\"fa check green size-28 clip-checkmark-circle-2\"></i>";

        }

        public string Resolve(Planning source, ControleDataRow destination, string destMember, ResolutionContext context)
        {
            //Unused
            //'<i class="fa check yellow size-28 fa-exclamation-triangle"></i>';
            if (!string.IsNullOrEmpty(source.ErrorMessage))
            {
                return "<i class=\"fa check red size-28 fa clip-cancel-circle-2\"></i>";
            }

            return "<i class=\"fa check green size-28 clip-checkmark-circle-2\"></i>";
        }
    }

    public class ReportIconResolver : IValueResolver<Planning, ReportDataRow, string>
    {
        public static Func<Planning, string> convert = source => ReportIconConvert(source);
        public static string ReportIconConvert(Planning source)
        {
            if (!string.IsNullOrEmpty(source.ErrorMessage))
            {
                return "<span class=\"btn btn-red no-button red border\"><i class=\"fa fa-ban\"></i></span>";
            }

            return "<span class=\"btn btn-green no-button green\"><i class=\"fa fa-check-circle\"></i></span>";

        }
        public string Resolve(Planning source, ReportDataRow destination, string destMember, ResolutionContext context)
        {
            //Unused
            //'<span class="btn btn-orange no-button orange border"><i class="fa fa-exclamation-triangle"></i></span>';
            if (!string.IsNullOrEmpty(source.ErrorMessage))
            {
                return "<span class=\"btn btn-red no-button red border\"><i class=\"fa fa-ban\"></i></span>";
            }

            return "<span class=\"btn btn-green no-button green\"><i class=\"fa fa-check-circle\"></i></span>";
        }
    }

    public class CalendarStartDateResolver : IValueResolver<IDatePersistModel, IDateEntity, DateTime>
    {
        public static Func<IDatePersistModel, DateTime> convert = source => StartDateConvert(source);
        public static DateTime StartDateConvert(IDatePersistModel source)
        {
            int index = source.DateString.IndexOf(" - ");
            string startTime = source.WholeDay ? "00:00:00" : source.StartTimeString + ":00";
            string[] formats = new[] { "dd-MM-yyyy HH:mm:ss", "dd-MM-yyyy H:mm:ss" };
            return DateTime.ParseExact(source.DateString.Substring(0, index) + " " + startTime, formats,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);

        }

        public DateTime Resolve(IDatePersistModel source, IDateEntity destination, DateTime destMember,
            ResolutionContext context)
        {
            int index = source.DateString.IndexOf(" - ");
            string startTime = source.WholeDay ? "00:00:00" : source.StartTimeString + ":00";
            string[] formats = new[] { "dd-MM-yyyy HH:mm:ss", "dd-MM-yyyy H:mm:ss" };
            return DateTime.ParseExact(source.DateString.Substring(0, index) + " " + startTime, formats,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);
        }
    }

    public class CalendarEndDateResolver : IValueResolver<IDatePersistModel, IDateEntity, DateTime>
    {
        public static Func<IDatePersistModel, DateTime> convert = source => EndDateConvert(source);
        public static DateTime EndDateConvert(IDatePersistModel source)
        {
            int index = source.DateString.IndexOf(" - ");
            string endTime = source.WholeDay ? "00:00:00" : source.EndTimeString + ":00";
            string[] formats = new[] { "dd-MM-yyyy HH:mm:ss", "dd-MM-yyyy H:mm:ss" };
            var dateTime = DateTime.ParseExact(source.DateString.Substring(index + 3, source.DateString.Length - (index + 3)) + " " + endTime, formats,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);
            if (source.WholeDay)
            {
                dateTime = dateTime.AddDays(1);
            }
            return dateTime;

        }
        public DateTime Resolve(IDatePersistModel source, IDateEntity destination, DateTime destMember,
            ResolutionContext context)
        {
            int index = source.DateString.IndexOf(" - ");
            string endTime = source.WholeDay ? "00:00:00" : source.EndTimeString + ":00";
            string[] formats = new[] { "dd-MM-yyyy HH:mm:ss", "dd-MM-yyyy H:mm:ss" };
            var dateTime = DateTime.ParseExact(source.DateString.Substring(index + 3, source.DateString.Length - (index + 3)) + " " + endTime, formats,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);
            if (source.WholeDay)
            {
                dateTime = dateTime.AddDays(1);
            }
            return dateTime;
        }
    }
}