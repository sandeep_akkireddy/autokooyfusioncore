﻿using FusionCore.Data;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class ClientDivision : BaseEntity
    {
        public int ClientId { get; set; }
        public Client Client { get; set; }
        public int DivisionId { get; set; }
        public Division Division { get; set; }
    }
}