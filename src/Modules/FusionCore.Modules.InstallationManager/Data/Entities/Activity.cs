﻿using FusionCore.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Activity : BaseEntity
    {
        public Activity()
        {
            ActivityFields = new List<ActivityField>();
            ActivityDynamicFields = new List<ActivityDynamicField>();
        }
        [MaxLength(256)]
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int AssetTypeId { get; set; }

        public ICollection<ActivityField> ActivityFields { get; set; }
        public ICollection<ActivityDynamicField> ActivityDynamicFields { get; set; }
        public virtual AssetType AssetType { get; set; }
    }
}
