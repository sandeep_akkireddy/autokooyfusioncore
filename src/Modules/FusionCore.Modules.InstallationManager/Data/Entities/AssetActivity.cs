﻿using FusionCore.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class AssetActivity : BaseEntity
    {
        public AssetActivity()
        {
            AssetActivityFields = new List<AssetActivityField>();
        }
        [MaxLength(256)]
        public string Name { get; set; }

        public int AssetId { get; set; }
        public int SortOrder { get; set; }

        public bool Active { get; set; }

        public virtual Asset Asset { get; set; }
        public ICollection<AssetActivityField> AssetActivityFields { get; set; }
    }
}
