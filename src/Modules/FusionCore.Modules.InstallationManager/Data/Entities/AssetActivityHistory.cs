﻿using FusionCore.Data;
using FusionCore.Modules.InstallationManager.Data.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class AssetActivityHistory : BaseEntity
    {
        public int? ItemId { get; set; }
        public int AssetId { get; set; }
        public int PlanningId { get; set; }
        public int Status { get; set; }
        public int Kind { get; set; }
        public string Remarks { get; set; }
        public byte[] Signature { get; set; }
        public int? AssetActivityId { get; set; }
        public int? AssetFileId { get; set; }

        [StringLength(200)]
        public string Name { get; set; }
        public string Value { get; set; }
        public int DynamicFieldTypeId { get; set; }
        public int SortOrder { get; set; }
        public int ActivitySortOrder { get; set; }

        public int NormHours { get; set; }
        public int RealHours { get; set; }
        public int RealMinutes { get; set; }

        public int TravelHours { get; set; }
        public int TravelMinutes { get; set; }

        public string Actions { get; set; }

        [StringLength(250)]
        public string OkayGivenBy { get; set; }

        public virtual AssetActivity AssetActivity { get; set; }
        public virtual Asset Asset { get; set; }

        public virtual AssetFile AssetFile { get; set; }

        public Planning Planning { get; set; }

        [NotMapped]
        public DynamicFieldType DynamicFieldType
        {
            get => (DynamicFieldType)DynamicFieldTypeId;
            set => DynamicFieldTypeId = (int)value;
        }
    }
}
