﻿using FusionCore.Core.Enums;
using FusionCore.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class AssetFile : BaseEntity
    {
        public int AssetId { get; set; }
        public int? PlanningId { get; set; }
        public int FileTypeId { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        public string Description { get; set; }
        public int SortOrder { get; set; }
        public int FileStatusId { get; set; }

        public virtual Planning Planning { get; set; }

        [NotMapped]
        public FileType FileType
        {
            get => (FileType)FileTypeId;
            set => FileTypeId = (int)value;
        }

        [NotMapped]
        public FileStatus FileStatus
        {
            get => (FileStatus)FileStatusId;
            set => FileStatusId = (int)value;
        }
    }
}
