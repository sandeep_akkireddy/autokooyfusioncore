﻿using FusionCore.Data;
using FusionCore.Modules.InstallationManager.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Timesheet : BaseEntity, IDateEntity
    {
        public Timesheet()
        {
            TimeSheetHomeWorkRows = new List<TimesheetHomeWorkRow>();
            TimeSheetTimeRows = new List<TimesheetTimeRow>();
        }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int WeekNumber { get; set; }
        public int Year { get; set; }
        public int KilometerStart { get; set; }
        public int KilometerEnd { get; set; }
        public DateTime? SendOnUtc { get; set; }
        public bool IsSupportShift { get; set; }


        public IList<TimesheetHomeWorkRow> TimeSheetHomeWorkRows { get; set; }
        public IList<TimesheetTimeRow> TimeSheetTimeRows { get; set; }

        [NotMapped] public bool IsSend => SendOnUtc.HasValue;
        [NotMapped] public bool AllAproved => TimeSheetTimeRows.All(x => x.IsSubmitted || x.DeletedOnUtc.HasValue);
    }
}
