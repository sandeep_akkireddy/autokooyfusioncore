﻿using FusionCore.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class TimesheetHomeWorkRow : BaseEntity
    {
        public int TimesheetId { get; set; }

        [Required]
        public DateTime Date { get; set; }
        public TimeSpan HomeWorkTime { get; set; }
        public TimeSpan WorkHomeTime { get; set; }
    }
}
