﻿using FusionCore.Data;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Division : BaseEntity
    {
        [MaxLength(256)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Middlename { get; set; }
        [MaxLength(256)]
        public string Lastname { get; set; }
        [MaxLength(256)]
        public string Address { get; set; }
        [MaxLength(50)]
        public string PostalCode { get; set; }
        [MaxLength(256)]
        public string City { get; set; }
        [MaxLength(256)]
        public string Country { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phonenumber { get; set; }
        [MaxLength(50)]
        public string Mobilenumber { get; set; }
        [MaxLength(256)]
        public string Website { get; set; }

        public int? LogoHeaderPictureId { get; set; }
        public int? LogoFooterPictureId { get; set; }

        public string EmailTemplateCustomerEmail { get; set; }
        public string EmailTemplateCustomerBody { get; set; }

        public string EmailTemplatePlannerBody { get; set; }
        public string EmailTemplatePlannerEmail { get; set; }

        public string GetContact => $"{Firstname}{(!string.IsNullOrEmpty(Middlename) ? " " + Middlename : "")} {Lastname}";
    }
}