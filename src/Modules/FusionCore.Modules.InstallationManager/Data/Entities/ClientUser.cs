﻿using FusionCore.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class ClientUser : BaseEntity
    {
        public ClientUser()
        {
            ClientUserRights = new List<ClientUserRight>();
        }
        public int ClientId { get; set; }

        [MaxLength(256)]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Middlename { get; set; }
        [MaxLength(256)]
        public string Lastname { get; set; }

        public bool Active { get; set; }

        [Required] [MaxLength(256)] public string Username { get; set; }

        public byte[] Key { get; set; }
        public byte[] Salt { get; set; }

        public virtual Client Client { get; set; }

        public ICollection<ClientUserRight> ClientUserRights { get; set; }

        public string GetFullname => $"{Firstname}{(!string.IsNullOrEmpty(Middlename) ? " " + Middlename : "")} {Lastname}";
    }
}