﻿using FusionCore.Data;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class AssetDynamicField : BaseEntity
    {
        public int AssetId { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Value { get; set; }

        public int SortOrder { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
