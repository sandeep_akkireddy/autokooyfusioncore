﻿using FusionCore.Data;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class EmployeeGroup : BaseEntity
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }
    }
}
