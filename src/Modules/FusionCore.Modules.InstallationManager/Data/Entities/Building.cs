﻿using FusionCore.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Building : BaseEntity
    {
        public Building()
        {
            BuildingLocations = new List<BuildingLocation>();
        }

        [MaxLength(256)] public string Name { get; set; }

        public int ClientId { get; set; }

        public virtual Client Client { get; set; }

        [MaxLength(256)]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Middlename { get; set; }
        [MaxLength(256)]
        public string Lastname { get; set; }
        [MaxLength(256)]
        public string Address { get; set; }
        [MaxLength(50)]
        public string PostalCode { get; set; }
        [MaxLength(256)]
        public string City { get; set; }
        [MaxLength(256)]
        public string Country { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phonenumber { get; set; }
        [MaxLength(50)]
        public string Mobilenumber { get; set; }
        [MaxLength(256)]
        public string Website { get; set; }
        public string Latitude { get; set; }
        public string Longtitude { get; set; }
        public string Remarks { get; set; }

        public int? LogoPictureId { get; set; }

        public string GetContact => $"{Firstname}{(!string.IsNullOrEmpty(Middlename) ? " " + Middlename : "")} {Lastname}";

        public ICollection<BuildingLocation> BuildingLocations { get; set; }
    }
}