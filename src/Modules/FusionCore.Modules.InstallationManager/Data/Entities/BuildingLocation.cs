﻿using FusionCore.Data;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class BuildingLocation : BaseEntity
    {
        [MaxLength(256)] public string Name { get; set; }

        public int BuildingId { get; set; }

        public virtual Building Building { get; set; }

        public string Floor { get; set; }
        public string RoomNumber { get; set; }
        public string Remarks { get; set; }
    }
}