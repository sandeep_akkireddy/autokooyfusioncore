﻿using FusionCore.Data;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class CalendarItem : BaseEntity, IDateEntity
    {
        [MaxLength(256)]
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool WholeDay { get; set; }
        public int CalendarStatusId { get; set; }
        public string Remark { get; set; }

        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        [NotMapped]
        public CalendarStatus CalendarStatus
        {
            get => (CalendarStatus)CalendarStatusId;
            set => CalendarStatusId = (int)value;
        }
    }
}
