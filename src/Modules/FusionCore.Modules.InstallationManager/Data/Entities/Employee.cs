﻿using FusionCore.Core.Interfaces;
using FusionCore.Data;
using FusionCore.Modules.InstallationManager.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Employee : BaseEntity, IHasSoftDelete
    {
        public Employee()
        {
            Employee2EmployeeGroups = new List<Employee2EmployeeGroup>();
            CalendarItems = new List<CalendarItem>();
        }
        [Required]
        [MaxLength(256)]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Middlename { get; set; }
        [Required]
        [MaxLength(256)]
        public string Lastname { get; set; }
        [MaxLength(256)]
        public string Address { get; set; }
        [MaxLength(50)]
        public string PostalCode { get; set; }
        [MaxLength(256)]
        public string City { get; set; }
        [MaxLength(256)]
        public string Country { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phonenumber { get; set; }
        [MaxLength(50)]
        public string Mobilenumber { get; set; }
        [MaxLength(256)]
        public string NavisionNumber { get; set; }

        public bool Active { get; set; }

        public string Username { get; set; }
        public byte[] Key { get; set; }
        public byte[] Salt { get; set; }

        public int EmployeeFunctionId { get; set; }

        public ICollection<Employee2EmployeeGroup> Employee2EmployeeGroups { get; set; }
        public ICollection<CalendarItem> CalendarItems { get; set; }

        [NotMapped]
        public EmployeeFunction EmployeeFunction
        {
            get => (EmployeeFunction)EmployeeFunctionId;
            set => EmployeeFunctionId = (int)value;
        }

        [NotMapped]
        public string GetFullname => $"{Firstname}{(!string.IsNullOrEmpty(Middlename) ? " " + Middlename : "")} {Lastname}";

        public string ResetKey { get; set; }
        public DateTime? DeletedOnUtc { get; set; }
    }
}
