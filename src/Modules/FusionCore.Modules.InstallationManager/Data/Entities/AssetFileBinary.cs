using FusionCore.Data;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class AssetFileBinary : BaseEntity
    {
        public int AssetFileId { get; set; }
        public byte[] File { get; set; }
    }
}
