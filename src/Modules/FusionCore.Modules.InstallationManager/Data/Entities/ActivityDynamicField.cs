﻿using FusionCore.Data;
using FusionCore.Modules.InstallationManager.Data.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class ActivityDynamicField : BaseEntity
    {
        public int ActivityId { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Value { get; set; }
        public int SortOrder { get; set; }
        public int DynamicFieldTypeId { get; set; }

        [NotMapped]
        public DynamicFieldType DynamicFieldType
        {
            get => (DynamicFieldType)DynamicFieldTypeId;
            set => DynamicFieldTypeId = (int)value;
        }

        public virtual Activity Activity { get; set; }
    }
}
