﻿using FusionCore.Core.Interfaces;
using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Asset : BaseEntity, IHasSoftDelete
    {
        public Asset()
        {
            AssetDynamicFields = new List<AssetDynamicField>();
            AssetFields = new List<AssetField>();
            AssetActivityHistories = new List<AssetActivityHistory>();
            AssetFiles = new List<AssetFile>();
        }
        [MaxLength(256)]
        public string Name { get; set; }

        public int AssetTypeId { get; set; }

        public int? BuildingId { get; set; }
        public int? BuildingLocationId { get; set; }

        public virtual Building Building { get; set; }
        public virtual BuildingLocation BuildingLocation { get; set; }
        public virtual AssetType AssetType { get; set; }

        public ICollection<AssetDynamicField> AssetDynamicFields { get; set; }
        public ICollection<AssetField> AssetFields { get; set; }
        public ICollection<AssetFile> AssetFiles { get; set; }
        public ICollection<AssetActivityHistory> AssetActivityHistories { get; set; }
        public DateTime? DeletedOnUtc { get; set; }
    }
}