﻿using FusionCore.Data;
using FusionCore.Modules.InstallationManager.Data.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class AssetField : BaseEntity
    {
        public int AssetId { get; set; }
        public int ActivityId { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string LatestValue { get; set; }
        public int DynamicFieldTypeId { get; set; }
        public int SortOrder { get; set; }
        public int ActivitySortOrder { get; set; }

        public virtual Activity Activity { get; set; }
        public virtual Asset Asset { get; set; }

        [NotMapped]
        public DynamicFieldType DynamicFieldType
        {
            get => (DynamicFieldType)DynamicFieldTypeId;
            set => DynamicFieldTypeId = (int)value;
        }
    }
}
