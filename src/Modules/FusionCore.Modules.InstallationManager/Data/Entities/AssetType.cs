﻿using FusionCore.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class AssetType : BaseEntity
    {
        public AssetType()
        {
            Activities = new List<Activity>();
        }
        [StringLength(100)]
        public string Name { get; set; }
        public int SortOrder { get; set; }

        public ICollection<Activity> Activities { get; set; }
    }
}
