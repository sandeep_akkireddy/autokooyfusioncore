﻿using FusionCore.Data;
using FusionCore.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Planning : BaseEntity, IDateEntity
    {
        public Planning()
        {
            PlanningEmployees = new List<PlanningEmployee>();
            AssetActivityHistories = new List<AssetActivityHistory>();
        }

        public int? UserId { get; set; }
        public int? AssetId { get; set; }
        public int? AssetActivityId { get; set; }
        public int? DivisionId { get; set; }
        public int? ClientId { get; set; }
        public int? BuildingId { get; set; }
        public int? BuildingLocationId { get; set; }
        public int? MainEmployeeId { get; set; }

        public int StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Repeatable { get; set; }
        public int? RepeatableType { get; set; }
        public int? RepeatableCount { get; set; }
        public int? RepeatablePlanningParentId { get; set; }

        [StringLength(256)]
        public string ErrorMessage { get; set; }
        public string Remark { get; set; }
        public string Materials { get; set; }
        public string MaterialsRemark { get; set; }
        public string ExtraEmail { get; set; }
        public string ExtraEmailInformation { get; set; }
        public string CustomColor { get; set; }
        public byte[] Signature { get; set; }
        [Required, StringLength(256)]
        public string OfferNumber { get; set; }

        [NotMapped]
        public PlanningStatus Status
        {
            get => (PlanningStatus)StatusId;
            set => StatusId = (int)value;
        }

        public virtual Asset Asset { get; set; }
        public virtual AssetActivity AssetActivity { get; set; }
        public virtual Division Division { get; set; }
        public virtual Client Client { get; set; }
        public virtual Building Building { get; set; }
        public virtual BuildingLocation BuildingLocation { get; set; }
        public virtual Employee MainEmployee { get; set; }
        public virtual User FusionUser { get; set; }

        public DateTime? CheckListStart { get; set; }
        public DateTime? CheckListEnd { get; set; }
        public DateTime? CheckListPauze { get; set; }
        public string Notification { get; set; }
        public DateTime? NotificationReadOn { get; set; }

        public ICollection<PlanningEmployee> PlanningEmployees { get; set; }
        public ICollection<AssetActivityHistory> AssetActivityHistories { get; set; }

        [NotMapped] public bool HasAssetActivityHistories => AssetActivityHistories.Any();
        [NotMapped] public bool SignatureAlreadyPresent => Signature != null && Signature.Length > 0;

        [NotMapped]
        public bool IsFilled =>
            BuildingLocationId.HasValue
            && AssetActivityId.HasValue
            && AssetId.HasValue
            && BuildingId.HasValue
            && ClientId.HasValue
        ;

        [NotMapped] public string Filename => $"{OfferNumber}-pid{Id}.pdf";

        [NotMapped]
        public string CustomColorAssistant
        {
            get
            {
                if (!string.IsNullOrEmpty(CustomColor))
                {
                    string[] split = CustomColor.Replace("rgb(", "").Replace(")", "").Split(',');
                    if (split.Length == 3)
                    {
                        for (int index = 0; index < split.Length; index++)
                        {
                            int value = Convert.ToInt32(split[index].Trim());
                            if (index < 2) value += 30;
                            if (value > 255) value = 255;
                            split[index] = value.ToString();
                        }

                        return $"{split[0]},{split[1]},{split[2]}";
                    }
                }
                return "";
            }
        }
    }
}
