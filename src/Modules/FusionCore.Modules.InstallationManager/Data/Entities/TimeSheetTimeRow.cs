﻿using FusionCore.Core.Interfaces;
using FusionCore.Data;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class TimesheetTimeRow : BaseEntity, IHasSoftDelete
    {
        public int TimesheetId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? PlanningId { get; set; }
        public int? CalendarItemId { get; set; }

        public TimeSpan WorkTime { get; set; }
        public TimeSpan TravelTime { get; set; }

        public DateTime? SubmittedOnUtc { get; set; }

        [NotMapped] public bool IsSubmitted => SubmittedOnUtc.HasValue;

        public virtual CalendarItem CalendarItem { get; set; }
        public virtual Planning Planning { get; set; }
        public DateTime? DeletedOnUtc { get; set; }
        public string OfferNumber { get; set; }
        public string Description { get; set; }
    }
}
