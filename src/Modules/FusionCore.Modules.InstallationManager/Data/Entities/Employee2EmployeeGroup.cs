﻿using FusionCore.Data;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class Employee2EmployeeGroup : BaseEntity
    {
        public int EmployeeId { get; set; }
        public int EmployeeGroupId { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual EmployeeGroup EmployeeGroup { get; set; }
    }
}
