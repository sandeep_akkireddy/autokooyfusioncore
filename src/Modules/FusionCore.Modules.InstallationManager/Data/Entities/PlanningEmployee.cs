﻿using FusionCore.Data;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class PlanningEmployee : BaseEntity
    {
        public int PlanningId { get; set; }
        public Planning Planning { get; set; }
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
    }
}