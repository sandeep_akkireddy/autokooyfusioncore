﻿using FusionCore.Data;
using FusionCore.Modules.InstallationManager.Data.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class ActivityField : BaseEntity
    {
        public int ActivityId { get; set; }

        [StringLength(256)]
        public string Name { get; set; }
        public int DynamicFieldTypeId { get; set; }
        public int SortOrder { get; set; }

        [NotMapped]
        public DynamicFieldType DynamicFieldType
        {
            get => (DynamicFieldType)DynamicFieldTypeId;
            set => DynamicFieldTypeId = (int)value;
        }
    }
}
