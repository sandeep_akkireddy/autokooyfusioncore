﻿using FusionCore.Data;

namespace FusionCore.Modules.InstallationManager.Data.Entities
{
    public class ClientUserRight : BaseEntity
    {
        public int ClientUserId { get; set; }

        public bool AllBuildings { get; set; }
        public bool AllLocations { get; set; }
        public int? BuildingId { get; set; }
        public int? BuildingLocationId { get; set; }

        public ClientUser ClientUser { get; set; }
        public Building Building { get; set; }
        public BuildingLocation BuildingLocation { get; set; }
    }
}