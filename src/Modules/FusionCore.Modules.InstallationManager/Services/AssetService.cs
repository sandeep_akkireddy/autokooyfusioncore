﻿using AutoMapper;
using FusionCore.Core.Enums;
using FusionCore.Data;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class AssetService : BaseService, IAssetService
    {
        private readonly IGenericRepository<Asset> _assetRepository;
        private readonly IGenericRepository<AssetFile> _assetFileRepository;
        private readonly IGenericRepository<AssetFileBinary> _assetFileByteRepository;
        private readonly IGenericRepository<AssetDynamicField> _assetDynamicFieldRepository;
        private readonly IGenericRepository<AssetActivity> _assetActivityRepository;
        private readonly IGenericRepository<AssetActivityField> _assetActivityFieldRepository;
        private readonly IGenericRepository<AssetActivityHistory> _assetActivityHistoryRepository;
        private readonly FusionCoreContext _dbContext;
        private readonly IActivityService _activityService;
        private readonly IMapper _mapper;

        public AssetService(
            IGenericRepository<Asset> assetRepository,
            IGenericRepository<AssetDynamicField> assetDynamicFieldRepository,
            IGenericRepository<AssetActivity> assetActivityRepository,
            IGenericRepository<AssetActivityField> assetActivityFieldRepository,
            IGenericRepository<AssetFile> assetFileRepository,
            IGenericRepository<AssetActivityHistory> assetActivityHistoryRepository,
            FusionCoreContext dbContext,
            IActivityService activityService,
            IMapper mapper,
            IGenericRepository<AssetFileBinary> assetFileByteRepository)
        {
            _assetRepository = assetRepository;
            _assetDynamicFieldRepository = assetDynamicFieldRepository;
            _assetActivityRepository = assetActivityRepository;
            _assetActivityFieldRepository = assetActivityFieldRepository;
            _dbContext = dbContext;
            _activityService = activityService;
            _mapper = mapper;
            _assetFileByteRepository = assetFileByteRepository;
            _assetActivityHistoryRepository = assetActivityHistoryRepository;
            _assetFileRepository = assetFileRepository;
        }
        #region Asset
        public IPagedList<Asset> GetAllAssets<TKey>(Expression<Func<Asset, bool>> predicate = null,
            Expression<Func<Asset, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<Asset> queryable = _assetRepository.GetAll(predicate, true, x => x.Building, x => x.BuildingLocation, x => x.AssetDynamicFields);
            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }

            return new PagedList<Asset>(queryable, pageIndex, pageSize);
        }

        public Asset GetAsset(int id)
        {
            return _assetRepository.Get(x => x.Id == id, x => x.AssetType);
        }

        public bool PersistAsset(AssetPersistModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (model.Id.HasValue)
            {
                //Edit
                Asset dbEntity = GetAsset(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _assetRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                Asset dbEntity = new Asset();
                _mapper.Map(model, dbEntity);
                _assetRepository.Insert(dbEntity);

                //Copy all activities from asset type
                foreach (Activity activity in _activityService.GetActivitesByAssetType(model.AssetTypeId))
                {
                    AssetActivity assetActivity = new AssetActivity
                    {
                        AssetId = dbEntity.Id,
                        Name = activity.Name,
                        SortOrder = activity.SortOrder,
                    };

                    foreach (ActivityDynamicField field in _activityService.GetActivityDynamicFieldsByActivityId(activity.Id))
                    {
                        if (string.IsNullOrEmpty(field.Name)) continue;

                        _assetDynamicFieldRepository.Insert(new AssetDynamicField
                        {
                            AssetId = dbEntity.Id,
                            Name = field.Name,
                            Value = field.Value ?? ""
                        });
                    }

                    foreach (ActivityField field in _activityService.GetActivityFieldsByActivityId(activity.Id))
                    {
                        assetActivity.AssetActivityFields.Add(new AssetActivityField
                        {
                            Name = field.Name,
                            SortOrder = field.SortOrder,
                            DynamicFieldTypeId = field.DynamicFieldTypeId
                        });
                    }

                    _assetActivityRepository.Insert(assetActivity);
                }
                return true;
            }

            return false;
        }

        public async Task DeleteAsset(int id)
        {
            Asset asset = await _assetRepository.GetByIdAsync(id);
            if (asset != null)
            {
                _assetRepository.Delete(asset);
            }
        }

        public IPagedList<Asset> GetAssetsByBuildingLocationId<TKey>(int buildingLocationId, Expression<Func<Asset, bool>> predicate = null, Expression<Func<Asset, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = Int32.MaxValue)
        {
            IQueryable<Asset> queryable = _assetRepository.GetAll(x => x.BuildingLocationId == buildingLocationId);
            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }

            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }

            return new PagedList<Asset>(queryable, pageIndex, pageSize);
        }

        public void PersistAsset(Asset asset)
        {
            if (asset == null)
            {
                throw new ArgumentNullException(nameof(asset));
            }

            _assetRepository.Update(asset);
        }
        #endregion
        #region DynamicFields
        public IPagedList<AssetDynamicField> GetAllAssetDynamicFields<TKey>(int assetId, Expression<Func<AssetDynamicField, bool>> predicate = null,
            Expression<Func<AssetDynamicField, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<AssetDynamicField> queryable = _assetDynamicFieldRepository.GetAll(x => x.AssetId == assetId);
            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }

            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }

            return new PagedList<AssetDynamicField>(queryable, pageIndex, pageSize);
        }

        public AssetDynamicField GetAssetDynamicField(int id)
        {
            return _assetDynamicFieldRepository.Get(x => x.Id == id);
        }

        public int? PersistAssetDynamicField(AssetDynamicFieldPersistModel assetDynamicField)
        {
            if (assetDynamicField == null)
            {
                throw new ArgumentNullException(nameof(assetDynamicField));
            }

            if (assetDynamicField.Id.HasValue)
            {
                //Edit
                AssetDynamicField dbEntity = GetAssetDynamicField(assetDynamicField.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(assetDynamicField, dbEntity);
                    _assetDynamicFieldRepository.Update(dbEntity);

                    return dbEntity.Id;
                }
            }
            else
            {
                //insert
                AssetDynamicField dbEntity = new AssetDynamicField();
                _mapper.Map(assetDynamicField, dbEntity);
                _assetDynamicFieldRepository.Insert(dbEntity);
                return dbEntity.Id;
            }

            return null;
        }

        public async Task DeleteAssetDynamicField(int id)
        {
            AssetDynamicField assetDynamicField = await _assetDynamicFieldRepository.GetByIdAsync(id);
            if (assetDynamicField != null)
            {
                _assetDynamicFieldRepository.Delete(assetDynamicField);
            }
        }
        #endregion
        #region Activities
        public IPagedList<AssetActivity> GetAllAssetActivities<TKey>(int assetId, Expression<Func<AssetActivity, bool>> predicate = null, Expression<Func<AssetActivity, TKey>> orderBy = null,
            bool asc = true, int pageIndex = 0, int pageSize = Int32.MaxValue, bool loadFields = false)
        {
            IQueryable<AssetActivity> queryable = _assetActivityRepository.GetAll(x => x.AssetId == assetId);
            if (loadFields)
            {
                queryable = queryable.Include(x => x.AssetActivityFields);
            }
            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }

            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }

            return new PagedList<AssetActivity>(queryable, pageIndex, pageSize);
        }

        public AssetActivity GetAssetActivity(int id, bool loadAll = false)
        {
            Expression<Func<AssetActivity, object>>[] includes = new Expression<Func<AssetActivity, object>>[]
            {
                x => x.AssetActivityFields,
            };
            if (loadAll)
            {
                includes = new Expression<Func<AssetActivity, object>>[]
                {
                    x => x.AssetActivityFields,
                    x => x.Asset,
                    x => x.Asset.Building,
                    x => x.Asset.Building.Client,
                    x => x.Asset.Building.Client.ClientDivisions
                };
            }
            return _assetActivityRepository.Get(x => x.Id == id, includes);
        }

        public AssetActivityField GetAssetActivityField(int id)
        {
            return _assetActivityFieldRepository.Get(x => x.Id == id);
        }

        public async Task<AssetActivity> GetAssetActivityAsync(int id)
        {
            return await _assetActivityRepository.GetAsync(x => x.Id == id, includes: x => x.AssetActivityFields);
        }

        public int? PersistAssetActivityField(AssetActivityFieldPersistModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (model.Id.HasValue)
            {
                //Edit
                AssetActivityField dbEntity = GetAssetActivityField(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _assetActivityFieldRepository.Update(dbEntity);

                    return dbEntity.Id;
                }
            }
            else
            {
                //insert
                AssetActivityField dbEntity = new AssetActivityField();
                _mapper.Map(model, dbEntity);
                _assetActivityFieldRepository.Insert(dbEntity);
                return dbEntity.Id;
            }

            return null;
        }

        public int? PersistAssetActivityField(AssetActivityFieldNamePersistModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (model.Id.HasValue)
            {
                //Edit
                AssetActivityField dbEntity = GetAssetActivityField(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _assetActivityFieldRepository.Update(dbEntity);

                    return dbEntity.Id;
                }
            }
            else
            {
                //insert
                AssetActivityField dbEntity = new AssetActivityField();
                _mapper.Map(model, dbEntity);
                _assetActivityFieldRepository.Insert(dbEntity);
                return dbEntity.Id;
            }

            return null;
        }

        public int? PersistAssetActivityField(AssetActivityFieldValuePersistModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (model.Id.HasValue)
            {
                //Edit
                AssetActivityField dbEntity = GetAssetActivityField(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _assetActivityFieldRepository.Update(dbEntity);

                    return dbEntity.Id;
                }
            }
            else
            {
                //insert
                AssetActivityField dbEntity = new AssetActivityField();
                _mapper.Map(model, dbEntity);
                _assetActivityFieldRepository.Insert(dbEntity);
                return dbEntity.Id;
            }

            return null;
        }

        public async Task DeleteAssetActivityField(int id)
        {
            AssetActivityField assetActivityField = await _assetActivityFieldRepository.GetByIdAsync(id);
            if (assetActivityField != null)
            {
                _assetActivityFieldRepository.Delete(assetActivityField);
            }
        }

        public void UpdateAssetActivity(AssetActivity assetActivity)
        {
            _assetActivityRepository.Update(assetActivity);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveLatestValues(Planning planning)
        {
            List<AssetActivityHistory> histories =
                planning.AssetActivityHistories.Where(
                    x => x.PlanningId == planning.Id && (x.AssetActivityId == planning.AssetActivityId || !x.AssetActivityId.HasValue)).ToList();
            AssetActivity asset = GetAssetActivity(planning.AssetActivityId.GetValueOrDefault());
            List<AssetActivityField> fields = asset.AssetActivityFields.ToList();
            foreach (AssetActivityField field in fields)
            {
                AssetActivityHistory history = histories.FirstOrDefault(x => x.DynamicFieldType == field.DynamicFieldType && x.Name == field.Name && x.Kind == (int)FieldKind.Field);
                if (history != null)
                {
                    field.LatestValue = history.Value;
                }
            }

            List<AssetDynamicField> dynamicfields = asset.Asset.AssetDynamicFields.ToList();
            foreach (AssetDynamicField field in dynamicfields)
            {
                AssetActivityHistory history = histories.FirstOrDefault(x => x.Name == field.Name && x.Kind == (int)FieldKind.DynamicField);
                if (history != null)
                {
                    field.Value = history.Value;
                }
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAssetFileAsync(AssetFile assetFile)
        {
            await _assetFileRepository.DeleteAsync(assetFile);
        }

        public async Task CreateAssetFileAsync(AssetFile assetFile)
        {
            await _assetFileRepository.InsertAsync(assetFile);
        }

        public AssetActivityHistory GetAssetActivityHistoryById(int id)
        {
            return _assetActivityHistoryRepository.GetAll(x => x.Id == id, includes: x => x.AssetFile).FirstOrDefault();
        }

        public IPagedList<AssetFile> GetAssetFilesByType(int assetId, FileType type, int pageIndex, int pageSize)
        {
            IQueryable<AssetFile> query = _assetFileRepository.GetAll(x => x.AssetId == assetId && x.FileTypeId == (int)type);
            query = query
                    .Include(x => x.Planning)
                        .ThenInclude(x => x.Client)
                    .Include(x => x.Planning)
                        .ThenInclude(x => x.Building)
                    .Include(x => x.Planning)
                        .ThenInclude(x => x.BuildingLocation)
                ;
            return new PagedList<AssetFile>(query.OrderByDescending(x => x.CreatedOnUtc), pageIndex, pageSize);
        }

        public async Task<AssetFile> GetAssetFileByIdAsync(int id)
        {
            return await _assetFileRepository.GetByIdAsync(id);
        }

        public IList<AssetFile> GetAssetFilesByDateAndType(DateTime date, FileType fileType)
        {
            return _assetFileRepository.GetAll(x => x.FileTypeId == (int)fileType && x.CreatedOnUtc.Date == date.Date).ToList();
        }

        public IList<AssetFile> GetAllAssetFilesWithoutBytes(int skip, int take)
        {
            return _assetFileRepository.GetAll(x => !_assetFileByteRepository.Table.Any(y => y.AssetFileId == x.Id)).OrderByDescending(x => x.Id).Skip(skip).Take(take).ToList();
        }

        public int? PersistAssetActivity(AssetActivityPersistModel assetActivity)
        {
            if (assetActivity == null)
            {
                throw new ArgumentNullException(nameof(assetActivity));
            }

            if (assetActivity.Id.HasValue)
            {
                //Edit
                AssetActivity dbEntity = GetAssetActivity(assetActivity.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(assetActivity, dbEntity);
                    _assetActivityRepository.Update(dbEntity);

                    return dbEntity.Id;
                }
            }
            else
            {
                //insert
                AssetActivity dbEntity = new AssetActivity();
                _mapper.Map(assetActivity, dbEntity);
                _assetActivityRepository.Insert(dbEntity);
                return dbEntity.Id;
            }

            return null;
        }

        public async Task DeleteAssetActivity(int id)
        {
            AssetActivity activity = await _assetActivityRepository.GetByIdAsync(id);
            if (activity != null)
            {
                List<AssetActivityField> fields = await _assetActivityFieldRepository.GetAll(x => x.AssetActivityId == id).ToListAsync();
                foreach (AssetActivityField field in fields)
                {
                    await _assetActivityFieldRepository.DeleteAsync(field);
                }
                await _assetActivityRepository.DeleteAsync(activity);
            }
        }
        #endregion
    }
}