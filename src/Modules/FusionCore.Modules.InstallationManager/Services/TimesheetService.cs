﻿using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Data;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class TimesheetService : BaseService, ITimesheetService
    {
        private readonly FusionCoreContext _dbContext;
        private readonly IGenericRepository<Timesheet> _timesheetRepository;
        private readonly IGenericRepository<TimesheetHomeWorkRow> _timesheetHomeWorkRowRepository;
        private readonly IGenericRepository<TimesheetTimeRow> _timesheetTimeRowRepository;
        private readonly ICalendarService _calendarService;
        private readonly IPlanningService _planningService;
        private readonly IMapper _mapper;

        public TimesheetService(
            IGenericRepository<Timesheet> timesheetRepository,
            IGenericRepository<TimesheetTimeRow> timesheetTimeRowRepository,
            IGenericRepository<TimesheetHomeWorkRow> timesheetHomeWorkRowRepository,
            FusionCoreContext dbContext,
            IMapper mapper,
            IPlanningService planningService,
            ICalendarService calendarService)
        {
            _timesheetRepository = timesheetRepository;
            _dbContext = dbContext;
            _mapper = mapper;
            _planningService = planningService;
            _calendarService = calendarService;
            _timesheetTimeRowRepository = timesheetTimeRowRepository;
            _timesheetHomeWorkRowRepository = timesheetHomeWorkRowRepository;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public IPagedList<Timesheet> GetAllTimeSheets<TKey>(Expression<Func<Timesheet, bool>> predicate = null,
            Expression<Func<Timesheet, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<Timesheet> queryable = _timesheetRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Timesheet>(queryable, pageIndex, pageSize);
        }

        public async Task<Timesheet> GetTimeSheetByEmployeeAndWeeknumberAsync(int employeeId, int weekNumber)
        {
            Expression<Func<Timesheet, object>>[] includes = new Expression<Func<Timesheet, object>>[]
            {
                x => x.TimeSheetHomeWorkRows,
                x => x.TimeSheetTimeRows,
            };
            return await _timesheetRepository.GetAsync(x => x.EmployeeId == employeeId && x.WeekNumber == weekNumber, includes: includes);
        }

        public Timesheet GetTimeSheet(int id)
        {
            return _timesheetRepository.Get(x => x.Id == id, x => x.TimeSheetHomeWorkRows, x => x.TimeSheetTimeRows);
        }

        public bool PersistTimesheet(TimesheetPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                Timesheet dbEntity = GetTimeSheet(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);

                    _timesheetRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                Timesheet dbEntity = new Timesheet();
                _mapper.Map(model, dbEntity);

                _timesheetRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }

        public async Task DeleteTimeSheet(int id)
        {
            Timesheet timesheet = await _timesheetRepository.GetByIdAsync(id);
            if (timesheet != null) _timesheetRepository.Delete(timesheet);
        }

        public Timesheet CreateBaseTimeSheet(int currentUserId, int weekNumber, DateTime startDate)
        {
            Timesheet timesheet = new Timesheet
            {
                EmployeeId = currentUserId,
                WeekNumber = weekNumber,
                Year = startDate.Year,
                StartDate = startDate,
                EndDate = startDate.AddDays(6)
            };

            for (int i = 0; i < 7; i++)
            {
                timesheet.TimeSheetHomeWorkRows.Add(new TimesheetHomeWorkRow
                {
                    Date = startDate.AddDays(i),
                    HomeWorkTime = TimeSpan.Zero,
                    WorkHomeTime = TimeSpan.Zero
                });
            }
            _timesheetRepository.Insert(timesheet);

            return timesheet;
        }

        public Timesheet ValidatePlanningsForTimeSheet(Timesheet timesheet)
        {
            IPagedList<Planning> plannings = _planningService.GetAllPlannings(x =>
                (x.MainEmployeeId == timesheet.EmployeeId ||
                 x.PlanningEmployees.Any(y => y.EmployeeId == timesheet.EmployeeId))
                && x.StartDate >= timesheet.StartDate
                && x.StartDate <= timesheet.EndDate, x => x.StartDate, false, loadOverviewIncludes: true);

            System.Collections.Generic.IList<CalendarItem> calanderItems =
                _calendarService.GetCalendarItemsByTimestamp(timesheet.EmployeeId, timesheet.StartDate,
                    timesheet.EndDate);

            foreach (TimesheetHomeWorkRow row in timesheet.TimeSheetHomeWorkRows)
            {
                foreach (Planning planning in plannings.Where(p => p.StartDate.Date == row.Date))
                {
                    if (timesheet.TimeSheetTimeRows.Any(x => x.PlanningId == planning.Id)) continue;

                    timesheet.TimeSheetTimeRows.Add(new TimesheetTimeRow
                    {
                        PlanningId = planning.Id,
                        TimesheetId = timesheet.Id,
                        StartDate = planning.StartDate,
                        EndDate = planning.EndDate,
                        TravelTime = TimeSpan.Zero,
                        WorkTime = TimeSpan.Zero,
                        OfferNumber = planning.OfferNumber,
                        Description = planning.Building.Name
                    });
                }

                foreach (CalendarItem calendarItem in calanderItems.Where(p => p.StartDate.Date == row.Date))
                {
                    if (timesheet.TimeSheetTimeRows.Any(x => x.CalendarItemId == calendarItem.Id)) continue;

                    timesheet.TimeSheetTimeRows.Add(new TimesheetTimeRow
                    {
                        CalendarItemId = calendarItem.Id,
                        TimesheetId = timesheet.Id,
                        StartDate = calendarItem.StartDate,
                        EndDate = calendarItem.EndDate,
                        TravelTime = TimeSpan.Zero,
                        WorkTime = TimeSpan.Zero,
                        OfferNumber = calendarItem.CalendarStatus.GetStringValue(),
                        Description = calendarItem.Subject
                    });
                }
            }

            _dbContext.SaveChanges();

            return timesheet;
        }

        public TimesheetHomeWorkRow GetTimeSheetHomeWorkRow(int id)
        {
            return _timesheetHomeWorkRowRepository.GetById(id);
        }

        public TimesheetTimeRow GetTimeSheetRow(int id)
        {
            return _timesheetTimeRowRepository.GetById(id);
        }

        public async Task<(bool showMessage, int weekNumber)> ShowTimeSheetMessageAsync(int currentUserId)
        {
            int week = DateTime.Now.GetWeekNumber();
            int year = DateTime.Now.Year;
            if ((DateTime.Now.DayOfWeek != DayOfWeek.Friday || DateTime.Now.Hour <= 17) &&
                DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday)
            {
                week--;
                year = DateTime.Now.AddDays(-7).Year;
            }

            if (DateTime.Now > new DateTime(2017, 7, 30))
            {
                Timesheet timesheet = await _timesheetRepository.GetAsync(x => x.EmployeeId == currentUserId && x.WeekNumber == week && x.StartDate.Year == year);
                if (timesheet == null || !timesheet.IsSend)
                {
                    return (true, week);
                }
            }

            return (false, week);
        }

        public async Task<TimesheetTimeRow> GetTimeSheetRowAsync(int id)
        {
            return await _timesheetTimeRowRepository.GetByIdAsync(id);
        }

        public async Task DeleteTimeSheetRowAsync(TimesheetTimeRow row)
        {
            await _timesheetTimeRowRepository.DeleteAsync(row);
        }
    }
}
