﻿using FusionCore.Data;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class ActivityService : BaseService, IActivityService
    {
        private readonly FusionCoreContext _dbContext;
        private readonly IGenericRepository<AssetType> _assetTypeRepository;
        private readonly IGenericRepository<Activity> _activityRepository;
        private readonly IGenericRepository<ActivityField> _activityFieldRepository;
        private readonly IGenericRepository<ActivityDynamicField> _activityDynamicFieldRepository;

        public ActivityService(
            FusionCoreContext dbContext,
            IGenericRepository<AssetType> assetTypeRepository,
            IGenericRepository<Activity> activityRepository,
            IGenericRepository<ActivityDynamicField> activityDynamicFieldRepository,
            IGenericRepository<ActivityField> activityFieldRepository)
        {
            _dbContext = dbContext;
            _assetTypeRepository = assetTypeRepository;
            _activityRepository = activityRepository;
            _activityDynamicFieldRepository = activityDynamicFieldRepository;
            _activityFieldRepository = activityFieldRepository;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public IList<AssetType> GetAssetTypes(Expression<Func<AssetType, bool>> predicate = null, params Expression<Func<AssetType, object>>[] includes)
        {
            return _assetTypeRepository.GetAll(predicate, includes: includes).OrderBy(x => x.SortOrder).ToList();
        }

        public void AddAssetType(string name)
        {
            _assetTypeRepository.Insert(new AssetType
            {
                Name = name,
                SortOrder = 0
            });
        }

        public AssetType GetAssetTypeById(int id)
        {
            return _assetTypeRepository.GetById(id);
        }

        public void DeleteAssetType(AssetType assetType)
        {
            _assetTypeRepository.Delete(assetType);
        }

        public void AddActivity(string name, int assetTypeId)
        {
            _activityRepository.Insert(new Activity
            {
                Name = name,
                AssetTypeId = assetTypeId
            });
        }

        public IList<Activity> GetActivitesByAssetType(int assetTypeId)
        {
            return _activityRepository.GetAll(x => x.AssetTypeId.Equals(assetTypeId)).OrderBy(x => x.SortOrder).ToList();
        }

        public void AddDynamicField(string name, int activityId, DynamicFieldType fieldType)
        {
            _activityDynamicFieldRepository.Insert(new ActivityDynamicField
            {
                ActivityId = activityId,
                Name = name,
                DynamicFieldType = fieldType
            });
        }

        public Activity GetActivityById(int id)
        {
            return _activityRepository.GetById(id);
        }

        public void DeleteActivity(Activity activity)
        {
            List<ActivityField> fields = _activityFieldRepository.GetAll(x => x.ActivityId == activity.Id).ToList();
            foreach (ActivityField field in fields)
            {
                _activityFieldRepository.Delete(field);
            }
            _activityRepository.Delete(activity);
        }

        public IList<ActivityDynamicField> GetActivityDynamicFieldsByActivityId(int id)
        {
            return _activityDynamicFieldRepository.GetAll(x => x.ActivityId == id).OrderBy(x => x.SortOrder).ToList();
        }

        public ActivityDynamicField GetActivityDynamicFieldById(int id)
        {
            return _activityDynamicFieldRepository.GetById(id);
        }

        public void DeleteActivityDynamicField(ActivityDynamicField activityDynamicField)
        {
            _activityDynamicFieldRepository.Delete(activityDynamicField);
        }

        public IList<ActivityField> GetActivityFieldsByActivityId(int id)
        {
            return _activityFieldRepository.GetAll(x => x.ActivityId == id).OrderBy(x => x.SortOrder).ToList();
        }

        public ActivityField GetActivityFieldById(int id)
        {
            return _activityFieldRepository.GetById(id);
        }

        public void DeleteActivityField(ActivityField activityField)
        {
            _activityFieldRepository.Delete(activityField);
        }

        public void AddField(string name, int activityId, DynamicFieldType dynamicFieldType)
        {
            _activityFieldRepository.Insert(new ActivityField
            {
                ActivityId = activityId,
                Name = name,
                DynamicFieldType = dynamicFieldType
            });
        }
    }
}
