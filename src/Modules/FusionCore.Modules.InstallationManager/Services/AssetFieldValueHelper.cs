﻿using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class AssetFieldValueHelper
    {
        public static int? ConvertToNullableInt(string value)
        {
            if (int.TryParse(value, out int result))
            {
                return result;
            }
            return null;
        }

        public static float? ConvertToNullableFloat(string value)
        {
            if (float.TryParse(value, out float result))
            {
                return result;
            }
            return null;
        }


        public static AssetFieldHuidigeEnVorigeWaarden GetHuidigeEnVorigeWaarden(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                AssetFieldHuidigeEnVorigeWaarden empty = new AssetFieldHuidigeEnVorigeWaarden { Huidige = 0, VorigeWaarden = new List<float?>() };
                while (empty.VorigeWaarden.Count < 5)
                {
                    empty.VorigeWaarden.Add(0);
                }
                return empty;
            }
            string[] waarden = value.Split(';');
            AssetFieldHuidigeEnVorigeWaarden result = new AssetFieldHuidigeEnVorigeWaarden
            {
                Huidige = ConvertToNullableFloat(waarden[0]),
                VorigeWaarden = new List<float?>()
            };
            for (int i = 1; i < waarden.Length; i++)
            {
                result.VorigeWaarden.Add(ConvertToNullableFloat(waarden[i]));
            }
            while (result.VorigeWaarden.Count < 5)
            {
                result.VorigeWaarden.Add(0);
            }
            return result;
        }

        public static AssetFieldHuidigeEnVorigeWaardeEnToelichting GetAssetFieldHuidigeEnVorigeWaardeEnToelichting(
            string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new AssetFieldHuidigeEnVorigeWaardeEnToelichting();
            }
            string[] waarden = value.Split(';');
            AssetFieldHuidigeEnVorigeWaardeEnToelichting result = new AssetFieldHuidigeEnVorigeWaardeEnToelichting
            {
                Huidige = waarden.Length > 0 ? ConvertToNullableFloat(waarden[0]) : null,
                Vorige = waarden.Length > 1 ? ConvertToNullableFloat(waarden[1]) : null,
                Toelichting = waarden.Length >= 2 ? waarden[2] : ""

            };
            return result;

        }

        public static AssetFieldAnalyseSk GetAnalyseSk(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new AssetFieldAnalyseSk { MinWaarde = 0, MaxWaarde = 0 };
            }


            string[] waarden = value.Split(';');
            AssetFieldAnalyseSk result = new AssetFieldAnalyseSk
            {
                MinWaarde = waarden.Length > 0 ? ConvertToNullableFloat(waarden[0]) : null,
                MaxWaarde = waarden.Length > 1 ? ConvertToNullableFloat(waarden[1]) : null,
                Waarde = waarden.Length > 2 ? ConvertToNullableFloat(waarden[2]) : null,
                Ontgassing = waarden.Length > 3 ? ConvertToNullableFloat(waarden[3]) : null,
                Suppletie = waarden.Length >= 4 ? ConvertToNullableFloat(waarden[4]) : null,
            };
            return result;

        }

        public static AssetFieldAnalyseKt GetAnalyseKt(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new AssetFieldAnalyseKt { MinWaarde = 0, MaxWaarde = 0 };
            }


            string[] waarden = value.Split(';');
            AssetFieldAnalyseKt result = new AssetFieldAnalyseKt
            {
                MinWaarde = waarden.Length > 0 ? ConvertToNullableFloat(waarden[0]) : null,
                MaxWaarde = waarden.Length > 1 ? ConvertToNullableFloat(waarden[1]) : null,
                Waarde = waarden.Length > 2 ? ConvertToNullableFloat(waarden[2]) : null,
                Suppletie = waarden.Length >= 3 ? ConvertToNullableFloat(waarden[3]) : null,
            };
            return result;

        }

        public static AssetFieldFiltersVSnaren GetFiltersVSnaren(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new AssetFieldFiltersVSnaren();
            }
            string[] waarden = value.Split(';');
            return new AssetFieldFiltersVSnaren
            {
                Fabrikant = waarden.Length > 0 ? waarden[0] : "",
                Aantal = waarden.Length > 1 ? waarden[1] : "",
                Type = waarden.Length > 2 ? waarden[2] : "",
                LxBxH = waarden.Length > 3 ? waarden[3] : "",
                Toelichting = waarden.Length > 4 ? waarden[4] : ""
            };
        }
        public static AssetFieldTvRtAv GetTvRtAv(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new AssetFieldTvRtAv();
            }
            string[] waarden = value.Split(';');
            AssetFieldTvRtAv result = new AssetFieldTvRtAv
            {
                Waarde = waarden.Length > 0 ? ConvertToNullableInt(waarden[0]) : null,
                Opmerking = waarden.Length > 1 ? waarden[1] : "",
                LxBxH = waarden.Length >= 2 ? waarden[2] : ""
            };
            return result;
        }

        public static string SetHuidigeEnVorigeWaardeEnToelichting(string huidige, string vorige, string toelichting)
        {
            huidige = huidige.Replace(".", ",");
            vorige = vorige.Replace(".", ",");

            return $"{huidige};{vorige};{toelichting}";
        }

        public static string SetHuidigeEnVorigeWaarden(string huidige, string vorige1, string vorige2, string vorige3, string vorige4, string vorige5)
        {
            huidige = huidige.Replace(".", ",");
            vorige1 = vorige1.Replace(".", ",");
            vorige2 = vorige2.Replace(".", ",");
            vorige3 = vorige3.Replace(".", ",");
            vorige4 = vorige4.Replace(".", ",");
            vorige5 = vorige5.Replace(".", ",");

            return $"{huidige};{vorige1};{vorige2};{vorige3};{vorige4};{vorige5}";
        }

        public static string SetAnalyseSk(string huidige, string ontgasser, string suppletie, string min, string max)
        {
            min = min.Replace(".", ",");
            max = max.Replace(".", ",");
            huidige = huidige.Replace(".", ",");
            suppletie = suppletie.Replace(".", ",");
            ontgasser = ontgasser.Replace(".", ".");
            return $"{min};{max};{huidige};{ontgasser};{suppletie}";
        }
        public static string SetAnalyseKt(string huidige, string suppletie, string min, string max)
        {
            min = min.Replace(".", ",");
            max = max.Replace(".", ",");
            huidige = huidige.Replace(".", ",");
            suppletie = suppletie.Replace(".", ",");
            return $"{min};{max};{huidige};{suppletie}";
        }

        public static string SetTvRtAv(string waarde, string opmerking, string lxbxh)
        {
            return $"{waarde};{opmerking};{lxbxh}";
        }

        public static string SetFiltersVSnaren(string fabrikant, string aantaal, string type, string lxbxh,
            string toelichting)
        {
            return $"{fabrikant};{aantaal};{type};{lxbxh};{toelichting}";
        }

    }

    public class AssetFieldFiltersVSnaren
    {
        public string Fabrikant { get; set; }
        public string Aantal { get; set; }
        public string Type { get; set; }
        public string LxBxH { get; set; }
        public string Toelichting { get; set; }
    }

    public class AssetFieldHuidigeEnVorigeWaarden
    {
        public float? Huidige { get; set; }
        public List<float?> VorigeWaarden { get; set; }

    }

    public class AssetFieldHuidigeEnVorigeWaardeEnToelichting
    {
        public float? Huidige { get; set; }
        public float? Vorige { get; set; }
        public string Toelichting { get; set; }
    }

    public class AssetFieldAnalyseSk
    {
        public float? MinWaarde { get; set; }
        public float? MaxWaarde { get; set; }
        public float? Waarde { get; set; }
        public float? Ontgassing { get; set; }
        public float? Suppletie { get; set; }
    }

    public class AssetFieldAnalyseKt
    {
        public float? MinWaarde { get; set; }
        public float? MaxWaarde { get; set; }
        public float? Waarde { get; set; }
        public float? Suppletie { get; set; }
    }

    public class AssetFieldTvRtAv
    {
        public int? Waarde { get; set; }
        public string Opmerking { get; set; }
        public string LxBxH { get; set; }
    }
}
