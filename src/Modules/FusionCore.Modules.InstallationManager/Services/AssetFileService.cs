using FusionCore.Data;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class AssetFileService : BaseService, IAssetFileService
    {
        private readonly IGenericRepository<AssetFile> _assetFileRepository;
        private readonly IGenericRepository<AssetFileBinary> _assetFileByteRepository;
        private readonly FusionCoreContext _dbContext;

        public AssetFileService(
            IGenericRepository<AssetFile> assetFileRepository,
            FusionCoreContext dbContext,
            IGenericRepository<AssetFileBinary> assetFileByteRepository)
        {
            _assetFileRepository = assetFileRepository;
            _dbContext = dbContext;
            _assetFileByteRepository = assetFileByteRepository;
        }

        public async Task<AssetFile> GetAssetFileByIdAsync(int id)
        {
            return await _assetFileRepository.GetByIdAsync(id);
        }
        public async Task DeleteAssetFileAsync(int id)
        {
            AssetFile assetFile = await _assetFileRepository.GetByIdAsync(id);
            if (assetFile != null)
            {
                await _assetFileRepository.DeleteAsync(assetFile);
            }
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public async Task<AssetFileBinary> GetBytesByAssetFileIdAsync(int assetFileId)
        {
            return await _assetFileByteRepository.GetAsync(x => x.AssetFileId == assetFileId);
        }

        public async Task InsertAssetFileBinaryAsync(AssetFileBinary assetFileBinary)
        {
            await _assetFileByteRepository.InsertAsync(assetFileBinary);
        }
        public async Task UpdateAssetFileBinaryAsync(AssetFileBinary assetFileBinary)
        {
            await _assetFileByteRepository.UpdateAsync(assetFileBinary);
        }

        public async Task<AssetFile[]> GetAssetFilesAsync<TKey>(Expression<Func<AssetFile, bool>> predicate = null, Expression<Func<AssetFile, TKey>> orderBy = null, bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<AssetFile> queryable = _assetFileRepository.GetAll(predicate, true);
            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }

            if(pageIndex > 0)
            {
                queryable = queryable.Skip(pageIndex * pageSize);
            }

            return await queryable.ToArrayAsync();
        }
    }
}
