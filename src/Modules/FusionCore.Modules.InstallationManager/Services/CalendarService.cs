using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public interface ICalendarService : IBaseService
    {
        CalendarItem GetCalendarItem(int id);
        IList<CalendarItem> GetCalendarItemsByTimestamp(int employeeId, DateTime start, DateTime end);
        Task<IList<CalendarItem>> GetCalendarItemsByTimestampAsync(List<int> groupEmployeeIds, DateTime start,
            DateTime end);
        int? PersistCalendarItem(CalendarItemPersistModel calendarItem);
        Task DeleteCalendarItemAsync(int id);
        Task UpdateCalendarItem(CalendarItem calendarItem);
    }

    public class CalendarService : BaseService, ICalendarService
    {
        private readonly IGenericRepository<CalendarItem> _calendarItemRepository;
        private readonly IGenericRepository<TimesheetTimeRow> _timesheetTimeRowRepository;
        private readonly IMapper _mapper;

        public CalendarService(
            IGenericRepository<CalendarItem> calendarItemRepository,
            IGenericRepository<TimesheetTimeRow> timesheetTimeRowRepository,
            IMapper mapper)
        {
            _calendarItemRepository = calendarItemRepository;
            _timesheetTimeRowRepository = timesheetTimeRowRepository;
            _mapper = mapper;
        }

        public IList<CalendarItem> GetCalendarItemsByTimestamp(int employeeId, DateTime start, DateTime end)
        {
            return _calendarItemRepository.GetAll(x =>
                x.EmployeeId == employeeId &&
                ((x.StartDate >= start && x.EndDate <= end) || (x.StartDate >= start && x.StartDate <= end) || (x.EndDate >= start && x.EndDate <= end) || (x.StartDate <= start && x.EndDate >= end))).ToList();
        }

        public async Task<IList<CalendarItem>> GetCalendarItemsByTimestampAsync(List<int> groupEmployeeIds,
            DateTime start, DateTime end)
        {
            return await _calendarItemRepository.GetAll(x =>
                (groupEmployeeIds == null || !groupEmployeeIds.Any() || groupEmployeeIds.Contains(x.EmployeeId)) &&
                ((x.StartDate >= start && x.EndDate <= end) || (x.StartDate >= start && x.StartDate <= end) || (x.EndDate >= start && x.EndDate <= end) || (x.StartDate <= start && x.EndDate >= end))
                ).ToListAsync();
        }

        public int? PersistCalendarItem(CalendarItemPersistModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            if (model.Id.HasValue)
            {
                //Edit
                var dbEntity = GetCalendarItem(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _calendarItemRepository.Update(dbEntity);

                    return dbEntity.Id;
                }
            }
            else
            {
                //insert
                var dbEntity = new CalendarItem();
                _mapper.Map(model, dbEntity);
                _calendarItemRepository.Insert(dbEntity);
                return dbEntity.Id;
            }

            return null;
        }

        public async Task DeleteCalendarItemAsync(int id)
        {
            var timeSheetEntries = await _timesheetTimeRowRepository.Table.Where(x => x.CalendarItemId.HasValue && x.CalendarItemId == id).ToListAsync();
            foreach (var entry in timeSheetEntries)
            {
                await _timesheetTimeRowRepository.DeleteAsync(entry);
            }

            var calendarItem = await _calendarItemRepository.GetByIdAsync(id);
            if (calendarItem != null)
            {
                await _calendarItemRepository.DeleteAsync(calendarItem);
            }
        }

        public async Task UpdateCalendarItem(CalendarItem calendarItem)
        {
            await _calendarItemRepository.UpdateAsync(calendarItem);
        }

        public CalendarItem GetCalendarItem(int id)
        {
            return _calendarItemRepository.GetById(id);
        }
    }
}
