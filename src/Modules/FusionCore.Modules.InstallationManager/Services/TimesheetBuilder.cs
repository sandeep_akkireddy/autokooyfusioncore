﻿using ClosedXML.Excel;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class TimesheetBuilder
    {
        private readonly XLWorkbook _workbook;
        private readonly IXLWorksheet _worksheet;
        private readonly Timesheet _timesheet;
        public string EmployeeName;
        public TimesheetBuilder(Timesheet timesheet, string employeeName)
        {
            _workbook = new XLWorkbook();
            _worksheet = _workbook.Worksheets.Add("Weekstaat");
            _timesheet = timesheet;
            EmployeeName = employeeName;
        }

        public void BuildWeekstaat(string filename)
        {
            _worksheet.ColumnWidth = 20;
            _worksheet.Cell("A1").WorksheetColumn().Width = 60;

            BuildHeader();
            _workbook.SaveAs(filename);
        }

        private void BuildHeader()
        {
            _worksheet.Cell("A5").Value = "Urenlijst / Weekstaat";
            _worksheet.Cell("A5").Style.Font.FontSize = 20;
            _worksheet.Cell("A5").Style.Font.Bold = true;

            DateTime dtStart = DateTime.Now.GetFirstDayOfWeek(_timesheet.WeekNumber);
            DateTime dtEind = dtStart.AddDays(6);

            _worksheet.Cell("A7").Value = $"Weeknummer: {_timesheet.WeekNumber}";
            _worksheet.Cell("A7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell("B7").Value = $"Van: {dtStart:dd-MMM-yyyy}";
            _worksheet.Cell("B7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell("C7").Value = $"Tot: {dtEind:dd-MMM-yyyy}";
            _worksheet.Cell("C7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell("D7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell("E7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell("F7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell("G7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell("H7").Style.Border.BottomBorder = XLBorderStyleValues.Thick;

            _worksheet.Cell("A9").Value = "Naam";
            _worksheet.Cell("B9").Value = EmployeeName;

            _worksheet.Cell("F7").Value = "Kilometerstand";
            _worksheet.Cell("G7").Value = $"Maandag: {_timesheet.KilometerStart}";
            _worksheet.Cell("H7").Value = $"Zondag: {_timesheet.KilometerEnd}";
            _worksheet.Cell("A10").Value = "Storingsdienst: ja / nee";
            _worksheet.Cell("B10").Value = _timesheet.IsSupportShift ? "Ja" : "Nee";

            int row = 11;
            for (DateTime dt = dtStart; dt <= dtEind; dt = dt.AddDays(1))
            {
                row = BuildDag(dt, row);
                row += 2;
            }

        }

        private double ConvertToDouble(TimeSpan ts)
        {
            return (ts.Hours + (double)ts.Minutes / 60);
        }

        private int BuildDag(DateTime dt, int startRow)
        {

            int row = startRow;
            _worksheet.Cell($"C{startRow}").Value = dt.ToString("dddd");

            row++;
            for (char ch = 'A'; ch < 'I'; ch++)
            {
                _worksheet.Cell($"{ch}{row}").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
                _worksheet.Cell($"{ch}{row}").Style.Border.TopBorder = XLBorderStyleValues.Thick;
                _worksheet.Cell($"{ch}{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thick;
                _worksheet.Cell($"{ch}{row}").Style.Border.RightBorder = XLBorderStyleValues.Thick;
            }
            _worksheet.Cell($"A{row}").Value = "Project:";
            _worksheet.Cell($"B{row}").Value = "Projectnummer:";
            _worksheet.Cell($"C{row}").Value = "Starttijd:";
            _worksheet.Cell($"D{row}").Value = "Eindtijd:";
            _worksheet.Cell($"E{row}").Value = "Uren:";
            _worksheet.Cell($"F{row}").Value = "Reistijd:";
            _worksheet.Cell($"G{row}").Value = "Totaal:";
            _worksheet.Cell($"H{row}").Value = "Woon/werk:";

            List<TimesheetTimeRow> regels = _timesheet.TimeSheetTimeRows.Where(r => r.StartDate.Date == dt.Date && !r.DeletedOnUtc.HasValue && r.StartDate.Ticks != 0 && r.EndDate.Ticks != 0).OrderBy(r => r.StartDate).ToList();
            TimesheetHomeWorkRow ww = _timesheet.TimeSheetHomeWorkRows.FirstOrDefault(w => w.Date == dt);
            if (ww != null && regels.Any())
            {
                TimesheetTimeRow firstregel = regels.First();
                row++;
                SetBordersProjectRegel(row);

                DateTime woonwerkDate = new DateTime(ww.HomeWorkTime.Ticks);
                _worksheet.Cell($"A{row}").Value = "Woon-werk:";
                _worksheet.Cell($"C{row}").Value = new DateTime(firstregel.StartDate.Ticks).AddHours(-1 * woonwerkDate.Hour).AddMinutes(-1 * woonwerkDate.Minute).ToShortTimeString();
                _worksheet.Cell($"D{row}").Value = new DateTime(firstregel.StartDate.Ticks).ToShortTimeString();
                _worksheet.Cell($"C{row}").Style.DateFormat.SetFormat("hh:mm");
                _worksheet.Cell($"D{row}").Style.DateFormat.SetFormat("hh:mm");
                _worksheet.Cell($"F{row}").Style.Fill.BackgroundColor = XLColor.Gray;
                _worksheet.Cell($"G{row}").Style.Fill.BackgroundColor = XLColor.Gray;
                _worksheet.Cell($"H{row}").Value = ConvertToDouble(ww.WorkHomeTime);
                _worksheet.Cell($"H{row}").Style.NumberFormat.SetFormat("0.00");
            }

            double total = 0;
            foreach (TimesheetTimeRow regel in regels)
            {
                row++;
                SetBordersProjectRegel(row);

                _worksheet.Cell($"A{row}").Value = regel.Description;
                _worksheet.Cell($"B{row}").Value = regel.OfferNumber;
                _worksheet.Cell($"C{row}").Value = new DateTime(regel.StartDate.Ticks).ToShortTimeString();
                _worksheet.Cell($"D{row}").Value = new DateTime(regel.EndDate.Ticks).ToShortTimeString();
                _worksheet.Cell($"E{row}").Value = new DateTime(regel.WorkTime.Ticks).ToShortTimeString();
                _worksheet.Cell($"F{row}").Value = new DateTime(regel.TravelTime.Ticks).ToShortTimeString();
                _worksheet.Cell($"C{row}").Style.DateFormat.SetFormat("hh:mm");
                _worksheet.Cell($"D{row}").Style.DateFormat.SetFormat("hh:mm");
                _worksheet.Cell($"E{row}").Style.DateFormat.SetFormat("hh:mm");
                _worksheet.Cell($"F{row}").Style.DateFormat.SetFormat("hh:mm");

                _worksheet.Cell($"G{row}").Value = ConvertToDouble(regel.WorkTime) + ConvertToDouble(regel.TravelTime);
                _worksheet.Cell($"G{row}").Style.NumberFormat.SetFormat("0.00");
                _worksheet.Cell($"H{row}").Style.Fill.BackgroundColor = XLColor.Gray;
                total += ConvertToDouble(regel.WorkTime) + ConvertToDouble(regel.TravelTime);
            }

            if (ww != null && regels.Any())
            {
                TimesheetTimeRow lastregel = regels.Last();
                row++;
                SetBordersProjectRegel(row);

                DateTime werkwoonDate = new DateTime(ww.WorkHomeTime.Ticks);

                _worksheet.Cell($"A{row}").Value = "Werk-woon:";
                _worksheet.Cell($"C{row}").Value = new DateTime(lastregel.EndDate.Ticks).ToShortTimeString();
                _worksheet.Cell($"D{row}").Value = new DateTime(lastregel.EndDate.Ticks).AddHours(werkwoonDate.Hour).AddMinutes(werkwoonDate.Minute).ToShortTimeString();
                _worksheet.Cell($"C{row}").Style.DateFormat.SetFormat("hh:mm");
                _worksheet.Cell($"D{row}").Style.DateFormat.SetFormat("hh:mm");
                _worksheet.Cell($"F{row}").Style.Fill.BackgroundColor = XLColor.Gray;
                _worksheet.Cell($"G{row}").Style.Fill.BackgroundColor = XLColor.Gray;
                _worksheet.Cell($"H{row}").Value = ConvertToDouble(ww.WorkHomeTime);
                _worksheet.Cell($"H{row}").Style.NumberFormat.SetFormat("0.00");
            }

            row++;
            _worksheet.Cell($"G{row}").Value = total;
            _worksheet.Cell($"G{row}").Style.NumberFormat.SetFormat("0.00");
            _worksheet.Cell($"G{row}").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell($"G{row}").Style.Border.TopBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell($"G{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell($"G{row}").Style.Border.RightBorder = XLBorderStyleValues.Thick;
            if (ww != null)
            {
                double wwTotal = ConvertToDouble(ww.HomeWorkTime) + ConvertToDouble(ww.WorkHomeTime);
                if (wwTotal > 0.5)
                {
                    wwTotal -= 0.5;
                }

                _worksheet.Cell($"H{row}").Value = wwTotal;
                _worksheet.Cell($"H{row}").Style.NumberFormat.SetFormat("0.00");
                _worksheet.Cell($"H{row}").Style.Border.BottomBorder = XLBorderStyleValues.Thick;
                _worksheet.Cell($"H{row}").Style.Border.TopBorder = XLBorderStyleValues.Thick;
                _worksheet.Cell($"H{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thick;
                _worksheet.Cell($"H{row}").Style.Border.RightBorder = XLBorderStyleValues.Thick;
            }

            for (char ch = 'A'; ch < 'I'; ch++)
            {
                _worksheet.Cell($"{ch}{row}").Style.Border.TopBorder = XLBorderStyleValues.Thick;
            }
            return row;
        }

        private void SetBordersProjectRegel(int row)
        {
            for (char ch = 'A'; ch < 'G'; ch++)
            {
                _worksheet.Cell($"{ch}{row}").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                _worksheet.Cell($"{ch}{row}").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                _worksheet.Cell($"{ch}{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                _worksheet.Cell($"{ch}{row}").Style.Border.RightBorder = XLBorderStyleValues.Thin;
            }
            _worksheet.Cell($"A{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell($"G{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell($"H{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thick;
            _worksheet.Cell($"I{row}").Style.Border.LeftBorder = XLBorderStyleValues.Thick;

        }

        public static string GetOfferNumberWithoutSerialNumber(string offerNumber)
        {
            if (string.IsNullOrWhiteSpace(offerNumber)) return string.Empty;

            offerNumber = offerNumber.Replace(" ", string.Empty);
            string[] parts = offerNumber.Split('-');
            int partsLength = parts.Length;
            if (parts.Last().StartsWith("0"))
            {
                return string.Join("-", parts.Take(partsLength - 1));
            }

            return offerNumber;
        }
    }
}
