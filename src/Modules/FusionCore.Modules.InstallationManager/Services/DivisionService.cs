﻿using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class DivisionService : BaseService, IDivisionService
    {
        private readonly IGenericRepository<Division> _divisionRepository;
        private readonly IMapper _mapper;

        public DivisionService(
            IGenericRepository<Division> divisionRepository,
            IMapper mapper)
        {
            _divisionRepository = divisionRepository;
            _mapper = mapper;
        }

        public IPagedList<Division> GetAllDivisions<TKey>(Expression<Func<Division, bool>> predicate = null,
            Expression<Func<Division, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<Division> queryable = _divisionRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Division>(queryable, pageIndex, pageSize);
        }

        public Division GetDivision(int id)
        {
            return _divisionRepository.Get(x => x.Id == id);
        }

        public bool PersistDivision(DivisionPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                Division dbEntity = GetDivision(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _divisionRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                Division dbEntity = new Division();
                _mapper.Map(model, dbEntity);
                _divisionRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }

        public async Task DeleteDivision(int id)
        {
            Division user = await _divisionRepository.GetByIdAsync(id);
            if (user != null) _divisionRepository.Delete(user);
        }
    }
}