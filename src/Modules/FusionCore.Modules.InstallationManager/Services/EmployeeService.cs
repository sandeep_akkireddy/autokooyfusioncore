﻿using AutoMapper;
using FusionCore.Core;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Backend.Login;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class EmployeeService : BaseService, IEmployeeService
    {
        private readonly IGenericRepository<Employee> _employeeRepository;
        private readonly IGenericRepository<EmployeeGroup> _employeeGroupRepository;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEmailSender _emailSender;
        private readonly MainSettings _mainSettings;

        public EmployeeService(
            IGenericRepository<Employee> employeeRepository,
            IGenericRepository<EmployeeGroup> employeeGroupRepository,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            IEmailSender emailSender,
            IOptions<MainSettings> optionsAccessor)
        {
            _employeeRepository = employeeRepository;
            _employeeGroupRepository = employeeGroupRepository;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
            _emailSender = emailSender;
            _mainSettings = optionsAccessor.Value;
        }

        public IPagedList<Employee> GetAllEmployees<TKey>(Expression<Func<Employee, bool>> predicate = null,
            Expression<Func<Employee, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<Employee> queryable = _employeeRepository.GetAll(predicate);
            queryable = queryable
                .Include(x => x.Employee2EmployeeGroups)
                .ThenInclude(y => y.EmployeeGroup);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Employee>(queryable, pageIndex, pageSize);
        }

        public Employee GetEmployee(int id)
        {
            return _employeeRepository.Get(x => x.Id == id, x => x.Employee2EmployeeGroups);
        }

        public bool PersistEmployee(EmployeePersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                Employee dbEntity = GetEmployee(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);

                    if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.PasswordControle))
                    {
                        (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
                        dbEntity.Salt = password.salt;
                        dbEntity.Key = password.key;
                    }

                    _employeeRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                Employee dbEntity = new Employee();
                _mapper.Map(model, dbEntity);

                if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.PasswordControle))
                {
                    (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
                    dbEntity.Salt = password.salt;
                    dbEntity.Key = password.key;
                }

                _employeeRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }

        public async Task DeleteEmployee(int id)
        {
            Employee employee = await _employeeRepository.GetByIdAsync(id);
            if (employee != null) _employeeRepository.Delete(employee);
        }

        public void AddEmployeeGroup(EmployeeGroup model)
        {
            _employeeGroupRepository.Insert(model);
        }

        public IPagedList<EmployeeGroup> GetAllEmployeeGroups<TKey>(Expression<Func<EmployeeGroup, bool>> predicate = null,
            Expression<Func<EmployeeGroup, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<EmployeeGroup> queryable = _employeeGroupRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<EmployeeGroup>(queryable, pageIndex, pageSize);
        }

        public Employee Validate(string identifier, string password, string loginTypeCode = Constants.DefaultLoginType)
        {
            Employee employee = _employeeRepository.Get(x => !string.IsNullOrEmpty(x.Username) &&
                x.Username.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));

            if (employee?.Key != null
                && employee.Salt != null
                && !employee.DeletedOnUtc.HasValue
                && PasswordHelper.ValidatePasswordHash(employee.Key, employee.Salt, password))
            {
                return employee;
            }

            return null;
        }

        public async Task SignIn(Employee employee, bool isPersistent = false)
        {
            ClaimsIdentity identity = new ClaimsIdentity(GetEmployeeClaims(employee), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await _httpContextAccessor.HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties { IsPersistent = isPersistent }
            );
        }

        public async Task SignOut()
        {
            await _httpContextAccessor.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public int GetCurrentUserId()
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;

            if (!httpContext.User.Identity.IsAuthenticated)
                return -1;

            Claim claim = httpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (claim == null)
                return -1;

            if (!int.TryParse(claim.Value, out int currentUserId))
                return -1;

            return currentUserId;
        }

        public Employee GetCurrentUser()
        {
            int currentUserId = GetCurrentUserId();
            if (currentUserId == -1) return null;

            Employee currentUser = _employeeRepository.GetById(currentUserId);
            if (currentUser == null || !currentUser.Active) return null;

            return currentUser;
        }

        public async Task SendPasswordResetEmail(ForgotModel model, string loginTypeCode = Constants.DefaultLoginType)
        {
            Employee employee = _employeeRepository.Get(
                c => string.Equals(c.Username, model.Email, StringComparison.OrdinalIgnoreCase)
            );

            if (employee != null)
            {
                Guid key = Guid.NewGuid();
                employee.ResetKey = key.ToString();
                _employeeRepository.Update(employee);
                //TODO: Change this for an editable template
                string url = $"{_httpContextAccessor.HttpContext.Request.GetBaseUrl()}/login/reset?key={key}&uid={employee.Id}";
                StringBuilder body = new StringBuilder();
                body.Append("<span style=\"font-size:14px; font-family:Arial, 'Helvetica Neue', Gotham; color: #676163;\">Beste " + employee.GetFullname + ",</span>");
                body.Append("<table cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:14px; font-family:Arial, 'Helvetica Neue', Gotham; color: #676163;\">");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("	<td>Je ontvangt deze email omdat je een nieuw wachtwoord wilt instellen voor je account, als je geen nieuw wachtwoordt hebt aangevraagd kun je deze email negeren:</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("	<td>");
                body.Append("	Via onderstaande link kun je je wachtwoord opnieuw aanmaken binnen je account omgeving:<br />");
                body.Append("	<strong><a href=\"" + url + "\">" + url + "</a></strong>");
                body.Append("	</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("	<td><em>Let op! bovenstaande link is maar 1 uur geldig!</em></td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<br />");
                body.Append("<table cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:14px;  font-family:'sans-serif', Helvetica, 'Helvetica Neue', Arial, Gotham; color:#434343;\">");
                body.Append("<tr>");
                body.Append("<td>");
                body.Append("Met vriendelijke groet |  Regards<br />");
                body.Append("Novi Media</td>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td valign=\"top\">");
                body.Append("<a href=\"https://www.novimedia.net\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/novi-media-logo-black.png\" width=\"200\" height=\"66\" alt=\"\"/></a><br />");
                body.Append("<br>");
                body.Append("Duizendknooplaan 19<br>");
                body.Append("NL-3452 AS Vleuten<br>");
                body.Append("<br>");
                body.Append("t: +31 (0)302682756<br>");
                body.Append("</td>");
                body.Append("<td valign=\"bottom\">");
                body.Append("<span style=\"color: #56af00\">info@novimedia.net<br>");
                body.Append("www.novimedia.net</span>");
                body.Append("</td>");
                body.Append("<tr>");
                body.Append("<td>&nbsp;</td>");
                body.Append("<td>&nbsp;</td>");
                body.Append("</tr>");
                body.Append("<tr>");
                body.Append("<td valign=\"top\">");
                body.Append("<table cellpadding=\"0\" cellspacing=\"4\">");
                body.Append("<tr>");
                body.Append("<td><a href=\"https://www.facebook.com/novimedia\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/icon-mail-facebook.png\" width=\"30\" height=\"\" alt=\"\"/></a></td>");
                body.Append("<td><a href=\"https://www.linkedin.com/company/novi-media\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/icon-mail-linkedin.png\" width=\"30\" height=\"\" alt=\"\"/></a></td>");
                body.Append("<td><a href=\"https://twitter.com/NoviMediaNL\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/icon-mail-twitter.png\" width=\"30\" height=\"\" alt=\"\"/></a></td>");
                body.Append("</tr>");
                body.Append("</table>");
                body.Append("</td>");
                body.Append("<td>");
                body.Append("<a href=\"https://www.novimedia.net/diensten/software-ontwikkeling/\" target=\"_blank\"><img src=\"https://www.novimedia.net/uploads/fusion-media-logo-black.png\" width=\"190\" height=\"80\" alt=\"\"/></a>");
                body.Append("</td>");
                body.Append("</tr>");
                body.Append("</tr>");
                body.Append("</table>");
                body.Append("<br />");
                await _emailSender.SendEmailAsync(model.Email, "Reset password", body.ToString());
            }
        }

        public bool ValidateResetGuid(Guid guid, int uid)
        {
            int mainSettingsResetUrlTimeoutInHours = _mainSettings.ResetUrlTimeoutInHours;

            Employee employee = _employeeRepository.Get(x => x.Id == uid &&
                       x.ResetKey == guid.ToString() &&
                       x.UpdatedOnUtc >= DateTime.UtcNow.AddHours(-mainSettingsResetUrlTimeoutInHours)
            );

            return employee != null;
        }

        public async Task UpdateEmployeeAsync(Employee user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            await _employeeRepository.UpdateAsync(user);
        }

        private IEnumerable<Claim> GetEmployeeClaims(Employee employee)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, employee.Id.ToString()),
                new Claim(ClaimTypes.Name, employee.GetFullname)
            };

            return claims;
        }
    }
}