using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public static class PlanningMapper
    {
        public static async Task<Planning> PrepareHistory(Planning planning, IPlanningService planningService)
        {
            var items = planning.AssetActivity.AssetActivityFields.Where(x => x.AssetActivityId == planning.AssetActivityId).OrderBy(x => x.SortOrder).ThenBy(x => x.Id);

            var normsHours = (planning.EndDate - planning.StartDate).Hours;

            var dynamicFields = planning.Asset.AssetDynamicFields.OrderBy(x => x.SortOrder).ThenBy(x => x.Id).ToArray();
            for (var i = 0; i < dynamicFields.Length; i++)
            {
                var item = dynamicFields[i];
                if (planning.AssetActivityHistories.Any(x => x.ItemId == item.Id && x.Kind == (int)FieldKind.DynamicField)) continue;

                var history = new AssetActivityHistory
                {
                    ItemId = item.Id,
                    AssetId = item.AssetId,
                    CreatedOnUtc = DateTime.UtcNow,
                    Kind = (int)FieldKind.DynamicField,
                    DynamicFieldType = DynamicFieldType.Text,
                    PlanningId = planning.Id,
                    SortOrder = i,
                    Name = item.Name,
                    Status = (int)PlanningStatus.PlanningActive,
                    NormHours = normsHours,
                    Value = item.Value
                };
                planning.AssetActivityHistories.Add(history);
            }

            foreach (var item in items)
            {
                if (planning.AssetActivityHistories.Any(x => x.ItemId == item.Id && x.Kind == (int)FieldKind.Field)) continue;
                var history = new AssetActivityHistory
                {
                    ItemId = item.Id,
                    AssetActivityId = item.AssetActivityId,
                    ActivitySortOrder = item.SortOrder,
                    AssetId = planning.AssetId.GetValueOrDefault(),
                    CreatedOnUtc = DateTime.UtcNow,
                    Kind = (int)FieldKind.Field,
                    DynamicFieldType = item.DynamicFieldType,
                    PlanningId = planning.Id,
                    SortOrder = item.SortOrder,
                    Name = item.Name,
                    Status = (int)PlanningStatus.PlanningActive,
                    NormHours = normsHours,
                    Value = item.LatestValue
                };

                switch (item.DynamicFieldType)
                {
                    case DynamicFieldType.Hv:
                        history.Value = $";{history.Value}";
                        break;
                    case DynamicFieldType.HvExplain:
                        {
                            var af =
                                AssetFieldValueHelper.GetAssetFieldHuidigeEnVorigeWaardeEnToelichting(
                                    history.Value);
                            history.Value = AssetFieldValueHelper.SetHuidigeEnVorigeWaardeEnToelichting("",
                                af.Huidige.ToString(), "");
                            break;
                        }

                    case DynamicFieldType.AnalyseSk:
                        {
                            var af = AssetFieldValueHelper.GetAnalyseSk(history.Value);
                            history.Value = AssetFieldValueHelper.SetAnalyseSk("", "", "", af.MinWaarde.ToString(),
                                af.MaxWaarde.ToString());
                            break;
                        }

                    case DynamicFieldType.AnalyseKt:
                        {
                            var af = AssetFieldValueHelper.GetAnalyseKt(history.Value);
                            history.Value = AssetFieldValueHelper.SetAnalyseKt("", "", af.MinWaarde.ToString(),
                                af.MaxWaarde.ToString());
                            break;
                        }

                    case DynamicFieldType.FiltersVSnaren:
                        history.Value = "";
                        break;

                    case DynamicFieldType.FiltersVSnarenWV:
                        {
                            var af = AssetFieldValueHelper.GetFiltersVSnaren(history.Value);
                            history.Value = AssetFieldValueHelper.SetFiltersVSnaren(af.Fabrikant, af.Aantal, af.Type, af.LxBxH, af.Toelichting);
                            break;
                        }
                }
                planning.AssetActivityHistories.Add(history);
            }

            await planningService.UpdatePlanningAsync(planning);

            return await planningService.GetPlanningAsync(planning.Id);
        }

        public static void ProcessSaveModel(PlanningSaveModel model, Planning planning,
            IPlanningService planningService)
        {
            var history = planning.Asset.AssetActivityHistories.Where(
                   x => x.PlanningId == planning.Id);
            if (!string.IsNullOrEmpty(model.Signature))
            {
                var data = Convert.FromBase64String(model.Signature);
                planning.Signature = data;
            }
            if (!string.IsNullOrEmpty(model.Remarks))
                planning.MaterialsRemark = model.Remarks;
            if (!string.IsNullOrEmpty(model.Actions))
                planning.Materials = model.Actions;

            planning.ExtraEmail = model.ExtraEmail;
            planning.ExtraEmailInformation = model.ExtraEmailInformation;

            var deser = JsonConvert.DeserializeObject<Dictionary<string, object>>(model.Dictionary);
            foreach (var field in history)
            {
                if (!string.IsNullOrEmpty(model.NormHours)) field.NormHours = Convert.ToInt32(model.NormHours);
                if (!string.IsNullOrEmpty(model.RealHours)) field.RealHours = Convert.ToInt32(model.RealHours);
                field.OkayGivenBy = model.OkayGivenBy;
                if (!string.IsNullOrEmpty(model.RealMinutes)) field.RealMinutes = Convert.ToInt32(model.RealMinutes);
                if (!string.IsNullOrEmpty(model.TravelHours)) field.TravelHours = Convert.ToInt32(model.TravelHours);
                if (!string.IsNullOrEmpty(model.TravelMinutes)) field.TravelMinutes = Convert.ToInt32(model.TravelMinutes);

                var keys = deser.Keys.Where(x => x.StartsWith("Model_" + field.Id + "_"));

                switch (field.DynamicFieldType)
                {
                    case DynamicFieldType.Image:
                        var imageKey = $"file_data-image-description-{field.Id}";
                        if (deser.ContainsKey(imageKey))
                        {
                            field.AssetFile.Description = deser[imageKey].ToString();
                        }
                        break;
                    case DynamicFieldType.HvExplain:
                        if (
                            deser.ContainsKey($"Model_{field.Id}_huidige") &&
                            deser.ContainsKey($"Model_{field.Id}_vorige") &&
                            deser.ContainsKey($"Model_{field.Id}_toelichting")
                        )
                        {
                            field.Value = AssetFieldValueHelper.SetHuidigeEnVorigeWaardeEnToelichting(
                                deser[$"Model_{field.Id}_huidige"].ToString(),
                                deser[$"Model_{field.Id}_vorige"].ToString(),
                                deser[$"Model_{field.Id}_toelichting"].ToString());
                        }

                        break;
                    case DynamicFieldType.Hv:
                        if (
                            deser.ContainsKey($"Model_{field.Id}_huidige") &&
                            deser.ContainsKey($"Model_{field.Id}_vorige0") &&
                            deser.ContainsKey($"Model_{field.Id}_vorige1") &&
                            deser.ContainsKey($"Model_{field.Id}_vorige2") &&
                            deser.ContainsKey($"Model_{field.Id}_vorige3") &&
                            deser.ContainsKey($"Model_{field.Id}_vorige4")
                            )
                        {
                            field.Value =
                                AssetFieldValueHelper.SetHuidigeEnVorigeWaarden(
                                    deser[$"Model_{field.Id}_huidige"]?.ToString(),
                                    deser[$"Model_{field.Id}_vorige0"]?.ToString(),
                                    deser[$"Model_{field.Id}_vorige1"]?.ToString(),
                                    deser[$"Model_{field.Id}_vorige2"]?.ToString(),
                                    deser[$"Model_{field.Id}_vorige3"]?.ToString(),
                                    deser[$"Model_{field.Id}_vorige4"]?.ToString());
                        }

                        break;
                    case DynamicFieldType.AnalyseSk:
                        if (
                            deser.ContainsKey($"Model_{field.Id}_huidige") &&
                            deser.ContainsKey($"Model_{field.Id}_ontgasser") &&
                            deser.ContainsKey($"Model_{field.Id}_suppletie") &&
                            deser.ContainsKey($"Model_{field.Id}_min") &&
                            deser.ContainsKey($"Model_{field.Id}_max")
                            )
                        {

                            field.Value = AssetFieldValueHelper.SetAnalyseSk(
                                deser[$"Model_{field.Id}_huidige"].ToString(),
                                deser[$"Model_{field.Id}_ontgasser"].ToString(),
                                deser[$"Model_{field.Id}_suppletie"].ToString(),
                                deser[$"Model_{field.Id}_min"].ToString(),
                                deser[$"Model_{field.Id}_max"].ToString());
                        }

                        break;
                    case DynamicFieldType.AnalyseKt:
                        if (
                            deser.ContainsKey($"Model_{field.Id}_huidige") &&
                            deser.ContainsKey($"Model_{field.Id}_suppletie") &&
                            deser.ContainsKey($"Model_{field.Id}_min") &&
                            deser.ContainsKey($"Model_{field.Id}_max")
                        )
                        {
                            field.Value =
                                AssetFieldValueHelper.SetAnalyseKt(
                                    deser[$"Model_{field.Id}_huidige"].ToString(),
                                    deser[$"Model_{field.Id}_suppletie"].ToString(),
                                    deser[$"Model_{field.Id}_min"].ToString(),
                                    deser[$"Model_{field.Id}_max"].ToString());
                        }

                        break;
                    case DynamicFieldType.FiltersVSnaren:
                    case DynamicFieldType.FiltersVSnarenWV:
                        if (
                            deser.ContainsKey($"Model_{field.Id}_1") &&
                            deser.ContainsKey($"Model_{field.Id}_2") &&
                            deser.ContainsKey($"Model_{field.Id}_3") &&
                            deser.ContainsKey($"Model_{field.Id}_4") &&
                            deser.ContainsKey($"Model_{field.Id}_5")
                        )
                        {
                            field.Value = AssetFieldValueHelper.SetFiltersVSnaren(
                                deser[$"Model_{field.Id}_1"].ToString(),
                                deser[$"Model_{field.Id}_2"].ToString(),
                                deser[$"Model_{field.Id}_3"].ToString(),
                                deser[$"Model_{field.Id}_4"].ToString(),
                                deser[$"Model_{field.Id}_5"].ToString());
                        }

                        break;
                    default:
                        {
                            var dic = new Dictionary<int, string>();

                            foreach (var key in keys)
                            {
                                var parts = key.Split('_');
                                if (parts.Length == 2)
                                {
                                    int number = Convert.ToInt16(parts[2]);
                                    dic.Add(number, deser[key].ToString());

                                }
                                else
                                {
                                    dic.Add(dic.Count, deser[key].ToString());
                                }

                            }

                            switch (dic.Count)
                            {
                                case 0:
                                    field.Value = "";
                                    break;
                                case 1:
                                    field.Value = dic[0];
                                    break;
                                default:
                                    var value = "";
                                    for (var i = dic.Keys.Min(); i <= dic.Keys.Max(); i++)
                                    {
                                        if (dic.ContainsKey(i))
                                        {
                                            value += dic[i];
                                        }
                                        value += ";";
                                    }
                                    field.Value = value;
                                    break;
                            }

                            break;
                        }
                }
            }

            if (!string.IsNullOrEmpty(model.Status))
            {
                planning.Status = (PlanningStatus)Convert.ToInt32(model.Status);
            }

            if (model.Status == ((int)PlanningStatus.TechnicallyReady).ToString())
            {
                planning.CheckListEnd = DateTime.Now;
                if (!string.IsNullOrEmpty(planning.Materials))
                {
                    planningService.SendActionsByMail(planning);
                }
            }
            else
            {
                planning.CheckListPauze = DateTime.Now;
            }
        }
    }
}
