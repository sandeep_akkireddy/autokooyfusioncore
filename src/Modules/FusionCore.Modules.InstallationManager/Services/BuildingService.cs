﻿using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class BuildingService : BaseService, IBuildingService
    {
        private readonly IGenericRepository<BuildingLocation> _buildingLocationRepository;
        private readonly IGenericRepository<Building> _buildingRepository;
        private readonly IGenericRepository<Asset> _assetRepository;
        private readonly IMapper _mapper;

        public BuildingService(
            IGenericRepository<Building> buildingRepository,
            IGenericRepository<BuildingLocation> buildingLocationRepository,
            IMapper mapper, IGenericRepository<Asset> assetRepository)
        {
            _buildingRepository = buildingRepository;
            _buildingLocationRepository = buildingLocationRepository;
            _mapper = mapper;
            _assetRepository = assetRepository;
        }


        public IPagedList<Building> GetAllBuildings<TKey>(Expression<Func<Building, bool>> predicate = null,
            Expression<Func<Building, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue, bool loadLocations = false)
        {
            IQueryable<Building> queryable = _buildingRepository.GetAll(predicate, includes: x => x.Client);
            if (loadLocations) queryable = queryable.Include(x => x.BuildingLocations);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Building>(queryable, pageIndex, pageSize);
        }

        public IPagedList<BuildingLocation> GetAllBuildingLocations<TKey>(int buildingId,
            Expression<Func<BuildingLocation, bool>> predicate = null, Expression<Func<BuildingLocation, TKey>> orderBy = null,
            bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<BuildingLocation> queryable = _buildingLocationRepository.GetAll(x => x.BuildingId == buildingId);
            if (predicate != null) queryable = queryable.Where(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<BuildingLocation>(queryable, pageIndex, pageSize);
        }

        public Building GetBuilding(int id)
        {
            return _buildingRepository.Get(x => x.Id == id);
        }

        public BuildingLocation GetBuildingLocation(int id)
        {
            return _buildingLocationRepository.Get(x => x.Id == id, x => x.Building);
        }

        public bool PersistBuilding(BuildingPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                Building dbEntity = GetBuilding(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _buildingRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                Building dbEntity = new Building();
                _mapper.Map(model, dbEntity);
                _buildingRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }

        public bool PersistBuildingLocation(BuildingLocationPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                BuildingLocation dbEntity = GetBuildingLocation(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _buildingLocationRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                BuildingLocation dbEntity = new BuildingLocation();
                _mapper.Map(model, dbEntity);
                _buildingLocationRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }

        public async Task DeleteBuilding(int id)
        {
            Building building = await _buildingRepository.GetByIdAsync(id);
            if (building != null) _buildingRepository.Delete(building);
        }

        public async Task DeleteBuildingLocationAsync(int id)
        {
            BuildingLocation buildingLocation = await _buildingLocationRepository.GetByIdAsync(id);
            if (buildingLocation != null)
            {
                bool hasAssets = _assetRepository.GetAll(x => x.BuildingLocationId == buildingLocation.Id).Any();
                if (!hasAssets)
                {
                    _buildingLocationRepository.Delete(buildingLocation);
                }
            }
        }
    }
}