using AutoMapper;
using ClosedXML.Excel;
using FusionCore.Core.Enums;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Data;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using FusionCore.Services.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class PlanningService : BaseService, IPlanningService
    {
        private readonly FusionCoreContext _dbContext;
        private readonly IGenericRepository<Planning> _planningRepository;
        private readonly IGenericRepository<AssetActivityHistory> _assetActivityHistory;
        private readonly IGenericRepository<AssetFile> _assetFileRepository;
        private readonly IAssetFileService _assetFileService;
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;
        private readonly PdfCreator _creator;
        private readonly IViewRenderService _viewRenderService;
        private readonly IFileService _fileService;
        private readonly IBuildingService _buildingService;
        private readonly IAssetService _assetService;
        private readonly IClientService _clientService;
        private readonly IDivisionService _divisionService;
        public InstallationManagerSettings InstallationManagerSettings { get; }

        public PlanningService(
            FusionCoreContext dbContext,
            IGenericRepository<Planning> planningRepository,
            IGenericRepository<AssetFile> assetFileRepository,
            IGenericRepository<AssetActivityHistory> assetActivityHistory,
            IMapper mapper,
            IEmailSender emailSender,
            IOptions<InstallationManagerSettings> optionsAccessor,
            PdfCreator creator,
            IViewRenderService viewRenderService,
            IFileService fileService,
            IBuildingService buildingService,
            IAssetService assetService,
            IClientService clientService,
            IDivisionService divisionService,
            IAssetFileService assetFileService)
        {
            _dbContext = dbContext;
            _planningRepository = planningRepository;
            _assetActivityHistory = assetActivityHistory;
            _mapper = mapper;
            _emailSender = emailSender;
            _creator = creator;
            _viewRenderService = viewRenderService;
            _fileService = fileService;
            _buildingService = buildingService;
            _assetService = assetService;
            _clientService = clientService;
            _divisionService = divisionService;
            _assetFileService = assetFileService;
            _assetFileRepository = assetFileRepository;
            InstallationManagerSettings = optionsAccessor.Value;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public IPagedList<Planning> GetAllPlannings<TKey>(Expression<Func<Planning, bool>> predicate = null,
            Expression<Func<Planning, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue, bool loadIncludes = false, bool loadOverviewIncludes = false)
        {
            Expression<Func<Planning, object>>[] includes = new Expression<Func<Planning, object>>[0];
            if (loadIncludes)
            {
                includes = new Expression<Func<Planning, object>>[]
                {
                    x => x.Building,
                    x => x.BuildingLocation,
                    x => x.AssetActivity,
                    x => x.AssetActivity.AssetActivityFields,
                    x => x.Asset,
                    x => x.Asset.AssetActivityHistories,
                    x => x.Division,
                    x => x.Client,
                    x => x.MainEmployee,
                    x => x.PlanningEmployees,
                    x => x.AssetActivityHistories
                };
            }
            if (loadOverviewIncludes)
            {
                includes = new Expression<Func<Planning, object>>[]
                {
                    x => x.Building,
                    x => x.AssetActivity,
                    x => x.Client,
                    x => x.Asset,
                    x => x.AssetActivity,
                    x => x.MainEmployee,
                    x => x.PlanningEmployees,
                };
            }

            IQueryable<Planning> queryable = _planningRepository.GetAll(predicate, true, includes);
            queryable = queryable.Where(x => x.StatusId != (int)PlanningStatus.Deleted);

            if (orderBy != null)
            {
                queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            }

            return new PagedList<Planning>(queryable, pageIndex, pageSize);
        }

        public Planning GetPlanning(int id)
        {
            IQueryable<Planning> plannings = _planningRepository.GetAll();
            plannings = plannings
                .Include(x => x.Building)
                .Include(x => x.BuildingLocation)
                .Include(x => x.AssetActivity)
                .ThenInclude(y => y.AssetActivityFields)
                .Include(x => x.Asset)
                .ThenInclude(y => y.AssetActivityHistories)
                .Include(x => x.Asset)
                .ThenInclude(y => y.AssetDynamicFields)
                .Include(x => x.Division)
                .Include(x => x.Client)
                .Include(x => x.MainEmployee)
                .Include(x => x.PlanningEmployees)
                .ThenInclude(x => x.Employee)
                .Include(x => x.AssetActivityHistories)
                .ThenInclude(y => y.AssetFile)
                .Include(x => x.FusionUser);
            return plannings.FirstOrDefault(x => x.Id == id);
        }

        public async Task<Planning> GetBasePlanningAsync(int id)
        {
            return await _planningRepository.GetByIdAsync(id);
        }

        public async Task<Planning> GetPlanningAsync(int id)
        {
            IQueryable<Planning> plannings = _planningRepository.GetAll();
            plannings = plannings
                .Include(x => x.Building)
                .Include(x => x.BuildingLocation)
                .Include(x => x.AssetActivity)
                .ThenInclude(y => y.AssetActivityFields)
                .Include(x => x.Asset)
                .ThenInclude(y => y.AssetActivityHistories)
                .Include(x => x.Asset)
                .ThenInclude(y => y.AssetDynamicFields)
                .Include(x => x.Division)
                .Include(x => x.Client)
                .Include(x => x.MainEmployee)
                .Include(x => x.PlanningEmployees)
                .ThenInclude(x => x.Employee)
                .Include(x => x.AssetActivityHistories)
                .ThenInclude(y => y.AssetFile)
                .Include(x => x.FusionUser);
            return await plannings.FirstOrDefaultAsync(x => x.Id == id);
        }

        public bool PersistPlanning(PlanningPersistModel model, User currentUser)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (!model.BuildingId.HasValue && model.BuildingLocationId.HasValue)
            {
                BuildingLocation location = _buildingService.GetBuildingLocation(model.BuildingLocationId.Value);
                model.BuildingId = location.BuildingId;
            }

            if (model.BuildingId.HasValue && !model.Id.HasValue)
            {
                model.OfferNumber = GetNewOfferNumber(model.BuildingId.GetValueOrDefault(), model.OfferNumber);
            }
            model.PlanningEmployees?.RemoveAll(x => !x.EmployeeId.HasValue);

            if (model.Id.HasValue)
            {
                //Edit
                Planning dbEntity = GetPlanning(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _planningRepository.Update(dbEntity);
                    return true;
                }
            }
            else
            {
                //insert
                Planning dbEntity = new Planning();
                _mapper.Map(model, dbEntity);
                dbEntity.UserId = currentUser.Id;
                _planningRepository.Insert(dbEntity);

                ProcessRepeatable(dbEntity, currentUser);

                return true;
            }

            return false;
        }

        public async Task DeletePlanning(int id)
        {
            Planning asset = await _planningRepository.GetByIdAsync(id);
            if (asset != null)
            {
                _planningRepository.Delete(asset);
            }
        }

        public async Task UpdatePlanningAsync(Planning planning)
        {
            await _planningRepository.UpdateAsync(planning);
        }

        public async Task InsertHistoryAsync(AssetActivityHistory history)
        {
            await _assetActivityHistory.InsertAsync(history);
        }

        public async Task SendActionsByMail(Planning planning)
        {
            if (string.IsNullOrWhiteSpace(planning.Materials)) return;
            User planner = planning.FusionUser;
            if (planner == null) throw new ArgumentNullException(nameof(planner));

            Division division = planning.Client.ClientDivisions.FirstOrDefault()?.Division;
            if (division == null) throw new ArgumentNullException(nameof(division));

            string body = division.EmailTemplatePlannerBody ?? "";
            if (!string.IsNullOrEmpty(body))
            {
                string plannerFullName = planner.GetFullName;
                string naamMonteur = planning.MainEmployee.GetFullname;

                body = body.Replace("[NAAM PLANNER]", plannerFullName);
                body = body.Replace("[ACTIES]", planning.Materials);
                body = body.Replace("[CONTROLE]", planning.AssetActivity?.Name);
                body = body.Replace("[DATUM]", planning.StartDate.ToString("dd/MM/yyyy"));
                body = body.Replace("[MONTEUR]", naamMonteur);
                body = body.Replace("[EMAIL MONTEUR]", planning.MainEmployee.Email);

                await _emailSender.SendEmailAsync(naamMonteur, division.EmailTemplatePlannerEmail, plannerFullName,
                    planner.Email, "Acties " + planning.Id + " " + planning.Asset.Name + " ", body);
            }
        }

        public async Task<string> GenerateRapportageHtml(Planning planning)
        {
            if (planning == null)
            {
                return null;
            }

            string viewType = !string.IsNullOrEmpty(planning.ErrorMessage) ? "Incorrect" : "Correct";
            Building building = _buildingService.GetBuilding(planning.BuildingId.GetValueOrDefault());
            int clientId = planning.ClientId ?? building.ClientId;
            Client client = _clientService.GetClient(clientId);
            Division division = _divisionService.GetDivision(planning.DivisionId.GetValueOrDefault());
            ReportPlanningModel model = new ReportPlanningModel
            {
                Planning = planning,
                Client = client,
                ClientLogo = _fileService.GetFile(division.LogoHeaderPictureId.GetValueOrDefault())?.FileBinary
            };
            return await _viewRenderService.RenderToStringAsync(
                    $"~/Areas/InstallationManager/Views/Reports/{viewType}.cshtml", model);
        }

        public async Task<byte[]> GenerateRapportage(Planning planning, bool isTemp = false, bool sendMail = true,
            bool updateIaf = false)
        {
            if (planning == null)
            {
                return null;
            }

            string viewType = !string.IsNullOrEmpty(planning.ErrorMessage) ? "Incorrect" : "Correct";
            Building building = _buildingService.GetBuilding(planning.BuildingId.GetValueOrDefault());
            int clientId = planning.ClientId ?? building.ClientId;
            Client client = _clientService.GetClient(clientId);
            Division division = _divisionService.GetDivision(planning.DivisionId.GetValueOrDefault());
            ReportPlanningModel model = new ReportPlanningModel
            {
                Planning = planning,
                Client = client,
                ClientLogo = _fileService.GetFile(division.LogoHeaderPictureId.GetValueOrDefault())?.FileBinary
            };
            _creator.Html = await _viewRenderService.RenderToStringAsync($"~/Areas/InstallationManager/Views/Reports/{viewType}.cshtml", model);
            byte[] file = _creator.CreatePdfFromHtml();

            if (isTemp)
            {
                return file;
            }

            if (updateIaf)
            {
                AssetFile iafToUpdate = planning.Asset?.AssetFiles.FirstOrDefault(x => x.FileTypeId == (int)FileType.Report);
                if (iafToUpdate != null)
                {
                    AssetFileBinary assetFileBytes = await _assetFileService.GetBytesByAssetFileIdAsync(iafToUpdate.Id);
                    if (assetFileBytes != null)
                    {
                        assetFileBytes.File = file;
                    }

                    await _dbContext.SaveChangesAsync();
                    return file;
                }
            }

            AssetFile iaf = new AssetFile
            {
                AssetId = planning.AssetId.GetValueOrDefault(),
                FileType = FileType.Report,
                Description = "Report " + planning.Id,
                Title = planning.OfferNumber,
                SortOrder = 0,
                PlanningId = planning.Id,
            };
            await _assetFileRepository.InsertAsync(iaf);

            AssetFileBinary assetFileBinary = new AssetFileBinary
            {
                AssetFileId = iaf.Id,
                File = file
            };
            await _assetFileService.InsertAssetFileBinaryAsync(assetFileBinary);

            if (sendMail)
            {
                await SendRapportageByMail(planning, assetFileBinary.File);
            }
            return file;
        }

        public async Task SendRapportageByMail(Planning planning, byte[] file)
        {
            string subject = "Rapportage " + planning.OfferNumber + " - " + planning.AssetActivity.Name;
            Client client = _clientService.GetClient(planning.ClientId.GetValueOrDefault());
            int divisionId = planning.DivisionId.GetValueOrDefault();
            Division division = _divisionService.GetDivision(divisionId);

            string body = division?.EmailTemplateCustomerBody ?? "";
            body = body.Replace("[NAAM KLANT]", client.GetContact);
            body = body.Replace("[CONTROLE]", planning.AssetActivity?.Name);
            body = body.Replace("[DATUM]", planning.StartDate.ToString("dd/MM/yyyy"));
            body = body.Replace("[STATUS]", subject);
            body = body.Replace("[PLANNING]", planning.MaterialsRemark ?? "");
            body = body.Replace("[TOEVOEGING]", planning.ExtraEmailInformation ?? "");
            List<string> ccList = new List<string>();
            if (!string.IsNullOrEmpty(planning.ExtraEmail))
            {
                string cc = planning.ExtraEmail.Replace(",", ";").Replace(" ", ";");
                foreach (string email in cc.Split(';'))
                {
                    if (!string.IsNullOrEmpty(email))
                    {
                        ccList.Add(email.Trim());
                    }
                }
            }

            Dictionary<string, byte[]> attachments = new Dictionary<string, byte[]>
            {
                { $"rapportage_{planning.Id}.pdf", file }
            };
            List<AssetActivityHistory> activityHistoriesWithFile = planning.AssetActivityHistories.Where(x => x.AssetFileId.HasValue && x.DynamicFieldType == DynamicFieldType.File).ToList();
            if (activityHistoriesWithFile.Any())
            {
                foreach (AssetActivityHistory history in activityHistoriesWithFile)
                {
                    AssetFile assetFile = await _assetFileService.GetAssetFileByIdAsync(history.AssetFileId.Value);
                    AssetFileBinary assetFileBytes = await _assetFileService.GetBytesByAssetFileIdAsync(history.AssetFileId.Value);
                    if (assetFile != null && assetFileBytes != null)
                    {
                        attachments.Add(assetFile.Title, assetFileBytes.File);
                    }
                }
            }

            await _emailSender.SendEmailAsync(
                division?.EmailTemplateCustomerEmail,
                division?.EmailTemplateCustomerEmail,
                planning.Building.Name,
                planning.Building.Email,
                subject,
                body,
                ccList,
                null,
                attachments
            );
        }

        public void DeleteHistory(AssetActivityHistory history)
        {
            _assetActivityHistory.Delete(history);
        }

        public void SaveLatestValues(Planning planning)
        {
            List<AssetActivityHistory> histories =
                planning.AssetActivityHistories.Where(
                    x => x.PlanningId == planning.Id && (x.AssetActivityId == planning.AssetActivityId || !x.AssetActivityId.HasValue)).ToList();
            List<AssetField> fields = planning.Asset.AssetFields.Where(x => x.ActivityId == planning.AssetActivityId).ToList();
            foreach (AssetField field in fields)
            {
                AssetActivityHistory history = histories.FirstOrDefault(x => x.DynamicFieldType == field.DynamicFieldType
                                                            && x.Name == field.Name);
                if (history != null)
                {
                    field.LatestValue = history.Value;
                }
            }
            SaveChanges();
        }

        public IList<Planning> GetPlanningByAssetActivityId(int activityId)
        {
            return _planningRepository.GetAll(x => x.AssetActivityId == activityId).ToList();
        }

        public string GetNewOfferNumber(int buildingId, string offerNumber)
        {
            int numberOfPlanningsForBuilding = _planningRepository.GetAll(ip => ip.BuildingId == buildingId).Count();
            numberOfPlanningsForBuilding++;

            return $"{offerNumber}-{numberOfPlanningsForBuilding:D5}";
        }

        public async Task<int?> SaveFileToHistory(int? imgId, int? planningId, AssetFile assetFile, DynamicFieldType fieldType)
        {
            if (imgId.HasValue)
            {
                AssetActivityHistory history = _assetService.GetAssetActivityHistoryById(imgId.Value);

                history.AssetFileId = assetFile.Id;
                _assetService.SaveChanges();
                history.AssetFile = assetFile;
                return imgId.Value;
            }
            else
            {
                Planning planning = await GetPlanningAsync(planningId.GetValueOrDefault());

                AssetActivityHistory firstItem = planning.AssetActivityHistories.FirstOrDefault();

                if (firstItem != null)
                {
                    AssetActivityHistory model = new AssetActivityHistory
                    {
                        AssetActivityId = firstItem.AssetActivityId,
                        ActivitySortOrder = firstItem.ActivitySortOrder,
                        AssetId = firstItem.AssetId,
                        Kind = (int)FieldKind.Field,
                        DynamicFieldType = fieldType,
                        PlanningId = firstItem.PlanningId,
                        SortOrder = 0,
                        Name = "",
                        Status = firstItem.Status,
                        NormHours = firstItem.NormHours,
                        AssetFileId = assetFile.Id,
                        CreatedOnUtc = DateTime.UtcNow,
                        UpdatedOnUtc = DateTime.UtcNow
                    };

                    planning.AssetActivityHistories.Add(model);
                    SaveChanges();
                    model.AssetFile = assetFile;
                    return model.Id;
                }

                return null;
            }
        }

        public byte[] GetExcelExport(List<Planning> plannings, DateTime start, DateTime end, int weeknumber)
        {
            CultureInfo cultureInfo = new CultureInfo("nl-NL", false);
            XLWorkbook workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("planningen");
            worksheet.ColumnWidth = 20;

            int rowNumber = BuildHeader(start, end, weeknumber, worksheet, cultureInfo, 0);

            void BuildRowHeader(int row, string value, double size)
            {
                worksheet.Cell("A" + row).Value = value;
                worksheet.Cell("A" + row).Style.Font.FontSize = size;
                worksheet.Cell("A" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("A" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("A" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("A" + row).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("B" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("B" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("B" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("C" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("C" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("C" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("D" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("D" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("D" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("E" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("E" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("E" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("F" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("F" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("F" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("G" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("G" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("G" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("H" + row).Style.Fill.BackgroundColor = XLColor.FromArgb(208, 206, 206);
                worksheet.Cell("H" + row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("H" + row).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell("H" + row).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            }

            void BuildRow(string field, string value)
            {
                worksheet.Cell(field).Value = value;
                worksheet.Cell(field).Style.Font.FontSize = 11;
                worksheet.Cell(field).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(field).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(field).Style.Border.RightBorder = XLBorderStyleValues.Thin;

            }

            BuildRowHeader(rowNumber, $"Week {weeknumber}", 14);
            rowNumber++;
            for (int i = 0; i < 7; i++)
            {
                DateTime currentDate = start.Date.AddDays(i);
                List<Planning> items = plannings.Where(x => x.StartDate.Date == currentDate.Date).ToList();
                if (!items.Any()) continue;

                BuildRowHeader(rowNumber, $"{currentDate.ToString(cultureInfo.DateTimeFormat.LongDatePattern)}", 12);
                rowNumber++;

                foreach (Planning item in items)
                {
                    BuildRow($"A{rowNumber}", item.MainEmployee?.GetFullname);
                    BuildRow($"B{rowNumber}", item.StartDate.ToString("HH:mm") + " - " + item.EndDate.ToString("HH:mm"));
                    BuildRow($"C{rowNumber}", item.OfferNumber);
                    BuildRow($"D{rowNumber}", item.Client?.Name);
                    BuildRow($"E{rowNumber}", item.Building?.City);
                    BuildRow($"F{rowNumber}", item.AssetActivity?.Name);

                    string planningTitle = item.OfferNumber;

                    if (item.Building != null)
                    {
                        planningTitle += " " + item.Building.Name;
                    }
                    if (item.Asset != null)
                    {
                        planningTitle += "<br />" + item.Asset.Name;
                    }

                    BuildRow($"G{rowNumber}", planningTitle);
                    BuildRow($"H{rowNumber}", item.Status.GetStringValue());
                    rowNumber++;
                }
                //Empty row
                rowNumber++;
            }

            using (MemoryStream ms = new MemoryStream())
            {
                workbook.SaveAs(ms);
                return ms.ToArray();
            }
        }

        private static int BuildHeader(DateTime start, DateTime end, int weeknumber, IXLWorksheet worksheet,
            CultureInfo cultureInfo, int rowNumber)
        {
            void RenderHeaderField(string field, string value)
            {
                worksheet.Cell(field).Value = value;
                worksheet.Cell(field).Style.Font.FontColor = XLColor.White;
                worksheet.Cell(field).Style.Font.Bold = true;
                worksheet.Cell(field).Style.Font.FontSize = 12;
                worksheet.Cell(field).Style.Fill.BackgroundColor = XLColor.FromArgb(31, 56, 100);
            }

            using (WebClient client = new WebClient())
            {
                byte[] imageData = client.DownloadData("https://login.aqgroup.nl/AQ-group-logo.jpg");

                worksheet.Pictures.Add(new MemoryStream(imageData))
                    .MoveTo(worksheet.Cell("A1"));

            }

            rowNumber += 6;

            worksheet.Cell("D3").Value = $"Planning {start.ToString(cultureInfo.DateTimeFormat.LongDatePattern)} t/m {end.ToString(cultureInfo.DateTimeFormat.LongDatePattern)}";
            worksheet.Cell("D3").Style.Font.Bold = true;
            worksheet.Cell("D3").Style.Font.FontSize = 14;
            rowNumber += 2;
            RenderHeaderField($"A{rowNumber}", "Werknemer");
            RenderHeaderField($"B{rowNumber}", "Datum & Tijd");
            RenderHeaderField($"C{rowNumber}", "Offertenummer");
            RenderHeaderField($"D{rowNumber}", "Bedrijf");
            RenderHeaderField($"E{rowNumber}", "Plaats");
            RenderHeaderField($"F{rowNumber}", "Activiteit");
            RenderHeaderField($"G{rowNumber}", "Omschrijving");
            RenderHeaderField($"H{rowNumber}", "Status");
            rowNumber++;
            return rowNumber;
        }

        private void ProcessRepeatable(Planning model, User currentUser)
        {
            DateTime maxEndDate = new DateTime(2099, 1, 1);
            if (model.Repeatable && model.RepeatableType.HasValue)
            {
                int count = model.RepeatableCount ?? 1;
                if (count >= 12)
                {
                    if (model.RepeatableType.Value != 6)
                    {
                        // Bepaal einddatum
                        maxEndDate = model.EndDate.AddYears(count / 12).AddDays(1);
                        count = 1000;

                    }
                }
                DateTime lastEndDate = DateTime.Now;
                int sequencenumber = 0;
                for (int i = 1; i <= count - 1 && lastEndDate < maxEndDate; i++)
                {
                    sequencenumber++;
                    Planning clone = CloneForRepeat(model, model.OfferNumber, sequencenumber);
                    switch (model.RepeatableType.Value)
                    {
                        case 1: // Wekelijks
                            clone.StartDate = clone.StartDate.AddDays(7 * i);
                            clone.EndDate = clone.EndDate.AddDays(7 * i);
                            break;
                        case 2: // Om de week
                            clone.StartDate = clone.StartDate.AddDays(14 * i);
                            clone.EndDate = clone.EndDate.AddDays(14 * i);
                            break;
                        case 3: // Maandelijks
                            clone.StartDate = clone.StartDate.AddMonths(1 * i);
                            clone.EndDate = clone.EndDate.AddMonths(1 * i);
                            break;
                        case 4: // Om de maand
                            clone.StartDate = clone.StartDate.AddMonths(2 * i);
                            clone.EndDate = clone.EndDate.AddMonths(2 * i);
                            break;
                        case 5: // Per kwartaal
                            clone.StartDate = clone.StartDate.AddDays(13 * 7 * i);
                            clone.EndDate = clone.EndDate.AddDays(13 * 7 * i);
                            break;
                        case 6: // Per Jaar
                            clone.StartDate = clone.StartDate.AddYears(1 * i);
                            clone.EndDate = clone.EndDate.AddYears(1 * i);
                            break;
                    }

                    clone.RepeatablePlanningParentId = model.Id;
                    clone.UserId = currentUser.Id;
                    lastEndDate = clone.EndDate;
                    _planningRepository.Insert(clone);
                }
            }
        }
        private Planning CloneForRepeat(Planning basePlanning, string offernumber, int sequencenumber)
        {
            Planning clone = new Planning
            {
                AssetId = basePlanning.AssetId,
                AssetActivityId = basePlanning.AssetActivityId,
                BuildingId = basePlanning.BuildingId,
                UpdatedOnUtc = basePlanning.UpdatedOnUtc,
                CreatedOnUtc = basePlanning.CreatedOnUtc,
                MainEmployeeId = basePlanning.MainEmployeeId,
                Repeatable = basePlanning.Repeatable,
                RepeatableCount = basePlanning.RepeatableCount,
                RepeatablePlanningParentId = basePlanning.Id,
                RepeatableType = basePlanning.RepeatableType,
                Status = basePlanning.Status,
                StartDate = basePlanning.StartDate,
                EndDate = basePlanning.EndDate,
                BuildingLocationId = basePlanning.BuildingLocationId,
                ClientId = basePlanning.ClientId,
                DivisionId = basePlanning.DivisionId,
                ErrorMessage = basePlanning.ErrorMessage,
                ExtraEmail = basePlanning.ExtraEmail,
                ExtraEmailInformation = basePlanning.ExtraEmailInformation,
                Materials = basePlanning.Materials,
                MaterialsRemark = basePlanning.MaterialsRemark,
                Remark = basePlanning.Remark,
                StatusId = basePlanning.StatusId,
                OfferNumber = offernumber + "-" + sequencenumber.ToString("D5")
            };


            return clone;
        }
    }
}
