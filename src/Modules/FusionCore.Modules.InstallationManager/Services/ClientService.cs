﻿using AutoMapper;
using FusionCore.Core;
using FusionCore.Core.Helpers;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Services
{
    public class ClientService : BaseService, IClientService
    {
        private readonly IGenericRepository<Client> _clientRepository;
        private readonly IGenericRepository<ClientUser> _clientUserRepository;
        private readonly IGenericRepository<ClientUserRight> _clientUserRightRepository;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClientService(
            IGenericRepository<Client> clientRepository,
            IGenericRepository<ClientUser> clientUserRepository,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            IGenericRepository<ClientUserRight> clientUserRightRepository)
        {
            _clientRepository = clientRepository;
            _clientUserRepository = clientUserRepository;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
            _clientUserRightRepository = clientUserRightRepository;
        }

        public IPagedList<Client> GetAllClients<TKey>(Expression<Func<Client, bool>> predicate = null,
            Expression<Func<Client, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<Client> queryable = _clientRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Client>(queryable, pageIndex, pageSize);
        }

        public IPagedList<ClientUser> GetClientUserByClientId<TKey>(int clientId,
            Expression<Func<ClientUser, bool>> predicate = null, Expression<Func<ClientUser, TKey>> orderBy = null,
            bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            IQueryable<ClientUser> queryable = _clientUserRepository.GetAll(x => x.ClientId == clientId);
            if (predicate != null) queryable = queryable.Where(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<ClientUser>(queryable, pageIndex, pageSize);
        }

        public ClientUser GetClientUser(int id)
        {
            ClientUser clientUser = _clientUserRepository.Table.Include(x => x.Client).FirstOrDefault(x => x.Id == id);
            return clientUser;
        }

        public bool PersistClientUser(ClientUserPersistModel model)
        {
            bool result = false;
            if (model.Id.HasValue)
            {
                //Edit
                ClientUser dbEntity = GetClientUser(model.Id.Value);
                if (dbEntity != null)
                {
                    ClientUser mappedDbEntity = _mapper.Map(model, dbEntity);
                    if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.PasswordControle))
                    {
                        (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
                        mappedDbEntity.Salt = password.salt;
                        mappedDbEntity.Key = password.key;
                    }

                    _clientUserRepository.Update(mappedDbEntity);
                    PersistClientUserRights(model.ClientUserRights, dbEntity.Id);
                    result = true;
                }
            }
            else
            {
                //insert
                ClientUser dbEntity = new ClientUser();
                _mapper.Map(model, dbEntity);

                if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.PasswordControle))
                {
                    (byte[] key, byte[] salt) password = PasswordHelper.GetPasswordHashAndSalt(model.Password);
                    dbEntity.Salt = password.salt;
                    dbEntity.Key = password.key;
                }

                _clientUserRepository.Insert(dbEntity);
                result = true;
            }

            return result;
        }

        private void PersistClientUserRights(List<ClientUserRightModel> modelRights, int clientUserId)
        {
            modelRights = modelRights.Where(x => x.AllBuildings || x.BuildingId.HasValue).ToList();
            List<ClientUserRight> rights = _clientUserRightRepository.Table.Where(x => x.ClientUserId == clientUserId).ToList();
            foreach (ClientUserRight clientUserRight in rights)
            {
                ClientUserRightModel modelRight = modelRights.FirstOrDefault(x => x.Id == clientUserRight.Id);
                if (modelRight != null)
                {
                    ClientUserRight mappedClientUserRight = _mapper.Map(modelRight, clientUserRight);
                    _clientUserRightRepository.Update(mappedClientUserRight);
                }
                else
                {
                    _clientUserRightRepository.Delete(clientUserRight);
                }
            }

            foreach (ClientUserRightModel clientUserRightModel in modelRights.Where(x => !x.Id.HasValue || x.Id == 0))
            {
                ClientUserRight clientUserRight = _mapper.Map<ClientUserRight>(clientUserRightModel);
                clientUserRight.ClientUserId = clientUserId;
                _clientUserRightRepository.Insert(clientUserRight);
            }
        }

        public async Task DeleteClientUser(int id)
        {
            ClientUser clientUser = await _clientUserRepository.GetByIdAsync(id);
            if (clientUser != null) _clientUserRepository.Delete(clientUser);
        }

        public async Task SignOut()
        {
            await _httpContextAccessor.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public ClientUser Validate(string identifier, string password, string loginTypeCode = Constants.DefaultLoginType)
        {
            ClientUser clientUser = _clientUserRepository.Get(x =>
                !string.IsNullOrEmpty(x.Username) &&
                x.Username.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));

            if (clientUser?.Key != null
                && clientUser.Salt != null
                && PasswordHelper.ValidatePasswordHash(clientUser.Key, clientUser.Salt, password))
            {
                return clientUser;
            }

            return null;
        }

        public async Task SignIn(ClientUser clientUser, bool isPersistent = false)
        {
            ClaimsIdentity identity = new ClaimsIdentity(GetClaims(clientUser), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await _httpContextAccessor.HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties { IsPersistent = isPersistent }
            );
        }

        public int GetCurrentUserId()
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;

            if (!httpContext.User.Identity.IsAuthenticated)
                return -1;

            Claim claim = httpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (claim == null)
                return -1;

            if (!int.TryParse(claim.Value, out int currentUserId))
                return -1;

            return currentUserId;
        }

        public ClientUser GetCurrentUser()
        {
            int currentUserId = GetCurrentUserId();
            if (currentUserId == -1) return null;

            ClientUser currentUser = _clientUserRepository.GetById(currentUserId);
            if (currentUser == null || !currentUser.Active) return null;

            return currentUser;
        }

        public async Task DeleteClientUserRight(int id)
        {
            ClientUserRight clientUserRight = await _clientUserRightRepository.GetByIdAsync(id);
            if (clientUserRight != null) _clientUserRightRepository.Delete(clientUserRight);
        }

        private IEnumerable<Claim> GetClaims(ClientUser clientUser)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, clientUser.Id.ToString()),
                new Claim(ClaimTypes.Name, clientUser.GetFullname)
            };

            return claims;
        }

        public Client GetClient(int id)
        {
            return _clientRepository.Get(x => x.Id == id, x => x.ClientDivisions);
        }

        public bool PersistClient(ClientPersistModel model)
        {
            model.ClientDivisions?.RemoveAll(x => !x.DivisionId.HasValue);

            if (model.Id.HasValue)
            {
                //Edit
                Client dbEntity = GetClient(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _clientRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                Client dbEntity = new Client();
                _mapper.Map(model, dbEntity);
                _clientRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }

        public async Task DeleteClient(int id)
        {
            Client client = await _clientRepository.GetByIdAsync(id);
            if (client != null) _clientRepository.Delete(client);
        }

        public List<ClientUserRight> GetClientUserRightsByClientUserId(int clientId)
        {
            return _clientUserRightRepository.GetAll(x => x.ClientUserId == clientId).ToList();
        }
    }
}