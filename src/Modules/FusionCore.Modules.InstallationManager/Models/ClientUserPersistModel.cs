﻿using FusionCore.Models.Common;
using FusionCore.Models.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ClientUserPersistModel : BaseModel
    {
        public ClientUserPersistModel()
        {
            ClientUserRights = new List<ClientUserRightModel>();
            AvailableBuildings = new List<SelectListItem>();
            AvailableLocations = new List<SelectListItem>();
        }
        public int ClientId { get; set; }
        [Required]
        [MaxLength(256)]
        public string Username { get; set; }

        [MaxLength(256)]
        public string Firstname { get; set; }

        [MaxLength(50)]
        public string Middlename { get; set; }

        [MaxLength(256)]
        public string Lastname { get; set; }

        [RequiredInsertValidator]
        public string Password { get; set; }

        [RequiredInsertValidator]
        [Compare("Password", ErrorMessage = "De wachtwoorden zijn niet gelijk.")]
        public string PasswordControle { get; set; }

        public bool Active { get; set; }
        public string ClientName { get; set; }

        public List<ClientUserRightModel> ClientUserRights { get; set; }
        public List<SelectListItem> AvailableBuildings { get; set; }
        public List<SelectListItem> AvailableLocations { get; set; }
    }

    public class ClientUserRightModel
    {
        public int? Id { get; set; }
        public int? BuildingId { get; set; }
        public int? BuildingLocationId { get; set; }

        public bool AllBuildings { get; set; }
        public bool AllLocations { get; set; }
    }
}