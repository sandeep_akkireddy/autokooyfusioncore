﻿using FusionCore.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetActivityPersistModel : BaseModel
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        public int AssetId { get; set; }
    }
}