﻿using System;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ExportObject
    {
        public string Werknemer { get; set; }
        public DateTime Datum { get; set; }
        public string Projectnummer { get; set; }
        public string Bedrijf { get; set; }
        public string Locatie { get; set; }
        public string Installatie { get; set; }
        public string Checklist { get; set; }
        public string Starttijd { get; set; }
        public string Eindtijd { get; set; }
        public string Normtijd { get; set; }
        public string Checkliststart { get; set; }
        public string Checklistend { get; set; }
        public string Tijd { get; set; }
    }
}
