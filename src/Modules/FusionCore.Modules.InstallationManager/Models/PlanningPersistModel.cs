﻿using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class PlanningPersistModel : BaseModel, IDatePersistModel
    {
        public PlanningPersistModel()
        {
            PlanningEmployees = new List<PlanningEmployeeModel>();
            AvailableStatusses = new List<SelectListItem>();
            AvailablePlanningEmployees = new List<SelectListItem>();
            AvailableDivisions = new List<SelectListItem>();
            AvailableClients = new List<SelectListItem>();
            AvailableBuildingLocations = new List<SelectListItem>();
            AvailableAssets = new List<SelectListItem>();
            AvailableAssetActivities = new List<SelectListItem>();
        }

        public int? AssetId { get; set; }
        public int? AssetActivityId { get; set; }
        public int? DivisionId { get; set; }
        public int? ClientId { get; set; }
        public int? BuildingId { get; set; }
        public int? BuildingLocationId { get; set; }
        public int? MainEmployeeId { get; set; }


        public bool Repeatable { get; set; }
        public int? RepeatableType { get; set; }
        public int? RepeatableCount { get; set; }
        public int? RepeatablePlanningParentId { get; set; }

        public int StatusId { get; set; }
        [Required]
        public string StartTimeString { get; set; }
        [Required]
        public string EndTimeString { get; set; }
        [Required]
        public string DateString { get; set; }

        public bool WholeDay { get; set; }
        public string Remark { get; set; }
        public string CustomColor { get; set; }

        public bool CustomColorToggle { get; set; }

        public string CustomColorAssistant
        {
            get
            {
                if (!string.IsNullOrEmpty(CustomColor))
                {
                    string[] split = CustomColor.Replace("rgb(", "").Replace(")", "").Split(',');
                    if (split.Length == 3)
                    {
                        for (int index = 0; index < split.Length; index++)
                        {
                            int value = Convert.ToInt32(split[index].Trim());
                            if (index < 2) value += 30;
                            if (value > 255) value = 255;
                            split[index] = value.ToString();
                        }

                        return $"{split[0]},{split[1]},{split[2]}";
                    }
                }
                return "";
            }
        }
        [Required, StringLength(256)]
        public string OfferNumber { get; set; }

        public List<PlanningEmployeeModel> PlanningEmployees { get; set; }
        public IList<SelectListItem> AvailableStatusses { get; set; }
        public List<SelectListItem> AvailablePlanningEmployees { get; set; }
        public List<SelectListItem> AvailableDivisions { get; set; }
        public List<SelectListItem> AvailableClients { get; set; }
        public List<SelectListItem> AvailableBuildingLocations { get; set; }
        public List<SelectListItem> AvailableAssets { get; set; }
        public List<SelectListItem> AvailableAssetActivities { get; set; }

        public class PlanningEmployeeModel
        {
            public int? EmployeeId { get; set; }
        }
    }
}
