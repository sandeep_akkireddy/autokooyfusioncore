﻿using System;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class CalendarRenderItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string ClassName { get; set; }
        public bool AllDay { get; set; }
    }
}
