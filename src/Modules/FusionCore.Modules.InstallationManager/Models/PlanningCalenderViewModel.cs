﻿using System.Diagnostics.CodeAnalysis;

namespace FusionCore.Modules.InstallationManager.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class PlanningCalenderViewModel
    {
        public string id { get; set; }
        public int[] resourceIds { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public bool allDay { get; set; }
        public string className { get; set; }
        public string color { get; set; }
        public bool editable { get; set; }
        public string extraInfo { get; set; }
        public string textColor { get; set; }
    }
}
