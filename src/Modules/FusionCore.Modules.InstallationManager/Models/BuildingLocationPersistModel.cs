﻿using FusionCore.Models.Common;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class BuildingLocationPersistModel : BaseModel
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        public string Remarks { get; set; }

        [MaxLength(256)]
        public string Floor { get; set; }

        [MaxLength(256)]
        public string RoomNumber { get; set; }

        public int BuildingId { get; set; }

        public string BuildingName { get; set; }
        public List<SelectListItem> UnassignedAssets { get; set; }
    }
}