﻿using FusionCore.Models.Common;
using FusionCore.Models.Validation;
using FusionCore.Modules.InstallationManager.Data.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class EmployeePersistModel : BaseModel
    {
        public EmployeePersistModel()
        {
            Employee2EmployeeGroups = new List<Employee2EmployeeGroupModel>();
            AvailableEmployeeGroups = new List<SelectListItem>();
        }
        [MaxLength(256)]
        [Required]
        public string Firstname { get; set; }

        [MaxLength(50)]
        public string Middlename { get; set; }

        [MaxLength(256)]
        [Required]
        public string Lastname { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        [MaxLength(50)]
        public string PostalCode { get; set; }

        [MaxLength(256)]
        public string City { get; set; }

        [MaxLength(256)]
        public string Country { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }

        [MaxLength(256)]
        public string NavisionNumber { get; set; }

        [MaxLength(50)]
        public string Phonenumber { get; set; }

        [MaxLength(50)]
        public string Mobilenumber { get; set; }

        [MaxLength(256)]
        public string Username { get; set; }

        [RequiredInsertValidator]
        public string Password { get; set; }

        [RequiredInsertValidator]
        [Compare("Password", ErrorMessage = "De wachtwoorden zijn niet gelijk.")]
        public string PasswordControle { get; set; }

        public bool Active { get; set; }
        public EmployeeFunction EmployeeFunction { get; set; }

        public List<Employee2EmployeeGroupModel> Employee2EmployeeGroups { get; set; }
        public List<SelectListItem> AvailableEmployeeGroups { get; set; }
    }

    public class Employee2EmployeeGroupModel
    {
        public int? EmployeeGroupId { get; set; }
    }
}