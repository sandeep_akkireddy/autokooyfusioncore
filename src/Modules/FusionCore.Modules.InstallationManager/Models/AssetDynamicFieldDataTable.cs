﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetDynamicFieldDataTable : BaseDataTable
    {
        public int AssetId { get; set; }
        public List<AssetDynamicFieldDataRow> Data { get; set; }
    }

    public class AssetDynamicFieldDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int SortOrder { get; set; }
    }
}