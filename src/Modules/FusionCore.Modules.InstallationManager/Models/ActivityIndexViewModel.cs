﻿namespace FusionCore.Modules.InstallationManager.Models
{
    public class ActivityIndexViewModel
    {
        public string NewAssetType { get; set; }
        public string NewActivity { get; set; }
        public int AssetType { get; set; }
        public string NewDynamicField { get; set; }
        public string NewField { get; set; }
        public int DynamicFieldType { get; set; }
    }
}
