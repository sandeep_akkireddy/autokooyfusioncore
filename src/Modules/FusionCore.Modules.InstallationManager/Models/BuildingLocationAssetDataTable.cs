﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class BuildingLocationAssetDataTable : BaseDataTable
    {
        public int BuildingLocationId { get; set; }
        public List<BuildingLocationAssetDataRow> Data { get; set; }
    }

    public class BuildingLocationAssetDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}