﻿using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetPersistModel : BaseModel
    {
        public AssetPersistModel()
        {
            Activities = new List<AssetActivity>();
        }
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }
        public string AssetTypeName { get; set; }

        [Required]
        public int AssetTypeId { get; set; }

        [Required]
        public int? BuildingLocationId { get; set; }

        public int? BuildingId { get; set; }

        public List<SelectListItem> AvailableLocations { get; set; }
        public List<SelectListItem> AvailableTypes { get; set; }
        public IList<AssetActivity> Activities { get; set; }
    }
}