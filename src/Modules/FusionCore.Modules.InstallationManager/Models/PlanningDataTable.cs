﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class PlanningDataTable : BaseDataTable
    {
        public List<PlanningDataRow> Data { get; set; }
    }

    public class PlanningDataRow
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Offernumber { get; set; }
        public string Activity { get; set; }
        public string Building { get; set; }
        public string BuildingId { get; set; }
        public string ClientId { get; set; }
        public string Employee { get; set; }
        public string Status { get; set; }
        public string ClientName { get; set; }
    }
}