﻿using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Validation;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class CalendarItemPersistModel : BaseModel, IDatePersistModel
    {
        [Required]
        [MaxLength(256)]
        public string Subject { get; set; }
        public bool WholeDay { get; set; }
        public string Remark { get; set; }
        [CalendarItemValidator]
        public string StartTimeString { get; set; }
        [CalendarItemValidator]
        public string EndTimeString { get; set; }
        [Required]
        public string DateString { get; set; }

        public int EmployeeId { get; set; }
        public CalendarStatus CalendarStatus { get; set; }
    }
}