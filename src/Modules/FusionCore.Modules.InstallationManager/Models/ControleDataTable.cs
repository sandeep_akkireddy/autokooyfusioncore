﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ControleDataTable : BaseDataTable
    {
        public List<ControleDataRow> Data { get; set; }
    }

    public class ControleDataRow
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Offernumber { get; set; }
        public string Employee { get; set; }
        public string Building { get; set; }
        public string Asset { get; set; }
        public string Activity { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        public string Icon { get; set; }
    }
}