﻿using FusionCore.Core.Enums;
using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetFilesDataTable : BaseDataTable
    {
        public List<AssetFileDataRow> Data { get; set; }
        public FileType Type { get; set; }
        public int AssetId { get; set; }
    }

    public class AssetFileDataRow
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string File { get; set; }
        public string Offernumber { get; set; }
        public string Company { get; set; }
        public string BuildingLocation { get; set; }
    }
}