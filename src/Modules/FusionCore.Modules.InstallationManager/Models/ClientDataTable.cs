﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ClientDataTable : BaseDataTable
    {
        public List<ClientDataRow> Data { get; set; }
    }

    public class ClientDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Phonenumber { get; set; }
    }
}