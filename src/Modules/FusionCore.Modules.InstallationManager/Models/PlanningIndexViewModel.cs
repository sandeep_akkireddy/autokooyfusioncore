﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class PlanningIndexViewModel
    {
        public PlanningIndexViewModel()
        {
            AvailableEmployees = new List<SelectListItem>();
            AvailableGroups = new List<SelectListItem>();
            AvailableLocations = new List<SelectListItem>();
        }
        public int? AssetActivity { get; set; }
        public List<SelectListItem> AvailableEmployees { get; set; }
        public List<SelectListItem> AvailableGroups { get; set; }
        public List<SelectListItem> AvailableLocations { get; set; }
        public int WeekNumber { get; set; }
    }
}
