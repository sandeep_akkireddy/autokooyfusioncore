﻿using FusionCore.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetActivityFieldValuePersistModel : BaseModel
    {
        [Required]
        [MaxLength(256)]
        public string LatestValue { get; set; }

        public int AssetActivityId { get; set; }
    }
}