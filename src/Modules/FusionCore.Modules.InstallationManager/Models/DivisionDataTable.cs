﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class DivisionDataTable : BaseDataTable
    {
        public List<DivisionDataRow> Data { get; set; }
    }

    public class DivisionDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Phonenumber { get; set; }
    }
}