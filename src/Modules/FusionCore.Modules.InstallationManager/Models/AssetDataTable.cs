﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetDataTable : BaseDataTable
    {
        public List<AssetDataRow> Data { get; set; }
    }

    public class AssetDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BuildingName { get; set; }
        public string LocationName { get; set; }
    }
}