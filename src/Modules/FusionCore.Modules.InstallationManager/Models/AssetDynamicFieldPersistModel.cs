﻿using FusionCore.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetDynamicFieldPersistModel : BaseModel
    {
        [Required]
        public int AssetId { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        [MaxLength(256)]
        public string Value { get; set; }
    }
}
