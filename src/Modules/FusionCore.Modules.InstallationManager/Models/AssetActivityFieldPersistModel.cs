﻿using FusionCore.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetActivityFieldPersistModel : BaseModel
    {
        [MaxLength(256)]
        public string Name { get; set; }

        public int DynamicFieldTypeId { get; set; }

        public int AssetActivityId { get; set; }
    }
}