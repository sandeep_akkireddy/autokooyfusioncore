﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ClientUsersDataTable : BaseDataTable
    {
        public int ClientId { get; set; }
        public List<ClientUserDataRow> Data { get; set; }
    }

    public class ClientUserDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}