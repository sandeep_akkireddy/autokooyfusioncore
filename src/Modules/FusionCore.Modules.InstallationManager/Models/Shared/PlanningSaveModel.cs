﻿namespace FusionCore.Modules.InstallationManager.Models.Shared
{
    public class PlanningSaveModel
    {
        public string PlanningId { get; set; }
        public string Status { get; set; }
        public string Signature { get; set; }
        public string Remarks { get; set; }
        public string Actions { get; set; }
        public string NormHours { get; set; }
        public string RealHours { get; set; }
        public string RealMinutes { get; set; }
        public string OkayGivenBy { get; set; }
        public string ExtraEmail { get; set; }
        public string ExtraEmailInformation { get; set; }
        public string Dictionary { get; set; }
        public string TravelHours { get; set; }
        public string TravelMinutes { get; set; }
    }
}
