﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class GroupManagementViewModel
    {
        public GroupManagementViewModel()
        {
            AvailableGroups = new List<SelectListItem>();
        }
        public List<SelectListItem> AvailableGroups { get; set; }
    }
}
