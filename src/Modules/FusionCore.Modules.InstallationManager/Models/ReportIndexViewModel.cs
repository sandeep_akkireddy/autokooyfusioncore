﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ReportIndexViewModel
    {
        [Required]
        public int Employee { get; set; }
        [Required]
        public int Client { get; set; }
        [Required]
        public int Building { get; set; }
        [Required]
        public DateTime Start { get; set; }
        [Required]
        public DateTime End { get; set; }

        public List<SelectListItem> AvailableEmployees { get; set; }
        public List<SelectListItem> AvailableClients { get; set; }
    }
}
