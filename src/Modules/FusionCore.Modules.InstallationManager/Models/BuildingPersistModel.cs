﻿using FusionCore.Models.Common;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class BuildingPersistModel : BaseModel
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Firstname { get; set; }

        [MaxLength(50)]
        public string Middlename { get; set; }

        [MaxLength(256)]
        public string Lastname { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        [MaxLength(50)]
        public string PostalCode { get; set; }

        [MaxLength(256)]
        public string City { get; set; }

        [MaxLength(256)]
        public string Country { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }

        [MaxLength(50)]
        public string Phonenumber { get; set; }

        [MaxLength(50)]
        public string Mobilenumber { get; set; }

        [MaxLength(256)]
        public string Website { get; set; }

        [MaxLength(250)]
        public string Latitude { get; set; }

        [MaxLength(250)]
        public string Longtitude { get; set; }

        public string Remarks { get; set; }

        public int ClientId { get; set; }
        public int? LogoPictureId { get; set; }

        public List<SelectListItem> AvailableClients { get; set; }
    }
}