﻿using FusionCore.Modules.InstallationManager.Data.Entities;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ReportPlanningModel
    {
        public Planning Planning { get; set; }
        public byte[] ClientLogo { get; set; }
        public Client Client { get; set; }
    }
}
