﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace FusionCore.Modules.InstallationManager.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class PlanningResourceViewModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public List<PlanningResourceChildViewModel> children { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class PlanningResourceChildViewModel
    {
        public int id { get; set; }
        public string title { get; set; }
    }
}
