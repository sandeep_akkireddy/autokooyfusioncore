﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class BuildingDataTable : BaseDataTable
    {
        public List<BuildingDataRow> Data { get; set; }
    }

    public class BuildingDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Phonenumber { get; set; }
        public string Client { get; set; }
    }
}