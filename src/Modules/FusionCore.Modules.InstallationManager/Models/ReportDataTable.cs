﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ReportDataTable : BaseDataTable
    {
        public List<ReportDataRow> Data { get; set; }
    }

    public class ReportDataRow
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Offernumber { get; set; }
        public string Client { get; set; }
        public string Building { get; set; }
        public string AssetActivity { get; set; }
        public string Asset { get; set; }
        public string Icon { get; set; }
    }
}