﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class BuildingLocationDataTable : BaseDataTable
    {
        public int BuildingId { get; set; }
        public List<BuildingLocationDataRow> Data { get; set; }
    }

    public class BuildingLocationDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}