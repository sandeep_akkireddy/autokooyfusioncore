﻿using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class ControlePersistModel : BaseModel
    {
        public Planning Planning { get; set; }
    }
}