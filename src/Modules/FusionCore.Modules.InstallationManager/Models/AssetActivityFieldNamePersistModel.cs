﻿using FusionCore.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetActivityFieldNamePersistModel : BaseModel
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        public int AssetActivityId { get; set; }
    }
}