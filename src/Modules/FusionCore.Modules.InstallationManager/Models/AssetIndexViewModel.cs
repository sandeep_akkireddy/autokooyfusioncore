﻿namespace FusionCore.Modules.InstallationManager.Models
{
    public class AssetIndexViewModel
    {
        public int? BuildingLocationId { get; set; }
        public int? Id { get; set; }
    }
}
