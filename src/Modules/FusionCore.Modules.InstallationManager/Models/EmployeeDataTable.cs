﻿using FusionCore.Models.Common;
using System.Collections.Generic;

namespace FusionCore.Modules.InstallationManager.Models
{
    public class EmployeeDataTable : BaseDataTable
    {
        public List<EmployeeDataRow> Data { get; set; }
    }

    public class EmployeeDataRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Group { get; set; }
        public string Phonenumber { get; set; }
    }
}