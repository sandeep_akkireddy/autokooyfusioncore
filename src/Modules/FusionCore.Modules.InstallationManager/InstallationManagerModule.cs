﻿using FusionCore.Core.Interfaces;

namespace FusionCore.Modules.InstallationManager
{
    public class InstallationManagerModule : IFusionCoreModule
    {
        public string GetSystemName()
        {
            return "FusionCore.Module.InstallationManager";
        }
    }
}