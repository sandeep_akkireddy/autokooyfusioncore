﻿using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class ReportController : FusionBaseController
    {
        private readonly IPlanningService _planningService;
        private readonly IClientService _clientService;
        private readonly IEmployeeService _employeeService;
        private readonly IBuildingService _buildingService;
        private readonly IAssetService _assetService;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly IAssetFileService _assetFileService;

        public ReportController(
            IPlanningService planningService,
            IMapper mapper,
            IClientService clientService,
            IEmployeeService employeeService,
            IBuildingService buildingService,
            ILogger<FileService> logger,
            IAssetService assetService,
            IAssetFileService assetFileService)
        {
            _planningService = planningService;
            _mapper = mapper;
            _clientService = clientService;
            _employeeService = employeeService;
            _buildingService = buildingService;
            _logger = logger;
            _assetService = assetService;
            _assetFileService = assetFileService;
        }

        public IActionResult Index()
        {
            ReportIndexViewModel model = new ReportIndexViewModel
            {
                AvailableClients = _clientService.GetAllClients(null, client => client.Name).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList(),
                AvailableEmployees = _employeeService.GetAllEmployees(x => x.Active, employee => employee.GetFullname).Select(x => new SelectListItem { Text = x.GetFullname, Value = x.Id.ToString() }).ToList()
            };
            return View(model);
        }

        public JsonResult DataTable(ReportDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<Planning, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "date":
                    orderBy = x => x.StartDate;
                    break;
                case "offernumber":
                    orderBy = x => x.OfferNumber;
                    break;
                case "client":
                    orderBy = x => x.Client.Name;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Planning, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
            {
                predicate = x =>
                        x.StatusId == (int)PlanningStatus.Complete &&
                        (x.OfferNumber.ToLower().Contains(searchValue.ToLower())
                         || x.Client.Name.ToLower().Contains(searchValue.ToLower())
                         || x.Building.Name.ToLower().Contains(searchValue.ToLower())
                        || x.AssetActivity.Name.ToLower().Contains(searchValue.ToLower()))
                    ;
            }
            else
            {
                predicate = x =>
                    x.StatusId == (int)PlanningStatus.Complete;
            }

            FusionCore.Models.Common.IPagedList<Planning> plannings = _planningService.GetAllPlannings(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize,
                loadOverviewIncludes: true);

            model.Data = _mapper.Map<List<ReportDataRow>>(plannings);
            model.RecordsFiltered = plannings.TotalCount;
            model.RecordsTotal = plannings.TotalCount;
            return new JsonResult(model);
        }

        public async Task<IActionResult> Pdf(int id)
        {
            Planning planning = _planningService.GetPlanning(id);
            byte[] result = await _planningService.GenerateRapportage(planning, true);
            System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
            {
                FileName = planning.Filename,
                Inline = true,
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());
            return File(result, "application/pdf", planning.Filename);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Planning planning = await _planningService.GetPlanningAsync(id);

            planning.Status = PlanningStatus.Deleted;
            _planningService.SaveChanges();
            return RedirectToAction("Index", "Report");
        }

        public async Task<IActionResult> Resend(int id)
        {
            try
            {
                Planning planning = await _planningService.GetPlanningAsync(id);
                byte[] file = await _planningService.GenerateRapportage(planning, true);
                await _planningService.SendRapportageByMail(planning, file);
                AddSuccessMessage("De email is verzonden.");
            }
            catch (Exception er)
            {
                _logger.LogError(er, $"Error resending for id {id}");
                AddErrorMessage("Opnieuw versturen is mislukt.");
            }

            return Redirect("/installationmanager/report");
        }

        [HttpGet]
        public IActionResult GetBuildings(int id)
        {
            List<Building> buildings = _buildingService.GetAllBuildings(x => x.ClientId == id, x => x.Name).ToList();
            return Json(buildings.Select(x => new { x.Id, name = x.Name }));
        }

        [HttpPost]
        public async Task<IActionResult> Export(ReportIndexViewModel model)
        {
            Expression<Func<Planning, bool>> predicate =
                    x => (x.MainEmployeeId == model.Employee ||
                          x.PlanningEmployees.Any(y => y.EmployeeId == model.Employee))
                         && x.StartDate >= model.Start
                         && x.EndDate <= model.End
                         && x.ClientId == model.Client
                         && x.BuildingId == model.Building
                ;

            FusionCore.Models.Common.IPagedList<Planning> plannings = _planningService.GetAllPlannings(predicate, x => x.CreatedOnUtc, loadIncludes: true);
            using (MemoryStream compressedFileStream = new MemoryStream())
            {
                using (ZipArchive zipArchive = new ZipArchive(compressedFileStream, ZipArchiveMode.Update, false))
                {
                    foreach (Planning planning in plannings)
                    {
                        ZipArchiveEntry zipEntry = zipArchive.CreateEntry(planning.OfferNumber + ".pdf");

                        byte[] file = await _planningService.GenerateRapportage(planning, true);
                        //Get the stream of the attachment
                        using (MemoryStream originalFileStream = new MemoryStream(file))
                        using (Stream stream = zipEntry.Open())
                        {
                            originalFileStream.Position = 0;
                            originalFileStream.CopyTo(stream);
                        }
                    }
                }

                return new FileContentResult(compressedFileStream.ToArray(), "application/zip")
                { FileDownloadName = $"export-{DateTime.Now.Ticks}.zip" };
            }
        }

        public IActionResult ProcessOldFiles(int skip, int take)
        {
            IList<AssetFile> assetFiles = _assetService.GetAllAssetFilesWithoutBytes(skip, take);
            StringBuilder result = new StringBuilder();
            string basePath = @"D:\Inetpub\vhosts\aqgroup.nl\mijn.aqgroup.nl";
            foreach (AssetFile file in assetFiles)
            {
                string path = basePath + file.Description;
                result.Append($"{file.Id} - {path} </br>" + Environment.NewLine);
                if (System.IO.File.Exists(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    if (bytes != null)
                    {
                        _assetFileService.InsertAssetFileBinaryAsync(new AssetFileBinary
                        {
                            File = bytes,
                            AssetFileId = file.Id
                        });
                    }
                }
            }
            return Content(result.ToString());
        }
    }
}
