using AutoMapper;
using FusionCore.Core.Enums;
using FusionCore.Core.Exceptions;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Modules.InstallationManager.Models.Shared;
using FusionCore.Modules.InstallationManager.Services;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class ControleController : FusionBaseController
    {
        private readonly IPlanningService _planningService;
        private readonly IAssetService _assetService;
        private readonly IAssetFileService _assetFileService;
        private readonly IMapper _mapper;

        public ControleController(
            IPlanningService planningService,
            IMapper mapper,
            IAssetService assetService,
            IAssetFileService assetFileService)
        {
            _planningService = planningService;
            _mapper = mapper;
            _assetService = assetService;
            _assetFileService = assetFileService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FileUpload(IFormFile file_data, int fileType, int planningId, string description, int historyTypeId)
        {
            try
            {
                if (file_data != null)
                {
                    var planning = await _planningService.GetPlanningAsync(planningId);
                    if (planning == null) throw new Exception($"Planning not found with id {planningId}");
                    if (file_data.Length <= 0) throw new FusionCoreException("File is empty");

                    var fileName = FileService.RemoveInvalidCharacters(file_data);
                    using (var ms = new MemoryStream())
                    {
                        await file_data.CopyToAsync(ms);
                        var fileBinary = ms.ToArray();

                        var assetFile = new AssetFile
                        {
                            AssetId = planning.AssetId.GetValueOrDefault(),
                            Title = fileName,
                            Description = description,
                            FileType = (FileType)fileType,
                            CreatedOnUtc = DateTime.UtcNow,
                            UpdatedOnUtc = DateTime.UtcNow,
                            PlanningId = planningId
                        };
                        await _assetService.CreateAssetFileAsync(assetFile);


                        var assetFileBinary = new AssetFileBinary()
                        {
                            AssetFileId = assetFile.Id,
                            File = fileBinary
                        };
                        await _assetFileService.InsertAssetFileBinaryAsync(assetFileBinary);

                        var result = await _planningService.SaveFileToHistory(null, planningId, assetFile, (DynamicFieldType)historyTypeId);
                        if (result.HasValue)
                        {
                            return Ok(new { id = result.Value, file = Convert.ToBase64String(fileBinary), description = (description ?? ""), createdon = assetFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), assetFile.Title, append = true });
                        }
                    }
                }
                return BadRequest();
            }
            catch (Exception er)
            {
                return BadRequest($"Fout tijdens uploaden file_data: {er.Message}");
            }
        }

        public JsonResult DataTable(ControleDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Expression<Func<Planning, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "date":
                    orderBy = x => x.StartDate;
                    break;
                case "offernumber":
                    orderBy = x => x.OfferNumber;
                    break;
                case "employee":
                    orderBy = x => x.MainEmployee.Lastname;
                    break;
                case "building":
                    orderBy = x => x.Building.Name;
                    break;
                case "asset":
                    orderBy = x => x.Asset.Name;
                    break;
                case "activity":
                    orderBy = x => x.AssetActivity.Name;
                    break;
                case "status":
                    orderBy = x => x.StatusId;
                    break;
                default:
                    orderBy = x => x.StartDate;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Planning, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
            {
                predicate = x =>
                        x.StatusId == (int)PlanningStatus.TechnicallyReady &&
                        (x.Asset.Name.ToLower().Contains(searchValue.ToLower())
                         || x.OfferNumber.ToLower().Contains(searchValue.ToLower())
                         || x.Building.Name.ToLower().Contains(searchValue.ToLower())
                         || x.AssetActivity.Name.ToLower().Contains(searchValue.ToLower())
                         || x.MainEmployee.Lastname.ToLower().Contains(searchValue.ToLower())
                         || x.MainEmployee.Firstname.ToLower().Contains(searchValue.ToLower())
                         || x.Client.Name.ToLower().Contains(searchValue.ToLower()))
                    ;
            }
            else
            {
                predicate = x =>
                    x.StatusId == (int)PlanningStatus.TechnicallyReady;
            }

            var plannings = _planningService.GetAllPlannings(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize,
                true);

            model.Data = _mapper.Map<List<ControleDataRow>>(plannings);
            model.RecordsFiltered = plannings.TotalCount;
            model.RecordsTotal = plannings.TotalCount;
            return new JsonResult(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var planning = await _planningService.GetPlanningAsync(id);

            planning.Status = PlanningStatus.Deleted;
            _planningService.SaveChanges();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> StatusUpdate(int id, int status)
        {
            var planning = await _planningService.GetPlanningAsync(id);

            if (status == (int)PlanningStatus.Complete)
            {
                await _planningService.GenerateRapportage(planning);
                planning.Status = PlanningStatus.Complete;
                _planningService.SaveChanges();
                _planningService.SaveLatestValues(planning);
            }
            return Content("");
        }

        public async Task<IActionResult> Get(int? id)
        {
            var model = id.HasValue ? _mapper.Map<ControlePersistModel>(await _planningService.GetPlanningAsync(id.Value)) : new ControlePersistModel();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] PlanningSaveModel model)
        {
            var planning = await _planningService.GetPlanningAsync(Convert.ToInt32(model.PlanningId));
            if (planning == null) throw new Exception($"Planning not found with id {model.PlanningId}");

            PlanningMapper.ProcessSaveModel(model, planning, _planningService);

            await _planningService.UpdatePlanningAsync(planning);
            await _assetService.SaveLatestValues(planning);

            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult SaveAssetActivityHistoryTitle(int? pk, string value)
        {
            if (pk == 0) pk = null;
            var item = _assetService.GetAssetActivityHistoryById(pk.GetValueOrDefault());
            item.Name = value;
            _assetService.SaveChanges();
            return Json(true);
        }

        public async Task<IActionResult> FileDownloadAsync(int id)
        {
            var history = _assetService.GetAssetActivityHistoryById(id);
            if (history?.AssetFile != null)
            {
                var fileDownloadName = history.AssetFile.Title;
                var extension = Path.GetExtension(fileDownloadName);
                if (string.IsNullOrEmpty(extension) || !FileHelper.IsKnownType(extension))
                {
                    fileDownloadName = fileDownloadName + ".pdf";
                }
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = fileDownloadName,
                    Inline = true,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                var assetFileBytes = await _assetFileService.GetBytesByAssetFileIdAsync(history.AssetFileId.Value);
                if (assetFileBytes != null)
                {
                    return File(assetFileBytes.File, FileHelper.GetMimeType(fileDownloadName),
                        fileDownloadName);
                }
            }

            return BadRequest();
        }

        public async Task<IActionResult> RemoveFile(int id, int planningId)
        {
            var planning = await _planningService.GetPlanningAsync(planningId);
            if (planning == null) throw new Exception($"Planning not found with id {planningId}");

            var source = planning.AssetActivityHistories.FirstOrDefault(x => x.Id == id);
            if (source != null)
            {
                var assetFile = source.AssetFile;
                source.AssetFile = null;
                if (source.DynamicFieldType == DynamicFieldType.Image || source.DynamicFieldType == DynamicFieldType.File)
                {
                    _planningService.DeleteHistory(source);
                }

                _planningService.SaveChanges();
                if (assetFile != null)
                {
                    await _assetService.DeleteAssetFileAsync(assetFile);
                }
            }
            return Content("");
        }

        public async Task<IActionResult> Pdf(int id, bool html = false)
        {
            var planning = _planningService.GetPlanning(id);
            if (html)
            {
                return Content(await _planningService.GenerateRapportageHtml(planning));
            }
            var result = await _planningService.GenerateRapportage(planning, true);
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = planning.Filename,
                Inline = true,
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());
            return File(result, "application/pdf", planning.Filename);
        }

        [HttpPost]
        public async Task<IActionResult> RotateImage(int id, string kind, int planningId)
        {
            var planning = await _planningService.GetPlanningAsync(planningId);
            if (planning == null) throw new Exception($"Planning not found with id {planningId}");

            var source = planning.AssetActivityHistories.FirstOrDefault(x => x.Id == id);
            if (source != null && source.AssetFileId.HasValue)
            {
                var imageBinary = await _assetFileService.GetBytesByAssetFileIdAsync(source.AssetFileId.Value);
                if (imageBinary != null)
                {
                    var bytes = FileHelper.RotateImage(imageBinary.File, kind, source.AssetFile.Title);
                    imageBinary.File = bytes;
                    await _assetFileService.SaveChangesAsync();
                    return Content("reload");
                }
            }
            return Content("");
        }
    }
}
