using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Modules.InstallationManager.Services;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class EmployeeController : FusionBaseController
    {
        private readonly IEmployeeService _employeeService;
        private readonly ICalendarService _calendarService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public EmployeeController(
            IEmployeeService employeeService,
            ICalendarService calendarService,
            IMapper mapper,
            IUserService userService)
        {
            _employeeService = employeeService;
            _calendarService = calendarService;
            _mapper = mapper;
            _userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult DataTable(EmployeeDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Expression<Func<Employee, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.GetFullname;
                    break;
                case "email":
                    orderBy = x => x.Email;
                    break;
                case "phonenumber":
                    orderBy = x => x.Phonenumber;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Employee, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
            {
                predicate = x =>
                        x.Firstname.ToLower().Contains(searchValue.ToLower())
                        || x.Lastname.ToLower().Contains(searchValue.ToLower())
                        || x.Email.ToLower().Contains(searchValue.ToLower())
                        || x.Employee2EmployeeGroups.Any(y => y.EmployeeGroup.Name.ToLower().Contains(searchValue.ToLower()))
                    ;
            }

            var employees = _employeeService.GetAllEmployees(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<EmployeeDataRow>>(employees);
            model.RecordsFiltered = employees.TotalCount;
            model.RecordsTotal = employees.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {
            var model = id.HasValue ? _mapper.Map<EmployeePersistModel>(_employeeService.GetEmployee(id.Value)) : new EmployeePersistModel();

            model.AvailableEmployeeGroups = _employeeService.GetAllEmployeeGroups(orderBy: x => x.Name)
                .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(EmployeePersistModel model)
        {
            if (ModelState.IsValid)
            {
                model.Employee2EmployeeGroups =
                    model.Employee2EmployeeGroups.Where(x => x.EmployeeGroupId.HasValue).ToList();
                return Json(_employeeService.PersistEmployee(model));
            }

            return Json(false);
        }

        public IActionResult GetCalendarItem(int? id, int? employeeId, DateTime? start, DateTime? end)
        {
            var model = id.HasValue ? _mapper.Map<CalendarItemPersistModel>(_calendarService.GetCalendarItem(id.Value)) : new CalendarItemPersistModel();
            if (!id.HasValue)
            {
                model.EmployeeId = employeeId.GetValueOrDefault();
                if (start.HasValue)
                {
                    model.DateString = start.Value.ToString("dd-MM-yyyy") + " - " + start.Value.ToString("dd-MM-yyyy");
                }
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult PersistCalendarItem(CalendarItemPersistModel model)
        {
            if (ModelState.IsValid)
            {
                return Json(_calendarService.PersistCalendarItem(model).HasValue);
            }

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _employeeService.DeleteEmployee(id);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteCalendarItem(int id)
        {
            await _calendarService.DeleteCalendarItemAsync(id);

            return RedirectToAction("Index");
        }

        public IActionResult GetCalendarItems(int employeeId, DateTime start, DateTime end)
        {
            var calendarItems = _calendarService.GetCalendarItemsByTimestamp(employeeId, start, end);

            return Json(calendarItems.Select(x => new CalendarRenderItem
            {
                Id = x.Id,
                AllDay = x.WholeDay,
                ClassName = "label-green",
                Start = x.StartDate,
                End = x.EndDate,
                Title = x.Subject
            }));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCalenderItems(int id, DateTime start, DateTime end)
        {
            var calendarItem = _calendarService.GetCalendarItem(id);
            //The whole day items get back as 02:00 for some reason, so only parse the date not the time
            if (calendarItem.WholeDay)
            {
                calendarItem.StartDate = new DateTime(start.Year, start.Month, start.Day, calendarItem.StartDate.Hour, calendarItem.StartDate.Minute, calendarItem.StartDate.Second, calendarItem.StartDate.Kind);
                calendarItem.EndDate = new DateTime(end.Year, end.Month, end.Day, calendarItem.EndDate.Hour, calendarItem.EndDate.Minute, calendarItem.EndDate.Second, calendarItem.EndDate.Kind);
            }
            else
            {
                calendarItem.StartDate = start;
                calendarItem.EndDate = end;
            }
            await _calendarService.UpdateCalendarItem(calendarItem);
            return Json("Updated");
        }

        public IActionResult GroupManagement()
        {
            var groups = _employeeService.GetAllEmployeeGroups(null, x => x.Name)
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();

            var model = new GroupManagementViewModel
            {
                AvailableGroups = groups,
            };
            return View(model);
        }

        public IActionResult GetEmployees(int? group)
        {
            if (group == 999) group = null;

            var currentUser = _userService.GetCurrentUser();
            var settings = _userService.GetSettings(currentUser);
            var key = $"Sort{group}";
            var employeeList = new Dictionary<int, int>();
            if (settings.ContainsKey(key) && !string.IsNullOrEmpty(settings[key]))
            {
                employeeList = JsonConvert.DeserializeObject<Dictionary<int, int>>(settings[key]);
            }
            var employees = _employeeService
                .GetAllEmployees(x => !group.HasValue || x.Employee2EmployeeGroups.Any(y => y.EmployeeGroupId == group), x => x.Firstname).ToList();

            var sortedEmployees = new List<Employee>();
            if (employeeList.Any() && employeeList.Count == employees.Count)
            {
                foreach (var item in employeeList.OrderBy(x => x.Value))
                {
                    var employee = employees.FirstOrDefault(x => x.Id == item.Key);
                    if (employee != null) sortedEmployees.Add(employee);
                }
            }
            else
            {
                sortedEmployees = employees;
            }

            return Json(sortedEmployees.Select(x => new
            {
                x.Id,
                name = x.GetFullname
            }));
        }

        public IActionResult SaveSort(int[] ids, int? group)
        {
            if (group == 999) group = null;

            var currentUser = _userService.GetCurrentUser();
            var settings = _userService.GetSettings(currentUser);
            var key = $"Sort{group}";
            settings.Remove(key);

            var employees = _employeeService
                .GetAllEmployees(x => !group.HasValue || x.Employee2EmployeeGroups.Any(y => y.EmployeeGroupId == group), x => x.Firstname).ToDictionary(x => x.Id, x => 0);

            for (var index = 0; index < ids.Length; index++)
            {
                var id = ids[index];
                employees[id] = index;
            }

            settings.Add(key, JsonConvert.SerializeObject(employees));
            _userService.PersistSettings(currentUser, settings);
            return Json(true);
        }
    }
}