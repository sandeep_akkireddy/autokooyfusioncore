﻿using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Mappers;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class PlanningController : FusionBaseController
    {
        private readonly IPlanningService _planningService;
        private readonly IEmployeeService _employeeService;
        private readonly IDivisionService _divisionService;
        private readonly IClientService _clientService;
        private readonly IBuildingService _buildingService;
        private readonly IAssetService _assetService;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IPlanningControllerMapper _planningControllerMapper;

        public PlanningController(
            IPlanningService planningService,
            IEmployeeService employeeService,
            IDivisionService divisionService,
            IClientService clientService,
            IBuildingService buildingService,
            IAssetService assetService,
            IMapper mapper,
            IUserService userService,
            IPlanningControllerMapper planningControllerMapper)
        {
            _planningService = planningService;
            _employeeService = employeeService;
            _divisionService = divisionService;
            _clientService = clientService;
            _buildingService = buildingService;
            _assetService = assetService;
            _mapper = mapper;
            _userService = userService;
            _planningControllerMapper = planningControllerMapper;
        }

        public IActionResult Index(int? activity, int buildingLocationId = 0, string employeeId = null, string groupId = null)
        {
            PlanningIndexViewModel model = _planningControllerMapper.PrepareIndexViewModel(activity, buildingLocationId, employeeId, groupId);
            return View(model);
        }

        public IActionResult Overview(int? activity)
        {
            PlanningOverviewViewModel model = new PlanningOverviewViewModel
            {
                AssetActivity = activity
            };
            return View(model);
        }

        public IActionResult Get(int? id, int? activity)
        {
            PlanningPersistModel model = id.HasValue
                ? _mapper.Map<PlanningPersistModel>(_planningService.GetPlanning(id.Value))
                : new PlanningPersistModel();
            if (activity.HasValue)
            {
                AssetActivity assetActivity = _assetService.GetAssetActivity(activity.Value, true);
                if (assetActivity != null)
                {
                    model.AssetActivityId = assetActivity.Id;
                    model.AssetId = assetActivity.AssetId;
                    model.BuildingLocationId = assetActivity.Asset.BuildingLocationId;
                    model.BuildingId = assetActivity.Asset.BuildingId;
                    model.ClientId = assetActivity.Asset.Building.ClientId;
                    model.DivisionId = assetActivity.Asset.Building.Client.ClientDivisions.FirstOrDefault()?.DivisionId;
                }
            }

            model.AvailableStatusses = Enum.GetValues(typeof(PlanningStatus)).Cast<PlanningStatus>().Where(x => x != PlanningStatus.Deleted).Select(x =>
                new SelectListItem
                {
                    Value = ((int)x).ToString(),
                    Text = x.GetStringValue(),
                    Selected = ((int)x) == model.StatusId
                }).ToList();
            model.AvailablePlanningEmployees = _employeeService.GetAllEmployees(x => x.Active, x => x.GetFullname).Select(x =>
                new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.GetFullname
                }).ToList();
            model.AvailableDivisions = _divisionService.GetAllDivisions(orderBy: x => x.Name)
                .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == model.DivisionId }).ToList();

            if (model.ClientId.HasValue)
            {
                model.AvailableClients = _clientService.GetAllClients(orderBy: x => x.Name).Select(x =>
                    new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == model.ClientId }).ToList();
            }

            if (model.BuildingLocationId.HasValue && model.BuildingId.HasValue)
            {
                model.AvailableBuildingLocations = _buildingService.GetAllBuildingLocations(model.BuildingId.Value, orderBy: x => x.Name)
                    .Select(x =>
                        new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == model.BuildingLocationId })
                    .ToList();

                if (model.AssetId.HasValue)
                {
                    model.AvailableAssets = _assetService.GetAllAssets(x => x.BuildingLocationId == model.BuildingLocationId, x => x.Name)
                        .Select(x =>
                            new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == model.AssetId })
                        .ToList();


                    if (model.AssetActivityId.HasValue)
                    {
                        model.AvailableAssetActivities = _assetService.GetAllAssetActivities(model.AssetId.Value, orderBy: x => x.Name)
                            .Select(x =>
                                new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == model.AssetActivityId })
                            .ToList();
                    }
                }
            }


            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(PlanningPersistModel model)
        {
            if (ModelState.IsValid)
            {
                return Json(_planningService.PersistPlanning(model, _userService.GetCurrentUser()));
            }

            return Json(false);
        }

        [HttpGet]
        public IActionResult GetClients(int id)
        {
            return Json(_clientService.GetAllClients(
                x => x.Active && x.ClientDivisions.Select(y => y.DivisionId).Any(y => y == id), x => x.Name).Select(x => new { x.Id, x.Name }));
        }

        [HttpGet]
        public IActionResult GetBuildings(int id)
        {
            List<BuildingLocation> buildingLocations = _buildingService.GetAllBuildings(
                x => x.ClientId == id, x => x.Name, loadLocations: true).SelectMany(x => x.BuildingLocations).ToList();
            return Json(buildingLocations.Select(x => new { x.Id, name = x.Building.Name + " - " + x.Name }));
        }

        [HttpGet]
        public IActionResult GetAssets(int id)
        {
            return Json(_assetService.GetAssetsByBuildingLocationId(id, orderBy: x => x.Name).Select(x => new { x.Id, name = x.Name }));
        }

        [HttpGet]
        public IActionResult GetAssetActivities(int id)
        {
            return Json(_assetService.GetAllAssetActivities(id, x => x.Active, x => x.Name).Select(x => new { x.Id, name = x.Name }));
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult GetPlanners(int[] employeeId = null, int[] groupId = null)
        {
            if (groupId?.Length == 0) groupId = null;

            List<int> groupEmployeeIds = new List<int>();
            if (groupId != null && groupId.Any() && groupId[0] != 0)
            {
                groupEmployeeIds = _employeeService.GetAllEmployees<Employee>(x =>
                    x.Employee2EmployeeGroups.Any(y => groupId.Contains(y.EmployeeGroupId))).Select(x => x.Id).ToList();
            }

            if (employeeId != null)
            {
                groupEmployeeIds.AddRange(employeeId);
            }

            FusionCore.Models.Common.IPagedList<Employee> planners = _employeeService.GetAllEmployees(x => x.Active, x => x.GetFullname);
            List<PlanningResourceViewModel> returnList = new List<PlanningResourceViewModel>();
            if (!groupEmployeeIds.Any())
            {
                returnList.Add(new PlanningResourceViewModel
                {
                    id = 0,
                    title = "Pre-planning"
                });
            }

            FusionCore.Data.Entities.User currentUser = _userService.GetCurrentUser();
            Dictionary<string, string> settings = _userService.GetSettings(currentUser);
            string groupKey = groupId?.FirstOrDefault().ToString();
            string key = $"Sort{groupKey}";
            Dictionary<int, int> employeeList = new Dictionary<int, int>();
            if (settings.ContainsKey(key) && !string.IsNullOrEmpty(settings[key]))
            {
                employeeList = JsonConvert.DeserializeObject<Dictionary<int, int>>(settings[key]).OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            }

            foreach (Employee planner in planners
                .Where(x => x.EmployeeFunction == EmployeeFunction.Employee).OrderBy(x => employeeList.ContainsKey(x.Id) ? employeeList[x.Id] : 0).ThenBy(s => s.GetFullname))
            {
                if (groupEmployeeIds.Any() && !groupEmployeeIds.Contains(planner.Id)) continue;

                returnList.Add(new PlanningResourceViewModel
                {
                    id = planner.Id,
                    title = planner.GetFullname,
                    children = null
                });
            }

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            return Content(JsonConvert.SerializeObject(returnList, jsonSerializerSettings));
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> GetPlannings(DateTime start, DateTime end, int buildingLocationId = 0, int[] employeeId = null, int[] groupId = null)
        {
            List<PlanningCalenderViewModel> plannings = new List<PlanningCalenderViewModel>();
            List<int> groupEmployeeIds = new List<int>();
            if (groupId != null)
            {
                groupEmployeeIds = _employeeService.GetAllEmployees<Employee>(x =>
                    x.Employee2EmployeeGroups.Any(y => groupId.Contains(y.EmployeeGroupId))).Select(x => x.Id).ToList();
            }

            if (employeeId != null)
            {
                groupEmployeeIds.AddRange(employeeId);
            }

            await _planningControllerMapper.FillPlanningsModelAsync(plannings, start, end, groupEmployeeIds, buildingLocationId);

            FusionCore.Data.Entities.User currentUser = _userService.GetCurrentUser();
            Dictionary<string, string> settings = _userService.GetSettings(currentUser);
            settings.Remove("buildingLocationId");
            settings.Remove("employeeId");
            settings.Remove("groupId");
            if (buildingLocationId != 0)
                settings.Add("buildingLocationId", buildingLocationId.ToString());

            if (employeeId != null && employeeId.Any())
                settings.Add("employeeId", string.Join(",", employeeId));

            if (groupId != null && groupId.Any())
                settings.Add("groupId", string.Join(",", groupId));
            _userService.PersistSettings(currentUser, settings);
            return Json(plannings);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePlanning(int id, DateTime start, DateTime end, int? resourceId)
        {
            Planning planning = await _planningService.GetPlanningAsync(id);
            if (planning.Status == PlanningStatus.Complete)
            {
                return Json("NotUpdated");
            }
            planning.StartDate = start;
            planning.EndDate = end;
            if (resourceId.HasValue && resourceId.Value != 0)
            {
                planning.MainEmployeeId = resourceId;
            }
            else
            {
                planning.MainEmployeeId = null;
                planning.PlanningEmployees.Clear();
            }

            await _planningService.UpdatePlanningAsync(planning);
            return Json("Updated");
        }

        public JsonResult DataTable(PlanningDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<Planning, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "date":
                    orderBy = x => x.StartDate;
                    break;
                case "offernumber":
                    orderBy = x => x.OfferNumber;
                    break;
                case "activity":
                    orderBy = x => x.AssetActivity.Name;
                    break;
                case "building":
                    orderBy = x => x.Building.Name;
                    break;
                case "status":
                    orderBy = x => x.StatusId;
                    break;
                case "employee":
                    orderBy = x => x.MainEmployee.GetFullname;
                    break;
                default:
                    orderBy = x => x.StartDate;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Planning, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>
                        x.OfferNumber.ToLower().Contains(searchValue.ToLower())
                        || x.AssetActivity.Name.ToLower().Contains(searchValue.ToLower())
                        || x.Building.Name.ToLower().Contains(searchValue.ToLower())
                        || x.MainEmployee.Firstname.ToLower().Contains(searchValue.ToLower())
                        || x.MainEmployee.Lastname.ToLower().Contains(searchValue.ToLower())
                    ;

            FusionCore.Models.Common.IPagedList<Planning> plannings = _planningService.GetAllPlannings(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize, false, true);

            model.Data = _mapper.Map<List<PlanningDataRow>>(plannings);
            model.RecordsFiltered = plannings.TotalCount;
            model.RecordsTotal = plannings.TotalCount;
            return new JsonResult(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Planning planning = await _planningService.GetBasePlanningAsync(id);

            planning.Status = PlanningStatus.Deleted;
            _planningService.SaveChanges();
            return RedirectToAction("Overview");
        }

        public async Task<IActionResult> DeleteJson(int id)
        {
            Planning planning = await _planningService.GetPlanningAsync(id);

            planning.Status = PlanningStatus.Deleted;
            _planningService.SaveChanges();
            return Json(true);
        }

        public IActionResult Print(int weeknumber, int buildingLocationId = 0, int[] employeeId = null, int[] groupId = null)
        {
            List<int> groupEmployeeIds = new List<int>();
            if (groupId != null && groupId.Any() && groupId[0] != 0)
            {
                groupEmployeeIds = _employeeService.GetAllEmployees<Employee>(x =>
                    x.Employee2EmployeeGroups.Any(y => groupId.Contains(y.EmployeeGroupId))).Select(x => x.Id).ToList();
            }

            if (employeeId != null)
            {
                groupEmployeeIds.AddRange(employeeId);
            }

            DateTime start = DateTime.UtcNow.GetFirstDayOfWeek(weeknumber);
            DateTime end = start.AddDays(6);
            List<Planning> items = _planningService.GetAllPlannings<Planning>(
                x =>
                    ((x.StartDate >= start && x.EndDate <= end) || (x.StartDate >= start && x.StartDate <= end) || (x.EndDate >= start && x.EndDate <= end) || (x.StartDate <= start && x.EndDate >= end))
                    && (groupEmployeeIds == null || !groupEmployeeIds.Any() || (groupEmployeeIds.Any(e => e == x.MainEmployeeId) || x.PlanningEmployees.Any(y => groupEmployeeIds.Any(e => e == y.EmployeeId))))
                    && (buildingLocationId == 0 || x.BuildingLocationId == buildingLocationId)
                , loadOverviewIncludes: true).ToList();

            byte[] excelFile = _planningService.GetExcelExport(items, start, end, weeknumber);

            return File(excelFile, "application/vnd.ms-excel", $"week_{weeknumber}.xls");
        }
    }
}
