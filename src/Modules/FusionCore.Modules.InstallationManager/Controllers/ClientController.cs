﻿using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class ClientController : FusionBaseController
    {
        private readonly IClientService _clientService;
        private readonly IDivisionService _divisionService;
        private readonly IEmployeeService _employeeService;
        private readonly IBuildingService _buildingService;
        private readonly IMapper _mapper;

        public ClientController(
            IClientService clientService,
            IDivisionService divisionService,
            IMapper mapper,
            IEmployeeService employeeService,
            IBuildingService buildingService)
        {
            _clientService = clientService;
            _divisionService = divisionService;
            _mapper = mapper;
            _employeeService = employeeService;
            _buildingService = buildingService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult DataTable(ClientDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<Client, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.Name;
                    break;
                case "contact":
                    orderBy = x => x.GetContact;
                    break;
                case "phonenumber":
                    orderBy = x => x.Phonenumber;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Client, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>
                        x.Name.ToLower().Contains(searchValue.ToLower())
                        || x.Firstname.ToLower().Contains(searchValue.ToLower())
                        || x.Lastname.ToLower().Contains(searchValue.ToLower())
                        || x.Email.ToLower().Contains(searchValue.ToLower())
                    ;

            FusionCore.Models.Common.IPagedList<Client> clients = _clientService.GetAllClients(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<ClientDataRow>>(clients);
            model.RecordsFiltered = clients.TotalCount;
            model.RecordsTotal = clients.TotalCount;
            return new JsonResult(model);
        }

        public JsonResult DataTableClientUsers(ClientUsersDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<ClientUser, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.Lastname;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<ClientUser, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>
                        x.Username.ToLower().Contains(searchValue.ToLower())
                        || x.Firstname.ToLower().Contains(searchValue.ToLower())
                        || x.Lastname.ToLower().Contains(searchValue.ToLower())
                    ;

            FusionCore.Models.Common.IPagedList<ClientUser> clients = _clientService.GetClientUserByClientId(model.ClientId,
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<ClientUserDataRow>>(clients);
            model.RecordsFiltered = clients.TotalCount;
            model.RecordsTotal = clients.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {
            ClientPersistModel model = id.HasValue
                ? _mapper.Map<ClientPersistModel>(_clientService.GetClient(id.Value))
                : new ClientPersistModel();

            model.AvailableDivisions = _divisionService.GetAllDivisions(null, x => x.Name)
                .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();

            return View(model);
        }

        public IActionResult GetClientUser(int? id, int? clientId)
        {
            ClientUserPersistModel model = id.HasValue
                ? _mapper.Map<ClientUserPersistModel>(_clientService.GetClientUser(id.Value))
                : new ClientUserPersistModel();

            if (clientId.HasValue)
            {
                model.ClientId = clientId.Value;
            }

            if (model.ClientId > 0)
            {
                model.AvailableBuildings = _buildingService.GetAllBuildings(x => x.ClientId == model.ClientId, x => x.Name)
                    .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
            }

            if (id.HasValue)
            {
                model.ClientUserRights = _mapper.Map<List<ClientUserRightModel>>(_clientService.GetClientUserRightsByClientUserId(id.Value).ToList());
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(ClientPersistModel model)
        {
            if (ModelState.IsValid) return Json(_clientService.PersistClient(model));

            return Json(false);
        }

        [HttpPost]
        public IActionResult PersistClientUser(ClientUserPersistModel model)
        {
            if (!string.IsNullOrEmpty(model.Username))
            {
                FusionCore.Models.Common.IPagedList<Employee> existingUser = _employeeService.GetAllEmployees<Employee>(x => !string.IsNullOrEmpty(x.Username) &&
                    x.Username.Equals(model.Username, StringComparison.InvariantCultureIgnoreCase));
                if (existingUser.Any())
                {
                    ModelState.AddModelError("Existing User", "Er bestaat een werknemer met deze login");
                }
            }

            if (ModelState.IsValid) return Json(_clientService.PersistClientUser(model));

            return Json(new { result = false, errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage)) });
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _clientService.DeleteClient(id);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteClientUser(int id)
        {
            await _clientService.DeleteClientUser(id);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult GetLocations(int id)
        {
            FusionCore.Models.Common.IPagedList<BuildingLocation> locations = _buildingService.GetAllBuildingLocations(id, orderBy: x => x.Name);
            return Json(locations.Select(x => new { x.Id, x.Name }));
        }
    }
}