﻿using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class ActivityController : FusionBaseController
    {
        private readonly IActivityService _activityService;
        private readonly IAssetService _assetService;
        private readonly ILogger _logger;

        public ActivityController(
            IActivityService activityService,
            IAssetService assetService,
            ILoggerFactory loggerFactory)
        {
            _activityService = activityService;
            _assetService = assetService;
            _logger = loggerFactory.CreateLogger(nameof(ActivityController));
        }

        public IActionResult Index()
        {
            ActivityIndexViewModel model = new ActivityIndexViewModel
            {
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult AddAssetType(string newAssetType)
        {
            IList<AssetType> existingAssetType = _activityService.GetAssetTypes(x =>
                x.Name.Equals(newAssetType, StringComparison.InvariantCultureIgnoreCase));
            if (existingAssetType == null || !existingAssetType.Any())
            {
                _activityService.AddAssetType(newAssetType);
                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public IActionResult SaveSortAssetType(int[] ids)
        {
            IList<AssetType> assetTypes = _activityService.GetAssetTypes();
            for (int index = 0; index < ids.Length; index++)
            {
                int id = ids[index];
                AssetType field = assetTypes.FirstOrDefault(x => x.Id == id);
                if (field != null)
                {
                    field.SortOrder = index;
                }
            }

            _activityService.SaveChanges();
            return Json(true);
        }

        public IActionResult GetAssetTypes()
        {
            return Json(_activityService.GetAssetTypes());
        }

        [HttpPost]
        public IActionResult DeleteAssetType(int id)
        {
            AssetType assetType = _activityService.GetAssetTypeById(id);
            if (assetType != null)
            {
                FusionCore.Models.Common.IPagedList<Asset> usedTypes = _assetService.GetAllAssets<Asset>(x =>
                    x.AssetTypeId.Equals(assetType.Id));
                if (!usedTypes.Any())
                {
                    _activityService.DeleteAssetType(assetType);
                    return Json(true);
                }
            }
            return Json(false);
        }

        [HttpPost]
        public IActionResult SaveSortActivity(int id, int[] ids)
        {
            IList<Activity> assetTypes = _activityService.GetActivitesByAssetType(id);
            for (int index = 0; index < ids.Length; index++)
            {
                int currentId = ids[index];
                Activity field = assetTypes.FirstOrDefault(x => x.Id == currentId);
                if (field != null)
                {
                    field.SortOrder = index;
                }
            }

            _activityService.SaveChanges();
            return Json(true);
        }

        public IActionResult GetActivities(int id)
        {
            return Json(_activityService.GetActivitesByAssetType(id));
        }

        [HttpPost]
        public IActionResult AddActivity(string newActivity, int assetType)
        {
            bool existingActivity = _activityService.GetActivitesByAssetType(assetType).Any(x =>
                x.Name.Equals(newActivity, StringComparison.InvariantCultureIgnoreCase));
            if (!existingActivity)
            {
                _activityService.AddActivity(newActivity, assetType);
                return Json(true);
            }

            return Json(false);
        }

        public IActionResult GetActivityAssetTypes()
        {
            IList<AssetType> assetTypes = _activityService.GetAssetTypes(includes: x => x.Activities);
            var returnList =
                assetTypes.OrderBy(x => x.Name).SelectMany(x => x.Activities.Select(y => new { y.Id, name = $"{x.Name} - {y.Name}" })).ToList();
            return Json(returnList);
        }

        [HttpPost]
        public IActionResult AddDynamicField(string newDynamicField, int id, int dynamicFieldType)
        {
            _activityService.AddDynamicField(newDynamicField, id, (DynamicFieldType)dynamicFieldType);
            return Json(true);
        }

        [HttpPost]
        public IActionResult DeleteActivity(int id)
        {
            Activity activity = _activityService.GetActivityById(id);
            if (activity != null)
            {
                _activityService.DeleteActivity(activity);
                return Json(true);
            }
            return Json(false);
        }

        public IActionResult GetActivityDynamicFields(int id)
        {
            return Json(_activityService.GetActivityDynamicFieldsByActivityId(id).OrderBy(x => x.SortOrder).Select(x => new
            {
                x.Id,
                x.Name,
                dynamicType = x.DynamicFieldType.GetStringValue()
            }));
        }

        [HttpPost]
        public IActionResult SaveSortActivityDynamicField(int id, int[] ids)
        {
            IList<ActivityDynamicField> assetTypes = _activityService.GetActivityDynamicFieldsByActivityId(id);
            for (int index = 0; index < ids.Length; index++)
            {
                int currentId = ids[index];
                ActivityDynamicField field = assetTypes.FirstOrDefault(x => x.Id == currentId);
                if (field != null)
                {
                    field.SortOrder = index;
                }
            }

            _activityService.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public IActionResult DeleteActivityDynamicField(int id)
        {
            ActivityDynamicField activityDynamicField = _activityService.GetActivityDynamicFieldById(id);
            if (activityDynamicField != null)
            {
                _activityService.DeleteActivityDynamicField(activityDynamicField);
                return Json(true);
            }
            return Json(false);
        }

        [HttpPost]
        public IActionResult SaveSortActivityField(int id, int[] ids)
        {
            IList<ActivityField> assetTypes = _activityService.GetActivityFieldsByActivityId(id);
            for (int index = 0; index < ids.Length; index++)
            {
                int currentId = ids[index];
                ActivityField field = assetTypes.FirstOrDefault(x => x.Id == currentId);
                if (field != null)
                {
                    field.SortOrder = index;
                }
            }

            _activityService.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public IActionResult DeleteActivityField(int id)
        {
            ActivityField activityField = _activityService.GetActivityFieldById(id);
            if (activityField != null)
            {
                _activityService.DeleteActivityField(activityField);
                return Json(true);
            }
            return Json(false);
        }

        public IActionResult GetActivityFields(int id)
        {
            return Json(_activityService.GetActivityFieldsByActivityId(id).OrderBy(x => x.SortOrder).Select(x => new
            {
                x.Id,
                x.Name,
                dynamicType = x.DynamicFieldType.GetStringValue()
            }));
        }

        [HttpPost]
        public IActionResult AddActivityField(string newField, int id, int dynamicFieldType)
        {
            _activityService.AddField(newField, id, (DynamicFieldType)dynamicFieldType);
            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult UpdateAssetType(int? pk, string value)
        {
            if (pk == 0) pk = null;
            try
            {
                AssetType assetType = _activityService.GetAssetTypeById(pk.GetValueOrDefault());
                assetType.Name = value;
                _activityService.SaveChanges();
                return Json(true);
            }
            catch (Exception er)
            {
                _logger.LogError(er, $"Error while updating asset type with id {pk}");
                return Json(false);
            }
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult UpdateActivity(int? pk, string value)
        {
            if (pk == 0) pk = null;
            try
            {
                Activity activity = _activityService.GetActivityById(pk.GetValueOrDefault());
                activity.Name = value;
                _activityService.SaveChanges();
                return Json(true);
            }
            catch (Exception er)
            {
                _logger.LogError(er, $"Error while updating activity with id {pk}");
                return Json(false);
            }
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult UpdateDynamicField(int? pk, string value)
        {
            if (pk == 0) pk = null;
            try
            {
                ActivityDynamicField activityDynamicField = _activityService.GetActivityDynamicFieldById(pk.GetValueOrDefault());
                activityDynamicField.Name = value;
                _activityService.SaveChanges();
                return Json(true);
            }
            catch (Exception er)
            {
                _logger.LogError(er, $"Error while updating dynamic field with id {pk}");
                return Json(false);
            }
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult UpdateActivityField(int? pk, string value)
        {
            if (pk == 0) pk = null;
            try
            {
                ActivityField activityField = _activityService.GetActivityFieldById(pk.GetValueOrDefault());
                activityField.Name = value;
                _activityService.SaveChanges();
                return Json(true);
            }
            catch (Exception er)
            {
                _logger.LogError(er, $"Error while updating asset type with id {pk}");
                return Json(false);
            }
        }
    }
}
