﻿using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class BuildingController : FusionBaseController
    {
        private readonly IAssetService _assetService;
        private readonly IBuildingService _buildingService;
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;

        public BuildingController(
            IAssetService assetService,
            IBuildingService buildingService,
            IClientService clientService,
            IMapper mapper)
        {
            _assetService = assetService;
            _buildingService = buildingService;
            _clientService = clientService;
            _mapper = mapper;
        }

        public IActionResult Index(int? id)
        {
            return View(id);
        }

        public JsonResult DataTable(BuildingDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<Building, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.Name;
                    break;
                case "contact":
                    orderBy = x => x.GetContact;
                    break;
                case "phonenumber":
                    orderBy = x => x.Phonenumber;
                    break;
                case "client":
                    orderBy = x => x.ClientId;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Building, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>
                        x.Name.ToLower().Contains(searchValue.ToLower())
                        || x.Firstname.ToLower().Contains(searchValue.ToLower())
                        || x.Lastname.ToLower().Contains(searchValue.ToLower())
                        || x.Email.ToLower().Contains(searchValue.ToLower())
                    ;

            FusionCore.Models.Common.IPagedList<Building> buildings = _buildingService.GetAllBuildings(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<BuildingDataRow>>(buildings);
            model.RecordsFiltered = buildings.TotalCount;
            model.RecordsTotal = buildings.TotalCount;
            return new JsonResult(model);
        }

        public JsonResult DataTableBuildingLocations(BuildingLocationDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<BuildingLocation, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.Name;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            FusionCore.Models.Common.IPagedList<BuildingLocation> buildingLocations = _buildingService.GetAllBuildingLocations(model.BuildingId,
                null,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<BuildingLocationDataRow>>(buildingLocations);
            model.RecordsFiltered = buildingLocations.TotalCount;
            model.RecordsTotal = buildingLocations.TotalCount;
            return new JsonResult(model);
        }

        public JsonResult DataTableBuildingLocationAssets(BuildingLocationAssetDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<Asset, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.Name;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            FusionCore.Models.Common.IPagedList<Asset> buildingLocations = _assetService.GetAssetsByBuildingLocationId(model.BuildingLocationId,
                null,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<BuildingLocationAssetDataRow>>(buildingLocations);
            model.RecordsFiltered = buildingLocations.TotalCount;
            model.RecordsTotal = buildingLocations.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {
            BuildingPersistModel model = id.HasValue
                ? _mapper.Map<BuildingPersistModel>(_buildingService.GetBuilding(id.Value))
                : new BuildingPersistModel();

            model.AvailableClients = _clientService.GetAllClients(orderBy: x => x.Name).Select(x => new SelectListItem
            {
                Selected = x.Id == model.ClientId,
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return View(model);
        }

        public IActionResult GetBuildingLocation(int? id, int? buildingId)
        {
            BuildingLocationPersistModel model = id.HasValue ? _mapper.Map<BuildingLocationPersistModel>(_buildingService.GetBuildingLocation(id.Value)) : new BuildingLocationPersistModel();
            if (buildingId.HasValue)
            {
                Building building = _buildingService.GetBuilding(buildingId.Value);
                if (building != null)
                {
                    model.BuildingId = building.Id;
                    model.BuildingName = building.Name;
                }
            }
            model.UnassignedAssets = _assetService.GetAllAssets(x => !x.BuildingLocationId.HasValue, x => x.Name).Select(x =>
                new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                }).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(BuildingPersistModel model)
        {
            if (ModelState.IsValid) return Json(_buildingService.PersistBuilding(model));

            return Json(false);
        }

        [HttpPost]
        public IActionResult PersistBuildingLocation(BuildingLocationPersistModel model)
        {
            if (ModelState.IsValid) return Json(_buildingService.PersistBuildingLocation(model));

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _buildingService.DeleteBuilding(id);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteBuildingLocation(int id)
        {
            await _buildingService.DeleteBuildingLocationAsync(id);

            return RedirectToAction("Index");
        }

        public IActionResult AddAssetToLocation(int buildingLocationId, int assetId)
        {
            BuildingLocation buildingLocation = _buildingService.GetBuildingLocation(buildingLocationId);
            Asset asset = _assetService.GetAsset(assetId);
            if (asset != null && buildingLocation != null)
            {
                asset.BuildingLocationId = buildingLocation.Id;
                asset.BuildingId = buildingLocation.BuildingId;
                _assetService.PersistAsset(asset);
                return Json(true);
            }

            return Json(false);
        }
    }
}