﻿using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class DivisionController : FusionBaseController
    {
        private readonly IDivisionService _divisionService;
        private readonly IMapper _mapper;

        public DivisionController(IDivisionService divisionService, IMapper mapper)
        {
            _divisionService = divisionService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult DataTable(DivisionDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<Division, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.Name;
                    break;
                case "contact":
                    orderBy = x => x.GetContact;
                    break;
                case "phonenumber":
                    orderBy = x => x.Phonenumber;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Division, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>
                        x.Name.ToLower().Contains(searchValue.ToLower())
                        || x.Firstname.ToLower().Contains(searchValue.ToLower())
                        || x.Lastname.ToLower().Contains(searchValue.ToLower())
                        || x.Email.ToLower().Contains(searchValue.ToLower())
                    ;

            FusionCore.Models.Common.IPagedList<Division> divisions = _divisionService.GetAllDivisions(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<DivisionDataRow>>(divisions);
            model.RecordsFiltered = divisions.TotalCount;
            model.RecordsTotal = divisions.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {
            DivisionPersistModel model = id.HasValue ? _mapper.Map<DivisionPersistModel>(_divisionService.GetDivision(id.Value)) : new DivisionPersistModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(DivisionPersistModel model)
        {
            if (ModelState.IsValid) return Json(_divisionService.PersistDivision(model));

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _divisionService.DeleteDivision(id);

            return RedirectToAction("Index");
        }
    }
}