using AutoMapper;
using FusionCore.Core.Enums;
using FusionCore.Core.Exceptions;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Controllers
{
    [Area("InstallationManager")]
    public class AssetController : FusionBaseController
    {
        private readonly IAssetService _assetService;
        private readonly IBuildingService _buildingService;
        private readonly IActivityService _activityService;
        private readonly IPlanningService _planningService;
        private readonly IAssetFileService _assetFileService;
        private readonly IMapper _mapper;

        public AssetController(
            IAssetService assetService,
            IBuildingService buildingService,
            IActivityService activityService,
            IMapper mapper,
            IPlanningService planningService,
            IAssetFileService assetFileService)
        {
            _assetService = assetService;
            _buildingService = buildingService;
            _activityService = activityService;
            _mapper = mapper;
            _planningService = planningService;
            _assetFileService = assetFileService;
        }

        public IActionResult Index(int? buildingLocationId, int? id)
        {
            AssetIndexViewModel model = new AssetIndexViewModel
            {
                BuildingLocationId = buildingLocationId,
                Id = id
            };
            return View(model);
        }

        public JsonResult DataTable(AssetDataTable model)
        {
            string sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            string sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            string searchValue = Request.Query["search[value]"].FirstOrDefault();

            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            Expression<Func<Asset, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "name":
                    orderBy = x => x.Name;
                    break;
                case "buildingName":
                    orderBy = x => x.Building.Name;
                    break;
                case "locationName":
                    orderBy = x => x.BuildingLocation.Name;
                    break;
                default:
                    orderBy = x => x.UpdatedOnUtc;
                    sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Asset, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>
                        x.Name.ToLower().Contains(searchValue.ToLower())
                        || x.AssetType.Name.ToLower().Contains(searchValue.ToLower())
                        || x.Building.Name.ToLower().Contains(searchValue.ToLower())
                        || x.BuildingLocation.Name.ToLower().Contains(searchValue.ToLower())
                    ;

            FusionCore.Models.Common.IPagedList<Asset> assets = _assetService.GetAllAssets(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<AssetDataRow>>(assets);
            model.RecordsFiltered = assets.TotalCount;
            model.RecordsTotal = assets.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id, int buildingLocationId = 0)
        {
            AssetPersistModel model = id.HasValue ? _mapper.Map<AssetPersistModel>(_assetService.GetAsset(id.Value)) : new AssetPersistModel();

            IEnumerable<BuildingLocation> buildingLocations = _buildingService.GetAllBuildings<Building>(loadLocations: true)
                .SelectMany(x => x.BuildingLocations);
            buildingLocationId = model.BuildingLocationId ?? buildingLocationId;
            IList<AssetType> assetTypes = _activityService.GetAssetTypes();

            List<SelectListItem> availableLocations = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Value = "",
                    Text = "Selecteer"
                }
            };
            availableLocations.AddRange(buildingLocations.OrderBy(x => x.Building.Name).ThenBy(x => x.Name).Select(x => new SelectListItem
            {
                Selected = x.Id == buildingLocationId,
                Text = $"{x.Building.Name} - {x.Name}",
                Value = x.Id.ToString()
            }).ToList());

            List<SelectListItem> availableTypes = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Value = "",
                    Text = "Selecteer"
                }
            };
            availableTypes.AddRange(assetTypes.OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Selected = x.Id == model.AssetTypeId,
                Text = $"{x.Name}",
                Value = x.Id.ToString()
            }).ToList());

            model.AvailableLocations = availableLocations;
            model.AvailableTypes = availableTypes;
            model.Activities = id.HasValue ? _assetService.GetAllAssetActivities(id.Value, null, x => x.SortOrder).OrderBy(x => x.Name).ToList() : new List<AssetActivity>();
            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(AssetPersistModel model)
        {
            if (ModelState.IsValid)
            {
                BuildingLocation location = _buildingService.GetBuildingLocation(model.BuildingLocationId.GetValueOrDefault());
                model.BuildingId = location?.BuildingId;
                return Json(_assetService.PersistAsset(model));
            }

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _assetService.DeleteAsset(id);

            return RedirectToAction("Index");
        }

        public JsonResult DataTableBuildingAssetDynamicFields(AssetDynamicFieldDataTable model)
        {
            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            FusionCore.Models.Common.IPagedList<AssetDynamicField> assets = _assetService.GetAllAssetDynamicFields(
                model.AssetId,
                null,
                x => x.SortOrder,
                true,
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<AssetDynamicFieldDataRow>>(assets);
            model.RecordsFiltered = assets.TotalCount;
            model.RecordsTotal = assets.TotalCount;
            return new JsonResult(model);
        }

        public async Task<IActionResult> DeleteAssetDynamicField(int id)
        {
            await _assetService.DeleteAssetDynamicField(id);

            return Json(true);
        }

        public IActionResult PersistAssetDynamicField(int? id, int assetId, string name, string value)
        {
            if (id == 0) id = null;
            AssetDynamicFieldPersistModel saveModel = new AssetDynamicFieldPersistModel
            {
                AssetId = assetId,
                Id = id,
                Name = name,
                Value = value
            };
            return Json(_assetService.PersistAssetDynamicField(saveModel));
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult PersistAssetActivityField(int? pk, string value, int assetActivityId)
        {
            if (pk == 0) pk = null;
            return Json(_assetService.PersistAssetActivityField(new AssetActivityFieldNamePersistModel
            {
                Id = pk,
                Name = value,
                AssetActivityId = assetActivityId
            }).HasValue);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult PersistAssetActivityFieldLatestValue(int id, int assetActivityId, string value)
        {
            return Json(_assetService.PersistAssetActivityField(new AssetActivityFieldValuePersistModel
            {
                Id = id,
                LatestValue = value,
                AssetActivityId = assetActivityId
            }).HasValue);
        }

        public async Task<IActionResult> DeleteAssetActivityField(int id)
        {
            await _assetService.DeleteAssetActivityField(id);
            return Json(true);
        }

        [HttpPost]
        public IActionResult SetActiveAssetActivity(int id)
        {
            AssetActivity assetActivity = _assetService.GetAssetActivity(id);
            if (assetActivity != null)
            {
                assetActivity.Active = !assetActivity.Active;
                _assetService.UpdateAssetActivity(assetActivity);
            }

            return Json(true);
        }

        public IActionResult LoadActivities(int id)
        {
            return View(_assetService.GetAllAssetActivities(id, null, x => x.SortOrder).ToList());
        }

        public IActionResult AddActivity(int assetId)
        {
            AssetActivityPersistModel model = new AssetActivityPersistModel
            {
                AssetId = assetId
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult AddActivity(AssetActivityPersistModel model)
        {
            if (ModelState.IsValid)
            {
                _assetService.PersistAssetActivity(model);
            }
            return Json(true);
        }

        public IActionResult AddActivityField(int assetActivityId)
        {
            AssetActivityFieldPersistModel model = new AssetActivityFieldPersistModel
            {
                AssetActivityId = assetActivityId
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult AddActivityField(AssetActivityFieldPersistModel model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Name)) model.Name = ((DynamicFieldType)model.DynamicFieldTypeId).GetStringValue();
                _assetService.PersistAssetActivityField(model);
            }
            return Json(true);
        }

        [HttpPost]
        public IActionResult SaveSort(int assetActivityId, int[] ids)
        {
            AssetActivity activity = _assetService.GetAssetActivity(assetActivityId);
            for (int index = 0; index < ids.Length; index++)
            {
                int id = ids[index];
                AssetActivityField field = activity.AssetActivityFields.FirstOrDefault(x => x.Id == id);
                if (field != null)
                {
                    field.SortOrder = index;
                }
            }

            _assetService.SaveChanges();
            return Json(true);
        }

        public async Task<IActionResult> DeleteActivity(int activity)
        {
            bool hasPlanningWithActivity = _planningService.GetPlanningByAssetActivityId(activity)?.Any() ?? false;
            if (!hasPlanningWithActivity)
            {
                await _assetService.DeleteAssetActivity(activity);
                return Json(true);
            }

            return Json(false);
        }

        public JsonResult GetFiles(AssetFilesDataTable model)
        {
            int pageSize = model.Length;
            int pageIndex = model.Start / model.Length;

            FusionCore.Models.Common.IPagedList<AssetFile> assets = _assetService.GetAssetFilesByType(model.AssetId, model.Type,
                pageIndex,
                pageSize);

            List<AssetFileDataRow> assetFileDataRows = _mapper.Map<List<AssetFileDataRow>>(assets);
            foreach (AssetFileDataRow row in assetFileDataRows)
            {
                if (string.IsNullOrEmpty(row.Company))
                {
                    row.Company = "Onbekend";
                }
                if (string.IsNullOrEmpty(row.BuildingLocation))
                {
                    row.BuildingLocation = "Onbekend";
                }
                if (string.IsNullOrEmpty(row.Offernumber))
                {
                    row.Offernumber = "Onbekend";
                }
            }
            model.Data = assetFileDataRows;
            model.RecordsFiltered = assets.TotalCount;
            model.RecordsTotal = assets.TotalCount;
            return new JsonResult(model);
        }

        public async Task<IActionResult> DownloadFile(int id)
        {
            AssetFile assetFile = await _assetService.GetAssetFileByIdAsync(id);
            AssetFileBinary assetFileBytes = await _assetFileService.GetBytesByAssetFileIdAsync(id);
            if (assetFileBytes != null)
            {
                string fileDownloadName = assetFile.Title;
                string extension = Path.GetExtension(fileDownloadName);
                if (string.IsNullOrEmpty(extension) || !FileHelper.IsKnownType(extension))
                {
                    fileDownloadName = fileDownloadName + ".pdf";
                }

                string contentType = FileHelper.GetMimeType(fileDownloadName);
                if (contentType == "application/octet-stream")
                {
                    if (string.IsNullOrEmpty(extension))
                    {
                        fileDownloadName = fileDownloadName + ".pdf";
                    }
                    contentType = FileHelper.GetMimeType(fileDownloadName);
                }
                return File(assetFileBytes.File, contentType, fileDownloadName);
            }

            return RedirectToAction("Error", "Common", new { area = "" });
        }

        public async Task<IActionResult> DeleteFile(int? id, DateTime? date)
        {
            if (id.HasValue)
            {
                AssetFile assetFile = await _assetService.GetAssetFileByIdAsync(id.Value);
                if (assetFile != null)
                {
                    await _assetService.DeleteAssetFileAsync(assetFile);
                    return Json(true);
                }
            }

            if (date.HasValue)
            {
                IList<AssetFile> assetFiles = _assetService.GetAssetFilesByDateAndType(date.Value, FileType.Image);
                foreach (AssetFile assetFile in assetFiles)
                {
                    await _assetService.DeleteAssetFileAsync(assetFile);
                }
                return Json(true);
            }

            return Json(false);
        }

        public IActionResult UpdateSortOrder(int id, int order)
        {
            AssetDynamicField dynamicField = _assetService.GetAssetDynamicField(id);
            List<AssetDynamicField> dynamicFields = _assetService.GetAllAssetDynamicFields(dynamicField.AssetId, null, x => x.SortOrder).ToList();

            int selectedIndex = dynamicFields.IndexOf(dynamicField);
            int newIndex = selectedIndex + order;
            if (newIndex < 0) newIndex = 0;
            if (newIndex > (dynamicFields.Count - 1)) newIndex = (dynamicFields.Count - 1);

            dynamicFields.RemoveAt(selectedIndex);
            dynamicFields.Insert(newIndex, dynamicField);

            for (int index = 0; index < dynamicFields.Count; index++)
            {
                AssetDynamicField item = dynamicFields[index];
                item.SortOrder = index;
            }
            _assetService.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FileUpload(IFormFile file_data, int assetId)
        {
            try
            {
                if (file_data != null)
                {
                    if (file_data.Length <= 0) throw new FusionCoreException("File is empty");

                    string fileName = FileService.RemoveInvalidCharacters(file_data);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        await file_data.CopyToAsync(ms);
                        byte[] fileBinary = ms.ToArray();
                        FileType fileType = fileName.ToLower().EndsWith(".pdf") ? FileType.Pdf : FileType.Image;

                        AssetFile assetFile = new AssetFile
                        {
                            AssetId = assetId,
                            Title = fileName,
                            Description = "",
                            FileType = fileType,
                            CreatedOnUtc = DateTime.UtcNow,
                            UpdatedOnUtc = DateTime.UtcNow,
                            PlanningId = null
                        };
                        await _assetService.CreateAssetFileAsync(assetFile);

                        if(fileType == FileType.Image)
                        {
                            fileBinary = FileHelper.Resize(fileBinary, 1280, 1024, fileName);
                        }

                        AssetFileBinary assetFileBinary = new AssetFileBinary()
                        {
                            AssetFileId = assetFile.Id,
                            File = fileBinary
                        };
                        await _assetFileService.InsertAssetFileBinaryAsync(assetFileBinary);

                        return Ok(new { id = assetFile.Id, file = Convert.ToBase64String(fileBinary), description = "", createdon = assetFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), assetFile.Title, append = true });
                    }
                }
                return BadRequest();
            }
            catch (Exception er)
            {
                return BadRequest($"Fout tijdens uploaden file_data: {er.Message}");
            }
        }

        public IActionResult ImageList(int id)
        {
            FusionCore.Models.Common.IPagedList<AssetFile> assets = _assetService.GetAssetFilesByType(id, FileType.Image, 0, Int32.MaxValue);
            return View(assets);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Image(int id)
        {
            AssetFile assetFile = await _assetFileService.GetAssetFileByIdAsync(id);
            if (assetFile != null)
            {
                AssetFileBinary assetFileBytes = await _assetFileService.GetBytesByAssetFileIdAsync(id);
                if (assetFileBytes != null)
                {
                    return File(assetFileBytes.File, FileHelper.GetMimeType(assetFile.Title), assetFile.Title);
                }
            }

            return BadRequest();
        }

        public async Task<IActionResult> ResizeImages(int start = 0, int pageSize = 500, int? fileId = null)
        {
            var files = await _assetFileService.GetAssetFilesAsync(x => x.FileTypeId == (int)FileType.Image && (!fileId.HasValue || x.Id == fileId.Value), x => x.Id, pageIndex: start, pageSize: pageSize);
            var result = new StringBuilder();
            foreach (var image in files)
            {
                var binary = await _assetFileService.GetBytesByAssetFileIdAsync(image.Id);
                if (binary != null)
                {
                    var oldSize = binary.File.Length;
                    var parsedImage = FileHelper.Resize(binary.File, 1280, 1024);
                    System.Drawing.Imaging.ImageFormat imageFormat = null;
                    var fileExtension = Path.GetExtension(image.Title);

                    switch (fileExtension.ToLower())
                    {
                        case ".bmp":
                            imageFormat = System.Drawing.Imaging.ImageFormat.Bmp;
                            break;
                        case ".png":
                            imageFormat = System.Drawing.Imaging.ImageFormat.Png;
                            break;
                        case ".gif":
                            imageFormat = System.Drawing.Imaging.ImageFormat.Gif;
                            break;
                        default:
                            imageFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                            break;
                    }

                    var newBytes = parsedImage.ToByteArray(imageFormat);
                    var newSize = newBytes.Length;
                    binary.File = newBytes;
                    await _assetFileService.UpdateAssetFileBinaryAsync(binary);
                    result.AppendLine($"Processing file {image.Id} with old size: {oldSize} and new size: {newSize}");
                }
            }
            return Content(result.ToString());
        }
    }
}