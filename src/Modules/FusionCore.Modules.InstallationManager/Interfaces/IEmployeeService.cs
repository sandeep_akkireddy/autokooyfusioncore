﻿using FusionCore.Core;
using FusionCore.Models.Backend.Login;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IEmployeeService : IBaseService
    {
        IPagedList<Employee> GetAllEmployees<TKey>(
            Expression<Func<Employee, bool>> predicate = null,
            Expression<Func<Employee, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Employee GetEmployee(int id);
        bool PersistEmployee(EmployeePersistModel model);
        Task DeleteEmployee(int id);
        void AddEmployeeGroup(EmployeeGroup model);
        IPagedList<EmployeeGroup> GetAllEmployeeGroups<TKey>(
            Expression<Func<EmployeeGroup, bool>> predicate = null,
            Expression<Func<EmployeeGroup, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Employee Validate(string identifier, string password, string loginTypeCode = Constants.DefaultLoginType);
        Task SignIn(Employee employee, bool isPersistent = false);
        Task SignOut();
        int GetCurrentUserId();
        Employee GetCurrentUser();
        Task SendPasswordResetEmail(ForgotModel model, string loginTypeCode = Constants.DefaultLoginType);
        bool ValidateResetGuid(Guid guid, int uid);
        Task UpdateEmployeeAsync(Employee user);
    }
}