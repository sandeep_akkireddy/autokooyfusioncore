using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Services;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IAssetFileService : IBaseService
    {
        Task<AssetFile> GetAssetFileByIdAsync(int id);
        Task DeleteAssetFileAsync(int id);
        Task SaveChangesAsync();
        Task<AssetFileBinary> GetBytesByAssetFileIdAsync(int assetFileId);
        Task InsertAssetFileBinaryAsync(AssetFileBinary assetFileBinary);
        Task UpdateAssetFileBinaryAsync(AssetFileBinary assetFileBinary);
        Task<AssetFile[]> GetAssetFilesAsync<TKey>(Expression<Func<AssetFile, bool>> predicate = null,
            Expression<Func<AssetFile, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue);
    }
}
