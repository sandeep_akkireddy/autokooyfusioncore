﻿using FusionCore.Core;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IClientService : IBaseService
    {
        IPagedList<Client> GetAllClients<TKey>(
            Expression<Func<Client, bool>> predicate = null,
            Expression<Func<Client, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Client GetClient(int id);
        bool PersistClient(ClientPersistModel model);
        Task DeleteClient(int id);

        IPagedList<ClientUser> GetClientUserByClientId<TKey>(
            int clientId,
            Expression<Func<ClientUser, bool>> predicate = null,
            Expression<Func<ClientUser, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        ClientUser GetClientUser(int id);
        bool PersistClientUser(ClientUserPersistModel model);
        Task DeleteClientUser(int id);
        Task SignOut();
        ClientUser Validate(string identifier, string password, string loginTypeCode = Constants.DefaultLoginType);
        Task SignIn(ClientUser clientUser, bool isPersistent = false);
        int GetCurrentUserId();
        ClientUser GetCurrentUser();
        Task DeleteClientUserRight(int id);
        List<ClientUserRight> GetClientUserRightsByClientUserId(int clientId);
    }
}