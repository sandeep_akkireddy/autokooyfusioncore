﻿using FusionCore.Core.Enums;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IAssetService : IBaseService
    {
        #region Asset
        IPagedList<Asset> GetAllAssets<TKey>(
            Expression<Func<Asset, bool>> predicate = null,
            Expression<Func<Asset, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Asset GetAsset(int id);
        bool PersistAsset(AssetPersistModel model);
        Task DeleteAsset(int id);
        IPagedList<Asset> GetAssetsByBuildingLocationId<TKey>(int buildingLocationId,
            Expression<Func<Asset, bool>> predicate = null,
            Expression<Func<Asset, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        void PersistAsset(Asset asset);
        #endregion
        #region DynamicFields
        IPagedList<AssetDynamicField> GetAllAssetDynamicFields<TKey>(int assetId,
            Expression<Func<AssetDynamicField, bool>> predicate = null,
            Expression<Func<AssetDynamicField, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue);
        AssetDynamicField GetAssetDynamicField(int id);
        int? PersistAssetDynamicField(AssetDynamicFieldPersistModel assetDynamicField);
        Task DeleteAssetDynamicField(int id);
        #endregion
        #region Activity
        IPagedList<AssetActivity> GetAllAssetActivities<TKey>(int assetId,
            Expression<Func<AssetActivity, bool>> predicate = null,
            Expression<Func<AssetActivity, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue, bool loadFields = false);
        AssetActivity GetAssetActivity(int id, bool loadAll = false);
        AssetActivityField GetAssetActivityField(int id);
        int? PersistAssetActivity(AssetActivityPersistModel assetActivity);
        Task DeleteAssetActivity(int id);
        Task<AssetActivity> GetAssetActivityAsync(int activityId);
        int? PersistAssetActivityField(AssetActivityFieldPersistModel assetActivityFieldPersistModel);
        int? PersistAssetActivityField(AssetActivityFieldNamePersistModel assetActivityFieldPersistModel);
        int? PersistAssetActivityField(AssetActivityFieldValuePersistModel assetActivityFieldPersistModel);
        Task DeleteAssetActivityField(int id);
        void UpdateAssetActivity(AssetActivity assetActivity);
        #endregion

        void SaveChanges();
        Task SaveLatestValues(Planning planning);
        Task DeleteAssetFileAsync(AssetFile assetFile);
        Task CreateAssetFileAsync(AssetFile assetFile);
        AssetActivityHistory GetAssetActivityHistoryById(int id);
        IPagedList<AssetFile> GetAssetFilesByType(int assetId, FileType type, int pageIndex, int pageSize);
        Task<AssetFile> GetAssetFileByIdAsync(int id);
        IList<AssetFile> GetAssetFilesByDateAndType(DateTime date, FileType fileType);
        IList<AssetFile> GetAllAssetFilesWithoutBytes(int skip, int take);
    }
}