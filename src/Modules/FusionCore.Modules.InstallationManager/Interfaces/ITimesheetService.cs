﻿using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface ITimesheetService : IBaseService
    {
        void SaveChanges();
        IPagedList<Timesheet> GetAllTimeSheets<TKey>(
            Expression<Func<Timesheet, bool>> predicate = null,
            Expression<Func<Timesheet, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Task<Timesheet> GetTimeSheetByEmployeeAndWeeknumberAsync(int employeeId, int weekNumber);

        Timesheet GetTimeSheet(int id);
        bool PersistTimesheet(TimesheetPersistModel model);
        Task DeleteTimeSheet(int id);
        Timesheet CreateBaseTimeSheet(int currentUserId, int weekNumber, DateTime startDate);
        Timesheet ValidatePlanningsForTimeSheet(Timesheet timesheet);
        TimesheetHomeWorkRow GetTimeSheetHomeWorkRow(int id);
        TimesheetTimeRow GetTimeSheetRow(int id);
        Task<(bool showMessage, int weekNumber)> ShowTimeSheetMessageAsync(int currentUserId);
        Task<TimesheetTimeRow> GetTimeSheetRowAsync(int id);
        Task DeleteTimeSheetRowAsync(TimesheetTimeRow row);
    }
}