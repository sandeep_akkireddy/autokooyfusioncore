﻿using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IDivisionService : IBaseService
    {
        IPagedList<Division> GetAllDivisions<TKey>(
            Expression<Func<Division, bool>> predicate = null,
            Expression<Func<Division, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Division GetDivision(int id);
        bool PersistDivision(DivisionPersistModel model);
        Task DeleteDivision(int id);
    }
}