﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IDatePersistModel
    {
        [Required]
        string StartTimeString { get; set; }
        [Required]
        string EndTimeString { get; set; }
        [Required]
        string DateString { get; set; }
        bool WholeDay { get; set; }
    }
    public interface IDateEntity
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }
}
