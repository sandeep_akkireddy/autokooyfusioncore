﻿using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IBuildingService : IBaseService
    {
        IPagedList<Building> GetAllBuildings<TKey>(
            Expression<Func<Building, bool>> predicate = null,
            Expression<Func<Building, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            bool loadLocations = false);

        IPagedList<BuildingLocation> GetAllBuildingLocations<TKey>(
            int buildingId,
            Expression<Func<BuildingLocation, bool>> predicate = null,
            Expression<Func<BuildingLocation, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Building GetBuilding(int id);
        BuildingLocation GetBuildingLocation(int id);
        bool PersistBuilding(BuildingPersistModel model);
        bool PersistBuildingLocation(BuildingLocationPersistModel model);
        Task DeleteBuilding(int id);
        Task DeleteBuildingLocationAsync(int id);
    }
}