﻿using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IActivityService : IBaseService
    {
        void SaveChanges();
        IList<AssetType> GetAssetTypes(Expression<Func<AssetType, bool>> predicate = null, params Expression<Func<AssetType, object>>[] includes);
        void AddAssetType(string name);
        AssetType GetAssetTypeById(int id);
        void DeleteAssetType(AssetType assetType);
        void AddActivity(string name, int assetTypeId);
        IList<Activity> GetActivitesByAssetType(int assetTypeId);
        void AddDynamicField(string name, int activityId, DynamicFieldType fieldType);
        Activity GetActivityById(int id);
        void DeleteActivity(Activity activity);
        IList<ActivityDynamicField> GetActivityDynamicFieldsByActivityId(int id);
        ActivityDynamicField GetActivityDynamicFieldById(int id);
        void DeleteActivityDynamicField(ActivityDynamicField activityDynamicField);
        IList<ActivityField> GetActivityFieldsByActivityId(int id);
        ActivityField GetActivityFieldById(int id);
        void DeleteActivityField(ActivityField activityField);
        void AddField(string name, int activityId, DynamicFieldType dynamicFieldType);
    }
}
