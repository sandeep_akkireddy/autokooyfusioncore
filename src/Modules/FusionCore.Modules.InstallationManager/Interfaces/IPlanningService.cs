﻿using FusionCore.Data.Entities;
using FusionCore.Models.Common;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Interfaces
{
    public interface IPlanningService : IBaseService
    {
        void SaveChanges();
        IPagedList<Planning> GetAllPlannings<TKey>(
            Expression<Func<Planning, bool>> predicate = null,
            Expression<Func<Planning, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            bool loadIncludes = false, bool loadOverviewIncludes = false);

        Planning GetPlanning(int id);
        Task<Planning> GetBasePlanningAsync(int id);
        Task<Planning> GetPlanningAsync(int id);
        bool PersistPlanning(PlanningPersistModel model, User getCurrentUser);
        Task DeletePlanning(int id);
        Task UpdatePlanningAsync(Planning planning);
        Task InsertHistoryAsync(AssetActivityHistory assetActivityHistory);
        Task SendActionsByMail(Planning planning);
        Task<byte[]> GenerateRapportage(Planning planning, bool isTemp = false, bool sendMail = true,
            bool updateIaf = false);

        Task<string> GenerateRapportageHtml(Planning planning);

        Task SendRapportageByMail(Planning planning, byte[] file);
        void DeleteHistory(AssetActivityHistory history);
        void SaveLatestValues(Planning planning);
        IList<Planning> GetPlanningByAssetActivityId(int activityId);
        string GetNewOfferNumber(int buildingId, string offerNumber);
        Task<int?> SaveFileToHistory(int? imgId, int? planningId, AssetFile assetFile, DynamicFieldType fieldType);
        byte[] GetExcelExport(List<Planning> items, DateTime start, DateTime end, int weeknumber);
    }
}
