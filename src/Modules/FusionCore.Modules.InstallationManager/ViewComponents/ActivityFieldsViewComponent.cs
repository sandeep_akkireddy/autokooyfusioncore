﻿using FusionCore.Modules.InstallationManager.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.ViewComponents
{
    public class ActivityFieldsViewComponent : ViewComponent
    {
        private readonly IAssetService _assetService;

        public ActivityFieldsViewComponent(IAssetService assetService)
        {
            _assetService = assetService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int activityId)
        {
            Data.Entities.AssetActivity model = await _assetService.GetAssetActivityAsync(activityId);
            return View(model);
        }
    }
}
