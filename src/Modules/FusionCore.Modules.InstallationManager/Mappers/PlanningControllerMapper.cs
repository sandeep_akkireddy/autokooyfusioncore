﻿using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Modules.InstallationManager.Data.Entities;
using FusionCore.Modules.InstallationManager.Data.Enums;
using FusionCore.Modules.InstallationManager.Interfaces;
using FusionCore.Modules.InstallationManager.Models;
using FusionCore.Modules.InstallationManager.Services;
using FusionCore.Services;
using FusionCore.Services.User;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace FusionCore.Modules.InstallationManager.Mappers
{
    public interface IPlanningControllerMapper : IBaseService
    {
        Task FillPlanningsModelAsync(List<PlanningCalenderViewModel> plannings, DateTime start, DateTime end, List<int> groupEmployeeIds, int buildingLocationId);
        PlanningIndexViewModel PrepareIndexViewModel(int? activity, int buildingLocationId, string employeeId, string groupId);
    }

    public class PlanningControllerMapper : BaseService, IPlanningControllerMapper
    {
        private readonly IPlanningService _planningService;
        private readonly ICalendarService _calendarService;
        private readonly IUserService _userService;
        private readonly IEmployeeService _employeeService;
        private readonly IBuildingService _buildingService;

        public PlanningControllerMapper(
            IPlanningService planningService,
            ICalendarService calendarService,
            IUserService userService,
            IEmployeeService employeeService,
            IBuildingService buildingService)
        {
            _planningService = planningService;
            _calendarService = calendarService;
            _userService = userService;
            _employeeService = employeeService;
            _buildingService = buildingService;
        }

        public async Task FillPlanningsModelAsync(List<PlanningCalenderViewModel> plannings, DateTime start, DateTime end, List<int> groupEmployeeIds, int buildingLocationId)
        {
            List<Planning> items = _planningService.GetAllPlannings<Planning>(
                x =>
                     ((x.StartDate >= start && x.EndDate <= end) || (x.StartDate >= start && x.StartDate <= end) || (x.EndDate >= start && x.EndDate <= end) || (x.StartDate <= start && x.EndDate >= end))
                , loadOverviewIncludes: true).ToList();

            if (groupEmployeeIds != null && groupEmployeeIds.Any())
            {
                items = items.Where(x =>
                    groupEmployeeIds.Any(e => e == x.MainEmployeeId) ||
                    x.PlanningEmployees.Any(y => groupEmployeeIds.Any(e => e == y.EmployeeId))).ToList();
            }

            if (buildingLocationId > 0)
            {
                items = items.Where(x => x.BuildingLocationId == buildingLocationId).ToList();
            }
            foreach (Planning item in items)
            {
                string planningTitle = item.OfferNumber;

                if (item.Building != null)
                {
                    planningTitle += " " + item.Building.Name;
                    string address =
                        $"{item.Building?.Address} {item.Building?.City}";
                    if (string.IsNullOrWhiteSpace(address.Trim()))
                    {
                        address = "<em>Geen adres aanwezig</em>";
                    }
                    planningTitle += $"<br/>{address}";


                }
                if (item.Asset != null)
                {
                    planningTitle += "<br />" + item.Asset.Name;
                    if (item.AssetActivity != null)
                    {
                        planningTitle += " - " + item.AssetActivity.Name;
                    }
                }
                if (!string.IsNullOrWhiteSpace(item.Remark))
                {
                    planningTitle += "<br />" + item.Remark.MaxLength();
                }

                plannings.Add(new PlanningCalenderViewModel
                {
                    allDay = false,
                    id = item.Id.ToString(),
                    title = planningTitle,
                    editable = true,
                    color = GetColorForPlanningStatus(item.Status, false, item.CustomColor),
                    start = item.StartDate.ToString("s", System.Globalization.CultureInfo.InvariantCulture),
                    end = item.EndDate.ToString("s", System.Globalization.CultureInfo.InvariantCulture),
                    resourceIds = new List<int> { item.MainEmployeeId.GetValueOrDefault(0) }.ToArray(),
                    extraInfo = item.Notification
                });

                if (item.PlanningEmployees.Any())
                {
                    List<int> employeeIds = new List<int>();
                    employeeIds.AddRange(item.PlanningEmployees.Select(x => x.EmployeeId));
                    plannings.Add(new PlanningCalenderViewModel
                    {
                        allDay = false,
                        id = $"{item.Id}-sub",
                        title = planningTitle,
                        editable = false,
                        color = GetColorForPlanningStatus(item.Status, true, item.CustomColorAssistant),
                        start = item.StartDate.ToString("s", System.Globalization.CultureInfo.InvariantCulture),
                        end = item.EndDate.ToString("s", System.Globalization.CultureInfo.InvariantCulture),
                        resourceIds = employeeIds.ToArray(),
                        extraInfo = item.Notification
                    });
                }
            }

            IList<CalendarItem> calendarItems = await _calendarService.GetCalendarItemsByTimestampAsync(groupEmployeeIds, start, end);

            foreach (CalendarItem calendarItem in calendarItems)
            {
                if (groupEmployeeIds.Any() && !groupEmployeeIds.Contains(calendarItem.EmployeeId)) continue;

                plannings.Add(new PlanningCalenderViewModel
                {
                    allDay = false,
                    id = "cal-" + calendarItem.Id,
                    start = calendarItem.StartDate.ToString("s", System.Globalization.CultureInfo.InvariantCulture),
                    end = calendarItem.EndDate.ToString("s", System.Globalization.CultureInfo.InvariantCulture),
                    title = calendarItem.Subject,
                    resourceIds = new[] { calendarItem.EmployeeId },
                    editable = false,
                    color = "#ffd9a9",
                    textColor = "#000",
                    extraInfo = calendarItem.Remark
                });
            }
        }

        public PlanningIndexViewModel PrepareIndexViewModel(int? activity, int buildingLocationId, string employeeId, string groupId)
        {
            FusionCore.Data.Entities.User currentUser = _userService.GetCurrentUser();
            Dictionary<string, string> settings = _userService.GetSettings(currentUser);
            if (settings != null && settings.Any())
            {
                if (settings.ContainsKey("employeeId"))
                {
                    employeeId = settings["employeeId"];
                }
                if (settings.ContainsKey("groupId"))
                {
                    groupId = settings["groupId"];
                }
                if (settings.ContainsKey("buildingLocationId"))
                {
                    buildingLocationId = Convert.ToInt32(settings["buildingLocationId"]);
                }
            }
            string[] employeeids = employeeId?.Split(',');
            List<SelectListItem> employees = _employeeService.GetAllEmployees(x => x.Active, x => x.GetFullname)
                .Select(x => new SelectListItem
                {
                    Text = x.GetFullname,
                    Value = x.Id.ToString(),
                    Selected = employeeids != null && employeeids.Any(y => y == x.Id.ToString())
                }).ToList();

            string[] groupids = groupId?.Split(',');
            List<SelectListItem> groups = _employeeService.GetAllEmployeeGroups(null, x => x.Name)
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString(),
                    Selected = groupids != null && groupids.Any(y => y == x.Id.ToString())
                }).ToList();

            IEnumerable<BuildingLocation> buildingLocations = _buildingService.GetAllBuildings<Building>(loadLocations: true)
                .SelectMany(x => x.BuildingLocations);

            List<SelectListItem> availableLocations = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Value = "",
                    Text = "Selecteer"
                }
            };
            availableLocations.AddRange(buildingLocations.OrderBy(x => x.Building.Name).ThenBy(x => x.Name).Select(x => new SelectListItem
            {
                Selected = x.Id == buildingLocationId,
                Text = $"{x.Building.Name} - {x.Name}",
                Value = x.Id.ToString()
            }).ToList());

            PlanningIndexViewModel model = new PlanningIndexViewModel
            {
                AssetActivity = activity,
                AvailableEmployees = employees,
                AvailableGroups = groups,
                AvailableLocations = availableLocations,
                WeekNumber = DateTime.Now.GetWeekNumber()
            };
            return model;
        }

        private string GetColorForPlanningStatus(PlanningStatus status, bool assist, string itemCustomColor)
        {
            if (!string.IsNullOrEmpty(itemCustomColor))
            {
                return ConvertoRgbToHex(itemCustomColor);
            }
            switch (status)
            {
                case PlanningStatus.PrePlanning:
                    return assist ? "#dcaa76" : "#c06012";
                case PlanningStatus.PlanningActive:
                    return assist ? "#517eca" : "#1254c0";
                case PlanningStatus.TechnicallyReady:
                    return assist ? "#7bd65f" : "#16bb03";
                case PlanningStatus.Complete:
                    return assist ? "#72b860" : "#0f8e00";
                default:
                    // Niet uitgevoerd?
                    return "#c01213";
            }
        }

        private string ConvertoRgbToHex(string itemCustomColor)
        {
            string[] split = itemCustomColor.Split(',');
            int red = Convert.ToInt32(split[0].Trim());
            if (red == 0) red++;
            int green = Convert.ToInt32(split[1].Trim());
            if (green == 0) red++;
            int blue = Convert.ToInt32(split[2].Trim());
            if (blue == 0) red++;
            Color myColor = Color.FromArgb(red, green, blue);

            return $"#{myColor.R:X2}{myColor.G:X2}{myColor.B:X2}";
        }
    }
}
