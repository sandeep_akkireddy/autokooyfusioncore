using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.TeamManager.Data.Entities;
using FusionCore.Modules.TeamManager.Interfaces;
using FusionCore.Modules.TeamManager.Models;
using FusionCore.Services;
using FusionCore.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.TeamManager.Services
{
    public class TeamService : BaseService, ITeamService
    {

        private readonly IGenericRepository<Teams> _teamRepository;
        private readonly IMapper _mapper;
        public TeamService(
            IGenericRepository<Teams> teamRepository,
            IMapper mapper)
        {
            _teamRepository = teamRepository;
            _mapper = mapper;

        }
        public bool ActiveTeam(int id, bool value)
        {
            var dbEntity = GetTeam(id);
            if (dbEntity != null)
            {
                dbEntity.IsActive = value;
                _teamRepository.Update(dbEntity);

                return true;
            }
            return false;
        }

        public async Task DeleteTeam(int id)
        {
            var user = await _teamRepository.GetByIdAsync(id);
            if (user != null)
            {
                int PageOrder = user.SortOrder;

                var allpages = _teamRepository.GetAll(x => x.SortOrder > PageOrder).ToList();
                allpages.ForEach(x =>
                {
                    x.SortOrder -= 1;
                });
                _teamRepository.UpdateAll(allpages);

                _teamRepository.Delete(user);
            }
        }

        public List<TeamsPersistModel> GetActiveTeams()
        {
            var teams = _teamRepository.GetAll(x => x.IsActive).OrderByDescending(x => x.SortOrder).ToList();
            return _mapper.Map<List<TeamsPersistModel>>(teams);

        }

        public IPagedList<Teams> GetAllTeams<TKey>(Expression<Func<Teams, bool>> predicate = null, Expression<Func<Teams, TKey>> orderBy = null, bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var queryable = _teamRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Teams>(queryable, pageIndex, pageSize);
        }

        public Teams GetTeam(int id)
        {
            return _teamRepository.Get(x => x.Id == id);
        }

        public bool OrderPageDown(int id, int sortorder)
        {
            var nextpageorder = (sortorder + 1);
            var pages = _teamRepository.GetAll(x => x.Id == id || x.SortOrder == nextpageorder).ToList();
            var currentpage = pages.FirstOrDefault(x => x.Id == id);
            var nextpage = pages.FirstOrDefault(x => x.Id != id);
            if (currentpage != null)
            {
                currentpage.SortOrder += 1;
            }
            if (nextpage != null)
            {
                if (nextpage.SortOrder > 1)
                {
                    nextpage.SortOrder -= 1;
                }
            }
            if (currentpage != null && nextpage != null)
            {
                _teamRepository.UpdateAll(pages);
            }
            return false;
        }

        public bool OrderPageUp(int id, int sortorder)
        {
            var prev_pageorder = (sortorder - 1);
            prev_pageorder = prev_pageorder <= 0 ? 1 : prev_pageorder;
            var pages = _teamRepository.GetAll(x => x.Id == id || x.SortOrder == prev_pageorder).ToList();
            var currentpage = pages.FirstOrDefault(x => x.Id == id);
            var prev_page = pages.FirstOrDefault(x => x.Id != id);
            if (currentpage != null)
            {
                if (currentpage.SortOrder > 1)
                {
                    currentpage.SortOrder -= 1;
                }
            }
            if (prev_page != null)
            {
                prev_page.SortOrder += 1;
            }
            if (currentpage != null && prev_page != null)
            {
                _teamRepository.UpdateAll(pages);
            }
            return false;
        }

        public bool PersistTeam(TeamsPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                var dbEntity = GetTeam(model.Id.Value);
                if (dbEntity != null)
                {
                    dbEntity.IsActive = model.IsActive;
                    dbEntity.TeamLogoId = model.TeamLogoId;
                    dbEntity.Function = model.Function;
                    dbEntity.Name = model.Name;
                    dbEntity.PageContent = model.PageContent;
                    //dbEntity.SortOrder = model.SortOrder;
                    dbEntity.UpdatedOnUtc = DateTime.Now;
                    dbEntity.LinkedinURL = model.LinkedinURL;


                    _teamRepository.Update(dbEntity);


                    return true;
                }
            }
            else
            {

                //insert
                var dbEntity = new Teams();
                var max_pageorder_page = _teamRepository.GetAll().OrderByDescending(x => x.SortOrder).FirstOrDefault();
                if (max_pageorder_page != null)
                {
                    model.SortOrder = max_pageorder_page.SortOrder + 1;
                }
                else
                {
                    model.SortOrder = 1;
                }
                _mapper.Map(model, dbEntity);
                _teamRepository.Insert(dbEntity);

                return true;
            }

            return false;
        }
    }
}
