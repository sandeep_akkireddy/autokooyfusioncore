using FusionCore.Modules.TeamManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.TeamManager.Data.Maps
{
	public class TeamsMap : IEntityTypeConfiguration<Teams>
	{
		public void Configure(EntityTypeBuilder<Teams> builder)
		{
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedOnAdd();
			builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
			builder.Property(x => x.Function).HasMaxLength(256);
			
			builder.Property(x => x.SortOrder);
			builder.Property(x => x.IsActive);
			builder.Property(x => x.PageContent);
			builder.Property(x => x.CreatedOnUtc);
			builder.Property(x => x.UpdatedOnUtc);
			builder.Property(x => x.TeamLogoId);
            builder.Property(x=>x.LinkedinURL);
		}
	}
}
