﻿using FusionCore.Core.Enums;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.TeamManager.Data.Entities;
using FusionCore.Modules.TeamManager.Interfaces;
using FusionCore.Modules.TeamManager.Models;
using FusionCore.Services.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.TeamManager.Data
{
	public class TeamInstaller : IInstallData
	{
		private ITeamService _teamService;

		public async Task Install(IServiceProvider serviceProvider, bool sampleData)
		{
			LoadServices(serviceProvider);

			await InstallTeamManagerMenu(serviceProvider);
			if (sampleData)
			{
				InstallTeamsDummyData();
			}

			DisposeServices();
		}
		private void DisposeServices()
		{
			_teamService = null;

		}
		private void LoadServices(IServiceProvider serviceProvider)
		{
			_teamService = (ITeamService)serviceProvider.GetService(typeof(ITeamService));
		}

		private async Task InstallTeamManagerMenu(IServiceProvider serviceProvider)
		{
			var menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

			var mainMenuItem = new MenuItem
			{
				Type = MenuType.Backend,
				Icon = "fa fa-user",
				Name = "Team",
				Area = "TeamManager",
				Controller = "Teams",
				Action = "Index",
				Parameters = null,
				DisplayOrder = 0,
				ParentId = null,
				HasNewButton = true
			};
			await menuService.InsertMenuItem(mainMenuItem);

		}

		private void InstallTeamsDummyData()
		{
			for (var i = 1; i < 16; i++)
			{
				_teamService.PersistTeam(new TeamsPersistModel
				{
					PageContent = "Content " + i,
					Function = "Function " + i,
					IsActive = true,
					Name = "Team " + i,
					SortOrder = i,
				});
			}
		}
	}
}
