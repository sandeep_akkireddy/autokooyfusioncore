﻿using AutoMapper;
using FusionCore.Modules.TeamManager.Data.Entities;
using FusionCore.Modules.TeamManager.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.TeamManager.Data
{
	public class AutomapperConfiguration : Profile
	{
		public AutomapperConfiguration()
		{
			CreateMap<Teams, TeamDataRow>();

			CreateMap<Teams, TeamsPersistModel>();
			CreateMap<TeamsPersistModel, Teams>();

		}
	}
}
