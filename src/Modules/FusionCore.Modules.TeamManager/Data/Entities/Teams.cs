using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.TeamManager.Data.Entities
{
    public class Teams : BaseEntity
    {
        [MaxLength(256)]
        public string Name { get; set; }
        [MaxLength(256)]
        public string Function { get; set; }
        public string PageContent { get; set; }
        public bool IsActive { get; set; }

        public int SortOrder { get; set; }

        public int? TeamLogoId { get; set; }

        public string LinkedinURL { get; set; }
    }
}
