﻿using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.TeamManager.Models
{
	public class TeamsDataTable : BaseDataTable
	{
		public List<TeamDataRow> Data { get; set; }
	}

	public class TeamDataRow
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public bool IsActive { get; set; }

		public int SortOrder { get; set; }

		public int? TeamLogoId { get; set; }

	}
}
