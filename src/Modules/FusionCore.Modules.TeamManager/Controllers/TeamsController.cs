﻿using AutoMapper;
using FusionCore.Core.Exceptions;
using FusionCore.Core.Extensions;
using FusionCore.Modules.TeamManager.Data.Entities;
using FusionCore.Modules.TeamManager.Interfaces;
using FusionCore.Modules.TeamManager.Models;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.TeamManager.Controllers
{
	[Area("TeamManager")]
	public class TeamsController : FusionBaseController
	{
		private readonly ITeamService _teamService;
		private readonly IMapper _mapper;
	

		public TeamsController(ITeamService teamService, IMapper mapper)
		{
			_teamService = teamService;
			_mapper = mapper;
			
		}

		public IActionResult Index()
		{
			return View();
		}
		public JsonResult DataTable(TeamsDataTable model)
		{
			var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
				.FirstOrDefault();
			var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
			var searchValue = Request.Query["search[value]"].FirstOrDefault();

			var pageSize = model.Length;
			var pageIndex = model.Start / model.Length;

			Expression<Func<Teams, IComparable>> orderBy;

			switch (sortColumn)
			{
				case "title":
					orderBy = x => x.Name;
					break;
				default:
					orderBy = x => x.SortOrder;
					break;
			}

			Expression<Func<Teams, bool>> predicate = null;
			if (!string.IsNullOrEmpty(searchValue))
				predicate = x =>
						x.Name.ToLower().Contains(searchValue.ToLower())
						|| x.UpdatedOnUtc.ToLongDateString().Contains(searchValue.ToLower());

			var pages = _teamService.GetAllTeams(
				predicate,
				orderBy,
				sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
				pageIndex,
				pageSize);

			model.Data = _mapper.Map<List<TeamDataRow>>(pages);
			
			model.RecordsFiltered = pages.TotalCount;
			model.RecordsTotal = pages.TotalCount;
			return new JsonResult(model);
		}

		public IActionResult Get(int? id)
		{
			var model = id.HasValue ? _mapper.Map<TeamsPersistModel>(_teamService.GetTeam(id.Value)) : new TeamsPersistModel();

			return View(model);
		}

		[HttpPost]
		public IActionResult Persist(TeamsPersistModel model)
		{
			if (ModelState.IsValid)
			{
				return Json(_teamService.PersistTeam(model));
			}

			return Json(false);
		}

		public async Task<IActionResult> Delete(int id)
		{
			await _teamService.DeleteTeam(id);

			return RedirectToAction("Index");
		}

		[HttpPost]
		public IActionResult Active(int id, bool value)
		{
			return Json(_teamService.ActiveTeam(id, value));
		}

		public IActionResult OrderUp(int id, int order)
		{
			_teamService.OrderPageUp(id, order);

			return RedirectToAction("Index");
		}

		public IActionResult OrderDown(int id, int order)
		{
			_teamService.OrderPageDown(id, order);

			return RedirectToAction("Index");
		}

	}
}
