﻿using FusionCore.Core.Interfaces;
using System;

namespace FusionCore.Modules.TeamManager
{
	public class TeamManagerModule : IFusionCoreModule
	{
		public string GetSystemName()
		{
			return "FusionCore.Module.TeamManager";
		}
	}
}
