using FusionCore.Models.Common;
using FusionCore.Modules.TeamManager.Data.Entities;
using FusionCore.Modules.TeamManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.TeamManager.Interfaces
{
	public interface ITeamService : IBaseService
	{
		IPagedList<Teams> GetAllTeams<TKey>(
			Expression<Func<Teams, bool>> predicate = null,
			Expression<Func<Teams, TKey>> orderBy = null,
			bool asc = true,
			int pageIndex = 0,
			int pageSize = int.MaxValue);

		Teams GetTeam(int id);
		bool PersistTeam(TeamsPersistModel model);
		Task DeleteTeam(int id);

		bool ActiveTeam(int id, bool value);

		bool OrderPageUp(int id, int sortorder);

		bool OrderPageDown(int id, int sortorder);

        List<TeamsPersistModel> GetActiveTeams();
		
	}
}
