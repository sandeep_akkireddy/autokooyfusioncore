using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Data.Enums;
using FusionCore.Modules.FundsManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.FundsManager.Interfaces
{
    public interface IFundsService : IBaseService
    {
        IPagedList<Funds> GetAllFunds<TKey>(
            Expression<Func<Funds, bool>> predicate = null,
            Expression<Func<Funds, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Funds GetFund(int id);
        bool PersistFund(FundsPersistModel model,string fundfilepath=null);
        Task DeleteFund(int id, string fundfilepath = null);
        bool ActiveFund(int id, bool value);

        List<FundFiles> GetFundFiles(int fundId);
        FundFiles GetFundFile(int id);

        List<FundCategory> GetFundCategories(int fundId);

        List<FundFiles> GetFundCategoryFiles(int fundId);

        GeneralSubscriptions GetGeneralSubscription(int id);

        LegalSubscriptions GetLegalSubscription(int id);

        List<SubscriptionsDataRow> GetFundGeneralSubscriptions(
           int fundid, string searchValue, string sortColumn,
            bool asc = true);

        List<SubscriptionsDataRow> GetFundLegalSubscriptions(
          int fundid, string searchValue, string sortColumn,
           bool asc = true);


        bool PersistFundGeneralSubscription(GeneralSubscriptionsPersistModel model);
        bool PersistFundLegalSubscription(LegalSubscriptionsPersistModel model);

        bool UpdateIdentification(int subscribeid, SubscriptionsTypes type,string from);

        bool DeleteSubscription(int subscribeid, int fundid, SubscriptionsTypes type);

        GeneralSubscriptionsPersistModel GetGeneralSubscriptionsPersist(int id);

        LegalSubscriptionsPersistModel GetLegalSubscriptionsPersist(int id);

        Funds GetLatestFund();

        Funds GetFundByTitle(string fundtitle);
        bool UpdatePayment(int subscribeid, SubscriptionsTypes type, IdentificationTypes identify);
    }
}
