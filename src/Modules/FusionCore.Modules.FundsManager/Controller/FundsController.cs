using AutoMapper;
using FusionCore.Core.Enums;
using FusionCore.Core.Exceptions;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.ImageCrop;
using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Data.Enums;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Modules.FundsManager.Models;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.FundsManager.Controller
{
    [Area("FundsManager")]
    public class FundsController : FusionBaseController
    {
        private readonly IFundsService _fundService;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;

        public FundsController(IFundsService fundService, IMapper mapper, IHostingEnvironment env, IConfiguration configuration, ILogger<FundsController> logger)
        {
            _fundService = fundService;
            _mapper = mapper;
            _env = env;
            _configuration = configuration;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult DataTable(FundsDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Expression<Func<Funds, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "title":
                    orderBy = x => x.Title;
                    break;
                default:
                    orderBy = x => x.CreatedOnUtc;
                    //sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Funds, bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x => (
                        x.Title.ToLower().Contains(searchValue.ToLower())
                        || x.CreatedOnUtc.ToLongDateString().Contains(searchValue.ToLower()));

            var pages = _fundService.GetAllFunds(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<FundDataRow>>(pages);
            model.RecordsFiltered = pages.TotalCount;
            model.RecordsTotal = pages.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {

            HttpContext.Session.SetComplexData("selectedfundid", null);
            HttpContext.Session.SetComplexData("selectedfund", null);

            HttpContext.Session.SetComplexData("fundfiles", null);

            HttpContext.Session.SetComplexData("fundwebsitefiles", null);

            HttpContext.Session.SetComplexData("fundcategories", null);

            HttpContext.Session.SetComplexData("fundsubcategoryfiles", null);


            var model = new FundsPersistModel();
            //try
            //{
            if (id.HasValue)
            {
                HttpContext.Session.SetComplexData("selectedfundid", id.Value.ToString());

                model = _mapper.Map<FundsPersistModel>(_fundService.GetFund(id.Value));

                HttpContext.Session.SetComplexData("selectedfund", model);

                var fundfiles = _fundService.GetFundFiles(id.Value);
                model.FundFiles = _mapper.Map<List<FundFilePersistModel>>(fundfiles);
                var imagesfundfiles = model.FundFiles.Where(x => !x.websitedownloadfiles).ToList();
                var websitefundfiles = model.FundFiles.Where(x => x.websitedownloadfiles).ToList();

                imagesfundfiles.ForEach(x => { x.TempID = Guid.NewGuid().ToString(); });
                HttpContext.Session.SetComplexData("fundfiles", imagesfundfiles);

                HttpContext.Session.SetComplexData("fundwebsitefiles", websitefundfiles);

                var fundCategory = _fundService.GetFundCategories(id.Value);
                model.FundCategory = _mapper.Map<List<FundCategoryPersistModel>>(fundCategory);
                var parentcateg = model.FundCategory.Where(x => !x.ParentId.HasValue).ToList();
                var subcateg = model.FundCategory.Where(x => x.ParentId.HasValue).ToList();

                var fundCategoryfiles = _mapper.Map<List<FundFilePersistModel>>(_fundService.GetFundCategoryFiles(id.Value));
                List<FundFilePersistModel> subcategoryFiles = new List<FundFilePersistModel>();
                ICollection<FundCategoryPersistModel> finalcateglist = new List<FundCategoryPersistModel>();
                parentcateg.ForEach(x =>
                {
                    x.TempId = Guid.NewGuid().ToString();
                    subcateg.Where(z => z.ParentId == x.Id).ToList().ForEach(z =>
                    {
                        z.TempId = x.TempId;
                        z.SubCategTempId = Guid.NewGuid().ToString();
                        finalcateglist.Add(z);
                        fundCategoryfiles.Where(f => f.FundCategoryId == z.Id).ToList().ForEach(f =>
                        {
                            f.FundCategoryTempId = z.SubCategTempId;
                            f.TempID = Guid.NewGuid().ToString();

                            model.FundFiles.Add(f);
                            subcategoryFiles.Add(f);
                        });
                    });
                    finalcateglist.Add(x);
                });
                model.FundCategory = finalcateglist;
                HttpContext.Session.SetComplexData("fundcategories", model.FundCategory);

                HttpContext.Session.SetComplexData("fundsubcategoryfiles", subcategoryFiles);

            }
            //}
            //catch (Exception ex)
            //{
            //    _logger.LogError("Get Fund", ex.Message, ex.StackTrace, ex.InnerException);
            //}
            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(FundsPersistModel model)
        {
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundfiles");
            var websitefundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundwebsitefiles");
            if (websitefundfiles != null)
                fundfiles.AddRange(websitefundfiles);
            model.FundFiles = fundfiles;
            model.FundCategory = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            var fundsubcategfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundsubcategoryfiles");
            if (fundsubcategfiles != null)
                fundfiles.AddRange(fundsubcategfiles);

            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();

            if (ModelState.IsValid) return Json(_fundService.PersistFund(model, fileSettings.FundFiles));

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
            await _fundService.DeleteFund(id, fileSettings.FundFiles);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Active(int id, bool value)
        {
            return Json(_fundService.ActiveFund(id, value));
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FileUpload(List<IFormFile> file_data, int fundId)
        {
            try
            {
                FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
                MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

                if (file_data != null)
                {
                    file_data.ForEach(z =>
                     {
                         if (z.Length <= 0) throw new FusionCoreException("File is empty");

                         var fileName = FileService.RemoveInvalidCharacters(z);
                         using (var ms = new MemoryStream())
                         {
                             z.CopyTo(ms);

                             var fileBinary = ms.ToArray();
                             if (!Directory.Exists(fileSettings.FundFiles))
                             {
                                 Directory.CreateDirectory(fileSettings.FundFiles);
                             }

                             //string GuidFileName = Guid.NewGuid().ToString() + fileName;
                             //System.IO.File.WriteAllBytes(fileSettings.FundFiles + GuidFileName, fileBinary);
                             string GuidFileName = FileHelper.FilePath(fileSettings.FundFiles, fileName, fileBinary, false);
                             string filepath = fileSettings.FundFiles + GuidFileName;
                             //FileHelper.savebitmap(ms, filepath, 1024, 576, true);
                             ImageCropHelper.CropFundImage(fileBinary, filepath);
                             var fundFile = new FundFilePersistModel
                             {
                                 FundId = fundId,
                                 //File = fileBinary,
                                 FileName = fileName,
                                 TempID = Guid.NewGuid().ToString(),
                                 FilePath = GuidFileName
                             };

                             var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundfiles");
                             if (fundfiles != null && fundfiles.Any())
                             {
                                 fundFile.SortOrder = fundfiles.Max(x => x.SortOrder) + 1;

                                 fundfiles.Add(fundFile);
                             }
                             else
                             {
                                 fundfiles = new List<FundFilePersistModel>();
                                 fundFile.SortOrder = 1;
                                 fundfiles.Add(fundFile);
                             }
                             HttpContext.Session.SetComplexData("fundfiles", fundfiles);
                         }
                     });

                    var fund_files = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundfiles");
                    List<dynamic> response = new List<dynamic>();
                    fund_files.ForEach(x =>
                    {

                        //response.Add(new { id = 0, file = Convert.ToBase64String(x.File), tempId = x.TempID, title = x.FileName, append = true, sortOrder = x.SortOrder });
                        response.Add(new { id = 0, file = (mainSettings.FusionUrl + fileSettings.FundFileRelativePath + x.FilePath), tempId = x.TempID, title = x.FileName, append = true, sortOrder = x.SortOrder });
                    });
                    var json = JsonConvert.SerializeObject(response);

                    //return Ok(new { id = 0, file = Convert.ToBase64String(fileBinary), tempId = fundFile.TempID, title = fundFile.FileName, append = true, sortOrder = fundFile.SortOrder });
                    return Ok(new { json });
                }
                return BadRequest();
            }
            catch (Exception er)
            {
                return BadRequest($"Fout tijdens uploaden file_data: {er.Message}");
            }
        }

        public async Task<IActionResult> RemoveFile(int id, string tempId, int fundId)
        {
            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();

            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundfiles");
            if (fundfiles != null)
            {
                var current_fundfile = new FundFilePersistModel();
                if (id == 0)
                {
                    current_fundfile = fundfiles.FirstOrDefault(x => x.TempID == tempId);
                }
                else
                {
                    current_fundfile = fundfiles.FirstOrDefault(x => x.Id == id);
                }
                if (current_fundfile != null)
                {
                    fundfiles.Remove(current_fundfile);
                    fundfiles.Where(x => x.SortOrder > current_fundfile.SortOrder).ToList().ForEach(x =>
                    {
                        x.SortOrder -= 1;
                    });
                    HttpContext.Session.SetComplexData("fundfiles", fundfiles);

                    if (id == 0)
                    {
                        var file = fileSettings.FundFiles + current_fundfile.FilePath;
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                    }
                }
            }
            return Content("");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> WebsiteFileUpload(IFormFile file_data, int fileType, int fundId, string filename)
        {
            try
            {
                FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
                MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

                if (!string.IsNullOrEmpty(filename) && !string.IsNullOrWhiteSpace(filename))
                {
                    if (file_data != null)
                    {

                        if (file_data.Length <= 0) throw new FusionCoreException("File is empty");

                        var fileName = FileService.RemoveInvalidCharacters(file_data);
                        using (var ms = new MemoryStream())
                        {
                            await file_data.CopyToAsync(ms);
                            var fileBinary = ms.ToArray();
                            if (!Directory.Exists(fileSettings.FundFiles))
                            {
                                Directory.CreateDirectory(fileSettings.FundFiles);
                            }

                            string GuidFileName = Guid.NewGuid().ToString() + fileName;
                            System.IO.File.WriteAllBytes(fileSettings.FundFiles + GuidFileName, fileBinary);

                            var fundFile = new FundFilePersistModel
                            {
                                CreatedOnUtc = DateTime.UtcNow,
                                UpdatedOnUtc = DateTime.UtcNow,
                                //File = fileBinary,
                                FileName = fileName,
                                FundId = fundId,
                                TempID = Guid.NewGuid().ToString(),
                                websitedownloadfiles = true,
                                DisplayName = filename,
                                FilePath = GuidFileName
                            };


                            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundwebsitefiles");
                            if (fundfiles != null && fundfiles.Any())
                            {
                                fundFile.SortOrder = fundfiles.Max(x => x.SortOrder) + 1;
                                fundfiles.Add(fundFile);
                            }
                            else
                            {
                                fundfiles = new List<FundFilePersistModel>();
                                fundFile.SortOrder = 1;
                                fundfiles.Add(fundFile);
                            }
                            HttpContext.Session.SetComplexData("fundwebsitefiles", fundfiles);

                            // return Ok(new { id = 0, file = Convert.ToBase64String(fileBinary), tempId = fundFile.TempID, createdon = fundFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), title = fundFile.DisplayName, append = true, sortOrder = fundFile.SortOrder });
                            return Ok(new { id = 0, file = (mainSettings.FusionUrl + fileSettings.FundFileRelativePath + fundFile.FilePath), tempId = fundFile.TempID, createdon = fundFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), title = fundFile.DisplayName, append = true, sortOrder = fundFile.SortOrder, filename = fundFile.FileName });

                        }
                    }
                }
                return BadRequest();
            }
            catch (Exception er)
            {
                return BadRequest($"Fout tijdens uploaden file_data: {er.Message}");
            }
        }


        public IActionResult WebsiteFileDownload(int id, string tempFile)
        {
            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
            MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

            var fundfile = new FundFilePersistModel();
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundwebsitefiles");
            if (id > 0)
            {
                //pagefile = _mapper.Map<PageFilePersistModel>(_pageService.GetPageFile(id));
                fundfile = fundfiles.FirstOrDefault(x => x.Id == id);
            }
            else
            {
                fundfile = fundfiles.FirstOrDefault(x => x.TempID == tempFile);
            }

            if (fundfile != null)
            {
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = fundfile.FileName,
                    Inline = true,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());

                byte[] file = System.IO.File.ReadAllBytes(fileSettings.FundFiles + fundfile.FilePath);
                //return File(fundfile.File, FileHelper.GetMimeType(fundfile.FileName), fundfile.FileName);
                return File(file, FileHelper.GetMimeType(fundfile.FileName), fundfile.FileName);
            }

            return BadRequest();
        }


        public async Task<IActionResult> WebsiteFileOrderUp(int id, int order, string tempId)
        {
            int currentfileorder = 0, previousfileorder = 0;
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundwebsitefiles");
            if (fundfiles != null)
            {
                var current_fundfile = new FundFilePersistModel();
                if (id == 0)
                {
                    current_fundfile = fundfiles.FirstOrDefault(x => x.TempID == tempId);
                }
                else
                {
                    current_fundfile = fundfiles.FirstOrDefault(x => x.Id == id);
                }

                if (current_fundfile != null)
                {
                    if (current_fundfile.SortOrder > 1)
                    {
                        var prev_pagefile = fundfiles.FirstOrDefault(x => x.SortOrder == current_fundfile.SortOrder - 1);
                        if (prev_pagefile != null)
                        {
                            prev_pagefile.SortOrder += 1;

                            previousfileorder = prev_pagefile.SortOrder;
                        }
                        current_fundfile.SortOrder -= 1;

                        currentfileorder = current_fundfile.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("fundwebsitefiles", fundfiles);
            }

            return Json(new { currenteleorder = currentfileorder, previuoseleorder = previousfileorder });
        }

        public async Task<IActionResult> WebsiteFileOrderDown(int id, int order, string tempId)
        {
            int currentfileorder = 0, nextfileorder = 0;
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundwebsitefiles");
            if (fundfiles != null)
            {
                var current_pagefile = new FundFilePersistModel();
                if (id == 0)
                {
                    current_pagefile = fundfiles.FirstOrDefault(x => x.TempID == tempId);
                }
                else
                {
                    current_pagefile = fundfiles.FirstOrDefault(x => x.Id == id);
                }

                if (current_pagefile != null)
                {
                    var maxsortnumber = fundfiles.Max(x => x.SortOrder);
                    if (current_pagefile.SortOrder < maxsortnumber)
                    {
                        var next_pagefile = fundfiles.FirstOrDefault(x => x.SortOrder == current_pagefile.SortOrder + 1);
                        if (next_pagefile != null)
                        {
                            next_pagefile.SortOrder -= 1;
                            nextfileorder = next_pagefile.SortOrder;
                        }
                        current_pagefile.SortOrder += 1;
                        currentfileorder = current_pagefile.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("fundwebsitefiles", fundfiles);
            }

            return Json(new { currenteleorder = currentfileorder, nexteleorder = nextfileorder });
        }

        public async Task<IActionResult> WebsiteRemoveFile(int id, string tempId, int fundId)
        {
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundwebsitefiles");
            if (fundfiles != null)
            {
                FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();

                var current_fundfile = new FundFilePersistModel();
                if (id == 0)
                {
                    current_fundfile = fundfiles.FirstOrDefault(x => x.TempID == tempId);
                }
                else
                {
                    current_fundfile = fundfiles.FirstOrDefault(x => x.Id == id);
                }
                if (current_fundfile != null)
                {
                    int currentsortorder = current_fundfile.SortOrder;
                    fundfiles.Where(x => x.SortOrder > currentsortorder).ToList().ForEach(x =>
                    {
                        x.SortOrder -= 1;
                    });
                    fundfiles.Remove(current_fundfile);
                    HttpContext.Session.SetComplexData("fundwebsitefiles", fundfiles);
                    if (id == 0)
                    {
                        var file = fileSettings.FundFiles + current_fundfile.FilePath;
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                    }
                }
            }
            return Content("");
        }


        [HttpPost]
        public IActionResult AddCategory(string title)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories == null)
            {
                fundcategories = new List<FundCategoryPersistModel>();
            }
            string tempId = Guid.NewGuid().ToString();
            FundCategoryPersistModel fundcategory = new FundCategoryPersistModel
            {
                Title = title.Trim(),
                TempId = tempId,
                Active = true
            };
            fundcategories.Add(fundcategory);
            HttpContext.Session.SetComplexData("fundcategories", fundcategories);

            return Json(tempId);
        }

        [HttpPost]
        public IActionResult ActiveCategory(string id, bool value)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null)
            {
                var curr_categ = fundcategories.FirstOrDefault(x => x.TempId == id);
                if (curr_categ != null)
                {
                    curr_categ.Active = value;
                    HttpContext.Session.SetComplexData("fundcategories", fundcategories);
                }
            }
            return Json(false);
        }

        [HttpPost]
        public IActionResult AddSubCategory(string title, string categid)
        {
            FundCategoryPersistModel fundcategory = new FundCategoryPersistModel();
            int max_sort_order = 1;
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories == null)
            { fundcategories = new List<FundCategoryPersistModel>(); }

            if (fundcategories.Any(x => x.TempId == categid && !string.IsNullOrEmpty(x.SubCategTempId)))
            {
                max_sort_order = fundcategories.Where(x => x.TempId == categid && !string.IsNullOrEmpty(x.SubCategTempId)).Max(x => x.SortOrder) + 1;
            }
            fundcategory.SubCategTempId = Guid.NewGuid().ToString();
            fundcategory.SortOrder = max_sort_order;
            fundcategory.Title = title.Trim();
            fundcategory.TempId = categid;
            fundcategory.Active = true;

            fundcategories.Add(fundcategory);

            HttpContext.Session.SetComplexData("fundcategories", fundcategories);

            return Json(fundcategory);
        }

        public async Task<IActionResult> subcategoryOrderUp(int order, string subcategtempId, string categid)
        {
            int currentfileorder = 0, previousfileorder = 0;
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");

            if (fundcategories != null)
            {
                var current_fundcategory = new FundCategoryPersistModel();

                current_fundcategory = fundcategories.FirstOrDefault(x => x.SubCategTempId == subcategtempId);

                if (current_fundcategory != null)
                {
                    if (current_fundcategory.SortOrder > 1)
                    {
                        var prev_pagefile = fundcategories.FirstOrDefault(x => x.TempId == categid && x.SortOrder == current_fundcategory.SortOrder - 1);
                        if (prev_pagefile != null)
                        {
                            prev_pagefile.SortOrder += 1;

                            previousfileorder = prev_pagefile.SortOrder;
                        }
                        current_fundcategory.SortOrder -= 1;

                        currentfileorder = current_fundcategory.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("fundcategories", fundcategories);

            }

            return Json(new { currenteleorder = currentfileorder, previuoseleorder = previousfileorder });
        }

        public async Task<IActionResult> subcategoryOrderDown(int order, string subcategtempId, string categid)
        {
            int currentfileorder = 0, nextfileorder = 0;
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null)
            {
                var current_fundcateg = new FundCategoryPersistModel();

                current_fundcateg = fundcategories.FirstOrDefault(x => x.SubCategTempId == subcategtempId);


                if (current_fundcateg != null)
                {
                    var maxsortnumber = fundcategories.Any(x => x.TempId == categid) ? fundcategories.Where(x => x.TempId == categid).Max(x => x.SortOrder) : 0;
                    if (current_fundcateg.SortOrder < maxsortnumber)
                    {
                        var next_pagefile = fundcategories.FirstOrDefault(x => x.TempId == categid && x.SortOrder == current_fundcateg.SortOrder + 1);
                        if (next_pagefile != null)
                        {
                            next_pagefile.SortOrder -= 1;
                            nextfileorder = next_pagefile.SortOrder;
                        }
                        current_fundcateg.SortOrder += 1;
                        currentfileorder = current_fundcateg.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("fundcategories", fundcategories);
            }

            return Json(new { currenteleorder = currentfileorder, nexteleorder = nextfileorder });
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> subcategoryfileFileUpload(IFormFile file_data, int fileType, string subcategId, string filename)
        {
            try
            {
                if (!string.IsNullOrEmpty(filename) && !string.IsNullOrWhiteSpace(filename))
                {
                    FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
                    MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

                    if (file_data != null)
                    {

                        if (file_data.Length <= 0) throw new FusionCoreException("File is empty");

                        var fileName = FileService.RemoveInvalidCharacters(file_data);
                        using (var ms = new MemoryStream())
                        {
                            await file_data.CopyToAsync(ms);
                            var fileBinary = ms.ToArray();
                            if (!Directory.Exists(fileSettings.FundFiles))
                            {
                                Directory.CreateDirectory(fileSettings.FundFiles);
                            }

                            string GuidFileName = Guid.NewGuid().ToString() + fileName;
                            System.IO.File.WriteAllBytes(fileSettings.FundFiles + GuidFileName, fileBinary);

                            var fundFile = new FundFilePersistModel
                            {
                                CreatedOnUtc = DateTime.UtcNow,
                                UpdatedOnUtc = DateTime.UtcNow,
                                // File = fileBinary,
                                FileName = fileName,
                                FundCategoryTempId = subcategId,
                                TempID = Guid.NewGuid().ToString(),
                                websitedownloadfiles = true,
                                DisplayName = filename,
                                FilePath = GuidFileName
                            };


                            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundsubcategoryfiles");
                            if (fundfiles != null && fundfiles.Any(x => x.FundCategoryTempId == subcategId))
                            {
                                fundFile.SortOrder = fundfiles.Where(x => x.FundCategoryTempId == subcategId).Max(x => x.SortOrder) + 1;
                                fundfiles.Add(fundFile);
                            }
                            else
                            {
                                if (fundfiles == null)
                                {
                                    fundfiles = new List<FundFilePersistModel>();
                                }
                                fundFile.SortOrder = 1;
                                fundfiles.Add(fundFile);
                            }
                            HttpContext.Session.SetComplexData("fundsubcategoryfiles", fundfiles);

                            // return Ok(new { file = Convert.ToBase64String(fileBinary), tempId = fundFile.TempID, FundCategoryTempId = fundFile.FundCategoryTempId, createdon = fundFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), title = fundFile.DisplayName, append = true, sortOrder = fundFile.SortOrder });
                            return Ok(new { file = (mainSettings.FusionUrl + fileSettings.FundFileRelativePath + fundFile.FilePath), tempId = fundFile.TempID, FundCategoryTempId = fundFile.FundCategoryTempId, createdon = fundFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), title = fundFile.DisplayName, append = true, sortOrder = fundFile.SortOrder, filename = fundFile.FileName });
                        }
                    }
                }
                return BadRequest();
            }
            catch (Exception er)
            {
                return BadRequest($"Fout tijdens uploaden file_data: {er.Message}");
            }
        }

        public IActionResult subcategoryfileFileDownload(string tempFile)
        {
            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
            MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

            var fundfile = new FundFilePersistModel();
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundsubcategoryfiles");

            fundfile = fundfiles.FirstOrDefault(x => x.TempID == tempFile);

            if (fundfile != null)
            {
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = fundfile.FileName,
                    Inline = true,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                byte[] file = System.IO.File.ReadAllBytes(fileSettings.FundFiles + fundfile.FilePath);
                //return File(fundfile.File, FileHelper.GetMimeType(fundfile.FileName), fundfile.FileName);
                return File(file, FileHelper.GetMimeType(fundfile.FileName), fundfile.FileName);
            }

            return BadRequest();
        }

        public IActionResult subcategoryFileOrderUp(int order, string tempId, string tempFile)
        {
            int currentfileorder = 0, previousfileorder = 0;
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundsubcategoryfiles");
            if (fundfiles != null)
            {
                var current_fundfile = new FundFilePersistModel();

                current_fundfile = fundfiles.FirstOrDefault(x => x.TempID == tempId);

                if (current_fundfile != null)
                {
                    if (current_fundfile.SortOrder > 1)
                    {
                        var oldsortorder = current_fundfile.SortOrder;
                        var updateorder = current_fundfile.SortOrder - 3;
                        if (updateorder < 1)
                        {
                            updateorder = 1;

                            current_fundfile.SortOrder = 1;
                        }
                        else
                        {
                            current_fundfile.SortOrder = updateorder;
                        }

                        fundfiles.Where(x => x.FundCategoryTempId == tempFile && x.TempID != tempId && x.SortOrder >= updateorder && x.SortOrder < oldsortorder).OrderBy(x => x.SortOrder).ToList().ForEach(x =>
                            {
                                updateorder++;
                                x.SortOrder = updateorder;
                            });

                        //var prev_pagefile = fundfiles.FirstOrDefault(x => x.FundCategoryTempId == tempFile && x.SortOrder == current_fundfile.SortOrder - 1);
                        //if (prev_pagefile != null)
                        //{
                        //    prev_pagefile.SortOrder += 1;

                        //    previousfileorder = prev_pagefile.SortOrder;
                        //}
                        //current_fundfile.SortOrder -= 1;

                        //currentfileorder = current_fundfile.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("fundsubcategoryfiles", fundfiles);
            }
            var filesorder = fundfiles.Where(x => x.FundCategoryTempId == tempFile).OrderBy(x => x.SortOrder).Select(x => x.TempID).ToArray();
            // return Json(new { currenteleorder = currentfileorder, previuoseleorder = previousfileorder });
            return Json(new { order = filesorder });
        }

        public IActionResult subcategoryFileOrderDown(int order, string tempId, string tempFile)
        {
            int currentfileorder = 0, nextfileorder = 0;
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundsubcategoryfiles");
            if (fundfiles != null)
            {
                var current_pagefile = new FundFilePersistModel();

                current_pagefile = fundfiles.FirstOrDefault(x => x.TempID == tempId);

                if (current_pagefile != null)
                {
                    var maxsortnumber = fundfiles.Any(x => x.FundCategoryTempId == tempFile) ? fundfiles.Where(x => x.FundCategoryTempId == tempFile).Max(x => x.SortOrder) : 0;
                    if (current_pagefile.SortOrder < maxsortnumber)
                    {
                        var oldsortorder = current_pagefile.SortOrder;
                        var updateorder = current_pagefile.SortOrder + 3;
                        if (updateorder > maxsortnumber)
                        {
                            current_pagefile.SortOrder = maxsortnumber;
                        }
                        else
                        {
                            current_pagefile.SortOrder = updateorder;
                        }

                        fundfiles.Where(x => x.FundCategoryTempId == tempFile && x.TempID != tempId && x.SortOrder > oldsortorder && x.SortOrder <= updateorder).OrderBy(x => x.SortOrder).ToList().ForEach(x =>
                          {
                              x.SortOrder = oldsortorder;
                              oldsortorder++;
                          });
                        //var next_pagefile = fundfiles.FirstOrDefault(x => x.FundCategoryTempId == tempFile && x.SortOrder == current_pagefile.SortOrder + 1);
                        //if (next_pagefile != null)
                        //{
                        //    next_pagefile.SortOrder -= 1;
                        //    nextfileorder = next_pagefile.SortOrder;
                        //}
                        //current_pagefile.SortOrder += 1;
                        //currentfileorder = current_pagefile.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("fundsubcategoryfiles", fundfiles);
            }
            var filesorder = fundfiles.Where(x => x.FundCategoryTempId == tempFile).OrderBy(x => x.SortOrder).Select(x => x.TempID).ToArray();
            //return Json(new { currenteleorder = currentfileorder, nexteleorder = nextfileorder });
            return Json(new { order = filesorder });
        }

        public IActionResult subcategoryRemoveFile(string tempId, string tempFile)
        {
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundsubcategoryfiles");
            if (fundfiles != null)
            {
                FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();

                var current_fundfile = new FundFilePersistModel();

                current_fundfile = fundfiles.FirstOrDefault(x => x.TempID == tempId);

                if (current_fundfile != null)
                {
                    int currentsortorder = current_fundfile.SortOrder;
                    fundfiles.Where(x => x.FundCategoryTempId == tempFile && x.SortOrder > currentsortorder).ToList().ForEach(x =>
                      {
                          x.SortOrder -= 1;
                      });
                    fundfiles.Remove(current_fundfile);
                    HttpContext.Session.SetComplexData("fundsubcategoryfiles", fundfiles);
                    if (current_fundfile.Id == 0)
                    {
                        var file = fileSettings.FundFiles + current_fundfile.FilePath;
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                    }
                }
            }
            return Content("");
        }

        public IActionResult subcategoryRemove(string tempId, string tempFile)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null && fundcategories.Any(x => x.TempId == tempId && x.SubCategTempId == tempFile))
            {
                var deletesubcateg = fundcategories.FirstOrDefault(x => x.TempId == tempId && x.SubCategTempId == tempFile);
                if (deletesubcateg != null)
                {
                    int currentsortorder = deletesubcateg.SortOrder;
                    fundcategories.Where(x => x.TempId == tempId && x.SortOrder > currentsortorder).ToList().ForEach(x =>
                    {
                        x.SortOrder -= 1;
                    });
                    fundcategories.Remove(deletesubcateg);
                    HttpContext.Session.SetComplexData("fundcategories", fundcategories);
                }
            }
            return Content("");
        }

        public IActionResult categoryRemove(string categid)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null)
            {
                var deletecateg = fundcategories.Where(x => x.TempId == categid);
                if (deletecateg != null && deletecateg.Any())
                {
                    int currentsortorder = deletecateg.FirstOrDefault(x => x.TempId == categid && string.IsNullOrEmpty(x.SubCategTempId)).SortOrder;
                    fundcategories.Where(x => x.SortOrder > currentsortorder && string.IsNullOrEmpty(x.SubCategTempId)).ToList().ForEach(x =>
                   {
                       x.SortOrder -= 1;
                   });
                    fundcategories = fundcategories.Where(x => x.TempId != categid).ToList();
                    HttpContext.Session.SetComplexData("fundcategories", fundcategories);
                }
            }
            return Content("");
        }

        public JsonResult subscriptionslistDataTable(SubscriptionsDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            var fundid_string = HttpContext.Session.GetComplexData<string>("selectedfundid");
            int fundid = 0;
            if (!string.IsNullOrEmpty(fundid_string))
            {
                fundid = Convert.ToInt32(fundid_string);
            }
            var general_pages = _fundService.GetFundGeneralSubscriptions(fundid, searchValue, sortColumn,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase));

            var legal_pages = _fundService.GetFundLegalSubscriptions(fundid, searchValue, sortColumn,
             sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase));

            List<SubscriptionsDataRow> _data = new List<SubscriptionsDataRow>();
            _data.AddRange(general_pages);
            _data.AddRange(legal_pages);
            _data = _data.OrderByDescending(x => x.CreateOn).ToList();

            //_data = _data.OrderByDescending(x => x.CreateOn).ToList();//below sorting the list order shown

            model.SubscribedRegistrations = _data.Count.ToString();
            model.CompletedRegistrations = _data.Count(x => x.isIdentified && x.Idealpaymentstatus==IDealPaymentStatus.paid).ToString();

            var PossibleCapital = Math.Round(_data.Sum(x => x.participation_Price), 2);
            //PossibleCapital = PossibleCapital - Math.Floor(PossibleCapital);
            if (PossibleCapital > 0)
            {
                model.PossibleCapital = "€ " + string.Format("{0:n}", Math.Round(PossibleCapital, 2));
            }
            else
            {
                model.PossibleCapital = "€ " + string.Format("{0:n0}", Math.Round(PossibleCapital, 2)) + ",-";
            }

            var TotalCapital = Math.Round(_data.Where(x => x.isIdentified && x.Idealpaymentstatus == IDealPaymentStatus.paid).Sum(x => x.participation_Price), 2);
            //TotalCapital = TotalCapital - Math.Floor(TotalCapital);
            if (TotalCapital > 0)
            {
                model.TotalCapital = "€ " + string.Format("{0:n}", Math.Round(TotalCapital, 2));
            }
            else
            {
                model.TotalCapital = "€ " + string.Format("{0:n0}", Math.Round(TotalCapital, 2)) + ",-";
            }

            var notidentified = _data.Where(x => !x.isIdentified).OrderByDescending(x => x.CreateOn).ToList();//.OrderByDescending(x => x.id).ToList();
            var identified = _data.Where(x => x.isIdentified).OrderBy(x => x.Idealpaymentstatus).ThenByDescending(x => x.CreateOn).ToList();//.ThenByDescending(x => x.id).ToList();
            notidentified.AddRange(identified);

            IPagedList<SubscriptionsDataRow> pages = new PagedList<SubscriptionsDataRow>(notidentified, pageIndex, pageSize);

            List<SubscriptionsDataRow> subscriptionsDataRows = pages.ToList();
           // var notidentified = pages.Where(x => !x.isIdentified).OrderByDescending(x => x.CreateOn).ToList();//.OrderByDescending(x => x.id).ToList();
            //var identified = pages.Where(x => x.isIdentified).OrderBy(x => x.Idealpaymentstatus).ThenByDescending(x => x.CreateOn).ToList();//.ThenByDescending(x => x.id).ToList();
            //notidentified.AddRange(identified);
            model.Data = subscriptionsDataRows;

            model.RecordsFiltered = pages.TotalCount;
            model.RecordsTotal = pages.TotalCount;
            return new JsonResult(model);
        }

        [HttpPost]
        public ActionResult Identify(int subscribeid, SubscriptionsTypes type,string from)
        {
            return Json(_fundService.UpdateIdentification(subscribeid, type,from));
        }

        public ActionResult Deletesubscription(int id, int fundid, SubscriptionsTypes type)
        {
            return Json(_fundService.DeleteSubscription(id, fundid, type));
        }

        [HttpGet]
        public ActionResult GetSubscription(int id, SubscriptionsTypes type)
        {
            var fund = HttpContext.Session.GetComplexData<FundsPersistModel>("selectedfund");
            switch (type)
            {
                case SubscriptionsTypes.General:
                    var generalsubscription = _fundService.GetGeneralSubscriptionsPersist(id);
                    generalsubscription.fundsPersistModel = fund;
                    return PartialView("_GeneralSubscription", generalsubscription);
                case SubscriptionsTypes.Legal:
                    var legalsubscription = _fundService.GetLegalSubscriptionsPersist(id);
                    legalsubscription.fundsPersistModel = fund;
                    return PartialView("_LegalSubscription", legalsubscription);
                default:
                    return Json(false);
            }

        }

        private string[] MapNatuurlijkPersoonForm(GeneralSubscriptionsPersistModel gsModel, IFormCollection collection)
        {
            string[] formVelden = new string[39];
            formVelden[0] = gsModel.fundsPersistModel.Title;
            formVelden[1] = gsModel.LastName;
            formVelden[2] = gsModel.Title;
            formVelden[3] = gsModel.FirstName;
            formVelden[4] = gsModel.Sex;
            formVelden[5] = gsModel.BirthPlace;
            formVelden[6] = gsModel.DateOfBirth;
            formVelden[7] = gsModel.HomeAddress;
            formVelden[8] = gsModel.PostalCode;
            formVelden[9] = gsModel.Residence;

            formVelden[10] = gsModel.CorrespondenceAddressType ? "Ja" : "Nee";
            formVelden[11] = gsModel.CorrespondenceAddress;
            formVelden[12] = gsModel.CorrespondencePostalCode;
            formVelden[13] = gsModel.CorrespondenceResidence;
            formVelden[14] = gsModel.Phone;
            formVelden[15] = gsModel.MobilePhone;
            formVelden[16] = gsModel.Email;
            formVelden[17] = gsModel.AccountNumber;
            formVelden[18] = gsModel.Iban;

            formVelden[19] = gsModel.Identificationtype.HasValue ? gsModel.Identificationtype.Value.ToString() : "";//collection["Identificationtype"];
            formVelden[20] = gsModel.IssuedAt;
            formVelden[21] = gsModel.IssuedOn;
            formVelden[22] = gsModel.ValidityDate;
            formVelden[23] = gsModel.MartialStatus;
            formVelden[24] = gsModel.MarriedWithContract;

            formVelden[25] = gsModel.MarriedWithoutContract;
            formVelden[26] = gsModel.SpouseLastName;
            formVelden[27] = gsModel.SpouseFirstName;
            formVelden[28] = gsModel.SpouseSex;
            formVelden[29] = gsModel.SpouseBirthPlace;
            formVelden[30] = gsModel.SpouseDateOfBirth;
            formVelden[31] = gsModel.NumberofParticipations;
            formVelden[32] = gsModel.AgreementStatus ? "Ja" : "Nee";


            formVelden[33] = collection["WWFT"] == "Wel" ? "Wel" : "Niet";

            formVelden[34] = collection["SalesCompany"] == "on" ? "Ja" : "Nee";
            formVelden[35] = collection["SalaryBonusWork"] == "on" ? "Ja" : "Nee";
            formVelden[36] = collection["Heritage"] == "on" ? "Ja" : "Nee";
            formVelden[37] = collection["BusinessActivities"] == "on" ? "Ja" : "Nee";
            formVelden[38] = collection["Others"] == "on" ? collection["OtherAssets"].ToString() : "Nee";

            return formVelden;
        }

        [HttpPost]
        public ActionResult SubscriptionsPersist(GeneralSubscriptionsPersistModel gsModel, IFormFile paspoort, IFormCollection collection, IFormFile uittreksel, LegalSubscriptionsPersistModel modellegal)
        {
            string _email = gsModel.Email;
            string _clientname = gsModel.FirstName + " " + gsModel.LastName, _fusionmailtemplate = string.Empty, _fundname = gsModel.fundsPersistModel.Title;
            bool wwft = collection["WWFT"] == "Wel";
            if (!string.IsNullOrEmpty(modellegal.RegisteredName))
            {
                modellegal.AgreementStatus = (collection["AgreementStatus"].ToString() == "Wel");
                modellegal.SalesCompany = modellegal.LegalSalesCompany;
                modellegal.SalaryBonusWork = modellegal.LegalSalaryBonusWork;
                modellegal.Heritage = modellegal.LegalHeritage;
                modellegal.BusinessActivities = modellegal.LegalBusinessActivities;
                modellegal.Others = modellegal.LegalOthers;
                modellegal.OtherAssets = modellegal.LegalOtherAssets;
                modellegal.WWFT = wwft;

                _fundService.PersistFundLegalSubscription(modellegal);
            }
            else
            {
                gsModel.AgreementStatus = (collection["AgreementStatus"].ToString() == "Wel");
                gsModel.CorrespondenceAddressType = (collection["CorrespondenceAddressType"].ToString().ToLower() == "true");
                gsModel.WWFT = wwft;

                _fundService.PersistFundGeneralSubscription(gsModel);
            }


            var config = _configuration.GetSection("MainSettings").Get<MainSettings>();
            var subscriptionemails = config.SubscriptionEmails;
            bool natuurlijk = collection["onderwerp"].Contains("Natuurlijk");
            string templateName = natuurlijk ? "email-sectie5-inschrijving-natuurlijk-persoon.htm" : "email-sectie5-inschrijving-rechtspersoon.htm";

            string template;
            var filepath = _env.ContentRootPath + "\\App_Data\\Templates\\";
            using (StreamReader sr = new StreamReader(filepath + templateName))
            {
                template = sr.ReadToEnd();
            }
            string[] formVelden = natuurlijk ? MapNatuurlijkPersoonForm(gsModel, collection) : MapRechtspoonForm(collection);

            string emailText = string.Format(template, formVelden);

            MailFunction mailFunction = GetMailFunction("info@novimedia.net", "Novi Media", subscriptionemails, "Sectie5 investments NV", emailText, true, "Inschrijving");
            //try
            //{
            if (paspoort != null)
            {
                MemoryStream memoryStream = new MemoryStream();
                paspoort.CopyTo(memoryStream);
                memoryStream.Position = 0;
                string filename = Guid.NewGuid().ToString() + paspoort.FileName;
                mailFunction.AttachmentsStreams.Add(filename, memoryStream);

            }
            if (uittreksel != null)
            {
                MemoryStream memoryStream = new MemoryStream();

                uittreksel.CopyTo(memoryStream);
                memoryStream.Position = 0;
                string filename = Guid.NewGuid().ToString() + uittreksel.FileName;
                mailFunction.AttachmentsStreams.Add(filename, memoryStream);

            }
            mailFunction.SendEmail();
            //}
            //catch (Exception ex)
            //{
            //  _logger.LogError("Novi Media Solutions email", ex);

            //}
            if (natuurlijk)
            {
                if (gsModel.Identificationtype.HasValue && gsModel.IdentificationPaymenttype.HasValue)
                {
                    //    switch (gsModel.Identificationtype.Value)
                    //    {
                    //        case IdentificationTypes.Idin_Ideal:
                    //            _fusionmailtemplate = "subscription-idin-ideal.htm";
                    //            formVelden = MapIdeal_PostandIdin(gsModel);
                    //            break;
                    //        case IdentificationTypes.Idin_Bank:
                    //            _fusionmailtemplate = "subscription-idin-bank.htm";
                    //            formVelden = MapBank_PostandIdin(gsModel);
                    //            break;
                    //        case IdentificationTypes.Post_Ideal:
                    //            _fusionmailtemplate = "subscription-post-ideal.htm";
                    //            formVelden = MapIdeal_PostandIdin(gsModel);
                    //            break;
                    //        case IdentificationTypes.Post_Bank:
                    //            _fusionmailtemplate = "subscription-post-bank.htm";
                    //            formVelden = MapBank_PostandIdin(gsModel);
                    //            break;
                    //        default:
                    //            break;
                    //    }
                    int identificationtype = (int)gsModel.Identificationtype.Value, identificationpaymenttype = (int)gsModel.IdentificationPaymenttype.Value;

                    if (identificationtype == 1 && identificationpaymenttype == 3)
                    {
                        //idin-ideal
                        _fusionmailtemplate = "subscription-idin-ideal.htm";
                        formVelden = MapIdeal_PostandIdin(gsModel);
                    }
                    else if (identificationtype == 1 && identificationpaymenttype == 4)
                    {
                        //idin-bank
                        _fusionmailtemplate = "subscription-idin-bank.htm";
                        formVelden = MapBank_PostandIdin(gsModel);
                    }
                    else if (identificationtype == 2 && identificationpaymenttype == 3)
                    {
                        //post-bank
                        _fusionmailtemplate = "subscription-post-bank.htm";
                        formVelden = MapBank_PostandIdin(gsModel);
                    }
                    else if (identificationtype == 2 && identificationpaymenttype == 4)
                    {
                        //post-ideal
                        _fusionmailtemplate = "subscription-post-ideal.htm";
                        formVelden = MapIdeal_PostandIdin(gsModel);
                    }

                }

            }
            else {
                if (gsModel.IdentificationPaymenttype.HasValue) {
                    int identificationpaymenttype = (int)gsModel.IdentificationPaymenttype.Value;
                    if (identificationpaymenttype == 3)
                    {
                        //post-bank
                        _fusionmailtemplate = "subscription-post-bank.htm";
                        formVelden = MapBank_PostandIdin(gsModel);
                    }
                    else if (identificationpaymenttype == 4)
                    {
                        //post-ideal
                        _fusionmailtemplate = "subscription-post-ideal.htm";
                        formVelden = MapIdeal_PostandIdin(gsModel);
                    }
                }
            }
            if (!string.IsNullOrEmpty(_fusionmailtemplate) && config.EnableClientSubscriptionMails)
            {

                using (StreamReader sr = new StreamReader(filepath + _fusionmailtemplate))
                {
                    template = sr.ReadToEnd();
                }

                emailText = string.Format(template, formVelden);
                string subject = string.Format("Uw inschrijving voor {0}", _fundname);
                MailFunction fuionmail = GetMailFunction("participaties@sectie5.nl", "Sectie5 investments NV", _email, _clientname, emailText, true, subject);
                //try
                //{

                fuionmail.SendEmail();
                //}
                //catch (Exception ex)
                //{
                //  _logger.LogError("Sectie5 investments NV", ex);
                //}
            }
            return Json(true);
        }
        private MailFunction GetMailFunction(string emailfrom, string emailfromname, string emailto, string emailtoname, string body, bool ishtml, string subject)
        {
            EmailSettings emailSettings = _configuration.GetSection("EmailSettings").Get<EmailSettings>();

            MailFunction mailFunction = new MailFunction(emailSettings)
            {
                EmailFrom = emailfrom,
                EmailFromName = emailfromname,
                EmailTo = emailto,
                EmailToName = emailtoname,
                Body = body,
                IsHtml = ishtml,
                Subject = subject,
            };

            return mailFunction;
        }
        private string[] MapIdeal_PostandIdin(GeneralSubscriptionsPersistModel gsmodel)
        {
            string[] formvalues = new string[4];
            formvalues[0] = gsmodel.FirstName + " " + gsmodel.LastName;
            formvalues[1] = gsmodel.fundsPersistModel.Title;
            formvalues[2] = gsmodel.NumberofParticipations;
            formvalues[3] = gsmodel.fundsPersistModel.Title;
            return formvalues;
        }

        private string[] MapBank_PostandIdin(GeneralSubscriptionsPersistModel gsmodel)
        {
            string[] formvalues = new string[10];
            formvalues[0] = gsmodel.FirstName + " " + gsmodel.LastName;
            formvalues[1] = gsmodel.fundsPersistModel.Title;
            formvalues[2] = gsmodel.NumberofParticipations;
            formvalues[3] = gsmodel.NumberofParticipations;
            formvalues[4] = gsmodel.fundsPersistModel.participation_price.ToString();
            formvalues[5] = gsmodel.fundsPersistModel.emission_cost.ToString();
            decimal total = 0,emissionval=0;
            if (!string.IsNullOrEmpty(gsmodel.NumberofParticipations) && !string.IsNullOrWhiteSpace(gsmodel.NumberofParticipations) && !string.IsNullOrEmpty(gsmodel.fundsPersistModel.participation_price.ToString()) && !string.IsNullOrWhiteSpace(gsmodel.fundsPersistModel.participation_price.ToString())
                && !string.IsNullOrEmpty(gsmodel.fundsPersistModel.emission_cost.ToString()) && !string.IsNullOrWhiteSpace(gsmodel.fundsPersistModel.emission_cost.ToString()))
            {
                emissionval = gsmodel.fundsPersistModel.emission_cost;
                total = (Convert.ToInt32(gsmodel.NumberofParticipations) * gsmodel.fundsPersistModel.participation_price) + emissionval;
            }

            formvalues[6] = total.ToString();
            formvalues[7] = gsmodel.Iban;
            formvalues[8] = gsmodel.fundsPersistModel.Title;
            formvalues[9] = formvalues[3] + " X " + formvalues[4] + (emissionval > 0 ? (" + " + emissionval) : "") + " = " + total;

            return formvalues;
        }

        private string[] MapRechtspoonForm(IFormCollection collection)
        {
            string[] formVelden = new string[33];
            formVelden[0] = collection["Fondsnaam"];
            formVelden[1] = collection["RegisteredName"];
            formVelden[2] = collection["RegisteredAddress"];
            formVelden[3] = collection["Location"];
            formVelden[4] = collection["PostalCode"];
            formVelden[5] = collection["RegistrationNumber"];
            formVelden[6] = collection["uittreksel"];
            formVelden[7] = collection["CorrespondenceAddress"];
            formVelden[8] = collection["CorrespondencePostalCode"];
            formVelden[9] = collection["Place"];

            formVelden[10] = collection["Phone"];
            formVelden[11] = collection["MobilePhone"];
            formVelden[12] = collection["Email"];
            formVelden[13] = collection["Rekeningnummer voor uitkeringen"];
            formVelden[14] = collection["Iban"];

            formVelden[15] = collection["LastName"];
            formVelden[16] = collection["Title"];
            formVelden[17] = collection["FirstName"];
            formVelden[18] = collection["Sex"];
            formVelden[19] = collection["BirthPlace"];
            formVelden[20] = collection["DateOfBirth"];
            formVelden[21] = GetIdentificationType(collection);// collection["Identificationtype"];
            formVelden[22] = collection["IssuedAt"];
            formVelden[23] = collection["IssuedOn"];
            formVelden[24] = collection["ValidityDate"];
            formVelden[25] = collection["NumberofParticipations"];
            formVelden[26] = collection["AgreementStatus"];

            formVelden[27] = collection["WWFT"] == "Wel" ? "Wel" : "Niet";
            formVelden[28] = collection["SalesCompany"] == "on" ? "Ja" : "Nee";
            formVelden[29] = collection["SalaryBonusWork"] == "on" ? "Ja" : "Nee";
            formVelden[30] = collection["Heritage"] == "on" ? "Ja" : "Nee";
            formVelden[31] = collection["BusinessActivities"] == "on" ? "Ja" : "Nee";
            formVelden[32] = collection["Others"] == "on" ? collection["Otherwise"].ToString() : "Nee";

            return formVelden;
        }
        private string GetIdentificationType(IFormCollection collection)
        {
            switch (collection["Identificationtype"])
            {
                case "1":
                    return IdentificationTypes.Idin_Ideal.ToString();
                case "2":
                    return IdentificationTypes.Idin_Bank.ToString();
                case "3":
                    return IdentificationTypes.Post_Ideal.ToString();
                case "4":
                    return IdentificationTypes.Post_Bank.ToString();
                default:
                    return null;
            }
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult EditCategory(string pk, string value, string name)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null)
            {
                var fc = fundcategories.FirstOrDefault(x => x.TempId == pk && string.IsNullOrEmpty(x.SubCategTempId));
                if (fc != null)
                {
                    fc.Title = value;
                }
                HttpContext.Session.SetComplexData("fundcategories", fundcategories);
            }

            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult EditSubCategory(string pk, string value, string name)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null)
            {
                var fc = fundcategories.FirstOrDefault(x => x.SubCategTempId == pk && !string.IsNullOrEmpty(x.TempId));
                if (fc != null)
                {
                    fc.Title = value;
                }
                HttpContext.Session.SetComplexData("fundcategories", fundcategories);
            }

            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult EditCategoryFile(string pk, string value, string name)
        {
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundsubcategoryfiles");
            if (fundfiles != null)
            {
                var current_fundfile = fundfiles.FirstOrDefault(x => x.TempID == pk);
                if (current_fundfile != null)
                {
                    current_fundfile.DisplayName = value;
                    HttpContext.Session.SetComplexData("fundsubcategoryfiles", fundfiles);
                }
            }

            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult EditFundFile(string pk, string value, string name)
        {
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundwebsitefiles");
            if (fundfiles != null)
            {
                var current_fundfile = fundfiles.FirstOrDefault(x => x.TempID == pk);
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrWhiteSpace(name))
                {
                    var fundfileid = Convert.ToInt32(name);
                    current_fundfile = fundfiles.FirstOrDefault(x => x.Id == fundfileid);
                }
                if (current_fundfile != null)
                {
                    current_fundfile.DisplayName = value;
                    HttpContext.Session.SetComplexData("fundwebsitefiles", fundfiles);
                }
            }

            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult ChangeFundFileOrder(string[] tempids)
        {
            var fundfiles = HttpContext.Session.GetComplexData<List<FundFilePersistModel>>("fundfiles");
            if (fundfiles != null && fundfiles.Any())
            {
                tempids.Select((val, i) => new { value = val, index = i }).ToList().ForEach(x =>
                {
                    var fund = fundfiles.FirstOrDefault(f => f.TempID == x.value);
                    if (fund != null)
                    {
                        fund.SortOrder = x.index + 1;
                    }
                });

                HttpContext.Session.SetComplexData("fundfiles", fundfiles.OrderBy(x => x.SortOrder).ToList());
            }

            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult UpdatePayment(int subscribeid, SubscriptionsTypes type)
        {
            try
            {
                bool update = _fundService.UpdatePayment(subscribeid, type, IdentificationTypes.Post_Bank);

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }


        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult ChangeFundCategoriesOrder(string[] tempids)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null && fundcategories.Any())
            {
                tempids.Select((val, i) => new { value = val, index = i }).ToList().ForEach(x =>
                {
                    var fund = fundcategories.FirstOrDefault(f => f.TempId == x.value && string.IsNullOrEmpty(f.SubCategTempId));
                    if (fund != null)
                    {
                        fund.SortOrder = x.index + 1;
                    }
                });
                HttpContext.Session.SetComplexData("fundcategories", fundcategories.OrderBy(x => x.SortOrder).ToList());
            }

            return Json(true);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult ChangeFundSubCategoriesOrder(string[] tempids, string parentid)
        {
            var fundcategories = HttpContext.Session.GetComplexData<List<FundCategoryPersistModel>>("fundcategories");
            if (fundcategories != null && fundcategories.Any())
            {
                tempids.Select((val, i) => new { value = val, index = i }).ToList().ForEach(x =>
                {
                    var fund = fundcategories.Where(z => z.TempId == parentid).FirstOrDefault(f => f.SubCategTempId == x.value);
                    if (fund != null)
                    {
                        fund.SortOrder = x.index + 1;
                    }
                });
                HttpContext.Session.SetComplexData("fundcategories", fundcategories.OrderBy(x => x.SortOrder).ToList());
            }

            return Json(true);
        }




    }

}
