using AutoMapper;
using FusionCore.Core.Exceptions;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Modules.FundsManager.Models;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.FundsManager.Controllers
{
    [Area("PageManager")]
    public class AdministratorController : FusionBaseController
    {
        private readonly IPageService _pageService;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _configuration;
        public AdministratorController(IPageService pageService, IMapper mapper, IConfiguration configuration, IHostingEnvironment env)
        {
            _pageService = pageService;
            _mapper = mapper;
            _env = env;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }


        public JsonResult DataTable(PageDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Expression<Func<Page, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "title":
                    orderBy = x => x.Title;
                    break;

                default:
                    orderBy = x => x.SortOrder;
                    //orderBy = x => x.UpdatedOnUtc;
                    //sortColumnDir = "desc";
                    break;
            }

            Expression<Func<Page, bool>> predicate = x => x.IsAdmin;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x => x.IsAdmin && (
                        x.Title.ToLower().Contains(searchValue.ToLower())
                        || x.UpdatedOnUtc.ToLongDateString().Contains(searchValue.ToLower()));

            var pages = _pageService.GetAllPages(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<PageDataRow>>(pages);
            model.RecordsFiltered = pages.TotalCount;
            model.RecordsTotal = pages.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {
            var model = new PagePersistModel();
            if (id.HasValue)
            {
                model = _mapper.Map<PagePersistModel>(_pageService.GetPage(id.Value));
                var pagefiles = _pageService.GetPageFiles(id.Value);
                model.PageFiles = _mapper.Map<List<PageFilePersistModel>>(pagefiles);
                HttpContext.Session.SetComplexData("pagefiles", model.PageFiles);
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(PagePersistModel model)
        {
            if (ModelState.IsValid)
            {
                var pagefiles = HttpContext.Session.GetComplexData<List<PageFilePersistModel>>("pagefiles");
                model.PageFiles = pagefiles;

                FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();

                return Json(_pageService.PersistPage(model, fileSettings.PageFiles));
            }

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();

            await _pageService.DeletePage(id, fileSettings.PageFiles);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Active(int id, bool value)
        {
            return Json(_pageService.ActivePage(id, value));
        }


        public IActionResult OrderUp(int id, int order)
        {
            _pageService.OrderPageUp(id, order);

            return RedirectToAction("Index");
        }

        public IActionResult OrderDown(int id, int order)
        {
            _pageService.OrderPageDown(id, order);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FileUpload(IFormFile file_data, int fileType, int pageId, string filename)
        {
            try
            {
                if (!string.IsNullOrEmpty(filename) && !string.IsNullOrWhiteSpace(filename))
                {
                    if (file_data != null)
                    {
                        FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
                        MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();


                        if (file_data.Length <= 0) throw new FusionCoreException("File is empty");

                        var fileName = FileService.RemoveInvalidCharacters(file_data);
                        using (var ms = new MemoryStream())
                        {
                            await file_data.CopyToAsync(ms);
                            var fileBinary = ms.ToArray();
                            if (!Directory.Exists(fileSettings.PageFiles))
                            {
                                Directory.CreateDirectory(fileSettings.PageFiles);
                            }

                            string GuidFileName = Guid.NewGuid().ToString() + fileName;
                            System.IO.File.WriteAllBytes(fileSettings.PageFiles + GuidFileName, fileBinary);

                            var pageFile = new PageFilePersistModel
                            {
                                CreatedOnUtc = DateTime.UtcNow,
                                UpdatedOnUtc = DateTime.UtcNow,
                                //File = fileBinary,
                                FileName = fileName,
                                PageId = pageId,
                                TempID = Guid.NewGuid().ToString(),
                                FilePath = GuidFileName,
                                DisplayName = filename
                            };


                            var pagefiles = HttpContext.Session.GetComplexData<List<PageFilePersistModel>>("pagefiles");
                            if (pagefiles != null && pagefiles.Any())
                            {
                                pageFile.SortOrder = pagefiles.Max(x => x.SortOrder) + 1;
                                pagefiles.Add(pageFile);
                            }
                            else
                            {
                                pagefiles = new List<PageFilePersistModel>();
                                pageFile.SortOrder = 1;
                                pagefiles.Add(pageFile);
                            }
                            HttpContext.Session.SetComplexData("pagefiles", pagefiles);

                            //return Ok(new { id = 0, file = Convert.ToBase64String(fileBinary), tempId = pageFile.TempID, createdon = pageFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), title = pageFile.FileName, append = true, sortOrder = pageFile.SortOrder });
                            return Ok(new { id = 0, file = (mainSettings.FusionUrl + fileSettings.PageFileRelativePath + GuidFileName), tempId = pageFile.TempID, createdon = pageFile.CreatedOnUtc.ConvertToUserTime().ToString("dd-MM-yyyy - HH:mm"), title = pageFile.DisplayName, append = true, sortOrder = pageFile.SortOrder });
                        }
                    }
                }
                return BadRequest();
            }
            catch (Exception er)
            {
                return BadRequest($"Fout tijdens uploaden file_data: {er.Message}");
            }
        }

        public IActionResult FileDownload(int id, string tempFile)
        {
            FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
            MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

            var pagefile = new PageFilePersistModel();
            var pagefiles = HttpContext.Session.GetComplexData<List<PageFilePersistModel>>("pagefiles");
            if (id > 0)
            {
                //pagefile = _mapper.Map<PageFilePersistModel>(_pageService.GetPageFile(id));
                pagefile = pagefiles.FirstOrDefault(x => x.Id == id);
            }
            else
            {
                pagefile = pagefiles.FirstOrDefault(x => x.TempID == tempFile);
            }

            if (pagefile != null)
            {
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = pagefile.FileName,
                    Inline = true,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());

                byte[] file = System.IO.File.ReadAllBytes(fileSettings.PageFiles + pagefile.FilePath);
                //return File(pagefile.File, FileHelper.GetMimeType(pagefile.FileName), pagefile.FileName);
                return File(file, FileHelper.GetMimeType(pagefile.FileName), pagefile.FileName);
            }

            return BadRequest();
        }


        public async Task<IActionResult> FileOrderUp(int id, int order, string tempId)
        {
            int currentfileorder = 0, previousfileorder = 0;
            var pagefiles = HttpContext.Session.GetComplexData<List<PageFilePersistModel>>("pagefiles");
            if (pagefiles != null)
            {
                var current_pagefile = new PageFilePersistModel();
                if (id == 0)
                {
                    current_pagefile = pagefiles.FirstOrDefault(x => x.TempID == tempId);
                }
                else
                {
                    current_pagefile = pagefiles.FirstOrDefault(x => x.Id == id);
                }

                if (current_pagefile != null)
                {
                    if (current_pagefile.SortOrder > 1)
                    {
                        var prev_pagefile = pagefiles.FirstOrDefault(x => x.SortOrder == current_pagefile.SortOrder - 1);
                        if (prev_pagefile != null)
                        {
                            prev_pagefile.SortOrder += 1;

                            previousfileorder = prev_pagefile.SortOrder;
                        }
                        current_pagefile.SortOrder -= 1;

                        currentfileorder = current_pagefile.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("pagefiles", pagefiles);
            }

            return Json(new { currenteleorder = currentfileorder, previuoseleorder = previousfileorder });
        }

        public async Task<IActionResult> FileOrderDown(int id, int order, string tempId)
        {
            int currentfileorder = 0, nextfileorder = 0;
            var pagefiles = HttpContext.Session.GetComplexData<List<PageFilePersistModel>>("pagefiles");
            if (pagefiles != null)
            {
                var current_pagefile = new PageFilePersistModel();
                if (id == 0)
                {
                    current_pagefile = pagefiles.FirstOrDefault(x => x.TempID == tempId);
                }
                else
                {
                    current_pagefile = pagefiles.FirstOrDefault(x => x.Id == id);
                }

                if (current_pagefile != null)
                {
                    var maxsortnumber = pagefiles.Max(x => x.SortOrder);
                    if (current_pagefile.SortOrder < maxsortnumber)
                    {
                        var next_pagefile = pagefiles.FirstOrDefault(x => x.SortOrder == current_pagefile.SortOrder + 1);
                        if (next_pagefile != null)
                        {
                            next_pagefile.SortOrder -= 1;
                            nextfileorder = next_pagefile.SortOrder;
                        }
                        current_pagefile.SortOrder += 1;
                        currentfileorder = current_pagefile.SortOrder;
                    }
                }
                HttpContext.Session.SetComplexData("pagefiles", pagefiles);
            }

            return Json(new { currenteleorder = currentfileorder, nexteleorder = nextfileorder });
        }


        public async Task<IActionResult> RemoveFile(int id, string tempId, int pageId)
        {
            var pagefiles = HttpContext.Session.GetComplexData<List<PageFilePersistModel>>("pagefiles");
            if (pagefiles != null)
            {
                FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();

                var current_pagefile = new PageFilePersistModel();
                if (id == 0)
                {
                    current_pagefile = pagefiles.FirstOrDefault(x => x.TempID == tempId);
                }
                else
                {
                    current_pagefile = pagefiles.FirstOrDefault(x => x.Id == id);
                }
                if (current_pagefile != null)
                {
                    int currentsortorder = current_pagefile.SortOrder;
                    pagefiles.Where(x => x.SortOrder > currentsortorder).ToList().ForEach(x =>
                    {
                        x.SortOrder -= 1;
                    });
                    pagefiles.Remove(current_pagefile);
                    HttpContext.Session.SetComplexData("pagefiles", pagefiles);
                    if (id == 0)
                    {
                        var file = fileSettings.PageFiles + current_pagefile.FilePath;
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                    }
                }
            }
            return Content("");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult EditPageFile(string pk, string value, string name)
        {
            var pagefiles = HttpContext.Session.GetComplexData<List<PageFilePersistModel>>("pagefiles");
            if (pagefiles != null)
            {
                var current_pagefile = pagefiles.FirstOrDefault(x => x.TempID == pk);
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrWhiteSpace(name))
                {
                    var fundfileid = Convert.ToInt32(name);
                    current_pagefile = pagefiles.FirstOrDefault(x => x.Id == fundfileid);
                }
                if (current_pagefile != null)
                {
                    current_pagefile.DisplayName = value;
                    HttpContext.Session.SetComplexData("pagefiles", pagefiles);
                }
            }

            return Json(true);
        }


    }
}
