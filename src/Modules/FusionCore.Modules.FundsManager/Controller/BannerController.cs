using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.FundsManager.Controller
{
    [Area("BannerManager")]
    public class BannerController : FusionBaseController
    {
        private readonly IBannerService _bannerService;
        private readonly IMapper _mapper;
        private readonly IPageService _pageService;


        public BannerController(IBannerService bannerService, IMapper mapper, IPageService pageService)
        {
            _bannerService = bannerService;
            _mapper = mapper;
            _pageService = pageService;

        }

        public IActionResult Index()
        {
            return View();
        }
        public JsonResult DataTable(BannerDataTable model)
        {
            var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
                .FirstOrDefault();
            var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
            var searchValue = Request.Query["search[value]"].FirstOrDefault();

            var pageSize = model.Length;
            var pageIndex = model.Start / model.Length;

            Expression<Func<Banner, IComparable>> orderBy;

            switch (sortColumn)
            {
                case "title":
                    orderBy = x => x.Title;
                    break;
                default:
                    orderBy = x => x.sortOrder;
                    break;
            }

            Expression<Func<Banner
                , bool>> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
                predicate = x =>
                        x.Title.ToLower().Contains(searchValue.ToLower())
                        || x.UpdatedOnUtc.ToLongDateString().Contains(searchValue.ToLower());

            var pages = _bannerService.GetAllBanners(
                predicate,
                orderBy,
                sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
                pageIndex,
                pageSize);

            model.Data = _mapper.Map<List<BannerDataRow>>(pages);

            model.RecordsFiltered = pages.TotalCount;
            model.RecordsTotal = pages.TotalCount;
            return new JsonResult(model);
        }

        public IActionResult Get(int? id)
        {
            var model = id.HasValue ? _mapper.Map<BannerPersistModel>(_bannerService.GetBanner(id.Value)) : new BannerPersistModel();
            Expression<Func<Page, bool>> predicate = x => x.Active;
            Expression<Func<Page, IComparable>> orderBy = x => x.Title;
            var pages = _pageService.GetAllPages(predicate, orderBy);
            ViewBag.Pages = _mapper.Map<List<PagePersistModel>>(pages);

            return View(model);
        }

        [HttpPost]
        public IActionResult Persist(BannerPersistModel model)
        {
            if (ModelState.IsValid)
            {
                return Json(_bannerService.PersistBanner(model));
            }

            return Json(false);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _bannerService.DeleteBanner(id);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Active(int id, bool value)
        {
            return Json(_bannerService.ActiveBanner(id, value));
        }

        public IActionResult OrderUp(int id, int order)
        {
            _bannerService.OrderBannerUp(id, order);

            return RedirectToAction("Index");
        }

        public IActionResult OrderDown(int id, int order)
        {
            _bannerService.OrderBannerDown(id, order);

            return RedirectToAction("Index");
        }

    }
}
