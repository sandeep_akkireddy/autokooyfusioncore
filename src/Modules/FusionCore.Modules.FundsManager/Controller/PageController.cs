using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using FusionCore.Core.Extensions;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Modules.FundsManager.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FusionCore.Modules.FundsManager.Controllers
{
	[Area("PageManager")]
	public class PageController : FusionBaseController
	{
		private readonly IPageService _pageService;
		private readonly IMapper _mapper;

		public PageController(IPageService pageService, IMapper mapper)
		{
			_pageService = pageService;
			_mapper = mapper;
		}

		public IActionResult Index()
		{
			return View();
		}

		public JsonResult DataTable(PageDataTable model)
		{
			var sortColumn = Request.Query["columns[" + Request.Query["order[0][column]"].FirstOrDefault() + "][data]"]
				.FirstOrDefault();
			var sortColumnDir = Request.Query["order[0][dir]"].FirstOrDefault() ?? "";
			var searchValue = Request.Query["search[value]"].FirstOrDefault();

			var pageSize = model.Length;
			var pageIndex = model.Start / model.Length;

			Expression<Func<Page, IComparable>> orderBy;

			switch (sortColumn)
			{
				case "title":
					orderBy = x => x.Title;
					break;
				default:
					orderBy = x => x.CreatedOnUtc;
					//sortColumnDir = "desc";
					break;
			}

			Expression<Func<Page, bool>> predicate = x => !x.IsAdmin;
			if (!string.IsNullOrEmpty(searchValue))
				predicate = x => !x.IsAdmin && (
						x.Title.ToLower().Contains(searchValue.ToLower())
						|| x.UpdatedOnUtc.ToLongDateString().Contains(searchValue.ToLower()));

			var pages = _pageService.GetAllPages(
				predicate,
				orderBy,
				sortColumnDir.Equals("asc", StringComparison.InvariantCultureIgnoreCase),
				pageIndex,
				pageSize);

			model.Data = _mapper.Map<List<PageDataRow>>(pages);
			model.RecordsFiltered = pages.TotalCount;
			model.RecordsTotal = pages.TotalCount;
			return new JsonResult(model);
		}

		public IActionResult Get(int? id)
		{
			var model = id.HasValue ? _mapper.Map<PagePersistModel>(_pageService.GetPage(id.Value)) : new PagePersistModel();

			return View(model);
		}

		[HttpPost]
		public IActionResult Persist(PagePersistModel model)
		{
			if (ModelState.IsValid) return Json(_pageService.PersistPage(model));

			return Json(false);
		}

		public async Task<IActionResult> Delete(int id)
		{
			await _pageService.DeletePage(id);

			return RedirectToAction("Index");
		}

		[HttpPost]
		public IActionResult Active(int id, bool value)
		{
			return Json(_pageService.ActivePage(id, value));
		}

	}
}
//}
