using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.FundsManager.Models
{
    public class FundCategoryPersistModel : BaseModel
    {
        [Required]
        public string Title { get; set; }
        public int? ParentId { get; set; }
        public virtual FundCategoryPersistModel Parent { get; set; }
        public int SortOrder { get; set; }

        public int FundId { get; set; }
        public virtual FundsPersistModel Fund { get; set; }
        public bool Active { get; set; }

        public string TempId { get; set; }
        public string SubCategTempId { get; set; }

        public virtual ICollection<FundCategoryPersistModel> SubCategories { get; set; }

        public virtual ICollection<FundFilePersistModel> Files { get; set; }
    }
}
