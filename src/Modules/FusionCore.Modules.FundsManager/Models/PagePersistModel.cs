using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Data.Enums;

namespace FusionCore.Modules.FundsManager.Models
{
    public class PagePersistModel: BaseModel
	{
		[Required]
		[MaxLength(256)]
		public string Title { get; set; }

		[MaxLength(256)]
		public string Header { get; set; }
		//[Url]
		public string Url { get; set; }

		public Location Location { get; set; }

		//public int? ContentId { get; set; }
		public string Content { get; set;  }
		public Template Template { get; set; }

		public bool Active { get; set; }

		public string MetaTitle { get; set; }

		public string MetaDescription { get; set; }

		public string MetaKeywords { get; set; }

		public DateTime CreatedOnUtc { get; set; }

		public bool IsAdmin { get; set; }

		public int SortOrder { get; set; }

		public ICollection<PageFilePersistModel> PageFiles { get; set; }
	}
}
