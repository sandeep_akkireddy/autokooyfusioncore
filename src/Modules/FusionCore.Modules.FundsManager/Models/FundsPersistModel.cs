using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FusionCore.Modules.FundsManager.Models
{
    public class FundsPersistModel : BaseModel
    {
        public DateTime CreatedOnUtc { get; set; }
        [Required]
        [StringLength(100)]

        public string Title { get; set; }

        public FundStatus fundStatus { get; set; }
        public string FundStartDate { get; set; }
        public string FundEndDate { get; set; }

        public bool Active { get; set; }

        public string Description { get; set; }

        public string Rendementen { get; set; }

        public string GeneralDescription { get; set; }

        [StringLength(100)]
        public string MetaTitle { get; set; }

        [StringLength(150)]
        public string MetaDescription { get; set; }

        [StringLength(100)]
        public string MetaKeywords { get; set; }

        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string Phonenumber { get; set; }
        public string Email { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public bool SplashActive { get; set; }
        public int? SplashImageId { get; set; }


        public string SplashImageTitle { get; set; }

        public string SplashImageCaption { get; set; }
        public int? ThumbnailImageId { get; set; }
        public string ThumbnailImageTitle { get; set; }
        public string ThumbnailImageCaption { get; set; }


        public int? External_Id { get; set; }

        public bool activeregistration { get; set; }
        public string participationprice { get; set; }


        public decimal participation_price { get; set; }
        public string Iban { get; set; }

        public string Dateofcapital { get; set; }

        public string emissioncost { get; set; }

        public decimal emission_cost { get; set; }

        public int MaximumSubscriptions { get; set; }

        public ICollection<FundFilePersistModel> FundFiles { get; set; }
        public ICollection<FundCategoryPersistModel> FundCategory { get; set; }

        public string SubTitle { get; set; }
        [Required]
        public FundType fundType { get; set; }

        public FundDesignations? designation { get; set; }
        public string sectionnumber { get; set; }
        public string chapter { get; set; }
    }
}
