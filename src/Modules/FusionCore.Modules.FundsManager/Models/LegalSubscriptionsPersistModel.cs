using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.FundsManager.Models
{
    public class LegalSubscriptionsPersistModel : BaseModel
    {
        public string RegisteredName { get; set; }

        public string RegisteredAddress { get; set; }

        public string Location { get; set; }

        public string PostalCode { get; set; }

        public string RegistrationNumber { get; set; }

        public string FileName { get; set; }

        public string CorrespondenceAddress { get; set; }

        public string CorrespondencePostalCode { get; set; }

        public string Place { get; set; }

        public string Phone { get; set; }

        public string MobilePhone { get; set; }

        public string Email { get; set; }

        public string Iban { get; set; }

        public string LastName { get; set; }
        
        [StringLength(100)]
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string Sex { get; set; }

        public string BirthPlace { get; set; }

        public string DateOfBirth { get; set; }

        public string IdentityFileName { get; set; }

        public string IssuedAt { get; set; }

        public string IssuedOn { get; set; }

        public string ValidityDate { get; set; }

        public string NumberofParticipations { get; set; }

        public bool AgreementStatus { get; set; }


        public IdentificationTypes? Identificationtype { get; set; }

        public IdentificationTypes? IdentificationPaymenttype { get; set; }

        public IDinStatus? IDinstatus { get; set; }
        public IDealPaymentStatus? Idealpaymentstatus { get; set; }
        public int FundsId { get; set; }

        public bool IsIdentified { get; set; }
        public string AccountNumber { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public FundsPersistModel fundsPersistModel { get; set; }
        public bool SalesCompany { get; set; }
        public bool SalaryBonusWork { get; set; }
        public bool Heritage { get; set; }
        public bool BusinessActivities { get; set; }
        public bool Others { get; set; }

        public string OtherAssets { get; set; }
        public bool? WWFT { get; set; }
        public bool LegalSalesCompany { get; set; }
        public bool LegalSalaryBonusWork { get; set; }
        public bool LegalHeritage { get; set; }
        public bool LegalBusinessActivities { get; set; }
        public bool LegalOthers { get; set; }

        public string LegalOtherAssets { get; set; }
    }
}
