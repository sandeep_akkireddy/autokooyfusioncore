using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.FundsManager.Models
{
    public class GeneralSubscriptionsPersistModel : BaseModel
    {
        public string LastName { get; set; }
       
        [StringLength(100)]
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string Sex { get; set; }

        public string BirthPlace { get; set; }

        public string DateOfBirth { get; set; }

        public string HomeAddress { get; set; }

        public string PostalCode { get; set; }

        public string Residence { get; set; }

        public bool CorrespondenceAddressType { get; set; }

        public string CorrespondenceAddress { get; set; }

        public string CorrespondencePostalCode { get; set; }

        public string CorrespondenceResidence { get; set; }

        public string Phone { get; set; }

        public string MobilePhone { get; set; }

        public string Email { get; set; }

        public string Iban { get; set; }

        public string FileName { get; set; }

        public string IssuedAt { get; set; }

        public string IssuedOn { get; set; }

        public string ValidityDate { get; set; }

        public string MartialStatus { get; set; }

        public string MarriedWithContract { get; set; }

        public string MarriedWithoutContract { get; set; }

        public string UnMarriedWithContract { get; set; }

        public string UnMarriedWithoutContract { get; set; }

        public string SpouseLastName { get; set; }

        public string SpouseFirstName { get; set; }

        public string SpouseSex { get; set; }

        public string SpouseBirthPlace { get; set; }

        public string SpouseDateOfBirth { get; set; }

        public string NumberofParticipations { get; set; }

        public bool AgreementStatus { get; set; }


        public IdentificationTypes? Identificationtype { get; set; }

        public IdentificationTypes? IdentificationPaymenttype { get; set; }

        public IDinStatus? IDinstatus { get; set; }
        public IDealPaymentStatus? Idealpaymentstatus { get; set; }


        public int FundsId { get; set; }

        public bool IsIdentified { get; set; }

        public string AccountNumber { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public FundsPersistModel fundsPersistModel { get; set; }
        public bool SalesCompany { get; set; }
        public bool SalaryBonusWork { get; set; }
        public bool Heritage { get; set; }
        public bool BusinessActivities { get; set; }
        public bool Others { get; set; }

        public string OtherAssets { get; set; }
        public bool? WWFT { get; set; }
    }
}
