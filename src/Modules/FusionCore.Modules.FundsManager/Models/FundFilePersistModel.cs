using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Models
{
    public class FundFilePersistModel : BaseModel
    {
        public byte[] File { get; set; }
        public int? FundId { get; set; }
        public string FileName { get; set; }
        public string TempID { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }

        public int SortOrder { get; set; }
        public bool websitedownloadfiles { get; set; }
        public int? Kind { get; set; }
        public int? FundCategoryId { get; set; }

        public string FundCategoryTempId { get; set; }
        public string DisplayName { get; set; }
        public string FilePath { get; set; }
    }
}
