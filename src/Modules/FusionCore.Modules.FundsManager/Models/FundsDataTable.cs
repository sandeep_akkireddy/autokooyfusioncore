using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Models
{
    public class FundsDataTable : BaseDataTable
    {
        public List<FundDataRow> Data { get; set; }
    }
    public class FundDataRow
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public bool Active { get; set; }
    }
}
