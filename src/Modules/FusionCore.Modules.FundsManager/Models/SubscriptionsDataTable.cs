using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Models
{
    public class SubscriptionsDataTable : BaseDataTable
    {
        public List<SubscriptionsDataRow> Data { get; set; }

        public string SubscribedRegistrations { get; set; }
        public string CompletedRegistrations { get; set; }
        public string PossibleCapital { get; set; }
        public string TotalCapital { get; set; }
    }
    public class SubscriptionsDataRow
    {
        public DateTime CreateOn { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Place { get; set; }
        public decimal participation_Price { get; set; }
        public string ParticipationPrice { get; set; }
        public IdentificationTypes? identify { get; set; }
        public string identification { get; set; }
        public bool isIdentified { get; set; }
        public string NumberofParticipants { get; set; }
        public int id { get; set; }

        public SubscriptionsTypes subscriptionType { get; set; }

        public IDealPaymentStatus? Idealpaymentstatus { get; set; }
        public IDinStatus? IDinstatus { get; set; }

        public IdentificationTypes? IdentificationPaymenttype { get; set; }

    }
}
