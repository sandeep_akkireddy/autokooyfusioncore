using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Data.Enums;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Modules.FundsManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FusionCore.Modules.FundsManager.Services
{
    public class FundsService : BaseService, IFundsService
    {
        private readonly IGenericRepository<Funds> _fundRepository;
        private readonly IGenericRepository<FundFiles> _fundFileRepository;
        private readonly IGenericRepository<FundCategory> _fundCategoryRepository;
        private readonly IGenericRepository<GeneralSubscriptions> _fundGeneralSubscriptionRepository;
        private readonly IGenericRepository<LegalSubscriptions> _fundLegalSubscriptionRepository;

        private readonly IMapper _mapper;
        private readonly IGenericRepository<Banner> _bannerRepository;

        public FundsService(
            IGenericRepository<Funds> fundRepository,
            IGenericRepository<FundFiles> fundFileRepository,
              IGenericRepository<FundCategory> fundCategoryRepository,
              IGenericRepository<GeneralSubscriptions> fundGeneralSubscriptionRepository,
              IGenericRepository<LegalSubscriptions> fundLegalSubscriptionRepository,
            IMapper mapper,
            IGenericRepository<Banner> bannerRepository)
        {
            _fundRepository = fundRepository;
            _fundFileRepository = fundFileRepository;
            _fundCategoryRepository = fundCategoryRepository;
            _fundGeneralSubscriptionRepository = fundGeneralSubscriptionRepository;
            _fundLegalSubscriptionRepository = fundLegalSubscriptionRepository;
            _mapper = mapper;
            _bannerRepository = bannerRepository;
        }


        public bool ActiveFund(int id, bool value)
        {
            var dbEntity = GetFund(id);
            if (dbEntity != null)
            {
                dbEntity.Active = value;
                _fundRepository.Update(dbEntity);

                return true;
            }
            return false;
        }

        public async Task DeleteFund(int id, string fundfilepath = null)
        {
            List<string> deletefundfilenames = new List<string>();


            var fundfiles = _fundFileRepository.GetAll(x => x.FundId == id).ToList();
            if (fundfiles != null && fundfiles.Any())
            {
                deletefundfilenames.AddRange(fundfiles.Select(x => x.FilePath).ToList());
                _fundFileRepository.DeleteAll(fundfiles);
            }

            var fundcategory = _fundCategoryRepository.GetAll(x => x.FundId == id).ToList();
            if (fundcategory != null && fundcategory.Any())
            {
                _fundCategoryRepository.DeleteAll(fundcategory);
            }

            var general = _fundGeneralSubscriptionRepository.GetAll(x => x.FundsId == id).ToList();
            if (general != null && general.Any())
            {
                _fundGeneralSubscriptionRepository.DeleteAll(general);
            }

            var legal = _fundLegalSubscriptionRepository.GetAll(x => x.FundsId == id).ToList();
            if (legal != null && legal.Any())
            {
                _fundLegalSubscriptionRepository.DeleteAll(legal);
            }
            var banner = _bannerRepository.GetAll(x => x.FundID == id).ToList();
            if (banner != null && banner.Any())
            {
                banner.ForEach(x =>
                {
                    x.bannerType = BannerType.Presentatie;
                    x.FundID = (int?)null;
                });
                _bannerRepository.UpdateAll(banner);
            }

            var fund = await _fundRepository.GetByIdAsync(id);
            if (fund != null)
            {
                _fundRepository.Delete(fund);
                if (deletefundfilenames != null && deletefundfilenames.Any())
                {
                    DeletePhysicalFiles(deletefundfilenames, fundfilepath);
                }
            }
        }

        public IPagedList<Funds> GetAllFunds<TKey>(Expression<Func<Funds, bool>> predicate = null, Expression<Func<Funds, TKey>> orderBy = null, bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var queryable = _fundRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Funds>(queryable, pageIndex, pageSize);
        }

        public Funds GetFund(int id)
        {
            return _fundRepository.Get(x => x.Id == id);
        }

        public FundFiles GetFundFile(int id)
        {
            return _fundFileRepository.Get(x => x.Id == id);
        }

        public List<FundFiles> GetFundFiles(int fundId)
        {
            return _fundFileRepository.GetAll(x => x.FundId == fundId && !x.FundCategoryId.HasValue).ToList();
        }
        public List<FundCategory> GetFundCategory(int fundId)
        {
            return _fundCategoryRepository.GetAll(x => x.FundId == fundId).ToList();
        }
        void DeletePhysicalFiles(List<string> filenames, string fundfilepath)
        {
            if (filenames != null && filenames.Any())
            {
                filenames.ForEach(x =>
                {
                    if (System.IO.File.Exists(fundfilepath + x))
                    {
                        System.IO.File.Delete(fundfilepath + x);
                    }
                });
            }

        }
        public bool PersistFund(FundsPersistModel model, string fundfilepath = null)
        {
            List<string> deletefundfilenames = new List<string>();
            string format = "dd-MM-yyyy";
            if (model.Id.HasValue)
            {
                var dbEntity = GetFund(model.Id.Value);
                if (dbEntity != null)
                {
                    dbEntity.Title = model.Title;
                    dbEntity.Address = model.Address;
                    dbEntity.City = model.City;
                    dbEntity.Country = model.Country;
                    if (!string.IsNullOrEmpty(model.Dateofcapital) && !string.IsNullOrWhiteSpace(model.Dateofcapital))
                        dbEntity.Dateofcapital = DateTime.ParseExact(model.Dateofcapital, format,
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.None);
                    dbEntity.Description = model.Description;
                    dbEntity.emissioncost = model.emissioncost;
                    dbEntity.emission_cost = model.emission_cost;
                    if (!string.IsNullOrEmpty(model.FundEndDate) && !string.IsNullOrWhiteSpace(model.FundEndDate))
                        dbEntity.FundEndDate = DateTime.ParseExact(model.FundEndDate, format,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None);

                    if (!string.IsNullOrEmpty(model.FundStartDate) && !string.IsNullOrWhiteSpace(model.FundStartDate))
                        dbEntity.FundStartDate = DateTime.ParseExact(model.FundStartDate, format,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None);

                    dbEntity.fundStatus = model.fundStatus;
                    dbEntity.Active = model.Active;
                    dbEntity.activeregistration = model.activeregistration;
                    dbEntity.Iban = model.Iban;
                    dbEntity.MetaDescription = model.MetaDescription;
                    dbEntity.MetaKeywords = model.MetaKeywords;
                    dbEntity.MetaTitle = model.MetaTitle;
                    dbEntity.UpdatedOnUtc = DateTime.Now;
                    dbEntity.Zipcode = model.Zipcode;
                    dbEntity.ThumbnailImageCaption = model.ThumbnailImageCaption;
                    dbEntity.ThumbnailImageId = model.ThumbnailImageId;
                    dbEntity.ThumbnailImageTitle = model.ThumbnailImageTitle;
                    dbEntity.SplashImageCaption = model.SplashImageCaption;
                    dbEntity.SplashImageId = model.SplashImageId;
                    dbEntity.SplashImageTitle = model.SplashImageTitle;
                    dbEntity.participationprice = model.participationprice;
                    dbEntity.participation_price = model.participation_price;
                    dbEntity.GeneralDescription = model.GeneralDescription;
                    dbEntity.fundStatus = model.fundStatus;
                    dbEntity.MaximumSubscriptions = model.MaximumSubscriptions;
                    dbEntity.SubTitle = model.SubTitle;
                    dbEntity.fundType = model.fundType;
                    dbEntity.designation = model.designation;
                    dbEntity.sectionnumber = model.sectionnumber;
                    dbEntity.chapter = model.chapter;

                    _fundRepository.Update(dbEntity);


                    #region files
                    var fundimages = model.FundFiles.Where(x => !x.websitedownloadfiles).ToList();
                    var fundfiles = model.FundFiles.Where(x => x.websitedownloadfiles && string.IsNullOrEmpty(x.FundCategoryTempId)).ToList();

                    var fundFileEntity = GetFundFiles(model.Id.Value);
                    if (fundFileEntity != null && fundFileEntity.Any())
                    {

                        var new_pageFilesEntity = new List<FundFiles>();
                        _mapper.Map(fundimages, new_pageFilesEntity);

                        #region fund images
                        var ids = fundimages.Where(x => x.Id > 0 && x.Id.HasValue).Select(x => x.Id.Value).ToList();

                        var removeEntity = fundFileEntity.Where(x => !x.websitedownloadfiles && !ids.Contains(x.Id) && x.Id > 0).ToList();
                        deletefundfilenames.AddRange(removeEntity.Select(x => x.FilePath).ToList());
                        _fundFileRepository.DeleteAll(removeEntity);

                        var changeimagesorder = fundFileEntity.Where(x => x.Id > 0).ToList();
                        changeimagesorder.ForEach(x =>
                        {
                            var edit = new_pageFilesEntity.FirstOrDefault(z => z.Id == x.Id);
                            if (edit != null)
                            {
                                x.SortOrder = edit.SortOrder;
                            }
                        });

                        _fundFileRepository.UpdateAll(fundFileEntity);

                        var addEntity = new_pageFilesEntity.Where(x => x.Id == 0).ToList();
                        addEntity.ForEach(x => { x.FundId = model.Id.Value; });
                        _fundFileRepository.InsertAll(addEntity);
                        #endregion


                        var new_fundFilesEntity = new List<FundFiles>();
                        _mapper.Map(fundfiles, new_fundFilesEntity);

                        #region fund files

                        var fundfiles_ids = fundfiles.Where(x => x.Id > 0 && x.Id.HasValue).Select(x => x.Id.Value).ToList();

                        var fundfiles_removeEntity = fundFileEntity.Where(x => x.websitedownloadfiles && !fundfiles_ids.Contains(x.Id) && x.Id > 0).ToList();

                        deletefundfilenames.AddRange(fundfiles_removeEntity.Select(x => x.FilePath).ToList());
                        _fundFileRepository.DeleteAll(fundfiles_removeEntity);


                        var changeorder = fundFileEntity.Where(x => x.Id > 0).ToList();
                        changeorder.ForEach(x =>
                        {
                            var edit = new_fundFilesEntity.FirstOrDefault(z => z.Id == x.Id);
                            if (edit != null)
                            {
                                x.SortOrder = edit.SortOrder;
                                x.DisplayName = edit.DisplayName;
                            }
                        });
                        _fundFileRepository.UpdateAll(fundFileEntity);

                        var fundfiles_addEntity = new_fundFilesEntity.Where(x => x.Id == 0).ToList();
                        fundfiles_addEntity.ForEach(x => { x.FundId = model.Id.Value; });
                        _fundFileRepository.InsertAll(fundfiles_addEntity);

                        #endregion

                    }
                    else
                    {
                        if (fundimages != null && fundimages.Any())
                        {
                            var fundFilesEntity = new List<FundFiles>();
                            _mapper.Map(fundimages, fundFilesEntity);

                            _fundFileRepository.InsertAll(fundFilesEntity);
                        }

                        if (fundfiles != null && fundfiles.Any())
                        {
                            var fundFilesEntity = new List<FundFiles>();
                            _mapper.Map(fundfiles, fundFilesEntity);

                            _fundFileRepository.InsertAll(fundFilesEntity);
                        }

                    }

                    #endregion

                    #region category

                    var fundsubcategoryfiles = model.FundFiles.Where(x => x.websitedownloadfiles && !string.IsNullOrEmpty(x.FundCategoryTempId)).ToList();

                    var parentCategory = model.FundCategory.Where(x => string.IsNullOrEmpty(x.SubCategTempId)).ToList();
                    var subcategory = model.FundCategory.Where(x => !string.IsNullOrEmpty(x.SubCategTempId)).ToList();

                    var fundCategoryEntity = GetFundCategory(model.Id.Value);
                    if (fundCategoryEntity != null && fundCategoryEntity.Any())
                    {
                        var categoryfiles = GetFundCategoryFiles(model.Id.Value);

                        #region deleting removed items
                        var parent_categ_ids = parentCategory.Where(x => x.Id.HasValue && x.Id.Value > 0).Select(x => x.Id).ToArray();
                        var sub_categ_ids = subcategory.Where(x => x.Id.HasValue && x.Id.Value > 0).Select(x => x.Id).ToArray();

                        var removeCategoryEntity = fundCategoryEntity.Where(x => !parent_categ_ids.Contains(x.Id) && x.Id > 0 && !x.ParentId.HasValue).ToList();
                        var removeSubCategoryEntity = fundCategoryEntity.Where(x => !sub_categ_ids.Contains(x.Id) && x.Id > 0 && x.ParentId.HasValue).ToList();
                        removeCategoryEntity.AddRange(removeSubCategoryEntity);

                        if (removeCategoryEntity != null && removeCategoryEntity.Any())
                            _fundCategoryRepository.DeleteAll(removeCategoryEntity);


                        if (categoryfiles != null && categoryfiles.Any())
                        {
                            var subcateg_file_ids = fundsubcategoryfiles.Where(x => x.Id.HasValue && x.Id > 0).Select(x => x.Id).ToArray();
                            var delete_subcateg_files = categoryfiles.Where(x => !subcateg_file_ids.Contains(x.Id) && x.Id > 0).ToList();
                            if (delete_subcateg_files != null && delete_subcateg_files.Any())
                            {
                                deletefundfilenames.AddRange(delete_subcateg_files.Select(x => x.FilePath).ToList());
                                _fundFileRepository.DeleteAll(delete_subcateg_files);
                            }
                        }
                        #endregion

                        var add_category = parentCategory.Where(x => !x.Id.HasValue || x.Id == 0).ToList();
                        var edit_categ = parentCategory.Where(x => x.Id.HasValue && x.Id > 0).ToList();
                        if (add_category != null && add_category.Any())
                        {
                            InsertCategories(add_category, subcategory, fundsubcategoryfiles, model.Id.Value);
                        }
                        if (edit_categ != null && edit_categ.Any())
                        {
                            List<FundCategory> addfundCategories = new List<FundCategory>();
                            List<FundFiles> addfundfiles = new List<FundFiles>();


                            List<FundCategory> editfundCategories = new List<FundCategory>();
                            List<FundFiles> editfundfiles = new List<FundFiles>();

                            edit_categ.ForEach(x =>
                                {
                                    var _editcateg = fundCategoryEntity.FirstOrDefault(z => z.Id == x.Id);
                                    if (_editcateg != null)
                                    {
                                        _editcateg.Title = x.Title;
                                        _editcateg.UpdatedOnUtc = DateTime.Now;
                                        _editcateg.Active = x.Active;
                                        _editcateg.SortOrder = x.SortOrder;

                                        editfundCategories.Add(_editcateg);

                                        subcategory.Where(z => z.TempId == x.TempId).ToList().ForEach(s =>
                                        {
                                            var _editsubcateg = fundCategoryEntity.FirstOrDefault(z => z.Id == s.Id);
                                            if (_editsubcateg != null)
                                            {
                                                _editsubcateg.Active = s.Active;
                                                _editsubcateg.Title = s.Title;
                                                _editsubcateg.UpdatedOnUtc = DateTime.Now;
                                                _editsubcateg.SortOrder = s.SortOrder;
                                                editfundCategories.Add(_editsubcateg);

                                                fundsubcategoryfiles.Where(f => f.FundCategoryTempId == s.SubCategTempId).ToList().ForEach(m =>
                                                {
                                                    var files = categoryfiles.FirstOrDefault(f => f.Id == m.Id);
                                                    if (files != null)
                                                    {
                                                        files.SortOrder = m.SortOrder;
                                                        files.UpdatedOnUtc = DateTime.Now;
                                                        files.DisplayName = m.DisplayName;

                                                        editfundfiles.Add(files);
                                                    }
                                                    else
                                                    {
                                                        FundFiles ff = new FundFiles();
                                                        ff.CreatedOnUtc = DateTime.Now;
                                                        ff.File = m.File;
                                                        ff.FileName = m.FileName;
                                                        ff.FundCategory = _editsubcateg;
                                                        ff.FundCategoryId = _editsubcateg.Id;
                                                        ff.Kind = m.Kind;
                                                        ff.SortOrder = m.SortOrder;
                                                        ff.UpdatedOnUtc = DateTime.Now;
                                                        ff.websitedownloadfiles = m.websitedownloadfiles;
                                                        ff.FundId = model.Id.Value;
                                                        ff.DisplayName = m.DisplayName;
                                                        ff.FilePath = m.FilePath;

                                                        addfundfiles.Add(ff);
                                                    }
                                                });

                                            }
                                            else
                                            {
                                                FundCategory sub_category = new FundCategory();
                                                sub_category.Parent = _editcateg;
                                                sub_category.ParentId = _editcateg.Id;
                                                sub_category.Active = s.Active;
                                                sub_category.CreatedOnUtc = DateTime.Now;
                                                sub_category.FundId = model.Id.Value;
                                                sub_category.Title = s.Title;
                                                sub_category.UpdatedOnUtc = DateTime.Now;
                                                sub_category.SortOrder = s.SortOrder;

                                                addfundCategories.Add(sub_category);

                                                fundsubcategoryfiles.Where(f => f.FundCategoryTempId == s.SubCategTempId).ToList().ForEach(m =>
                                                {

                                                    FundFiles ff = new FundFiles();
                                                    ff.CreatedOnUtc = DateTime.Now;
                                                    ff.File = m.File;
                                                    ff.FileName = m.FileName;
                                                    ff.FundCategory = sub_category;
                                                    ff.FundCategoryId = sub_category.Id;
                                                    ff.Kind = m.Kind;
                                                    ff.SortOrder = m.SortOrder;
                                                    ff.UpdatedOnUtc = DateTime.Now;
                                                    ff.websitedownloadfiles = m.websitedownloadfiles;
                                                    ff.FundId = model.Id.Value;
                                                    ff.DisplayName = m.DisplayName;
                                                    ff.FilePath = m.FilePath;
                                                    addfundfiles.Add(ff);
                                                });
                                            }
                                        });
                                    }


                                });


                            if (editfundCategories.Any())
                                _fundCategoryRepository.UpdateAll(editfundCategories);

                            if (editfundfiles.Any())
                                _fundFileRepository.UpdateAll(editfundfiles);

                            if (addfundCategories.Any())
                                _fundCategoryRepository.InsertAll(addfundCategories);

                            if (addfundfiles.Any())
                                _fundFileRepository.InsertAll(addfundfiles);

                        }

                    }
                    else
                    {
                        InsertCategories(parentCategory, subcategory, fundsubcategoryfiles, model.Id.Value);
                    }

                    #endregion


                    if (deletefundfilenames != null && deletefundfilenames.Any())
                        DeletePhysicalFiles(deletefundfilenames, fundfilepath);

                    return true;
                }
            }
            else
            {
                var dbEntity = new Funds();
                _mapper.Map(model, dbEntity);
                _fundRepository.Insert(dbEntity);
                return true;
            }

            return false;
        }
        void InsertCategories(List<FundCategoryPersistModel> parentCategory, List<FundCategoryPersistModel> subcategory, List<FundFilePersistModel> fundsubcategoryfiles, int fundId)
        {
            List<FundCategory> parent = new List<FundCategory>();
            List<FundFiles> subcategory_files = new List<FundFiles>();
            parentCategory.ForEach(x =>
            {
                FundCategory category = new FundCategory();
                category.Active = x.Active;
                category.CreatedOnUtc = DateTime.Now;
                category.FundId = fundId;
                category.Title = x.Title;
                category.UpdatedOnUtc = DateTime.Now;
                category.SortOrder = x.SortOrder;

                parent.Add(category);

                subcategory.Where(z => z.TempId == x.TempId).ToList().ForEach(z =>
                {
                    FundCategory sub_category = new FundCategory();
                    sub_category.Parent = category;
                    sub_category.ParentId = category.Id;
                    sub_category.Active = z.Active;
                    sub_category.CreatedOnUtc = DateTime.Now;
                    sub_category.FundId = fundId;
                    sub_category.Title = z.Title;
                    sub_category.UpdatedOnUtc = DateTime.Now;
                    sub_category.SortOrder = z.SortOrder;

                    parent.Add(sub_category);

                    fundsubcategoryfiles.Where(m => m.FundCategoryTempId == z.SubCategTempId).ToList().ForEach(m =>
                    {
                        FundFiles ff = new FundFiles();
                        ff.CreatedOnUtc = DateTime.Now;
                        ff.File = m.File;
                        ff.FileName = m.FileName;
                        ff.FundCategory = sub_category;
                        ff.FundCategoryId = sub_category.Id;
                        ff.Kind = m.Kind;
                        ff.SortOrder = m.SortOrder;
                        ff.UpdatedOnUtc = DateTime.Now;
                        ff.websitedownloadfiles = m.websitedownloadfiles;
                        ff.FundId = fundId;
                        ff.DisplayName = m.DisplayName;
                        ff.FilePath = m.FilePath;
                        subcategory_files.Add(ff);
                    });

                });

            });

            _fundCategoryRepository.InsertAll(parent);
            _fundFileRepository.InsertAll(subcategory_files);
        }

        public List<FundCategory> GetFundCategories(int fundId)
        {
            return _fundCategoryRepository.GetAll(x => x.FundId == fundId).ToList();
        }
        public List<FundFiles> GetFundCategoryFiles(int fundId)
        {
            return _fundFileRepository.GetAll(x => x.FundId == fundId && x.FundCategoryId.HasValue).ToList();
        }


        public GeneralSubscriptions GetGeneralSubscription(int id)
        {
            return _fundGeneralSubscriptionRepository.GetById(id);
        }
        public LegalSubscriptions GetLegalSubscription(int id)
        {
            return _fundLegalSubscriptionRepository.GetById(id);
        }
        public bool PersistFundGeneralSubscription(GeneralSubscriptionsPersistModel model)
        {

            if (model.Id.HasValue)
            {
                //Edit
                var dbEntity = GetGeneralSubscription(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _fundGeneralSubscriptionRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                var dbEntity = new GeneralSubscriptions();
                _mapper.Map(model, dbEntity);
                _fundGeneralSubscriptionRepository.Insert(dbEntity);

                MinusFundSubscriptionCount(model.FundsId);
                return true;
            }

            return false;
        }

        public bool PersistFundLegalSubscription(LegalSubscriptionsPersistModel model)
        {

            if (model.Id.HasValue)
            {
                //Edit
                var dbEntity = GetLegalSubscription(model.Id.Value);
                if (dbEntity != null)
                {
                    _mapper.Map(model, dbEntity);
                    _fundLegalSubscriptionRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                var dbEntity = new LegalSubscriptions();
                _mapper.Map(model, dbEntity);
                _fundLegalSubscriptionRepository.Insert(dbEntity);

                MinusFundSubscriptionCount(model.FundsId);
                return true;
            }

            return false;
        }
        bool MinusFundSubscriptionCount(int fundid)
        {
            var fund = GetFund(fundid);
            if (fund != null)
            {
                if (fund.MaximumSubscriptions > 0)
                {
                    fund.MaximumSubscriptions = fund.MaximumSubscriptions - 1;
                }
                else
                {
                    fund.activeregistration = false;
                }
                _fundRepository.Update(fund);
            }
            return true;
        }
        bool AddFundSubscriptionCount(int fundid)
        {
            Funds fund = _fundRepository.GetById(fundid);
            if (fund != null)
            {
                fund.MaximumSubscriptions = fund.MaximumSubscriptions + 1;
                _fundRepository.Update(fund);
            }
            return true;
        }
        public List<SubscriptionsDataRow> GetFundGeneralSubscriptions(int fundid, string searchValue, string sortColumn, bool asc = true)
        {
            Func<GeneralSubscriptions, bool> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
            {
                predicate = x => (x.FirstName + x.LastName).ToLower().Contains(searchValue.ToLower().Trim()) || x.CreatedOnUtc.ToLongDateString().Contains(searchValue.ToLower());
            }
            Func<GeneralSubscriptions, IComparable> orderBy = null;
            switch (sortColumn)
            {
                case "name":
                    orderBy = x => (x.FirstName + x.LastName);
                    break;
                default:
                    orderBy = x => x.CreatedOnUtc;
                    break;
            }

            var queryable = _fundGeneralSubscriptionRepository.GetAll(x => x.FundsId == fundid);
            if (predicate != null)
            {
                queryable = queryable.Where(predicate).AsQueryable();
            }
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy).AsQueryable() : queryable.OrderByDescending(orderBy).AsQueryable();


            List<SubscriptionsDataRow> general_subscription = new List<SubscriptionsDataRow>();
            if (queryable.Any())
                general_subscription = queryable.Select(x => new SubscriptionsDataRow { id = x.Id, subscriptionType = SubscriptionsTypes.General, Address = x.HomeAddress, CreateOn = x.CreatedOnUtc, identify = x.Identificationtype, Name = (x.FirstName + " " + x.LastName), NumberofParticipants = x.NumberofParticipations, Place = x.BirthPlace, isIdentified = x.IsIdentified, Idealpaymentstatus = x.Idealpaymentstatus, IDinstatus = x.IDinstatus, IdentificationPaymenttype = x.IdentificationPaymenttype }).ToList();
            Funds singleFund = GetFund(fundid);
            decimal price = singleFund?.participation_price ?? 0;
            general_subscription.ForEach(x =>
            {
                if (x.identify.HasValue && x.isIdentified)
                {

                    if ((x.IDinstatus.HasValue && (int)x.IDinstatus.Value == (int)IDinStatus.autherized && x.identify.Value == IdentificationTypes.Idin_Ideal))//&& x.identify.Value == IdentificationTypes.Idin_Bank) ||
                    {
                        x.identification = "Geïdentificeerd-(iDin)";
                    }
                    else
                    {
                        x.identification = "Geïdentificeerd";
                    }
                }
                else
                {
                    x.identification = "Niet geïdentificeerd";
                }

                if (!string.IsNullOrWhiteSpace(x.NumberofParticipants) && !string.IsNullOrEmpty(x.NumberofParticipants))
                {
                    int nop = Convert.ToInt32(x.NumberofParticipants);
                    var calcprice = (price * nop);
                    x.participation_Price = calcprice;
                    string participation_price = "0";
                    var _price = calcprice;// - Math.Floor(calcprice);
                    if (_price > 0)
                    {
                        participation_price = string.Format("{0:n}", Math.Round(calcprice, 2));
                    }
                    else
                    {
                        participation_price = string.Format("{0:n0}", Math.Round(calcprice, 2)) + ",-";
                    }
                    x.ParticipationPrice = participation_price;
                }
                else
                {
                    x.ParticipationPrice = "€ 0";
                }

            });
            return general_subscription;
        }

        public List<SubscriptionsDataRow> GetFundLegalSubscriptions(int fundid, string searchValue, string sortColumn, bool asc = true)
        {
            Func<LegalSubscriptions, bool> predicate = null;
            if (!string.IsNullOrEmpty(searchValue))
            {
                predicate = x => (x.FirstName + x.LastName).ToLower().Contains(searchValue.ToLower().Trim()) || x.CreatedOnUtc.ToLongDateString().Contains(searchValue.ToLower());
            }
            Func<LegalSubscriptions, IComparable> orderBy = null;
            switch (sortColumn)
            {
                case "name":
                    orderBy = x => (x.FirstName + x.LastName);
                    break;
                default:
                    orderBy = x => x.CreatedOnUtc;
                    break;
            }

            var queryable = _fundLegalSubscriptionRepository.GetAll(x => x.FundsId == fundid);
            if (predicate != null)
            {
                queryable = queryable.Where(predicate).AsQueryable();
            }
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy).AsQueryable() : queryable.OrderByDescending(orderBy).AsQueryable();

            List<SubscriptionsDataRow> legal_subscription = queryable.Select(x => new SubscriptionsDataRow { id = x.Id, subscriptionType = SubscriptionsTypes.Legal, Address = x.RegisteredAddress, CreateOn = x.CreatedOnUtc, identify = x.Identificationtype, Name = (x.FirstName + " " + x.LastName), NumberofParticipants = x.NumberofParticipations, Place = x.BirthPlace, isIdentified = x.IsIdentified, Idealpaymentstatus = x.Idealpaymentstatus, IDinstatus = x.IDinstatus, IdentificationPaymenttype = x.IdentificationPaymenttype }).ToList();

            Funds singleFund = GetFund(fundid);
            decimal price = singleFund?.participation_price ?? 0;

            legal_subscription.ForEach(x =>
            {
                if (x.identify.HasValue && x.isIdentified)
                {

                    //if (x.identify.Value == IdentificationTypes.Idin_Bank || x.identify.Value == IdentificationTypes.Idin_Ideal)
                    //{
                    //    x.identification = "Geïdentificeerd-(iDin)";
                    //}
                    //else
                    //{
                    x.identification = "Geïdentificeerd";
                    //}
                }
                else
                {
                    x.identification = "Niet geïdentificeerd";
                }
                if (!string.IsNullOrWhiteSpace(x.NumberofParticipants) && !string.IsNullOrEmpty(x.NumberofParticipants))
                {
                    int nop = Convert.ToInt32(x.NumberofParticipants);
                    var calcprice = (price * nop);
                    x.participation_Price = calcprice;
                    string participation_price = "0";
                    var _price = calcprice - Math.Floor(calcprice);
                    if (_price > 0)
                    {
                        participation_price = string.Format("{0:n}", Math.Round(calcprice, 2));
                    }
                    else
                    {
                        participation_price = string.Format("{0:n0}", Math.Round(calcprice, 2)) + ",-";
                    }
                    x.ParticipationPrice = participation_price;
                }
                else
                {
                    x.ParticipationPrice = "€ 0";
                }
            });
            return legal_subscription;
        }

        public bool UpdateIdentification(int subscribeid, SubscriptionsTypes type, string from)
        {
            if (type == SubscriptionsTypes.General)
            {

                GeneralSubscriptions general = _fundGeneralSubscriptionRepository.GetById(subscribeid);
                if (from == "identify")
                {
                    general.IsIdentified = !general.IsIdentified;
                }
                else if (from == "payment")
                {
                    general.IsIdentified = true;
                }
                _fundGeneralSubscriptionRepository.Update(general);

                return true;
            }
            else if (type == SubscriptionsTypes.Legal)
            {
                LegalSubscriptions legal = _fundLegalSubscriptionRepository.GetById(subscribeid);
                if (from == "identify")
                {
                    legal.IsIdentified = !legal.IsIdentified;
                }
                else if (from == "payment")
                {
                    legal.IsIdentified = true;
                }

                _fundLegalSubscriptionRepository.Update(legal);

                return true;
            }
            return false;
        }

        public bool DeleteSubscription(int subscribeid, int fundid, SubscriptionsTypes type)
        {
            bool isdeleted = false;
            if (type == SubscriptionsTypes.General)
            {
                GeneralSubscriptions general = _fundGeneralSubscriptionRepository.GetById(subscribeid);
                _fundGeneralSubscriptionRepository.Delete(general);
                isdeleted = true;
            }
            else if (type == SubscriptionsTypes.Legal)
            {
                LegalSubscriptions legal = _fundLegalSubscriptionRepository.GetById(subscribeid);
                _fundLegalSubscriptionRepository.Delete(legal);
                isdeleted = true;
            }

            AddFundSubscriptionCount(fundid);
            return isdeleted;
        }
    
        public GeneralSubscriptionsPersistModel GetGeneralSubscriptionsPersist(int id)
        {
            GeneralSubscriptionsPersistModel generalpersit = new GeneralSubscriptionsPersistModel();
            var general = GetGeneralSubscription(id);
            _mapper.Map(general, generalpersit);
            return generalpersit;
        }

        public LegalSubscriptionsPersistModel GetLegalSubscriptionsPersist(int id)
        {
            LegalSubscriptionsPersistModel legalpersit = new LegalSubscriptionsPersistModel();
            var legal = GetLegalSubscription(id);
            _mapper.Map(legal, legalpersit);
            return legalpersit;
        }

        public Funds GetLatestFund()
        {
            return _fundRepository.GetAll(x => x.Active).OrderByDescending(x => x.CreatedOnUtc).FirstOrDefault();
        }

        public Funds GetFundByTitle(string fundtitle)
        {
            if (!string.IsNullOrEmpty(fundtitle))
                fundtitle = fundtitle.ToLower().Trim();
            return _fundRepository.Get(e => e.Title.ToLower().Trim() == fundtitle);
        }

        public bool UpdatePayment(int subscribeid, SubscriptionsTypes type, IdentificationTypes identify)
        {
            if (type == SubscriptionsTypes.General)
            {
                GeneralSubscriptions general = GetGeneralSubscription(subscribeid);
                //general.IsIdentified = true; //fusion client side identify onchange event is triggered and from there this value is assigned

                if (!general.Identificationtype.HasValue)
                    general.Identificationtype = identify;

                general.Idealpaymentstatus = IDealPaymentStatus.paid;
                _fundGeneralSubscriptionRepository.Update(general);
                return true;
            }
            else if (type == SubscriptionsTypes.Legal)
            {
                LegalSubscriptions legal = GetLegalSubscription(subscribeid);
                //legal.IsIdentified = true;//fusion client side identify onchange event is triggered and from there this value is assigned

                if (!legal.Identificationtype.HasValue)
                    legal.Identificationtype = identify;

                legal.Idealpaymentstatus = IDealPaymentStatus.paid;
                _fundLegalSubscriptionRepository.Update(legal);
                return true;
            }
            return false;
        }

    }
}
