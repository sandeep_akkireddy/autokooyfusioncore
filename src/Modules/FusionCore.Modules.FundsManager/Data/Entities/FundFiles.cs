using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Entities
{
    public class FundFiles : BaseEntity
    {
        public byte[] File { get; set; }
        public int? FundId { get; set; }
        public virtual Funds Fund { get; set; }
        public string FileName { get; set; }
        public int SortOrder { get; set; }

        public int? Kind { get; set; }
        public int? FundCategoryId { get; set; }

        public virtual FundCategory FundCategory { get; set; }

        public bool websitedownloadfiles { get; set; }

        public string DisplayName { get; set; }

        public string FilePath { get; set; }
    }
}
