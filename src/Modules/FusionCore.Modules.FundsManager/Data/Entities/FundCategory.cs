using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Entities
{
    public class FundCategory : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        public int? ParentId { get; set; }
        public virtual FundCategory Parent { get; set; }
        public int SortOrder { get; set; }

        public int FundId { get; set; }
        public virtual Funds Fund { get; set; }
        public bool Active { get; set; }

       
    }
}
