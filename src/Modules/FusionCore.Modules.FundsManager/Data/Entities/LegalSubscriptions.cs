using FusionCore.Data;
using FusionCore.Modules.FundsManager.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Entities
{
    public class LegalSubscriptions : BaseEntity
    {
        public string RegisteredName { get; set; }

        public string RegisteredAddress { get; set; }

        public string Location { get; set; }

        public string PostalCode { get; set; }

        public string RegistrationNumber { get; set; }

        public string FileName { get; set; }

        public string CorrespondenceAddress { get; set; }

        public string CorrespondencePostalCode { get; set; }

        public string Place { get; set; }

        public string Phone { get; set; }

        public string MobilePhone { get; set; }

        public string Email { get; set; }

        public string Iban { get; set; }

        public string LastName { get; set; }
       
        [StringLength(100)]
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string Sex { get; set; }

        public string BirthPlace { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string IdentityFileName { get; set; }

        public string IssuedAt { get; set; }

        public DateTime IssuedOn { get; set; }

        public DateTime ValidityDate { get; set; }

        public string NumberofParticipations { get; set; }

        public bool AgreementStatus { get; set; }


        public IdentificationTypes? Identificationtype { get; set; }

        public IdentificationTypes? IdentificationPaymenttype { get; set; }

        public IDinStatus? IDinstatus { get; set; }
        public IDealPaymentStatus? Idealpaymentstatus { get; set; }
        public int FundsId { get; set; }

        public virtual Funds Funds { get; set; }

        public bool IsIdentified { get; set; }
        public string AccountNumber { get; set; }

       public bool SalesCompany { get; set; }
        public bool SalaryBonusWork { get; set; }
        public bool Heritage { get; set; }
        public bool BusinessActivities { get; set; }
        public bool Others { get; set; }
        

        public string OtherAssets { get; set; }
        public bool? WWFT { get; set; }
    }
}
