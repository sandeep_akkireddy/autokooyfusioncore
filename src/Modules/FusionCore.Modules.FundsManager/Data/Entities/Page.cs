using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FusionCore.Data;
using FusionCore.Modules.FundsManager.Data.Enums;

namespace FusionCore.Modules.FundsManager.Data.Entities
{
	public class Page : BaseEntity
	{
		[MaxLength(256)]
		public string Title { get; set; }

		[MaxLength(256)]
		public string Header { get; set; }

		public string Url { get; set; }

		public int LocationId { get; set; }
		[NotMapped]
		public Location Location
		{
			get => (Location)LocationId;
			set => LocationId = (int)value;
		}


		//public int? ContentId { get; set; }
		//public virtual Content Content { get; set; }
		public string Content { get; set; }

		public bool Active { get; set; }

		public string MetaTitle { get; set; }

		public string MetaDescription { get; set; }

		public string MetaKeywords { get; set; }

		public int TemplateId { get; set; }

		[NotMapped]
		public Template Template
		{
			get => (Template)TemplateId;
			set => TemplateId = (int)value;
		}

		//public DateTime CreatedOn { get; set; }

		public bool IsAdmin { get; set; }

		public int SortOrder { get; set; }
		public ICollection<PageFiles> PageFiles { get; set; }
	}
}
