using FusionCore.Modules.FundsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Maps
{
    class PageFilesMap : IEntityTypeConfiguration<PageFiles>
    {
        public void Configure(EntityTypeBuilder<PageFiles> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.FileName);
            builder.Property(x => x.SortOrder);

            builder.Property(x => x.File);
            builder.HasOne(x => x.Page).WithMany().HasForeignKey(x => x.PageId);
            builder.Property(x => x.DisplayName);
            builder.Property(x => x.FilePath);

        }
    }
}
