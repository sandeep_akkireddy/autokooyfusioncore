using FusionCore.Modules.FundsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Maps
{
    public class BannerMap : IEntityTypeConfiguration<Banner>
    {
        public void Configure(EntityTypeBuilder<Banner> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Title).IsRequired().HasMaxLength(256);
            builder.Property(x => x.bannerType);
            builder.Property(x => x.Active);
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.Description);
            builder.Property(x => x.Header);
            builder.Property(x => x.FundID);
            builder.HasOne(x => x.Funds).WithMany().HasForeignKey(x => x.FundID);
            builder.Property(x => x.sortOrder);
            builder.Property(x => x.HeaderRed);
            builder.HasOne(x => x.Page).WithMany().HasForeignKey(x => x.PageID);
            builder.Property(x=>x.buttontext);
        }
    }
}
