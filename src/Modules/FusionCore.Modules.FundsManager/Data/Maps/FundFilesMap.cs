using FusionCore.Modules.FundsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Maps
{
    public class FundFilesMap : IEntityTypeConfiguration<FundFiles>
    {
        public void Configure(EntityTypeBuilder<FundFiles> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.FileName);

            builder.Property(x => x.File);
            builder.HasOne(x => x.Fund).WithMany().HasForeignKey(x => x.FundId);
            builder.HasOne(x => x.FundCategory).WithMany().HasForeignKey(x => x.FundCategoryId);
            builder.Property(x => x.Kind);
            builder.Property(x => x.SortOrder);
            builder.Property(x => x.UpdatedOnUtc);
            builder.Property(x => x.websitedownloadfiles);
            builder.Property(x=>x.DisplayName);
            builder.Property(x => x.FilePath);
        }
    }
}
