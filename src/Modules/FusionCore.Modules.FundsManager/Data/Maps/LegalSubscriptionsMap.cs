using FusionCore.Modules.FundsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Maps
{
    public class LegalSubscriptionsMap : IEntityTypeConfiguration<LegalSubscriptions>
    {
        public void Configure(EntityTypeBuilder<LegalSubscriptions> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Title).IsRequired().HasMaxLength(256);
            builder.Property(x => x.RegisteredName);
            builder.Property(x => x.RegisteredAddress);
            builder.Property(x => x.Location);
            builder.Property(x => x.PostalCode);
            builder.Property(x => x.RegistrationNumber);
            builder.Property(x => x.FileName);
            builder.Property(x => x.CorrespondenceAddress);
            builder.Property(x => x.CorrespondencePostalCode);
            builder.Property(x => x.Place);
            builder.Property(x => x.Phone);
            builder.Property(x => x.MobilePhone);
            builder.Property(x => x.Email);
            builder.Property(x => x.Iban);
            builder.Property(x => x.LastName);
            builder.Property(x => x.FirstName);
            builder.Property(x => x.Sex);
            builder.Property(x => x.BirthPlace);
            builder.Property(x => x.DateOfBirth);
            builder.Property(x => x.IdentityFileName);
            builder.Property(x => x.IssuedAt);
            builder.Property(x => x.IssuedOn);
            builder.Property(x => x.ValidityDate);
            builder.Property(x => x.NumberofParticipations);
            builder.Property(x => x.AgreementStatus);
            builder.Property(x => x.Identificationtype);

            builder.Property(x => x.IdentificationPaymenttype);
            builder.Property(x => x.IDinstatus);
            builder.Property(x => x.Idealpaymentstatus);
            builder.HasOne(x => x.Funds).WithMany().HasForeignKey(x => x.FundsId);
            builder.Property(x => x.IsIdentified);
            builder.Property(x => x.AccountNumber);
            builder.Property(x => x.SalesCompany);
            builder.Property(x => x.SalaryBonusWork);
            builder.Property(x => x.Heritage);
            builder.Property(x => x.BusinessActivities);
            builder.Property(x => x.Others);
            builder.Property(x => x.OtherAssets);
            builder.Property(x => x.WWFT);
        }
    }
}
