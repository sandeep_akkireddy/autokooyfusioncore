using FusionCore.Modules.FundsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Maps
{
    public class FundsMap : IEntityTypeConfiguration<Funds>
    {
        public void Configure(EntityTypeBuilder<Funds> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Title).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Active);
            builder.Property(x => x.activeregistration);
            builder.Property(x => x.Address);
            builder.Property(x => x.City);
            builder.Property(x => x.Country);
            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.Dateofcapital);
            builder.Property(x => x.Description);
            builder.Property(x => x.Email);
            builder.Property(x => x.emissioncost);
            builder.Property(x => x.emission_cost);
            builder.Property(x => x.External_Id);
            builder.Property(x => x.FundEndDate);
            builder.Property(x => x.FundStartDate);
            builder.Property(x => x.fundStatus);
            builder.Property(x => x.GeneralDescription);
            builder.Property(x => x.Iban);
            builder.Property(x => x.Latitude);
            builder.Property(x => x.Longitude);
            builder.Property(x => x.MaximumSubscriptions);
            builder.Property(x => x.MetaDescription).HasMaxLength(150);
            builder.Property(x => x.MetaKeywords).HasMaxLength(100);
            builder.Property(x => x.MetaTitle).HasMaxLength(100);
            builder.Property(x => x.participationprice);
            builder.Property(x => x.participation_price);
            builder.Property(x => x.Phonenumber);
            builder.Property(x => x.Rendementen);
            builder.Property(x => x.SplashActive);
            builder.Property(x => x.SplashImageCaption);
            builder.Property(x => x.SplashImageId);
            builder.Property(x => x.SplashImageTitle);
            builder.Property(x => x.ThumbnailImageCaption);
            builder.Property(x => x.ThumbnailImageId);
            builder.Property(x => x.ThumbnailImageTitle);
            builder.Property(x => x.UpdatedOnUtc);
            builder.Property(x => x.Zipcode);
            builder.Property(x => x.SubTitle);
            builder.Property(x => x.fundType);
            builder.Property(x => x.designation);
            builder.Property(x => x.sectionnumber);
            builder.Property(x => x.chapter);
        }
    }
}
