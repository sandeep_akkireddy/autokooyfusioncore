using FusionCore.Modules.FundsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Maps
{
    public class GeneralSubscriptionsMap : IEntityTypeConfiguration<GeneralSubscriptions>
    {
        public void Configure(EntityTypeBuilder<GeneralSubscriptions> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.LastName);

            builder.Property(x => x.Title).IsRequired().HasMaxLength(256);

            builder.Property(x => x.FirstName);
            builder.Property(x => x.Sex);

            builder.Property(x => x.BirthPlace);
            builder.Property(x => x.DateOfBirth);

            builder.Property(x => x.HomeAddress);
            builder.Property(x => x.PostalCode);

            builder.Property(x => x.Residence);

            builder.Property(x => x.CorrespondenceAddressType);
            builder.Property(x => x.CorrespondenceAddress);

            builder.Property(x => x.CorrespondencePostalCode);
            builder.Property(x => x.CorrespondenceResidence);

            builder.Property(x => x.Phone);
            builder.Property(x => x.MobilePhone);
            builder.Property(x => x.Email);

            builder.Property(x => x.Iban);
            builder.Property(x => x.FileName);

            builder.Property(x => x.IssuedAt);
            builder.Property(x => x.IssuedOn);
            builder.Property(x => x.ValidityDate);
            builder.Property(x => x.MartialStatus);
            builder.Property(x => x.MarriedWithContract);
            builder.Property(x => x.MarriedWithoutContract);
            builder.Property(x => x.UnMarriedWithContract);
            builder.Property(x => x.UnMarriedWithoutContract);
            builder.Property(x => x.SpouseLastName);
            builder.Property(x => x.SpouseFirstName);
            builder.Property(x => x.SpouseSex);
            builder.Property(x => x.SpouseBirthPlace);
            builder.Property(x => x.SpouseDateOfBirth);
            builder.Property(x => x.NumberofParticipations);
            builder.Property(x => x.AgreementStatus);
            builder.Property(x => x.Identificationtype);

            builder.Property(x => x.IdentificationPaymenttype);
            builder.Property(x => x.IDinstatus);
            builder.Property(x => x.Idealpaymentstatus);


            builder.HasOne(x => x.Funds).WithMany().HasForeignKey(x => x.FundsId);
            builder.Property(x => x.IsIdentified);
            builder.Property(x => x.AccountNumber);
          
            builder.Property(x => x.OtherAssets);
            builder.Property(x => x.WWFT);
            builder.Property(x => x.SalesCompany);
            builder.Property(x => x.SalaryBonusWork);
            builder.Property(x => x.Heritage);
            builder.Property(x => x.BusinessActivities);
            builder.Property(x => x.Others);

    }
}
}
