using FusionCore.Modules.FundsManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Maps
{
    public class FundCategoryMap : IEntityTypeConfiguration<FundCategory>
    {
        public void Configure(EntityTypeBuilder<FundCategory> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.CreatedOnUtc);
            builder.Property(x => x.Title);

            builder.HasOne(x => x.Fund).WithMany().HasForeignKey(x => x.FundId);
            builder.HasOne(x => x.Parent).WithMany().HasForeignKey(x => x.ParentId);

            builder.Property(x => x.SortOrder);
            builder.Property(x => x.UpdatedOnUtc);
            builder.Property(x => x.Active);
        }
    }
}

