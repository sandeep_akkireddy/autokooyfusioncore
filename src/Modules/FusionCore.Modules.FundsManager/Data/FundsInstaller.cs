using FusionCore.Core.Enums;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Services.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.FundsManager.Data
{
    public class FundsInstaller : IInstallData
    {
        private IFundsService _fundService;

        private IGenericRepository<Funds> _fundRepository;

        public async Task Install(IServiceProvider serviceProvider, bool sampleData)
        {
            LoadServices(serviceProvider);
            await InstallFundManagerMenu(serviceProvider);

            if (sampleData)
            {
                InstallFundDummyData();
            }

            DisposeServices();
        }
        private void DisposeServices()
        {
            _fundService = null;

        }
        private void LoadServices(IServiceProvider serviceProvider)
        {
            _fundService = (IFundsService)serviceProvider.GetService(typeof(IFundsService));

        }
        private void InstallFundDummyData()
        {
            for (var i = 0; i < 16; i++)
            {
                _fundService.PersistFund(new Models.FundsPersistModel
                {
                    Title = "Funds - Novi Media " + i,
                    CreatedOnUtc = DateTime.Now,
                    Active = true,
                    MetaTitle = "test",
                    MetaDescription = "test",
                    MetaKeywords = "test",

                });

                _fundService.PersistFundGeneralSubscription(new Models.GeneralSubscriptionsPersistModel
                {
                    CreatedOnUtc = DateTime.Now,
                    FirstName = "firstname " + i,
                    LastName = "lastname ",
                    HomeAddress = "address",
                    BirthPlace = "place",
                    IsIdentified = false,
                    Idealpaymentstatus = Enums.IDealPaymentStatus.paid,
                    FundsId = 1,
                    Title = "General Subscription " + i

                }) ;
                _fundService.PersistFundLegalSubscription(new Models.LegalSubscriptionsPersistModel
                {
                    CreatedOnUtc = DateTime.Now,
                    FirstName = "firstname " + i,
                    LastName = "lastname ",
                    BirthPlace = "place",
                    IsIdentified = false,
                    Idealpaymentstatus = Enums.IDealPaymentStatus.paid,
                    FundsId = 1,
                    Title = "Legal Subscription " + i
                });
            }
        }
        private async Task InstallFundManagerMenu(IServiceProvider serviceProvider)
        {
            var menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

            var mainMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "fa fa-building",
                Name = "Fondsen",
                Area = "FundsManager",
                Controller = "Funds",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 0,
                ParentId = null,
                HasNewButton = true
            };
            await menuService.InsertMenuItem(mainMenuItem);

        }

    }
}
