using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Enums
{
    public enum FundType
    {
        none,
        storefund,
        heathcarefund,
        housingfund,
        overigefond
    }
}
