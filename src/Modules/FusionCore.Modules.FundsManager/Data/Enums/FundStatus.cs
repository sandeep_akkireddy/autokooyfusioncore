using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Enums
{
    public enum FundStatus
    {
        None,
        New,
        Active,
        Liquidated,
        OutOfScope,
        Stopped
    }
}
