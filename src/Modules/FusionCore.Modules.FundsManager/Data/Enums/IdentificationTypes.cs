using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Enums
{
    public enum IdentificationTypes
    {
        none,
        Idin_Ideal,
        Idin_Bank,
        Post_Ideal,
        Post_Bank
    }
}
