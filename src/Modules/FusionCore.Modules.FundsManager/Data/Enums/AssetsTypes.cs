using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Enums
{
    public enum AssetsTypes
    {
        none,
        SalesCompany,
        SalaryBonusWork,
        Heritage,
        BusinessActivities,
        Other
    }
}
