using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data.Enums
{
    public enum IDealPaymentStatus
    {
        none,
        notpaid,
        paid
    }
}
