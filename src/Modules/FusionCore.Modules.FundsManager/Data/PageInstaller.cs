using FusionCore.Core.Enums;
using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Data.Interfaces;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Interfaces;
using FusionCore.Modules.FundsManager.Services;
using FusionCore.Services.Common;
using System;
using System.Linq;
using System.Threading.Tasks;
using FusionCore.Modules.FundsManager.Models;
using FusionCore.Modules.FundsManager.Data.Enums;

namespace FusionCore.Modules.FundsManager.Data
{
	public class PageInstaller : IInstallData
	{
		private IPageService _pageService;

		private IGenericRepository<Page> _pageRepository;

		public async Task Install(IServiceProvider serviceProvider, bool sampleData)
		{
			LoadServices(serviceProvider);
			await InstallPageManagerMenu(serviceProvider);
			await InstallAdministratorManagerMenu(serviceProvider);
			if (sampleData)
			{
				InstallPageDummyData();
				InstallAdministratorDummyData();

			}

			DisposeServices();
		}
		private void DisposeServices()
		{
			_pageService = null;

		}
		private void LoadServices(IServiceProvider serviceProvider)
		{
			_pageService = (IPageService)serviceProvider.GetService(typeof(IPageService));

		}
		private void InstallPageDummyData()
		{
			for (var i = 0; i < 16; i++)
			{
				_pageService.PersistPage(new PagePersistModel
				{
					Title = "Page - Novi Media " + i,
					Header = "test",
					Url = "https://www.novimediasolutions.nl",
					Location = Location.Main,
					Template = Template.AddressPage,
					CreatedOnUtc = DateTime.Now,
					Active = true,
					MetaTitle = "test",
					MetaDescription = "test",
					MetaKeywords = "test"
				});
			}
		}

		private void InstallAdministratorDummyData()
		{
			for (var i = 0; i < 16; i++)
			{
				_pageService.PersistPage(new PagePersistModel
				{
					Title = "Administrator - Novi Media " + i,
					Header = "test",
					Url = "https://www.novimediasolutions.nl",
					Location = Location.Main,
					Template = Template.AddressPage,
					CreatedOnUtc = DateTime.Now,
					Active = true,
					MetaTitle = "test",
					MetaDescription = "test",
					MetaKeywords = "test",
					IsAdmin = true,
					SortOrder = i
				});
			}
		}
		private async Task InstallPageManagerMenu(IServiceProvider serviceProvider)
		{
			var menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

			var mainMenuItem = new MenuItem
			{
				Type = MenuType.Backend,
				Icon = "clip-stack",
				Name = "Pagina's",
				Area = "PageManager",
				Controller = "Page",
				Action = "Index",
				Parameters = null,
				DisplayOrder = 0,
				ParentId = null,
				HasNewButton = true
			};
			await menuService.InsertMenuItem(mainMenuItem);

		}

		private async Task InstallAdministratorManagerMenu(IServiceProvider serviceProvider)
		{
			var menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

			var mainMenuItem = new MenuItem
			{
				Type = MenuType.Backend,
				Icon = "clip-stack",
				Name = "Beheerder",
				Area = "PageManager",
				Controller = "Administrator",
				Action = "Index",
				Parameters = null,
				DisplayOrder = 0,
				ParentId = null,
				HasNewButton = true
			};
			await menuService.InsertMenuItem(mainMenuItem);

		}
	}
}
