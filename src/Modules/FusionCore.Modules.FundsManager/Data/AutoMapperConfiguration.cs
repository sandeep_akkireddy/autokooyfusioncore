using AutoMapper;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Models;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace FusionCore.Modules.FundsManager.Data
{
    public class AutomapperConfiguration : Profile
    {
        public AutomapperConfiguration()
        {
            CreateMap<Funds, FundDataRow>();

            CreateMap<Funds, FundsPersistModel>()
                .ForMember(d => d.FundStartDate, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.FundStartDate)))
                .ForMember(d => d.FundEndDate, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.FundEndDate)))
                .ForMember(d => d.Dateofcapital, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.Dateofcapital)));

            CreateMap<FundsPersistModel, Funds>()
                  .ForMember(d => d.FundStartDate, o => o.MapFrom<DateTime?>(s => ConvertDateTime.Convert(s.FundStartDate)))
                  .ForMember(d => d.FundEndDate, o => o.MapFrom<DateTime?>(s => ConvertDateTime.Convert(s.FundEndDate)))
                  .ForMember(d => d.Dateofcapital, o => o.MapFrom<DateTime?>(s => ConvertDateTime.Convert(s.Dateofcapital)));

            CreateMap<FundFiles, FundFilePersistModel>();
            CreateMap<FundFilePersistModel, FundFiles>();

            CreateMap<FundCategory, FundCategoryPersistModel>();
            CreateMap<FundCategoryPersistModel, FundCategory>();

            CreateMap<GeneralSubscriptions, GeneralSubscriptionsPersistModel>()
                 .ForMember(d => d.DateOfBirth, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.DateOfBirth)))
                 .ForMember(d => d.SpouseDateOfBirth, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.SpouseDateOfBirth)))
                 .ForMember(d => d.ValidityDate, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.ValidityDate)))
                 .ForMember(d => d.IssuedOn, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.IssuedOn)));

            CreateMap<GeneralSubscriptionsPersistModel, GeneralSubscriptions>()
                  .ForMember(d => d.DateOfBirth, o => o.MapFrom(s => ConvertDateTime.ConvertToDate(s.DateOfBirth)))
                  .ForMember(d => d.SpouseDateOfBirth, o => o.MapFrom(s => ConvertDateTime.ConvertToDate(s.SpouseDateOfBirth)))
                  .ForMember(d => d.ValidityDate, o => o.MapFrom(s => ConvertDateTime.ConvertToDate(s.ValidityDate)))
                  .ForMember(d => d.IssuedOn, o => o.MapFrom(s => ConvertDateTime.ConvertToDate(s.IssuedOn)));


            CreateMap<LegalSubscriptions, LegalSubscriptionsPersistModel>()
                 .ForMember(d => d.DateOfBirth, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.DateOfBirth)))
                 .ForMember(d => d.ValidityDate, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.ValidityDate)))
                 .ForMember(d => d.IssuedOn, o => o.MapFrom(s => ConvertDateTime.ConvertToString(s.IssuedOn)));
            CreateMap<LegalSubscriptionsPersistModel, LegalSubscriptions>()
                  .ForMember(d => d.DateOfBirth, o => o.MapFrom(s => ConvertDateTime.ConvertToDate(s.DateOfBirth)))
                  .ForMember(d => d.ValidityDate, o => o.MapFrom(s => ConvertDateTime.ConvertToDate(s.ValidityDate)))
                  .ForMember(d => d.IssuedOn, o => o.MapFrom(s => ConvertDateTime.ConvertToDate(s.IssuedOn)));


            CreateMap<Banner, BannerDataRow>();

            CreateMap<Banner, BannerPersistModel>();
            CreateMap<BannerPersistModel, Banner>();

            CreateMap<Page, PageDataRow>();

            CreateMap<Page, PagePersistModel>();
            CreateMap<PagePersistModel, Page>();

            CreateMap<PageFiles, PageFilePersistModel>();
            CreateMap<PageFilePersistModel, PageFiles>();

        }
    }

    public class ConvertDateTime
    {
        public static DateTime? Convert(string source)
        {
            if (!string.IsNullOrEmpty(source) && !string.IsNullOrWhiteSpace(source))
            {
                string[] formats = new[] { "yyyy-MM-dd", "M-d-yyyy", "dd-MM-yyyy", "MM-dd-yyyy", "M.d.yyyy", "dd.MM.yyyy", "MM.dd.yyyy" };
                return DateTime.ParseExact(source, formats,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None);
            }
            return null;
        }

        public static string ConvertToString(DateTime? source)
        {
            if (source.HasValue)
            {
                return source.Value.ToString("dd-MM-yyyy");
            }

            return null;
        }

        public static DateTime ConvertToDate(string source)
        {

            string[] formats = new[] { "yyyy-MM-dd", "M-d-yyyy", "dd-MM-yyyy", "MM-dd-yyyy", "M.d.yyyy", "dd.MM.yyyy", "MM.dd.yyyy" };
           
         
            return DateTime.ParseExact(source, formats,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);


        }
    }
}
