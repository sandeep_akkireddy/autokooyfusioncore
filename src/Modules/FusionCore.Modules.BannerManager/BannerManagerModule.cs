using FusionCore.Core.Interfaces;
using System;

namespace FusionCore.Modules.BannerManager
{
    public class BannerManagerModule : IFusionCoreModule
    {
        public string GetSystemName()
        {
            return "FusionCore.Module.BannerManager";
        }
    }
}
