using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.BannerManager.Data.Entities;
using FusionCore.Modules.BannerManager.Interfaces;
using FusionCore.Modules.BannerManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.BannerManager.Services
{
    public class BannerService : BaseService, IBannerService
    {
        private readonly IGenericRepository<Banner> _bannerRepository;
        private readonly IMapper _mapper;
        public BannerService(
            IGenericRepository<Banner> bannerRepository,
            IMapper mapper)
        {
            _bannerRepository = bannerRepository;
            _mapper = mapper;

        }
        public bool ActiveBanner(int id, bool value)
        {
            var dbEntity = GetBanner(id);
            if (dbEntity != null)
            {
                dbEntity.Active = value;
                _bannerRepository.Update(dbEntity);

                return true;
            }
            return false;
        }

        public async Task DeleteBanner(int id)
        {
            var user = await _bannerRepository.GetByIdAsync(id);
            if (user != null)
            {
                int PageOrder = user.sortOrder;

                var allpages = _bannerRepository.GetAll(x => x.sortOrder > PageOrder).ToList();
                allpages.ForEach(x =>
                {
                    x.sortOrder -= 1;
                });
                _bannerRepository.UpdateAll(allpages);

                _bannerRepository.Delete(user);
            }
        }

        public List<BannerPersistModel> GetActiveBanner()
        {
            Expression<Func<Banner, bool>> predicate = x => x.Active;
            Expression<Func<Banner, object>> includes = x => x.Funds;
            var banner = _bannerRepository.GetAll(predicate,true,includes).OrderByDescending(x => x.sortOrder);
            var model= _mapper.Map<List<BannerPersistModel>>(banner);
         
            return model;
        }

        public IPagedList<Banner> GetAllBanners<TKey>(Expression<Func<Banner, bool>> predicate = null, Expression<Func<Banner, TKey>> orderBy = null, bool asc = true, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var queryable = _bannerRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Banner>(queryable, pageIndex, pageSize);
        }

        public Banner GetBanner(int id)
        {
            return _bannerRepository.Get(x => x.Id == id);
        }

        public bool OrderBannerDown(int id, int sortorder)
        {
            var nextbannerorder = (sortorder + 1);
            var banner = _bannerRepository.GetAll(x => x.Id == id || x.sortOrder == nextbannerorder).ToList();
            var currentbanner = banner.FirstOrDefault(x => x.Id == id);
            var nextbanner = banner.FirstOrDefault(x => x.Id != id);
            if (currentbanner != null)
            {
                currentbanner.sortOrder += 1;
            }
            if (nextbanner != null)
            {
                if (nextbanner.sortOrder > 1)
                {
                    nextbanner.sortOrder -= 1;
                }
            }
            if (currentbanner != null && nextbanner != null)
            {
                _bannerRepository.UpdateAll(banner);
            }
            return false;
        }

        public bool OrderBannerUp(int id, int sortorder)
        {
            var prev_bannerorder = (sortorder - 1);
            prev_bannerorder = prev_bannerorder <= 0 ? 1 : prev_bannerorder;
            var banner = _bannerRepository.GetAll(x => x.Id == id || x.sortOrder == prev_bannerorder).ToList();
            var currentbanner = banner.FirstOrDefault(x => x.Id == id);
            var prev_banner = banner.FirstOrDefault(x => x.Id != id);
            if (currentbanner != null)
            {
                if (currentbanner.sortOrder > 1)
                {
                    currentbanner.sortOrder -= 1;
                }
            }
            if (prev_banner != null)
            {
                prev_banner.sortOrder += 1;
            }
            if (currentbanner != null && prev_banner != null)
            {
                _bannerRepository.UpdateAll(banner);
            }
            return false;
        }

        public bool PersistBanner(BannerPersistModel model)
        {
            if (model.Id.HasValue)
            {
                //Edit
                var dbEntity = GetBanner(model.Id.Value);
                if (dbEntity != null)
                {
                    dbEntity.Active = model.Active;
                    dbEntity.BannerFileID = model.BannerFileID;
                    dbEntity.Header = model.Header;
                    dbEntity.HeaderRed = model.HeaderRed;
                    dbEntity.Description = model.Description;
                    dbEntity.Title = model.Title;
                    dbEntity.sortOrder = model.sortOrder;
                    dbEntity.UpdatedOnUtc = DateTime.Now;
                    dbEntity.bannerType = model.bannerType;
                    dbEntity.FundID = model.FundID;

                    _bannerRepository.Update(dbEntity);

                    return true;
                }
            }
            else
            {
                //insert
                var dbEntity = new Banner();
                var max_pageorder_page = _bannerRepository.GetAll().OrderByDescending(x => x.sortOrder).FirstOrDefault();
                if (max_pageorder_page != null)
                {
                    model.sortOrder = max_pageorder_page.sortOrder + 1;
                }
                else
                {
                    model.sortOrder = 1;
                }
                _mapper.Map(model, dbEntity);
                _bannerRepository.Insert(dbEntity);

                return true;
            }

            return false;
        }
    }
}
