using FusionCore.Models.Common;
using FusionCore.Modules.BannerManager.Data.Entities;
using FusionCore.Modules.BannerManager.Models;
using FusionCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.BannerManager.Interfaces
{
    public interface IBannerService : IBaseService
    {
        IPagedList<Banner> GetAllBanners<TKey>(
            Expression<Func<Banner, bool>> predicate = null,
            Expression<Func<Banner, TKey>> orderBy = null,
            bool asc = true,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        Banner GetBanner(int id);
        bool PersistBanner(BannerPersistModel model);
        Task DeleteBanner(int id);

        bool ActiveBanner(int id, bool value);

        bool OrderBannerUp(int id, int sortorder);

        bool OrderBannerDown(int id, int sortorder);

        List<BannerPersistModel> GetActiveBanner();
    }
}
