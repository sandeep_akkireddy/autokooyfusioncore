using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.BannerManager.Models
{
    public class BannerDataTable : BaseDataTable
    {
        public List<BannerDataRow> Data { get; set; }
    }
    public class BannerDataRow
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool Active { get; set; }

        public int sortOrder { get; set; }

        public int? BannerFileID { get; set; }
    }
}
