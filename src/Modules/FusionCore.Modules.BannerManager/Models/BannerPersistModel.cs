using FusionCore.Models.Common;
using FusionCore.Modules.BannerManager.Data.Enums;
using FusionCore.Modules.FundsManager.Data.Entities;
using FusionCore.Modules.FundsManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.BannerManager.Models
{
    public class BannerPersistModel : BaseModel
    {
        [Required]
        [StringLength(256)]
        public string Title { get; set; }
        public bool Active { get; set; }
        [Required]
        public BannerType bannerType { get; set; }
        public string Header { get; set; }
        [MaxLength(200)]
        public string Description { get; set; }
        public int? FundID { get; set; }
        public int? BannerFileID { get; set; }
        public int sortOrder { get; set; }
        public FundsPersistModel Funds { get; set; }
        public string HeaderRed { get; set; }
    }
}
