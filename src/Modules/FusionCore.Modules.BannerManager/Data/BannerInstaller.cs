using FusionCore.Core.Enums;
using FusionCore.Core.Interfaces;
using FusionCore.Data.Entities;
using FusionCore.Modules.BannerManager.Interfaces;
using FusionCore.Modules.BannerManager.Models;
using FusionCore.Services.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FusionCore.Modules.BannerManager.Data
{
    public class BannerInstaller : IInstallData
    {
        private IBannerService _bannerService;

        public async Task Install(IServiceProvider serviceProvider, bool sampleData)
        {
            LoadServices(serviceProvider);

            await InstallBannerManagerMenu(serviceProvider);
            if (sampleData)
            {
                InstallBannersDummyData();
            }

            DisposeServices();
        }
        private void DisposeServices()
        {
            _bannerService = null;

        }
        private void LoadServices(IServiceProvider serviceProvider)
        {
            _bannerService = (IBannerService)serviceProvider.GetService(typeof(IBannerService));
        }

        private async Task InstallBannerManagerMenu(IServiceProvider serviceProvider)
        {
            var menuService = (IMenuService)serviceProvider.GetService(typeof(IMenuService));

            var mainMenuItem = new MenuItem
            {
                Type = MenuType.Backend,
                Icon = "fa fa-user",
                Name = "Banner",
                Area = "BannerManager",
                Controller = "Banner",
                Action = "Index",
                Parameters = null,
                DisplayOrder = 0,
                ParentId = null,
                HasNewButton = true
            };
            await menuService.InsertMenuItem(mainMenuItem);

        }

        private void InstallBannersDummyData()
        {
            for (var i = 1; i < 16; i++)
            {
                _bannerService.PersistBanner(new BannerPersistModel
                {
                    Active = true,
                    Title = "Team " + i,
                    sortOrder = i,
                    bannerType = Enums.BannerType.Presentatie
                });
            }
        }
    }
}
