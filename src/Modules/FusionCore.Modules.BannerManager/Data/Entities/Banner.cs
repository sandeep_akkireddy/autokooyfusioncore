using FusionCore.Data;
using FusionCore.Modules.BannerManager.Data.Enums;
using FusionCore.Modules.FundsManager.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.BannerManager.Data.Entities
{
    public class Banner : BaseEntity
    {
        [Required]
        [StringLength(256)]
        public string Title { get; set; }
        public bool Active { get; set; }
        [Required]
        public BannerType bannerType { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public int? FundID { get; set; }
        public virtual Funds Funds { get; set; }

        public int? BannerFileID { get; set; }
        public int sortOrder { get; set; }

        public string HeaderRed { get; set; }
    }
}
