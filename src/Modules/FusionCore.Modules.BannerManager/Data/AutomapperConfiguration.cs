using AutoMapper;
using FusionCore.Modules.BannerManager.Models;
using System;
using System.Collections.Generic;
using System.Text;
using FusionCore.Modules.BannerManager.Data.Entities;
namespace FusionCore.Modules.BannerManager.Data
{
    public class AutomapperConfiguration : Profile
    {
        public AutomapperConfiguration()
        {
            CreateMap<Banner, BannerDataRow>();

            CreateMap<Banner, BannerPersistModel>();
            CreateMap<BannerPersistModel, Banner>();
        }
    }
}
