using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using FusionCore.Data.Interfaces;
using FusionCore.Models.Common;
using FusionCore.Modules.PageManager.Data.Entities;
using FusionCore.Modules.PageManager.Models;
using FusionCore.Modules.PageManager.Interfaces;
using FusionCore.Services;
using System.Collections.Generic;

namespace FusionCore.Modules.PageManager.Services
{
    public class PageService : BaseService, IPageService
    {
        private readonly IGenericRepository<Page> _pageRepository;
        private readonly IGenericRepository<PageFiles> _pageFileRepository;
        private readonly IMapper _mapper;

        public PageService(
            IGenericRepository<Page> pageRepository,
            IGenericRepository<PageFiles> pageFileRepository,
            IMapper mapper)
        {
            _pageRepository = pageRepository;
            _pageFileRepository = pageFileRepository;
            _mapper = mapper;
        }

        public IPagedList<Page> GetAllPages<TKey>(Expression<Func<Page, bool>> predicate = null,
            Expression<Func<Page, TKey>> orderBy = null, bool asc = true,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var queryable = _pageRepository.GetAll(predicate);
            if (orderBy != null) queryable = asc ? queryable.OrderBy(orderBy) : queryable.OrderByDescending(orderBy);
            return new PagedList<Page>(queryable, pageIndex, pageSize);
        }

        public Page GetPage(int id)
        {
            return _pageRepository.Get(x => x.Id == id);
        }


        public async Task DeletePage(int id,string pagefilepath=null)
        {
            List<string> deletefundfilenames = new List<string>();
            var user = await _pageRepository.GetByIdAsync(id);
            if (user != null)
            {
                bool IsAdmin = user.IsAdmin;
                int PageOrder = user.SortOrder;

                if (IsAdmin)
                {
                    var allpages = _pageRepository.GetAll(x => x.IsAdmin && x.SortOrder > PageOrder).ToList();
                    allpages.ForEach(x =>
                    {
                        x.SortOrder -= 1;
                    });
                    _pageRepository.UpdateAll(allpages);

                    var removeFilePages = _pageFileRepository.GetAll(x => x.PageId == id).ToList();
                    deletefundfilenames.AddRange(removeFilePages.Select(x => x.FilePath).ToList());
                    _pageFileRepository.DeleteAll(removeFilePages);
                }

                _pageRepository.Delete(user);

                if (deletefundfilenames != null && deletefundfilenames.Any())
                {
                    DeletePhysicalFiles(deletefundfilenames, pagefilepath);
                }
            }
        }
        void DeletePhysicalFiles(List<string> filenames, string pagefilepath)
        {
            if (filenames != null && filenames.Any())
            {
                filenames.ForEach(x =>
                {
                    if (System.IO.File.Exists(pagefilepath + x))
                    {
                        System.IO.File.Delete(pagefilepath + x);
                    }
                });
            }

        }
        public bool PersistPage(PagePersistModel model,string pagefilepath = null)
        {
            if (model.Id.HasValue)
            {
                List<string> deletefilenames = new List<string>();
                //Edit
                var dbEntity = GetPage(model.Id.Value);
                if (dbEntity != null)
                {
                    dbEntity.Active = model.Active;
                    dbEntity.Content = model.Content;
                    dbEntity.Header = model.Header;
                    dbEntity.MetaDescription = model.MetaDescription;
                    dbEntity.MetaKeywords = model.MetaKeywords;
                    dbEntity.MetaTitle = model.MetaTitle;
                    dbEntity.TemplateId = (int)model.Template;
                    dbEntity.Title = model.Title;
                    dbEntity.UpdatedOnUtc = DateTime.Now;
                    dbEntity.Url = model.Url;

                    _pageRepository.Update(dbEntity);

                    if (model.IsAdmin)
                    {
                        var pageFileEntity = GetPageFiles(model.Id.Value);
                        if (pageFileEntity != null && pageFileEntity.Any())
                        {
                            var new_pageFilesEntity = new List<PageFiles>();
                            _mapper.Map(model.PageFiles, new_pageFilesEntity);

                            var ids = model.PageFiles.Where(x => x.Id > 0 && x.Id.HasValue).Select(x => x.Id.Value).ToList();

                            var removeEntity = pageFileEntity.Where(x => !ids.Contains(x.Id) && x.Id > 0).ToList();
                            deletefilenames.AddRange(removeEntity.Select(x=>x.FilePath).ToList());
                            _pageFileRepository.DeleteAll(removeEntity);

                            var changeorder = pageFileEntity.Where(x => x.Id > 0).ToList();
                            changeorder.ForEach(x =>
                            {
                                var edit = new_pageFilesEntity.FirstOrDefault(z => z.Id == x.Id);
                                if (edit != null)
                                {
                                    x.SortOrder = edit.SortOrder;
                                    x.DisplayName = edit.DisplayName;
                                }
                            });
                            _pageFileRepository.UpdateAll(pageFileEntity);

                            var addEntity = new_pageFilesEntity.Where(x => x.Id == 0).ToList();
                            addEntity.ForEach(x => { x.PageId = model.Id.Value; });
                            _pageFileRepository.InsertAll(addEntity);

                        }
                        else if (model.PageFiles != null && model.PageFiles.Any())
                        {
                            var pageFilesEntity = new List<PageFiles>();
                            _mapper.Map(model.PageFiles, pageFilesEntity);
                            pageFilesEntity.ForEach(x =>
                            {
                                x.PageId = dbEntity.Id;
                            });
                            _pageFileRepository.InsertAll(pageFilesEntity);
                        }
                    }
                    if (deletefilenames != null && deletefilenames.Any())
                    {
                        DeletePhysicalFiles(deletefilenames, pagefilepath);
                    }
                    return true;
                }
            }
            else
            {
                //initialize page order
                if (model.IsAdmin)
                {
                    var max_pageorder_page = _pageRepository.GetAll(x => x.IsAdmin).OrderByDescending(x => x.SortOrder).FirstOrDefault();
                    if (max_pageorder_page != null)
                    {
                        model.SortOrder = max_pageorder_page.SortOrder + 1;
                    }
                    else
                    {
                        model.SortOrder = 1;
                    }
                }
                //insert
                var dbEntity = new Page();
                _mapper.Map(model, dbEntity);
                _pageRepository.Insert(dbEntity);
                if (model.PageFiles != null && model.PageFiles.Any())
                {
                    var pageFilesEntity = new List<PageFiles>();
                    _mapper.Map(model.PageFiles, pageFilesEntity);
                    pageFilesEntity.ForEach(x =>
                    {
                        x.PageId = dbEntity.Id;
                    });
                    _pageFileRepository.InsertAll(pageFilesEntity);
                }
                return true;
            }

            return false;
        }

        public bool ActivePage(int id, bool value)
        {
            var dbEntity = GetPage(id);
            if (dbEntity != null)
            {
                dbEntity.Active = value;
                _pageRepository.Update(dbEntity);

                return true;
            }
            return false;
        }

        public bool OrderPageUp(int id, int sortorder)
        {
            var prev_pageorder = (sortorder - 1);
            prev_pageorder = prev_pageorder <= 0 ? 1 : prev_pageorder;
            var pages = _pageRepository.GetAll(x => x.IsAdmin && (x.Id == id || x.SortOrder == prev_pageorder)).ToList();
            var currentpage = pages.FirstOrDefault(x => x.Id == id);
            var prev_page = pages.FirstOrDefault(x => x.Id != id);
            if (currentpage != null)
            {
                if (currentpage.SortOrder > 1)
                {
                    currentpage.SortOrder -= 1;
                }
            }
            if (prev_page != null)
            {
                prev_page.SortOrder += 1;
            }
            if (prev_page != null && currentpage != null)
            {
                _pageRepository.UpdateAll(pages);
            }
            return false;
        }

        public bool OrderPageDown(int id, int sortorder)
        {
            var nextpageorder = (sortorder + 1);
            var pages = _pageRepository.GetAll(x => x.IsAdmin && (x.Id == id || x.SortOrder == nextpageorder)).ToList();
            var currentpage = pages.FirstOrDefault(x => x.Id == id);
            var nextpage = pages.FirstOrDefault(x => x.Id != id);
            if (currentpage != null)
            {
                currentpage.SortOrder += 1;
            }
            if (nextpage != null)
            {
                if (nextpage.SortOrder > 1)
                {
                    nextpage.SortOrder -= 1;
                }
            }
            if (nextpage != null && currentpage != null)
            {
                _pageRepository.UpdateAll(pages);
            }
            return false;
        }

        public List<PageFiles> GetPageFiles(int pageId)
        {
            return _pageFileRepository.GetAll(x => x.PageId == pageId).OrderBy(x=>x.SortOrder).ToList();
        }

        public PageFiles GetPageFile(int id)
        {
            return _pageFileRepository.Get(x => x.Id == id);
        }

        public List<PagePersistModel> GetActivePages(bool isadmin)
        {
            var teams = _pageRepository.GetAll(x => x.Active && x.IsAdmin == isadmin).OrderByDescending(x => x.SortOrder).ToList();
            return _mapper.Map<List<PagePersistModel>>(teams);
        }

        public bool IsExistsURL(string url)
        {
            return _pageRepository.GetAll(x => x.Active && !x.IsAdmin && url.ToLower().Trim().Contains(x.Url.ToLower().Trim())).Any();
        }

        public PagePersistModel GetPageByUrl(string url)
        {
            var page = _pageRepository.Get(x => x.Active && !x.IsAdmin && url.ToLower().Trim().Contains(x.Url.ToLower().Trim()));
            return _mapper.Map<PagePersistModel>(page);
        }
    }
}
