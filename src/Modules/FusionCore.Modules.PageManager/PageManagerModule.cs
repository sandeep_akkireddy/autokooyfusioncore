﻿using System;
using System.Collections.Generic;
using System.Text;
using FusionCore.Core.Interfaces;

namespace FusionCore.Modules.PageManager
{
    public class PageManagerModule : IFusionCoreModule
    {
		public string GetSystemName()
		{
			return "FusionCore.Module.PageManager";
		}
	}
}
