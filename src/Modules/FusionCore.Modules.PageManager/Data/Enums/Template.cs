﻿using FusionCore.Core.Extensions;

namespace FusionCore.Modules.PageManager.Data.Enums
{
	public enum Template
	{
		[StringValue("Standaard")]
		Standard = 1,
		[StringValue("Lege pagina")]
		LawPage = 2,
		[StringValue("Adres pagina")]
		AddressPage = 3
	}
}
