﻿using FusionCore.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.PageManager.Data.Enums
{
	public enum Location
	{
		[StringValue("None")]
		None=1,
		[StringValue("Main")]
		Main=2,
		[StringValue("Footer")]
		Footer=3
	}
}
