using FusionCore.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.PageManager.Data.Entities
{
	public class PageFiles : BaseEntity
	{
		public byte[] File { get; set; }
		public int SortOrder { get; set; }
		public int PageId { get; set; }
		public Page Page { get; set; }
		public string FileName { get; set; }

        public string DisplayName { get; set; }
        public string FilePath { get; set; }
	}
}
