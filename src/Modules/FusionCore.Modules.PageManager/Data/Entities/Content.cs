﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FusionCore.Data;
using FusionCore.Modules.PageManager.Data.Enums;

namespace FusionCore.Modules.PageManager.Data.Entities
{
	public class Content : BaseEntity
	{
		public Content()
		{
			Pages = new List<Page>();
		}
		public int ContentId { get; set; }

		public string Name { get; set; }

		public ICollection<Page> Pages { get; set; }
	}
}
