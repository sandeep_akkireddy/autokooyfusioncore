﻿using AutoMapper;
using FusionCore.Modules.PageManager.Data.Entities;
using FusionCore.Modules.PageManager.Models;
using System;
using FusionCore.Modules.PageManager.Interfaces;

namespace FusionCore.Modules.PageManager.Data
{
	public class AutomapperConfiguration : Profile
	{
		public AutomapperConfiguration()
		{
			CreateMap<Page, PageDataRow>();

			CreateMap<Page, PagePersistModel>();
			CreateMap<PagePersistModel, Page>();

			CreateMap<PageFiles, PageFilePersistModel>();
			CreateMap<PageFilePersistModel, PageFiles>();

		}
	}
}
