﻿using FusionCore.Modules.PageManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionCore.Modules.PageManager.Data.Maps
{
	public class ContentMap : IEntityTypeConfiguration<Content>
	{
		public void Configure(EntityTypeBuilder<Content> builder)
		{
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedOnAdd();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}
