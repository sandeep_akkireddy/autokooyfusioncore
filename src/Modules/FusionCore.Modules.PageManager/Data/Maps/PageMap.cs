﻿using FusionCore.Data.Maps;
using FusionCore.Modules.PageManager.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FusionCore.Modules.PageManager.Data.Maps
{
	public class PageMap : IEntityTypeConfiguration<Page>
	{
		public void Configure(EntityTypeBuilder<Page> builder)
		{
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedOnAdd();
			builder.Property(x => x.Title).IsRequired().HasMaxLength(256);
			builder.Property(x => x.Header).HasMaxLength(256);
			builder.Property(x => x.Url);
			builder.Property(x => x.Location);
			builder.Property(x => x.Template);
			//builder.HasOne(x => x.Content).WithMany().HasForeignKey(x => x.ContentId);
			builder.Property(x => x.Content);
			builder.Property(x => x.Active);
			builder.Property(x => x.MetaTitle);
			builder.Property(x => x.MetaDescription);
			builder.Property(x => x.MetaKeywords);
			builder.Property(x=>x.CreatedOnUtc);
			builder.Property(x => x.IsAdmin);
			builder.Property(x => x.SortOrder);
		}
	}
}
