using FusionCore.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FusionCore.Modules.PageManager.Models
{
	public class PageFilePersistModel : BaseModel
	{

		public string FileName { get; set; }
		public byte[] File { get; set; }
		public int SortOrder { get; set; }
		public int PageId { get; set; }

		public DateTime CreatedOnUtc { get; set; }
		public DateTime UpdatedOnUtc { get; set; }
		public string TempID { get; set; }
        public string DisplayName { get; set; }
        public string FilePath { get; set; }
    }
}
