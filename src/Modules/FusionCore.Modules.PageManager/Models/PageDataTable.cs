﻿using System;
using System.Collections.Generic;
using FusionCore.Models.Common;
using FusionCore.Modules.PageManager.Data.Enums;

namespace FusionCore.Modules.PageManager.Models
{
	public class PageDataTable : BaseDataTable
	{
		public List<PageDataRow> Data { get; set; }
	}

	public class PageDataRow
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Header { get; set; }
		public string Url { get; set; }
		public Location Location { get; set; }
		public Template Template { get; set; }
		public string Content { get; set; }

		public bool Active { get; set; }

		public DateTime CreatedOnUtc { get; set; }
		public bool IsAdmin { get; set; }

		public int SortOrder { get; set; }
	}
}
