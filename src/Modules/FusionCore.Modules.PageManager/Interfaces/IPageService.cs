using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FusionCore.Models.Common;
using FusionCore.Modules.PageManager.Data.Entities;
using FusionCore.Modules.PageManager.Models;
using FusionCore.Services;

namespace FusionCore.Modules.PageManager.Interfaces
{
    public interface IPageService : IBaseService
    {
        IPagedList<Page> GetAllPages<TKey>(
         Expression<Func<Page, bool>> predicate = null,
         Expression<Func<Page, TKey>> orderBy = null,
         bool asc = true,
         int pageIndex = 0,
         int pageSize = int.MaxValue);

        Page GetPage(int id);
        bool PersistPage(PagePersistModel model, string pagefilepath = null);
        Task DeletePage(int id, string pagefilepath = null);

        bool ActivePage(int id, bool value);

        bool OrderPageUp(int id, int sortorder);

        bool OrderPageDown(int id, int sortorder);

        List<PageFiles> GetPageFiles(int pageId);
        PageFiles GetPageFile(int id);

        List<PagePersistModel> GetActivePages(bool isadmin);

        bool IsExistsURL(string url);

        PagePersistModel GetPageByUrl(string url);
    }
}
