Migrations
====================

To make a migration, make sure you are using the SQL provider not the inmemory provider. Change the value 'true' to 'false' in the appsettings.Development.json.
Also, as a temporary workaround go to 'FusionCoreContext.cs' and change 'var entryAssembly = Assembly.GetEntryAssembly();' to 'var entryAssembly = Assembly.Load("FusionCore")'
Go to the web project folder, start a command prompt there and run the following command:

> dotnet ef migrations add NAME --project ../Libraries/FusionCore.Migrations


PDF Creator
====================
To run the pdf creator you need to have the following installed: https://wkhtmltopdf.org/downloads.html
Set the bin location in the settings.json