
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ImageProcessor;
using ImageProcessor.Imaging;

namespace FusionCore.ImageCrop
{
    public class ImageCropHelper
    {
        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public static byte[] ResizeAndCrop(byte[] image, int width, int height)
        {
            using (var ms = new MemoryStream())
            {
                using (var imgf = new ImageFactory(true))
                    imgf.Load(image).Resize(new ResizeLayer(new Size(width, height), ResizeMode.Crop)).Save(ms);

                return ms.ToArray();
            }
        }
        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        public static void CropFundImage(byte[] image, string filepath)
        {
            ImageCodecInfo encoder = GetEncoder(ImageFormat.Jpeg);

            System.Drawing.Imaging.Encoder myEncoder =
                System.Drawing.Imaging.Encoder.Quality;

            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);
            myEncoderParameters.Param[0] = myEncoderParameter;

            int origWidth;
            int origHeight;
            using (var s = new MemoryStream(image))
            {
                //byte[] image = ReadFully(fs);
                using (Bitmap m = new Bitmap(s))
                {
                    origWidth = m.Width;
                    origHeight = m.Height;
                }
            }
            //int sngRatio = origWidth / origHeight;

            int newWidth = 1000;
            int newHeight = 650;//(newWidth / sngRatio);

            if (origWidth > newWidth && origHeight > newHeight)
            {

            }
            else
            {
                newWidth = origWidth;

                //1000x650 dimension
                //10:6.5 aspect ratio
                //1.538 decimal 
                newHeight = (origWidth * 1000) / 1538;
            }
            byte[] cropimage = ResizeAndCrop(image, newWidth, newHeight);

            using (Stream s = new MemoryStream(cropimage))
            {
                using (Bitmap bmp = new Bitmap(s))
                {
                    bmp.Save(filepath, encoder, myEncoderParameters);
                }
            }
        }
    }
}
