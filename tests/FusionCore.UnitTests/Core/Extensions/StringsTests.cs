﻿using System.Collections.Generic;
using FusionCore.Core.Extensions;
using NUnit.Framework;

namespace FusionCore.UnitTests.Core.Extensions
{
    [TestFixture]
    public class StringsTests
    {
        [Test]
        public void WordCount_With_Null_Returns_0()
        {
            var result = Strings.WordCount(null);
            Assert.IsTrue(result == 0);
        }

        [Test]
        public void WordCount_With_Spaces_Returns_Expected_Value()
        {
            var result = "test test test test".WordCount();
            Assert.IsTrue(result == 4);
        }

        [Test]
        public void WordCount_With_Dot_Returns_Expected_Value()
        {
            var result = "test.test.test.test".WordCount();
            Assert.IsTrue(result == 4);
        }

        [Test]
        public void WordCount_With_QuestionMark_Returns_Expected_Value()
        {
            var result = "test?test?test?test".WordCount();
            Assert.IsTrue(result == 4);
        }

        [Test]
        public void WordCount_Returns_Expected_Value()
        {
            var result = "test test?test.test".WordCount();
            Assert.IsTrue(result == 4);
        }

        [Test]
        public void FirstCharToUpper_With_Null_Returns_Expected_Value()
        {
            var result = Strings.FirstCharToUpper(null);
            Assert.IsTrue(result == string.Empty);
        }

        [Test]
        public void FirstCharToUpper_Returns_Expected_Value()
        {
            var result = "test".FirstCharToUpper();
            Assert.IsTrue(result == "Test");

        }

        [Test]
        public void HighlightKeywords_With_Null_Returns_Null()
        {
            var result = Strings.HighlightKeywords(null, null, null);
            Assert.IsNull(result);
        }

        [Test]
        public void HighlightKeywords_With_Null_Keywords_Returns_BaseValue()
        {
            var baseString = "test highlight test";
            var result = baseString.HighlightKeywords(null, null);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == baseString);
        }

        [Test]
        public void HighlightKeywords_With_Empty_Keywords_Returns_BaseValue()
        {
            var baseString = "test highlight test";
            var result = baseString.HighlightKeywords(new List<string>(), null);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == baseString);
        }

        [Test]
        public void HighlightKeywords_Returns_Expected_Value()
        {
            var baseString = "test highlight test";
            var className = "testclass";
            var result = baseString.HighlightKeywords(new List<string> { "highlight" }, className);
            Assert.IsNotNull(result);
            Assert.IsTrue(result != baseString);
            Assert.IsTrue(result.Contains(className));
        }

        [Test]
        public void StripHtml_With_Null_Value_Returns_Empty_String()
        {
            Assert.IsTrue(Strings.StripHtml(null) == string.Empty);
        }

        [Test]
        public void StripHtml_Returns_Expected_Value()
        {
            var expectedResult = "<p><i><u><b><strong>test</strong></b></u></i></p><br><br />";
            var baseString = $"<div>{expectedResult}</div>";
            var result = baseString.StripHtml();
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }

        [Test]
        public void StripHtml_All_Returns_Expected_Value()
        {
            var expectedResult = "test";
            var baseString = $"<div><p><i><u><b><strong>{expectedResult}</strong></b></u></i></p><br><br /></div>";
            var result = baseString.StripHtml(false, false, false, false, false);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }

        [Test]
        public void StripHtml_ignoreParagraphs_Returns_Expected_Value()
        {
            var expectedResult = "<i><u><b><strong>test</strong></b></u></i><br><br />";
            var baseString = $"<div><p><P>{expectedResult}</P></p></div>";
            var result = baseString.StripHtml(false);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }

        [Test]
        public void StripHtml_ignoreItalic_Returns_Expected_Value()
        {
            var expectedResult = "<p><u><b><strong>test</strong></b></u></p><br><br />";
            var baseString = $"<div><i><I>{expectedResult}</I></i></div>";
            var result = baseString.StripHtml(ignoreItalic: false);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }

        [Test]
        public void StripHtml_ignoreUnderline_Returns_Expected_Value()
        {
            var expectedResult = "<p><i><b><strong>test</strong></b></i></p><br><br />";
            var baseString = $"<div><u><U>{expectedResult}</U><u></div>";
            var result = baseString.StripHtml(ignoreUnderline: false);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }

        [Test]
        public void StripHtml_ignoreBold_Returns_Expected_Value()
        {
            var expectedResult = "<p><i><u><b><strong>test</strong></b></u></i></p>";
            var baseString = $"<div>{expectedResult}<br><br /><BR></div>";
            var result = baseString.StripHtml(ignoreLinebreak: false);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }

        [Test]
        public void StripHtml_ignoreLinebreak_Returns_Expected_Value()
        {
            var expectedResult = "<p><i><u>test</u></i></p><br><br />";
            var baseString = $"<div><b><B><strong><StrOng>{expectedResult}</b></B></strong></StrOng></div>";
            var result = baseString.StripHtml(ignoreBold: false);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }

        [Test]
        public void StripHtml_ignoreTags_Returns_Expected_Value()
        {
            var expectedResult = "<test><p><i><u><b><strong>test</strong></b></u></i></p><br><br /><test>";
            var baseString = $"<div><TEST>{expectedResult}<TEST></div>";
            var result = baseString.StripHtml(otherTagsToIgnore: new List<string> { "test" });
            Assert.IsNotNull(result);
            Assert.IsTrue(result == expectedResult);
        }
    }
}
