﻿using System;
using System.Globalization;
using FusionCore.Core.Extensions;
using NUnit.Framework;

namespace FusionCore.UnitTests.Core.Extensions
{
	[TestFixture]
	public class DatesTests
	{
		[Test]
		public void Age_Returns_Expected_Result()
		{
			var dt = new DateTime(1983, 01, 01, 12, 00, 00);
			var result = dt.Age();

			Assert.IsTrue(result > 0);
			Assert.IsTrue(result == (DateTime.Now.Year - dt.Year));
		}

		[Test]
		public void GetDayNumber_Returns_Expected_Result()
		{
			var dt1 = new DateTime(2016, 01, 01);
			var dt2 = new DateTime(2016, 01, 02);
			var dt3 = new DateTime(2016, 01, 03);
			var dt4 = new DateTime(2016, 01, 04);

			Assert.IsTrue(dt1.GetDayNumber() == "1st");
			Assert.IsTrue(dt2.GetDayNumber() == "2nd");
			Assert.IsTrue(dt3.GetDayNumber() == "3rd");
			Assert.IsTrue(dt4.GetDayNumber() == "4th");
		}

		[Test]
		public void IsWeekday_Returns_Expected_Result()
		{
			var dt1 = new DateTime(2016, 01, 01);
			var dt2 = new DateTime(2016, 01, 02);
			var dt3 = new DateTime(2016, 01, 03);
			var dt4 = new DateTime(2016, 01, 04);
			var dt5 = new DateTime(2016, 01, 05);
			var dt6 = new DateTime(2016, 01, 06);
			var dt7 = new DateTime(2016, 01, 07);
			Assert.IsTrue(dt1.IsWeekday());
			Assert.IsTrue(!dt2.IsWeekday());
			Assert.IsTrue(!dt3.IsWeekday());
			Assert.IsTrue(dt4.IsWeekday());
			Assert.IsTrue(dt5.IsWeekday());
			Assert.IsTrue(dt6.IsWeekday());
			Assert.IsTrue(dt7.IsWeekday());
		}

		[Test]
		public void IsWeekend_Returns_Expected_Result()
		{
			var dt1 = new DateTime(2016, 01, 01);
			var dt2 = new DateTime(2016, 01, 02);
			var dt3 = new DateTime(2016, 01, 03);
			var dt4 = new DateTime(2016, 01, 04);
			var dt5 = new DateTime(2016, 01, 05);
			var dt6 = new DateTime(2016, 01, 06);
			var dt7 = new DateTime(2016, 01, 07);
			Assert.IsTrue(!dt1.IsWeekend());
			Assert.IsTrue(dt2.IsWeekend());
			Assert.IsTrue(dt3.IsWeekend());
			Assert.IsTrue(!dt4.IsWeekend());
			Assert.IsTrue(!dt5.IsWeekend());
			Assert.IsTrue(!dt6.IsWeekend());
			Assert.IsTrue(!dt7.IsWeekend());
		}

		[Test]
		public void IsLeapYear_Returns_Expected_Result()
		{

			var dt1 = new DateTime(2002, 01, 01);
			var dt2 = new DateTime(2004, 01, 02);
			Assert.IsTrue(!dt1.IsLeapYear());
			Assert.IsTrue(dt2.IsLeapYear());
		}

		[Test]
		public void ElapsedSeconds_Returns_Expected_Result()
		{
			var seconds = 10;
			var dt1 = DateTime.Now.AddSeconds(-seconds);
			var result = dt1.ElapsedSeconds();
			Assert.IsTrue(Convert.ToInt32(result) == seconds);
		}

		[Test]
		public void GetFirstDayOfMonth_Returns_Expected_Result()
		{
			var dt1 = new DateTime(2017, 02, 20);
			var result = dt1.GetFirstDayOfMonth();
			Assert.IsTrue(result != null);
			Assert.IsTrue(result.Year == dt1.Year);
			Assert.IsTrue(result.Month == dt1.Month);
			Assert.IsTrue(result.Day == 1);
		}

		[Test]
		public void GetLastDayOfMonth_Returns_Expected_Result()
		{
			var dt = new DateTime(2018, 01, 05);
			var result = dt.GetLastDayOfMonth();
			Assert.IsTrue(result != null);
			Assert.IsTrue(result.Year == dt.Year);
			Assert.IsTrue(result.Month == dt.Month);
			Assert.IsTrue(result.Day == 31);
		}

		[TestCase(0, 0, "just now")]
		[TestCase(0, -59, "just now")]
		[TestCase(0, -60, "1 minute ago")]
		[TestCase(0, -61, "1 minute ago")]
		[TestCase(0, -119, "1 minute ago")]
		[TestCase(0, -120, "2 minutes ago")]
		[TestCase(0, -3599, "59 minutes ago")]
		[TestCase(0, -3600, "1 hour ago")]
		[TestCase(0, -7159, "1 hour ago")]
		[TestCase(0, -7200, "2 hours ago")]
		[TestCase(0, -86399, "23 hours ago")]
		[TestCase(0, -86400, "yesterday")]
		[TestCase(-1, 0, "yesterday")]
		[TestCase(-3, 0, "3 days ago")]
		[TestCase(-6, 0, "6 days ago")]
		[TestCase(-7, 0, "1 week ago")]
		[TestCase(-13, 0, "1 week ago")]
		[TestCase(-14, 0, "2 weeks ago")]
		[TestCase(-30, 0, "5 weeks ago")]
		public void PrettyDate_Returns_Expected_Result(int dayDifference, int secondsDifference, string expectedResult)
		{
			var result = DateTime.Now.AddDays(dayDifference).AddSeconds(secondsDifference).PrettyDate();
			Console.WriteLine(result);
			Assert.IsTrue(result != null);
			Assert.IsTrue(result == expectedResult);
		}


		[Test]
		public void PrettyDate_31Days_Returns_Expected_Result()
		{
			var result = DateTime.Now.AddDays(-31).PrettyDate();
			Assert.IsTrue(result != null);
			Assert.IsTrue(result == DateTime.Now.AddDays(-31).ToString(CultureInfo.InvariantCulture));
		}

		[Test]
		public void PrettyDate_Large_Returns_Expected_Result()
		{
			var result = DateTime.Now.AddDays(-32).PrettyDate();
			Assert.IsTrue(result != null);
			Assert.IsTrue(result == DateTime.Now.AddDays(-32).ToString(CultureInfo.InvariantCulture));
		}
	}
}
